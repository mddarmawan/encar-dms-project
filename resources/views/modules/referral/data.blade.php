@extends('layout')

@section('content')

	<div class="content-header">
		<h6>
			<small>Penjualan</small>
			Referral
		</h6>
		<ul class="header-tools right">
			<li><a href="#referral" class="chip"><i class="fa fa-user-secret"></i> Data Referral</a></li>
		</ul>
	</div>


<ul id="referral_status" class="side-nav fixed stats">
	<li class="side-title">Status Referral</li>
</ul>

<div class="wrapper scroll">
	<ul class="collapse" data-collapsible="accordion" >
		<li>
		    <div class="collapsible-header" >
		      	<i class="material-icons">today</i> 
		      		Riwayat 
		      	<i class="fa fa-chevron-down right" style="font-size:12px"></i>
		     </div>
		    <div class="collapsible-body" style="padding:0;">
		      	<div id="dataRiwayat">

				</div>
		     </div>
		</li>
	</ul>
	<div id="dataReferral">

	</div>	
</div>

<div id="referral" class="modal" style="width:960px">	
    <h6 class="modal-title blue-grey darken-1">
    	Data Referral
    	<span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:0;position: relative;">
     	@include("modules.referral.data_referral")	
    </div>
</div>


<div id="validasi" class="modal modal-fixed-footer" style="width:400px;height:260px">
    <form method="POST" action="{{url('api/diskon/konfirmasi')}}">
	    <h6 class="modal-title blue-grey darken-1">
	    	Konfirmasi
	    	<span class="modal-close right material-icons" style="margin-top:-3px">close</span>
	    </h6>
	    <div class="modal-content" >
	    		{{csrf_field()}}
	     		<input type="hidden" name="id" id="id"/>
	     		<table class="info payment" style="border:0;margin:0">
				     <tr>
						<td width="150px">Tanggal Transaksi</td>
						<td width="10px">:</td>
						<td class=""><input type="text"  value="{{date('d/m/Y')}}"  class="datepicker" name="tgl" required="" /></td>
					</tr>
				     <tr>
						<td width="150px">Keterangan</td>
						<td width="10px">:</td>
						<td class=""></td>
					</tr>
				     <tr>
						<td colspan="3"><textarea name="ket" ></textarea></td>
					</tr>
				</table>
	     		
	    </div>
	    <div class="modal-footer">
	      <button type="submit" class="modal-action waves-effect waves-green btn-flat "> Simpan</button>
	    </div>
     </form>
</div>

<script>
$(function() {
	$.ajax({
        type: "GET",
        url: "{{url('api/diskon/status')}}"
    }).done(function(data){
    	for(var i in data){
    		var item = data[i];
    		if (item.referral_jumlah == null){
    			item.referral_jumlah = 0;
    		}
    		$("#referral_status").append('<li><a class="waves-effect waves-light">'+item.referral_nama+' <span class="new badge" data-badge-caption="">'+item.referral_jumlah+'</span></a></li>');
    	}
    });

	var db = {
        loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/diskon/referral')}}",
                    data: filter
                });
            },

    };

    $("#dataReferral").jsGrid({
        height: "92%",
        width: "100%",
        editing: false,
        sorting: true,
        filtering: false,
        autoload: true,
        paging: true,
        pageSize: 30,
        pageButtonCount: 5,
        noDataContent: "Tidak Ada Data",
 
        controller: db,
 
        fields: [
            { name: "referral_nama", title:"Nama Referral", type: "text", width: 100 },
            { name: "referral_telp", title:"No Telepon", type: "text", width: 80 },
            { name: "referral_info", title:"Info. Rekening", type: "text", width: 120 },
            { name: "spk_tgl", title:"Tgl. SPK", type: "text", width: 80, align:"center"},
            { name: "spk_id", title:"No SPK", type: "text", width: 70, align:"center" },
            { name: "spk_pel_nama", title:"Nama Pemesan", type: "text", width: 120 },
            { name: "sales_nama", title:"Sales", type: "text", width: 120},
            { name: "spkd_komisi", title:"Komisi (Rp)", type: "text", width: 100, align:"right"},
            { type: "control",  width:50, align:"center", deleteButton:false, editButton:false, itemTemplate: function(value, item) {
                    var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                    
                    var $customButton = $("<a href='#validasi' class='green-text' title='Transfer'><span class='material-icons'  style='font-size:20px'>done</span></a>")
                        .click(function(e) {
                            $("#id").val(item.spkd_id);

                            $("#validasi").modal("open");
                            e.stopPropagation();
                        });
                    
                    return $result.add($customButton);
                } 
            }
        ]
    });


	var db_log = {
        loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/diskon/riwayat')}}",
                    data: filter
                });
            },
    };

    $("#dataRiwayat").jsGrid({
        height: "380px",
        width: "100%",
 
        filtering: true,
        sorting: true,
        autoload: true, 
    	
 
        controller: db_log,
 
        fields: [
            { name: "referral_nama", title:"Nama Referral", type: "text", width: 150, validate:"required" },
            { name: "referral_telp", title:"Telepon", type: "text", width: 100, validate: "required" },
            { name: "spk_tgl", title:"Tanggal SPK", type: "text", width: 80, align:"center"},
            { name: "spk_id", title:"No SPK", type: "text", width: 70, validate: "required" },
            { name: "spk_pel_nama", title:"Nama Pelanggan", type: "text", width: 100 },
            { name: "sales_nama", title:"Sales", type: "text", width: 100 },
            { name: "spkd_komisi", title:"Komisi (Rp)", type: "text", width: 100, align: "right" },
            { name: "spkr_tgl", title:"Tanggal Transfer", type: "text", width: 80, align:"center"},
            { type: "control", width:70, editButton:false,deleteButton: false }
        ]
    });
 
});
</script>


@endsection