var firebaseDB = firebase.database();
var connectedRef = firebase.database().ref(".info/connected");
connectedRef.on("value", function(snap) {
if (snap.val() === true) {
	$("#onlineStatus").html("<span class='box-span green'>Online</span>");
	//alert("connected");
} else {
	$("#onlineStatus").html("<span class='box-span red'>Offline</span>");
	//alert("not connected");
}
});
// //PARSING SPK & DISKON SALES
// $.ajax({
// 	type: "GET",
// 	url: "/api/firebase/sales/"
// }).done(function(data){
// 	if (data) {
// 		for(var i in data) {
// 			var sales = data[i];
// 			parseSPKSales(sales.sales_salesUid);			
// 			parseDiskonSales(sales.sales_salesUid);
// 		}
// 	}
// }); 

// //PARSING STOCK
// $.ajax({
// 	type: "GET",
// 	url: "/api/persediaan/stock/"
// }).done(function(data){
// 	if (data) {
// 		for (var i in data) {
// 			var stock = data[i];
// 			parseStock(stock);
// 		}
// 	}
// });
// parseKonfirmasi();

// function parseStock(stock){
// 	var items = {
// 		'trk_dh' : stock.trk_dh,
// 		'trk_mesin' : stock.trk_mesin,
// 		'trk_rangka' : stock.trk_rangka,
// 		'trk_tahun' : stock.trk_tahun,
// 		'variant_nama' : stock.variant_nama,
// 		'warna_nama' : stock.warna_nama
// 	}

// 	var db = firebaseDB.ref('data/tb_stock/' + stock.trk_dh);
// 	db.set(items);
// }

// function parseSPKSales(uid){
// 	var db = firebase.database().ref('sales/' + uid + '/spk/');
// 	db.on('value', function(snapshot) {
// 		snapshot.forEach(function(childSnapshot) {
// 			var item = childSnapshot.val();
// 			item['spk_id'] = childSnapshot.key;
// 			item['_token'] = $('meta[name="csrf-token"]').attr('content');

// 			if (item.spk_status == 90 || item.spk_status == 10){			
// 				$.ajax({
// 					type: "POST",
// 					url : "/api/firebase/spk",
// 					data: item
// 				}).fail(function(result){
// 					console.log(result);
// 				}).done(function(result){
// 					if (result){
// 						db.child(childSnapshot.key).remove();
// 						console.log(result);
// 					}
// 				})
// 			}else if (item.spk_status == 11){
// 				$.ajax({
// 					type: "POST",
// 					url : "/api/firebase/spkcancel",
// 					data: item
// 				}).fail(function(result){
// 					console.log(result);
// 				}).done(function(result){
// 					if (result){
// 						db.child(childSnapshot.key).remove();
// 						console.log(result);
// 					}
// 				})
// 			}
// 		});
// 	});
// }

// function parseDiskonSales(uid){
// 	var db = firebaseDB.ref('sales/' + uid + '/diskon/');
// 	db.on('value', function(snapshot) {
// 		snapshot.forEach(function(childSnapshot) {
// 			var item = childSnapshot.val();
// 			item['_token'] = $('meta[name="csrf-token"]').attr('content');
			
// 			if (item.diskon_status == 1){
// 				$.ajax({
// 					type: "POST",
// 					url: "/api/firebase/diskon/",
// 					data: item
// 				}).done(function(result){
// 					if (result){
// 						console.log(result);
// 					}
// 				}); 

// 			db.child(childSnapshot.key).remove();
// 			}
// 		});
// 	});
// }

// function parseKonfirmasi(){
// 	var db = firebaseDB.ref('konfirmasipembayaran');
// 	db.on('value', function(snapshot) {
// 		snapshot.forEach(function(childSnapshot) {
// 			var item = childSnapshot.val();
// 			item['_token'] = $('meta[name="csrf-token"]').attr('content');
// 			item['spk'] = childSnapshot.key; 
// 			$.ajax({
// 				type: "POST",
// 				url: "/api/firebase/pembayaran",
// 				data: item
// 			}).done(function(result){
// 				if (result){
// 					db.child(childSnapshot.key).remove();
// 					console.log(result);
// 				}
// 			});
// 		});
// 	});
// }