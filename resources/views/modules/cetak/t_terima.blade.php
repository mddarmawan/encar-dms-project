<style>
	body{
		margin:0;
		padding:0;
		font-size:14px;
		font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;
	}
	td, th{
		font-size:11px;
	}
	h5{
		margin: 5px 0 20px;
		font-weight:400;
	}
	.text-center{
		text-align:center;
	}
	.text-right{
		text-align:right;
	}
	table{
		width:100%;		
	}
	table, td, th, tr{
		border:0;
		border-spacing: 0;
		border-color:#fff;
		padding:0;
		border-collapse: collapse;
		vertical-align:top;
	}
	table th{
		border-top:1px solid #ddd;
		border-left:1px solid #ddd;
		border-right:1px solid #ddd;
		border-bottom:4px solid #ddd;
		padding:3px 7px;
		vertical-align:middle;
		background:#f5f5f5;
		border-spacing: 0;
		border-collapse: collapse;
	}
	
	table tfoot th{
		border-bottom:1px solid #ddd;
		border-top:4px solid #ddd;
	}
	
	.table td{
		padding:3px 7px;
		border-top:1px solid #ddd;
		border-left:1px solid #ddd;
		border-right:1px solid #ddd;
		border-bottom:1px solid #ddd;		
	}

	.bold{
		font-weight:bold;
	}

</style>

<page>
	<body style="margin: 50px; font-family: arial;">
		<div class="text-center" style="margin-bottom: 20px;"><b><h3>TANDA TERIMA</h3></b></div>
		<table style="margin-bottom: 20px;">
			<tr>
				<td style="padding-bottom: 10px; width: 100px;">Yang menerima :</td>
			</tr>
			<tr>
				<td>Nama</td>
				<td> : </td>
				<td> {{ $data->spk_terima_nama }} </td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td> : </td>
				<td> {{ $data->spk_terima_alamat }}</td>
			</tr>				
		</table>
		<div style="margin-bottom: 10px;">telah menerima BPKB dan Faktur asli dengan spesifikasi sebagai berikut :</div>
		<table style="margin-bottom: 20px;">
			<tr>
				<td style="width: 100px;">BPKB a/n</td>
				<td> : </td>
				<td> {{ $data->spk_stnk_nama }}</td>
			</tr>
			<tr>
				<td>No. BPKB</td>
				<td> : </td>
				<td> {{ $data->spk_no_bpkb }}</td>
			</tr>
			<tr>
				<td>No. Mesin</td>
				<td> : </td>
				<td> {{ $data->spk_no_mesin }}</td>
			</tr>
			<tr>
				<td>No. Rangka</td>
				<td> : </td>
				<td> {{ $data->spk_rangka }}</td>
			</tr>
			<tr>
				<td>No. Polisi</td>
				<td> : </td>
				<td> {{ $data->spk_nopol }}</td>
			</tr>	
			<tr>
				<td>No. DH</td>
				<td> : </td>
				<td> {{ $data->spk_dh }}</td>
			</tr>
			<tr>
				<td>Leasing</td>
				<td> : </td>
				<td> {{ $data->spk_leasing_nick }} / {{ $data->spk_asuransi_jenis }}</td>
			</tr>					
		</table>
		<br><br>
		<table style="text-align: center;">
			<tr>
				<td style="width: 30%;">
					Palembang, 27 Februari 2016 <br>
					Yang menerima
					<br><br><br><br>
					___________________
				</td>
				<td style="width: 30%;">
					Yang Menyerahkan
					<br><br><br><br>
					___________________
				</td>
				<td style="width: 30%;">
					Mengetahui
					<br><br><br><br>
					___________________
				</td>
			</tr>
		</table>
	</body>
</page>