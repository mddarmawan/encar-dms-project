@extends('layout')

@section('content')


<div class="content-header">
        <h6 class="left">
            <small>Pengat</small>
            Warna
        </h6>
    </div>

    <div id="data_grid" class="wrapper">
        <div class="nav-wrapper">
            
            <div class="nav-right">
                <a class="waves-effect waves-light btn-flat btn-filter" id="reset">Reset</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
            </div>
        </div>
        <div id="dataWarna">

        </div>  
    </div>


<script>

    $("#filter").click(function(e){
        e.preventDefault();
        loadData();
    });


    $("#reset").click(function(e){
        e.preventDefault();
        $("#filter_awal").val("");
        $("#filter_akhir").val("");
        status="";
        $(".tab li").removeClass("active");
        $(".tab li a[data-id='']").parent().addClass("active");
        loadData();
    });


    $("#export").click('click', function (event) {
        var args = [$('#dataWarna'), 'DATA_WARNA_KENDARAAN<?php echo date('dmY') ?>.xls'];   
        exportTableToExcel.apply(this, args);
    });


function type() {
    $.ajax({
        type: "GET",
        url: "{{url('api/kendaraan/type')}}"
    }).done(function(type) {
        type.unshift({ type_id: "0", type_nama: "" });

function warna() {

    var db = {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/warna')}}",
                    data: filter
                }).done(function(response) {
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/warna/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].warna_id] = {
                                warna_id: item[i].warna_id,
                                warna_hapus: item[i].warna_hapus,
                                warna_nama: item[i].warna_nama,
                                warna_type : item[i].warna_type,
                                warna_status : item[i].warna_status
                            };
                        }
                        var db = firebase.database().ref("data/tb_warna/");
                        db.set(data);
                    });
                });
            },

            insertItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "POST",
                    url: "{{url('/api/warna')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/warna/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].warna_id] = {
                                warna_id: item[i].warna_id,
                                warna_hapus: item[i].warna_hapus,
                                warna_nama: item[i].warna_nama,
                                warna_type : item[i].warna_type,
                                warna_status : item[i].warna_status
                            };
                        }
                        var db = firebase.database().ref("data/tb_warna/");
                        db.set(data);
                    });
                });    
            },

            updateItem: function(item) {
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/warna')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/warna/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].warna_id] = {
                                warna_id: item[i].warna_id,
                                warna_hapus: item[i].warna_hapus,
                                warna_nama: item[i].warna_nama,
                                warna_type : item[i].warna_type,
                                warna_status : item[i].warna_status
                            };
                        }
                        var db = firebase.database().ref("data/tb_warna/");
                        db.set(data);
                    });
                });
            },

            deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/warna')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(response){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/warna/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].warna_id] = {
                                warna_id: item[i].warna_id,
                                warna_hapus: item[i].warna_hapus,
                                warna_nama: item[i].warna_nama,
                                warna_type : item[i].warna_type,
                                warna_status : item[i].warna_status
                            };
                        }
                        var db = firebase.database().ref("data/tb_warna/");
                        db.set(data);
                    });                 
                });
            }
        };

        $("#dataWarna").jsGrid({
        height: "calc(100% - 40px)",
            width: "100%",
     
            filtering: true,
            editing: true,
            inserting: true,
            sorting: true,
            autoload: true,
     
            deleteConfirm: "Anda yakin akan menghapus data ini?",
     
            controller: db,
     
            fields: [
            
                { name: "warna_type", title:"Type", type: "select", items: type, valueField: "type_id", textField: "type_nama", width: 100, align:"left" },
                { name: "warna_nama", title:"nama", type: "text", width: 120, validate: "required" },
                { type: "control", width:70 }
            ]

        });

    
};
warna();

});
};
type();
</script>

@endsection