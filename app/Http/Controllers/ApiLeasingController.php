<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Leasing;
use App\SPK_Leasing;

class ApiLeasingController extends Controller
{
    function all() {
        return json_encode(DB::table("tb_leasing")->get());
    }

    function index(){
        $data = DB::table('tb_leasing')
                ->where('leasing_status', 1)
                ->where("leasing_hapus", 0)
                ->get();

            $result = array();
            foreach($data as $r){
            $item = array();
            $item['leasing_nama'] = $r->leasing_nama;
            $item['leasing_nick'] = $r->leasing_nick;
            $item['leasing_alamat'] = $r->leasing_alamat;
            $item['leasing_kota'] = $r->leasing_kota;
            $item['leasing_telp'] = $r->leasing_telp;
            if((!request("leasing_nama") || strrpos(strtolower($item['leasing_nama']), strtolower(request("leasing_nama"))) > -1) &&
                (!request("leasing_nick") || strrpos(strtolower($item['leasing_nick']), strtolower(request("leasing_nick"))) > -1) &&
                (!request("leasing_alamat") || strrpos(strtolower($item['leasing_alamat']), strtolower(request("leasing_alamat"))) > -1) &&
                (!request("leasing_kota") || strrpos(strtolower($item['leasing_kota']), strtolower(request("leasing_kota"))) > -1) &&
                (!request("leasing_telp") || strrpos(strtolower($item['leasing_telp']), strtolower(request("leasing_telp"))) > -1))  
            array_push($result, $item);
         }

        return json_encode($result);
    }

    function store(){
    	$this->validate(request(), [
            "leasing_nama"      => "required",
            "leasing_nick"      => "required",
            "leasing_kota"     => "required",
            "leasing_telp"     => "required"
        ]);

        
        $insert = array(
            "leasing_nama"     =>  request("leasing_nama"),
            "leasing_nick"     =>  request("leasing_nick"),
            "leasing_kota"           =>  request("leasing_kota"),
            "leasing_alamat"           =>  request("leasing_alamat"),
            "leasing_telp"          =>  request("leasing_telp")
        );

        $id = DB::table('tb_leasing')-> insertGetId($insert,'leasing_id');
        return json_encode(DB::table('tb_leasing') -> where("leasing_id", $id)->first());
    }


    function update(){
    	$this->validate(request(), [
            "leasing_nama"      => "required",
            "leasing_nick"      => "required",
            "leasing_kota"     => "required",
            "leasing_telp"     => "required"
        ]);

	    DB::table('tb_leasing')->where("leasing_id",request("leasing_id"))->update([
	        "leasing_nama"     		=>  request("leasing_nama"),
            "leasing_nick"     		=>  request("leasing_nick"),
            "leasing_kota"          =>  request("leasing_kota"),
            "leasing_alamat"        =>  request("leasing_alamat"),
            "leasing_telp"          =>  request("leasing_telp")
	    ]);

	    return json_encode(DB::table('tb_leasing')-> where("leasing_id",request("leasing_id"))->first());
    }


    function destroy(){
        $update = DB::table('tb_leasing')
            ->where("leasing_id",request("leasing_id"))
            -> update([
                "leasing_hapus"     => '1',
                "leasing_status"    => 0 
            ]);

        return json_encode($update);
    }
}
