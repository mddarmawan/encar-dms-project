@extends('layout')

@section('content')
<form id="import_form" style="border: 4px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="/api/pembelian/audit" class="form-horizontal" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    <input id="import_file" type="file" name="import_file" style="display:none"/>
</form>

<div class="content-header">
    <h6 class="left">
        <small>Pembelian</small>
        Transaksi Pembelian Kendaraan
    </h6>
    <ul class="header-tools right">
       <li><a href="#matching" class="chip tambah"><i class="fa fa-plus"></i> Import File</a></li>
       <li><a href="#tambah" class="chip tambah"><i class="fa fa-plus"></i> Pembelian Baru</a></li>
    </ul>
</div>

<div id="data_grid" class="wrapper">
    <div class="nav-wrapper">
        <div class="nav-right">
            <i style="margin-right: 5px" class="fa fa-filter" aria-hidden="true"></i>
            <span class="bold" style="font-size: 13px"> Periode :</span>
            <div class="input-group">
                <input type="text" class="input-filter datepicker" id="filter_awal" placeholder="Tanggal Awal" required="" />
                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
            </div>
            <span class="bold" style="font-size: 13px">s.d</span>
            <div class="input-group">
                <input type="text" class="input-filter datepicker"  id="filter_akhir" placeholder="Tanggal Akhir"  required="" />
                <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
            </div>
            <a class="waves-effect waves-light btn-flat btn-filter" id="filter">Cari</a>
            <a class="waves-effect waves-light btn-flat btn-filter" id="reset">Reset</a>
            <a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
        </div>
    </div>
    <div id="data">

    </div>  
</div>

<div id="matching" class="modal" style="width:1180px; overflow-x: hidden;"> 
    <form method="POST" action="{{url('/leashing')}}">
        <h6 class="modal-title blue-grey darken-1">
            <span id="matching_title">Audit Pembelian Kendaraan</span>
            <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
        </h6>

        <div class="modal-content" style="padding:10px;position: relative;">
            @include("modules.pembelian.audit")
        </div>

        <div style="padding:10px;text-align:right;background:#f5f5f5">
            <a id="btn_input" class="waves-effect waves-light btn"><i class="material-icons left">input</i> <span>Input Data</span></a>
        </div>
     </form>
</div>

<div id="tambah" class="modal modal-fixed-footer" style="width:800px; height: auto;">
    <form method="POST" action="{{url('/leashing')}}">
        <h6 class="modal-title blue-grey darken-1">
            <span id="form_title">Pembelian Baru</span>
            <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
        </h6>

        <div class="modal-content" style="padding:10px;position: relative;">
            @include("modules.pembelian.tambah")
        </div>

        <div style="padding:10px;text-align:right;background:#f5f5f5">
            <a class="waves-effect waves-light btn save"><i class="material-icons left">save</i> <span id="btn_save">Konfirmasi Pembelian</span></a>
        </div>
     </form>
</div>

<script>
var Upload = function (file) {
    this.file = file;
};

Upload.prototype.getType = function() {
    return this.file.type;
};
Upload.prototype.getSize = function() {
    return this.file.size;
};
Upload.prototype.getName = function() {
    return this.file.name;
};
Upload.prototype.doUpload = function () {
    var that = this;
    var formData = new FormData();

    // add assoc key values, this will be posts values
    formData.append("file", this.file, this.getName());
    formData.append("upload_file", true);
    formData.append("_token", '{{csrf_token()}}');

    $.ajax({
        type: "POST",
        url: "/api/pembelian/audit",
        xhr: function () {
            var myXhr = $.ajaxSettings.xhr();
            if (myXhr.upload) {
                myXhr.upload.addEventListener('progress', that.progressHandling, false);
            }
            return myXhr;
        },
        success: function (data) {
            localStorage.setItem('audit_items', JSON.stringify(data));
            loadAudit();
        },
        error: function (error) {
            alert("Error: " + error);
        },
        async: true,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        timeout: 60000
    });
};

Upload.prototype.progressHandling = function (event) {
    var percent = 0;
    var position = event.loaded || event.position;
    var total = event.total;
    var progress_bar_id = "#progress-wrp";
    if (event.lengthComputable) {
        percent = Math.ceil(position / total * 100);
    }

    // update progressbars classes so it fits your code
    $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
    $(progress_bar_id + " .status").text(percent + "%");
};

$("input:file").change(function() {
    var file = $(this)[0].files[0];
    var upload = new Upload(file);
    upload.doUpload();
});

// $("#import").click(function() {
//     $("#import_file").trigger("click");
// });

// $("input:file").change(function() {
//     $("#import_form").submit();
//     loadData();
// });

function loadAudit() {
    $("#table_audit > tr").remove();
    var items = localStorage.getItem('audit_items');
    var data = JSON.parse(items);

    for (var i in data) {
        var item = data[i];

        $('#table_audit').append('<tr id="audit_items_' + i + '"><td>' + i + '</td><td>' + item.trk_dh + '</td><td>' + item.trk_tgl.date + '</td><td>' + item.trk_rrn + '</td><td>' + item.trk_vendor + '</td><td>' + item.trk_mesin + '</td><td>' + item.trk_rangka + '</td><td><div class="switch"><label><input type="checkbox" onclick="setAutoMatching(\'' + i + '\')" id="audit_radio_' + i + '" checked><span class="lever"></span></label></div></td><td><a style="cursor: pointer;" onclick="deleteAudit(\'' + i + '\')"><i class="fa fa-close" title="Delete" style="color:red; font-size: 20px;"></i></a></td></tr>');
    }
}

function setAutoMatching(i) {
    var items = localStorage.getItem('audit_items');
    var data = JSON.parse(items);

    if (data[i].trk_automatching == 1) {
        data[i].trk_automatching = 0;
    } else {
        data[i].trk_automatching = 1;
    }

    localStorage.setItem('audit_items', JSON.stringify(data));
}

$("#btn_input").click(function() {
    var items = localStorage.getItem('audit_items');
    var data = JSON.parse(items);
    var item = $.map(data, function(el) { return el; })
    var audit = JSON.stringify(item);
    var send = {
       '_token' : '{{csrf_token()}}',
       'data' : audit
    }

    $.ajax({
        type : "POST",
        url  : "{{url('api/pembelian/import')}}",
        data : send
    }).done(function() {
        $("#matching").modal("close");
        loadData();
    }).fail(function(respone) {
        alert(response);
    });
});

function deleteAudit(i) {
    var items = localStorage.getItem('audit_items');
    var data = JSON.parse(items);
    delete data[i];

    localStorage.setItem('audit_items', JSON.stringify(data));

    $("#audit_items_" + i).remove();
    //loadAudit();
};

$("#filter").click(function(e){
    e.preventDefault();
    loadData();
});

$("#reset").click(function(e){
    e.preventDefault();
    $("#filter_awal").val("");
    $("#filter_akhir").val("");
    status="";
    $(".tab li").removeClass("active");
    $(".tab li a[data-id='']").parent().addClass("active");
    loadData();
});


$("#export").click('click', function (event) {
    var args = [$('#data'), 'STOCK_KENDARAAN_<?php echo date('dmY') ?>.xls'];   
    exportTableToExcel.apply(this, args);
});

function loadData() {

    var db = {
        loadData: function(filter) {
                var filter_awal = $("#filter_awal").val().trim();
                var filter_akhir = $("#filter_akhir").val().trim();
                if (filter_awal != ""){
                    filter['filter_awal'] = filter_awal;
                }
                if (filter_akhir != ""){
                    filter['filter_akhir'] = filter_akhir;
                }
                filter['spk_status'] = status;
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/pembelian')}}",
                    data: filter
                });
            },


        deleteItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "DELETE",
                url: "{{url('/api/pembelian')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            }).done(function(response) {
                if (response.result == 1){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/pembelian/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].trk_dh] = {
                                trk_dh: item[i].trk_dh,
                                trk_hapus: item[i].trk_hapus,
                                trk_rangka: item[i].trk_rangka,
                                trk_mesin : item[i].trk_mesin,
                                trk_tahun: item[i].trk_tahun,
                                variant_nama : item[i].type_nama + item[i].variant_nama,
                                warna_nama : item[i].warna_nama,
                            };
                        }
                        var db = firebase.database().ref("data/tb_stock/");
                        db.set(data);
                    });
                } else {
                    alert("ERR-00 Pembelian gagal dihapus!, silahkan hubungi administrator");
                }
            });
        }
    };
                
    $("#data").jsGrid({
        height: "calc(100% - 40px)",
        width: "100%",
        editing: false,
        sorting: true,
        filtering: true,
        autoload: true,
        paging: true,
        pageSize: 30,
        pageButtonCount: 5,
        noDataContent: "Tidak Ada Data",
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db,
 
        fields: [
            { name: "trk_ref", title:"Referensi", type: "text", width: 80, align:"center" },
            { name: "trk_invoice", title:"Vendor Invoice", type: "text", width: 80, align:"center" },
            { name: "trk_tgl", title:"Tgl. Invoice", type: "text", width: 70, align:"center" },
            { name: "trk_vendor", title:"Vendor", type: "text", width: 120},
            { name: "trk_dh", title:"NO DH", type: "text", width: 70, align:"center" },
            { name: "trk_rrn", title:"RRN", type: "text", width: 120 },
            { name: "trk_variant", title:"Type/Varian", type: "text", width: 120},
            { name: "trk_warna", title:"Warna", type: "text", width: 100},
            { name: "trk_harga", title:"Harga (Rp)", type: "number", width: 100, align:"right"},
            { type: "control",  width:50, align:"center", editButton:false, itemTemplate: function(value, item) {
                    var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
                    
                    var $customButton = $('<input class="jsgrid-button jsgrid-edit-button" type="button" title="Edit" style="float:left">')
                        .click(function(e) {
                            $("#state").val("edit");

                            $("#form_title").html("EDIT: "+ item.trk_ref);
                            $("#btn_save").html("Simpan Perubahan");

                            $("#_id").val(item.trk_id);
                            $("#trk_ref").val(item.trk_ref);
                            $("#trk_invoice").val(item.trk_invoice);
                            $("#trk_tgl").val(item.trk_tgl);
                            $("#trk_tahun").val(item.trk_tahun);
                            $("#trk_dh").val(item.trk_dh);
                            $("#trk_rrn").val(item.trk_rrn);
                            $("#trk_rangka").val(item.trk_rangka);
                            $("#trk_mesin").val(item.trk_mesin);
                            $("#trk_dpp").val(item.trk_dpp);
                            $("#trk_vendor").val(item.trk_vendorid);
                            $("#trk_warna").val(item.trk_warnaid);
                            $("#type").val(item.trk_type);
                            $("#trk_variant").val(item.trk_variantid);

                            $("#tambah").modal("open");
                            e.stopPropagation();
                        });
                    
                    return $result.add($customButton);
                } 
            }
        ]
    });
 
   
};
loadData();
</script>

@endsection