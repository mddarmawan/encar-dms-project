<?php

namespace App;

use Illuminate\Support\Facades\DB;

class config 
{
	var $config;
	
	function __construct(){
		$this->config = array();
		$data = DB::table("tb_config")->get();
		foreach($data as $d){
			$this->config[$d->config_name] = $d->config_value;
		}
	}
	
	public function get(){
		return (object) $this->config;		
	}
	
	public function set($value, $id){
		return DB::table("tb_config")->where("config_id",$id)->update(["config_value"=>$value]);
	}
	
	
}
