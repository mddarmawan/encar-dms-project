@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Pengaturan</small>
            Data Leasing
        </h6>
    </div>

<div id="data_grid" class="wrapper">
        <div class="nav-wrapper">
            
            <div class="nav-right">
                <a class="waves-effect waves-light btn-flat btn-filter" id="reset">Reset</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
            </div>

            

        </div>

        <div id="dataLeasing">

        </div>  

</div>

<script>

     $("#filter").click(function(e){
        e.preventDefault();
        loadData();
    });


    $("#reset").click(function(e){
        e.preventDefault();
        $("#filter_awal").val("");
        $("#filter_akhir").val("");
        status="";
        $(".tab li").removeClass("active");
        $(".tab li a[data-id='']").parent().addClass("active");
        loadData();
    });


    $("#export").click('click', function (event) {
       var args = [$('#dataLeasing'), 'DATA_LEASING_<?php echo date('dmY') ?>.xls'];    
        exportTableToExcel.apply(this, args);
    });
$(function() {

    var db_leasing = {
        loadData: function(filter) {
            return $.ajax({
                        type: "GET",
                        url: "{{url('api/leasing')}}",
                        data: filter
                    });
        },

        insertItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "POST",
                    url: "{{url('/api/leasing')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/leasing/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].leasing_id] = {
                                leasing_id: item[i].leasing_id,
                                leasing_hapus: item[i].leasing_hapus,
                                leasing_nama: item[i].leasing_nama,
                                leasing_nick: item[i].leasing_nick,
                                leasing_status: item[i].leasing_status
                            };
                        }
                        var db = firebase.database().ref("data/tb_leasing/");
                        db.set(data);
                    });
                });    
            },

       updateItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/leasing')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/leasing/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].leasing_id] = {
                                leasing_id: item[i].leasing_id,
                                leasing_hapus: item[i].leasing_hapus,
                                leasing_nama: item[i].leasing_nama,
                                leasing_nick: item[i].leasing_nick,
                                leasing_status: item[i].leasing_status
                            };
                        }
                        var db = firebase.database().ref("data/tb_leasing/");
                        db.set(data);
                    });
                });
            },

       deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/leasing')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(response){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/leasing/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].leasing_id] = {
                                leasing_id: item[i].leasing_id,
                                leasing_hapus: item[i].leasing_hapus,
                                leasing_nama: item[i].leasing_nama,
                                leasing_nick: item[i].leasing_nick,
                                leasing_status: item[i].leasing_status
                            };
                        }
                        var db = firebase.database().ref("data/tb_leasing/");
                        db.set(data);
                    });                
                });
            }
        };

    $("#dataLeasing").jsGrid({
        height: "calc(100% - 40px)",
        width: "100%",
 
        filtering: true,
        editing: true,
        inserting: true,
        sorting: true,
        autoload: true,
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db_leasing,
 
        fields: [
            { name: "leasing_nama", title:"Nama Leashing", type: "text", width: 150, validate: "required" },
            { name: "leasing_nick", title:"Singkatan", type: "text", width: 120, validate: "required" },
            { name: "leasing_alamat", title:"Alamat", type: "text", width: 200 },
            { name: "leasing_kota", title:"Kota", type: "text", width: 100, validate: "required" },
            { name: "leasing_telp", title:"Telepon", type: "text", width: 100, validate: "required" },
            { type: "control", width:70 }
        ]
    });
 
});
</script>

@endsection