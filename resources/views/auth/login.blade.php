<!DOCTYPE html>
<html>
    <head>
        <title>Login Encar</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="{{ asset('assets/css/materialize.min.css') }}"   media="screen,projection"/>
        <style type="text/css">
            *{
                padding: 0;margin: 0;
            }

            html, body{
                background-image: url("{{ asset('assets/images/background.jpg') }}");
                background-size: cover;
                font-family: calibri;
                height: 100%;
                width: 100%;
            }
            .hf-login{
                width: 400px;
                height: 100%;
                background: #fff;
                bottom:0;
                position: absolute;
                left: 0;
            }
            .nav-login{
                width: 100%;
                height: 70px;
                background: #099188;
                padding-top: 20px;
            }
            .foot-login{
                width: 100%;
                height: 40px;
                background: #099188;
                padding-top: 10px;
                bottom: 0;
                position: absolute;
            }
            .mid-login{
                width: 100%;
                height: 280px;
            }
        </style>
    </head>
    <body>
        <center>
        <div style="float: left;">
            <div class="hf-login">
                <div class="nav-login">
                    <h4 style="margin:0;color:#fff;">
                    DAIHATSU
                    </h4>
                </div>
                <div class="mid-login">
                    <img src="{{ asset('assets/images/logo.png') }}" width="50%" style="margin-top: 10%;">
                    <div class="row" style="margin: 0 10px;margin-top: 50px;">
                        <h5 style="margin:0; float: left;margin-left: 15px; color: #4d4d4d;">Login</h5>

                        <form class="col s12" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}

                            <div class="row">
                            <div class="input-field col s12">
                                <i class="material-icons prefix" style="margin-top: 12px;">person</i>
                                <input name="username" value="{{ old('username') }}" id="icon_prefix" type="text" class="validate" required autofocus>
                                <label for="icon_prefix">Username</label>
                            </div>
                            <div class="input-field col s12">
                                <i class="material-icons prefix"    style="margin-top: 12px;">lock</i>
                                <input name="password" id="icon_telephone" type="password" class="validate" required>
                                <label for="icon_telephone">Kata Sandi</label>
                            </div>
                            </div>
                            <button type="submit" class="waves-effect waves-light btn" style="float: right;"><i class="material-icons left">input</i>LOGIN</button>
                        </form>
                    </div>
                </div>
                <div class="foot-login">
                    <h6 style="margin:0;color:#fff;">
                    &copy;<b>ENCAR SYSTEM</b>
                    </h6>
                </div>
            </div>
            </div>
        </center>
        <script type="text/javascript" src="{{ asset('assets/js/jquery-2.1.4.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/materialize.js') }}"></script>
    </body>
</html>