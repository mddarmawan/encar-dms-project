<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use DateTime;

use App\Http\Controllers\Controller;

class ApiTagihanleasingController extends Controller
{
      function index(){
    	$data = DB::table('tb_spk_leasing')
    		->join('tb_spk', 'tb_spk_leasing.spkl_spk', '=', 'tb_spk.spk_id')
    		->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
    		->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
    		->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
    		->join('tb_spk_faktur', 'tb_spk_faktur.spkf_spk', '=', 'tb_spk.spk_id')
    		->leftjoin('tb_tr_kendaraan', 'tb_tr_kendaraan.trk_dh', '=', 'tb_spk.spk_dh')
    		->leftjoin('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk_leasing.spkl_leasing')    		
			->leftjoin('tb_asuransi_jenis', 'tb_asuransi_jenis.ajenis_id', '=', 'tb_spk_leasing.spkl_jenis_asuransi')
    		->leftjoin('tb_asuransi', 'tb_asuransi.asuransi_id', '=', 'tb_spk_leasing.spkl_asuransi')
    		->where("spk_pembayaran","=",1)
    		->get();

		$result = array();
		foreach($data as $r){
			$item = array();
			$item['spk_id'] = $r->spk_id;
			$item['spk_tgl'] = date_format(date_create($r->spk_tgl),"d/m/Y");
			$item['spk_pel_nama'] = $r->spk_pel_nama;
			$item['leasing_nama'] = $r->leasing_nama;
			$item['karyawan_nama'] = $r->karyawan_nama;
			$item['team_nama'] = $r->team_nama;
			$item['spkl_waktu'] = $r->spkl_waktu;
			$item['spkl_jenis_asuransi'] = $r->ajenis_nama;
			$item['spkl_asuransi'] = $r->asuransi_nama;
			$item['spkl_dp'] = number_format($r->spkl_dp,0,',','.');
			$item['spkl_droping'] = number_format($r->spkl_droping,0,',','.');
			$item['spkl_angsuran'] = number_format($r->spkl_angsuran,0,',','.');
			$piutang = $this->get_piutang_leasing($r->spkl_id);
			$jumlah = $r->spkl_droping;
			if ($piutang){
				$jumlah = $r->spkl_droping - $piutang->jumlah;				
			}else{
				$item['spkl_status'] = "0";
				$item['spkl_tglLunas'] = "";
			}
			$item['spkl_piutang'] = number_format($jumlah,0,',','.');
			$item['spkl_faktur'] = $this->get_date_format($r->spkf_tgl);
			$item['spkl_wgi'] = $this->get_diff_date($r->spkf_tgl,$r->trk_keluar);
			$item['spkl_gi'] = $this->get_date_format($r->trk_keluar);
			$item['spkl_wcetak'] = $this->get_diff_date($r->trk_keluar,$r->spkl_cetak);
			$item['spkl_cetak'] = $this->get_date_format($r->spkl_cetak);
			$item['spkl_wtagihan'] = $this->get_diff_date($r->spkl_cetak,$r->spkl_tagihan);
			$item['spkl_tagihan'] = $this->get_date_format($r->spkl_tagihan);
			$item['spkl_wlunas'] = $this->get_diff_date($r->spkl_tagihan,$r->spkl_lunas);
			$item['spkl_lunas'] = $this->get_date_format($r->spkl_lunas);
			$item['spkl_wrefund'] = $this->get_diff_date($r->spkl_lunas,$r->spkl_refund);
			$item['spkl_refund'] = $this->get_date_format($r->spkl_refund);
			$item['spkl_jumlah_refund'] = number_format($r->spkl_jumlah_refund,0,',','.');

			$item['spkl_status'] = "0";
			if ($item['spkl_gi']!=""){
				$item['spkl_status'] = "1";
			}

			if ($item['spkl_asuransi']==""){
				$item['spkl_asuransi']="<a href='javascript:;' class='orange-text' data-id='$r->spk_id' onclick='asuransi(this)'><i class='fa fa-pencil'></i> Update</a>";
			}

			if ($this->get_date_format($r->spkl_cetak)!="" and $item['spkl_tagihan']==""){
				$item['spkl_tagihan']="<a href='javascript:;' class='orange-text' data-id='$r->spk_id' onclick='tagihan(this)'><i class='fa fa-pencil'></i> Update</a>";
				$item['spkl_status'] = "2";
			}


			if ($this->get_date_format($r->spkl_cetak)!="" and $this->get_date_format($r->spkl_tagihan)!="" and $item['spkl_lunas']==""){
				$item['spkl_lunas']="<a href='javascript:;' class='orange-text' data-id='$r->spk_id' onclick='lunas(this)'><i class='fa fa-pencil'></i> Update</a>";
				$item['spkl_status'] = "3";
			}


			if ($this->get_date_format($r->spkl_lunas)!="" and $this->get_date_format($r->spkl_cetak)!="" and $this->get_date_format($r->spkl_tagihan)!="" and $item['spkl_refund']==""){
				$item['spkl_refund']="<a href='javascript:;' class='orange-text' data-id='$r->spk_id' onclick='refund(this)'><i class='fa fa-pencil'></i> Update</a>";
				$item['spkl_status'] = "4";
			}

			if ($this->get_date_format($r->spkl_lunas)!="" and $this->get_date_format($r->spkl_cetak)!="" and $this->get_date_format($r->spkl_tagihan)!="" and $this->get_date_format($r->spkl_refund)!=""){
				$item['spkl_status'] = "5";
			}
			if((!request("spk_id") || strrpos(strtolower($item['spk_id']), strtolower(request("spk_id"))) > -1) &&
				(!request("spk_tgl") || strrpos(strtolower($item['spk_tgl']), strtolower(request("spk_tgl"))) > -1) &&
				(!request("spk_pel_nama") || strrpos(strtolower($item['spk_pel_nama']), strtolower(request("spk_pel_nama"))) > -1) &&
				(!request("leasing_nama") || strrpos(strtolower($item['leasing_nama']), strtolower(request("leasing_nama"))) > -1) &&
				(!request("karyawan_nama") || strrpos(strtolower($item['karyawan_nama']), strtolower(request("karyawan_nama"))) > -1) &&
				(!request("team_nama") || strrpos(strtolower($item['team_nama']), strtolower(request("team_nama"))) > -1) &&
				(!request("spkl_waktu") || strrpos(strtolower($item['spkl_waktu']), strtolower(request("spkl_waktu"))) > -1) &&
				 (!request("spk_warna") || strrpos(strtolower($item['spk_warna']), strtolower(request("spk_warna"))) > -1) &&
				 (!request("spkl_jenis_asuransi") || strtolower($item['spkl_jenis_asuransi'])== strtolower(request("spkl_jenis_asuransi")))&&
				 (!request("spkl_asuransi") || strtolower($item['spkl_asuransi'])== strtolower(request("spkl_asuransi")))&&
				 (!request("spkl_dp") || strtolower($item['spkl_dp'])== strtolower(request("spkl_dp")))&&
				 (!request("spkl_droping") || strtolower($item['spkl_droping'])== strtolower(request("spkl_droping")))&&
				 (!request("spkl_piutang") || strtolower($item['spkl_piutang'])== strtolower(request("spkl_piutang")))&&
				 (!request("spkl_faktur") || strtolower($item['spkl_faktur'])== strtolower(request("spkl_faktur")))&&
				 (!request("spkl_wgi") || strtolower($item['spkl_wgi'])== strtolower(request("spkl_wgi")))&&
				 (!request("spkl_gi") || strtolower($item['spkl_gi'])== strtolower(request("spkl_gi")))&&
				 (!request("spkl_wcetak") || strtolower($item['spkl_wcetak'])== strtolower(request("spkl_wcetak")))&&
				 (!request("spkl_cetak") || strtolower($item['spkl_cetak'])== strtolower(request("spkl_cetak")))&&
				 (!request("spkl_wtagihan") || strtolower($item['spkl_wtagihan'])== strtolower(request("spkl_wtagihan")))&&
				 (!request("spkl_tagihan") || strtolower($item['spkl_tagihan'])== strtolower(request("spkl_tagihan")))&&
				 (!request("spkl_wlunas") || strtolower($item['spkl_wlunas'])== strtolower(request("spkl_wlunas")))&&
				 (!request("spkl_lunas") || strtolower($item['spkl_lunas'])== strtolower(request("spkl_lunas")))&&
				 (!request("spkl_wrefund") || strtolower($item['spkl_wrefund'])== strtolower(request("spkl_wrefund")))&&
				 (!request("spkl_refund") || strtolower($item['spkl_refund'])== strtolower(request("spkl_refund")))&&
				 (!request("spkl_jumlah_refund") || strtolower($item['spkl_jumlah_refund'])== strtolower(request("spkl_jumlah_refund")))&&
				 (!request("spkl_status") || strtolower($item['spkl_status'])== strtolower(request("spkl_status"))))	{

				$tgl = strtotime(str_replace("/","-",$item['spk_tgl']));
				if (request("filter_awal") && request("filter_akhir")){
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
					if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_awal")){
					$filter_awal = strtotime(request("filter_awal"));
					if ($filter_awal<=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_akhir")){
					$filter_akhir = strtotime(request("filter_akhir"));
					if ($filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else{
					array_push($result, $item);
				}
			}
		}

		return json_encode($result);
    }

     private function get_date_format($date){
    	if (!is_null($date)){
    		return date_format(date_create($date),"d/m/Y");
    	}
    	return "";
    }

    private function get_diff_date($date1, $date2){
    	if (is_null($date1) and is_null($date2)){
    		return "";
    	}

    	if (is_null($date2)){
    		$date2 = date("Y-m-d");
    	}

    	$startTimeStamp = strtotime($date2);
		$endTimeStamp = strtotime($date1);

		$timeDiff = abs($endTimeStamp - $startTimeStamp);

		$numberDays = $timeDiff/86400; 
		return intval($numberDays);
    }

    private function get_piutang_leasing($id){
    	$data = DB::table("tb_leasing_bayar")
    		->select(DB::raw('SUM(lbayar_nominal) as jumlah'), DB::raw("MAX(lbayar_tgl) as lunas"))
    		->where("lbayar_spkl",$id)
    		->first();
    	if (!is_null($data->jumlah)){
    		return $data;
    	}
    	return FALSE;
    }

    private function get_leasing($id){
    	$data = DB::table("tb_spk_leasing")
    		->join('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk_leasing.spkl_leasing')
    		->where("spkl_spk",$id)
    		->first();
    	return $data;
    }

    function get_asuransi(){
    	return DB::table('tb_asuransi')->get();
    }

    function asuransi_save(){
        $this->validate(request(), [
            "spkl_spk"      => "required",
            "spkl_asuransi"      => "required"
        ]);

        $proses=DB::table('tb_spk_leasing') -> where("spkl_spk",request("spkl_spk"))->update([
            "spkl_asuransi"          =>  request("spkl_asuransi")
        ]);
        
	    if ($proses){
	    	return 1;
	    }
	    return 0;
    }

    function cetak_save(){
        $this->validate(request(), [
            "spkl_spk"      => "required"
        ]);

        $proses= DB::table('tb_spk_leasing')
        	->where("spkl_spk", request("spkl_spk"))
        	->where("spkl_cetak",NULL)
        	->update([
				"spkl_cetak" =>  date("Y-m-d")
			]);

	    return 1;
    }

    function tagihan_save(){
        $this->validate(request(), [
            "spkl_spk"		=> "required",
            "spkl_tagihan"	=> "required"
        ]);

        $proses= DB::table('tb_spk_leasing')
        	->where("spkl_spk",request("spkl_spk"))
        	->update([
            	"spkl_tagihan"	=>  date_format(date_create(str_replace("/","-",request("spkl_tagihan"))),"Y-m-d")
        	]);

	    return 1;
    }

    function lunas_save(){
        $this->validate(request(), [
            "spkl_spk"		=> "required",
            "spkl_lunas"	=> "required"
        ]);

        $proses= DB::table('tb_spk_leasing')
        	->where("spkl_spk",request("spkl_spk"))
        	->update([
            	"spkl_lunas"	=> date_format(date_create(str_replace("/","-",request("spkl_lunas"))),"Y-m-d")
        	]);

	    return 1;
    }

    function refund_save(){
        $this->validate(request(), [
            "spkl_spk"				=> "required",
            "spkl_refund"			=> "required",
            "spkl_jumlah_refund"	=> "required",
        ]);

        $proses= DB::table('tb_spk_leasing')->where("spkl_spk",request("spkl_spk"))
        	->update([
	            "spkl_refund"			=>  date_format(date_create(str_replace("/","-",request("spkl_refund"))),"Y-m-d"),
	            "spkl_jumlah_refund"	=>  request("spkl_jumlah_refund"),
        	]);

	    return 1;
    }

  }