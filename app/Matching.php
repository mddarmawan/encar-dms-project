<?php

namespace App;

use Illuminate\Support\Facades\DB;
use App\config;
use App\Lib\Firebase\FirebaseInterface;
use App\Lib\Firebase\FirebaseLib;
use App\Lib\Firebase\FirebaseStub;

class Matching
{
	var $config;
	public function __construct(){
		$config = new config;
		
		$this->config = $config->get();
	}
	
    public function run(){
		$this->set_expired();
		$this->set_valid();
		$spk = $this->get_spk();
		$kendaraan = $this->get_kendaraan();
		
		$data['spk'] = $spk;
		$data['kendaraan'] = $kendaraan;
		
		$rawActivity = array();
		$rawUpdate = array();
		foreach($spk as $s){
			if ($s->sales_salesUid!=""){
				$rawActivity[$s->sales_salesUid] = array();
				$rawUpdate[$s->sales_salesUid] = array();
			}
			foreach($kendaraan as $k){
				if ($s->spk_kendaraan == $k->trk_variantid AND $s->spk_warna == $k->trk_warna){
					$data['match']['spk'] = $s->spk_id;
					$data['match']['dh'] = $k->trk_dh;
					$data['match']['tgl'] = date("Y-m-d");
					$data['match']['exp'] = $this->count_expired($s->spk_pel_kota);

					if($this->set_matching($data['match'])){	
						$activity = array();
						$activity['activity_judul'] = $s->spk_id." - MATCHED";
						$activity['activity_ket'] = "SPK ".$s->spk_id." telah matching dengan nomor DH kendaraan ".$k->trk_dh."\n\nHarap segera follow up Pelunasan Kendaraan atau PO Leashing.";
						$activity['activity_kategori'] = "spk";
						$activity['activity_author'] = "MATCHING";
						$activity['activity_tgl'] = date("Y-m-d H:i:s");
						array_push($rawActivity[$s->sales_salesUid],$activity);
						
						
						$update = array();
						$update['spk_dh'] = $data['match']['dh'];
						$update['spk_match'] = $data['match']['tgl'];
						$update['spk_waktu_match'] = $data['match']['exp'];
						$update['spk_status'] = 2;
						$update['spk_kend_rangka'] = $k->trk_rangka;
						$update['spk_kend_mesin'] = $k->trk_mesin;
						$update['spk_kend_warna'] = $k->warna_nama;
						$rawUpdate[$s->sales_salesUid][$data['match']['spk']] = $update;
					}
					//dd($data);
					break;
				}else if ($s->spk_kendaraan == $k->trk_variantid AND ($s->spk_warna == "" OR is_null($s->spk_warna))){			
					$data['match']['spk'] = $s->spk_id;
					$data['match']['dh'] = $k->trk_dh;
					$data['match']['tgl'] = date("Y-m-d");
					$data['match']['exp'] = $this->count_expired($s->spk_pel_kota);
					
					if($this->set_matching($data['match'])){
						$activity = array();
						$activity['activity_judul'] = $s->spk_id." - MATCHED";
						$activity['activity_ket'] = "SPK ".$s->spk_id." telah matching dengan nomor DH kendaraan ".$k->trk_dh."\n\nHarap segera follow up Pelunasan Kendaraan atau PO Leashing.";
						$activity['activity_kategori'] = "spk";
						$activity['activity_author'] = "MATCHING";
						$activity['activity_tgl'] = date("Y-m-d H:i:s");
						array_push($rawActivity[$s->sales_salesUid],$activity);
						
						$update = array();
						$update['spk_dh'] = $data['match']['dh'];
						$update['spk_match'] = $data['match']['tgl'];
						$update['spk_waktu_match'] = $data['match']['exp'];
						$update['spk_kend_rangka'] = $k->trk_rangka;
						$update['spk_kend_mesin'] = $k->trk_mesin;
						$update['spk_kend_warna'] = $k->warna_nama;
						$rawUpdate[$s->sales_salesUid][$data['match']['spk']] = $update;
					}
					//dd($data);
					break;
				}
			}
		}		
		$firebase = new FirebaseLib("https://dealer-ms.firebaseio.com/",'');
		$firebase->set('activity', $rawActivity);
		$firebase->set('spk_update', $rawUpdate);
		
	}
	
	private function get_spk(){
		$nonfleet = DB::table("tb_spk")
			->select("spk_id","spk_tgl","spk_kendaraan","spk_warna","spk_pel_kota","sales_salesUid")
			->join("tb_sales","spk_sales","=","sales_id")
			->join(DB::raw("(SELECT spk_id as bayar_spk,  IF(SUM(spkp_jumlah) IS NULL, 0, SUM(spkp_jumlah)) as bayar FROM tb_spk LEFT JOIN tb_spk_pembayaran ON spk_id = spkp_spk GROUP BY spk_id) vw_bayar"),"bayar_spk","=","spk_id")
			->where("bayar",">=",$this->config->match_limit_personal)
			->whereNull("spk_fleet")
			->whereNull("spk_dh")
			->where("spk_automatching",1)
			->where("spk_status",1);
			
		return DB::table("tb_spk")
			->select("spk_id","spk_tgl","spk_kendaraan","spk_warna","spk_pel_kota","sales_salesUid")
			->join("tb_sales","spk_sales","=","sales_id")
			->join(DB::raw("(SELECT spk_id as bayar_spk,  IF(SUM(spkp_jumlah) IS NULL, 0, SUM(spkp_jumlah)) as bayar FROM tb_spk LEFT JOIN tb_spk_pembayaran ON spk_id = spkp_spk GROUP BY spk_id) vw_bayar"),"bayar_spk","=","spk_id")
			->where("bayar",">=",$this->config->match_limit_fleet)
			->whereNotNull("spk_fleet")
			->where("spk_automatching",1)
			->where("spk_status",1)
			->whereNull("spk_dh")
            ->unionAll($nonfleet)
			->orderBy("spk_tgl","ASC")
			->get();
	}
	
	private function get_kendaraan(){
		return DB::table("vw_stock_kendaraan")
			->select("trk_dh","trk_tahun","trk_variantid","trk_warna","trk_mesin","trk_rangka","warna_nama")
			->where("trk_automatching",1)
			->orderBy("trk_tgl","ASC")
			->get();
	}
	
	private function set_matching($data){
		return DB::table("tb_spk")
			->where('spk_id', $data['spk'])
            ->update(['spk_dh' => $data['dh'], 'spk_match' => $data['tgl'],'spk_waktu_match' => $data['exp'],"spk_status"=>2]);
	}
	
	private function count_expired($kota){
		$kota = strtoupper($kota);
		if ($kota == "KOTA PALEMBANG"){
			$exp = date("Y-m-d", time() + (86400*3));
			if(date('w', time())>=4){
				$exp = date("Y-m-d", strtotime($exp) + 86400);
			}
			return $exp;
		}
		$exp = date("Y-m-d", time() + (86400*6));
		if(date('w', time())>=1){
			$exp = date("Y-m-d", strtotime($exp) + 86400);
		}
		return $exp;
	}
	
	
	private function set_expired(){
		DB::table("tb_spk")
			->join("vw_bayar","bayar_spk","=","spk_id")
			->whereNotNull("spk_waktu_match")
			->whereDate("spk_waktu_match", "<", date("Y-m-d"))
			->whereNotNull("spk_dh")
			->where("spk_status",2)
			->where("spk_pembayaran",0)
			->where("bayar","<","spk_harga")
			->update(["spk_dh"=>NULL, "spk_status"=>9]);
			
		DB::table("tb_spk")
			->select("spk_id","spk_pel_kota","spk_dh","spk_match","spk_waktu_match")
			->leftjoin("tb_spk_leasing","spkl_spk","=","spk_id")
			->whereDate("spk_waktu_match", "<", date("Y-m-d"))
			->whereNotNull("spk_dh")
			->where("spk_status",2)
			->where("spk_pembayaran",1)
			->whereNull("spkl_spk")
			->update(["spk_dh"=>NULL, "spk_status"=>9]);
	}
	
	private function set_valid(){
		//DEFAULT VALID
		DB::table("tb_spk")
			->join("vw_bayar","bayar_spk","=","spk_id")
			->where("spk_status",9)
			->whereNull("spk_dh")
			->whereNull("spk_match")
			->where("bayar",">=",$this->config->match_limit_personal)
			->where("spk_fleet","")
            ->update(["spk_status"=>1]);
		DB::table("tb_spk")
			->join("vw_bayar","bayar_spk","=","spk_id")
			->where("spk_status",9)
			->whereNull("spk_dh")
			->whereNull("spk_match")
			->where("bayar",">=",$this->config->match_limit_fleet)
			->where("spk_fleet","!=","")
            ->update(["spk_status"=>1]);	

		//VALID MATCH CASH
		DB::table("tb_spk")
			->join("vw_bayar","bayar_spk","=","spk_id")
			->where("spk_pembayaran",0)
			->where("spk_status",9)
			->whereNull("spk_dh")
			->whereNotNull("spk_match")
			->where("bayar",">=","spk_harga")
            ->update(["spk_status"=>1]);
		
		//VALID MATCH CREDIT			
		DB::table("tb_spk")
			->join("tb_spk_leasing","spkl_spk","=","spk_id")
			->where("spk_pembayaran",1)
			->where("spk_status",9)
			->whereNull("spk_dh")
			->whereNotNull("spk_match")
            ->update(["spk_status"=>1]);
	}
	
}
