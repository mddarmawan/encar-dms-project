<div id="dataKendaraan">

</div>


<script>
$("#filter").click(function(e){
        e.preventDefault();
        loadData();
    });

    $("#reset_kendaraan").click(function(e){
        e.preventDefault();
        $("#filter_awal").val("");
        $("#filter_akhir").val("");
        status="";
        $(".tab li").removeClass("active");
        $(".tab li a[data-id='']").parent().addClass("active");
        loadData();
    });


    $("#export_variant").click('click', function (event) {
        var args = [$('#dataVariant'), 'DATA_VARIANT_<?php echo date('dmY') ?>.xls'];   
        exportTableToExcel.apply(this, args);
    });

function loadDataStock() {
    var db = {
        loadData: function(filter) {

            return $.ajax({
                        type: "GET",
                        url: "{{url('api/manual_kendaraan/matching')}}",
                        data: filter
                    });
        },
    };

    $("#dataKendaraan").jsGrid({
        height: "calc(100% - 40px)",
        width: "100%",
 
        filtering: true,
        sorting: true,
        autoload: true,
        paging: true,
        pageSize: 30,
        pageButtonCount: 5,
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db,
 
        fields: [
            { name: "trk_dh", title:"No. dh", type: "text", width: 100, validate: "" },
            // { name: "variant_type", title:"type", type: "text", width: 120, validate: "required" },
            { name: "variant_nama", title:"variant", type: "text", width: 120 },
            // { name: "variant_serial", title:"Variant id", type: "text", width: 140, validate: "required" },
            { name: "trk_mesin", title:"no. mesin", type: "number", width: 140, validate: "" },
            { name: "warna_nama", title:"Warna", type: "number", width: 140, validate: "" },
            { name: "trk_rangka", title:"no. rangka", type: "text", width: 140, validate: "required", align:"right" },
        ]
    });
}
</script>