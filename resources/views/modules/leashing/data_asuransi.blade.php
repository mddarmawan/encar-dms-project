<div id="dataAsuransi">

</div>

<script>
function asuransi() {
    $.ajax({
        type: "GET",
        url: "{{url('api/asuransi')}}"
    }).done(function(jenis) {
        jenis.unshift({ jenAsuransi_id: "0", jenAsuransi_nama: "" });
        var db = {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/asuransi')}}",
                    data: filter
                });
            },

            insertItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "POST",
                    url: "{{url('/api/asuransi')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                });    
            },

            updateItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/asuransi')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                });
            },

            deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/asuransi')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                });
            }
        };

        $("#dataAsuransi").jsGrid({
            height: "380px",
            width: "100%",
     
            filtering: true,
            editing: true,
            inserting: true,
            sorting: true,
            autoload: true,
     
            deleteConfirm: "Anda yakin akan menghapus data ini?",
     
            controller: db,
     
            fields: [
                { name: "asuransi_nama", title:"Nama", type: "text", width: 100, validate: "required" , align:"center" },

                { name: "asuransi_telp", title:"No. Telp", type: "text", width: 130, validate: "required" , align:"center"},
                { name: "asuransi_alamat", title:"Alamat", type: "text", width: 170, validate: "" , align:"center"},
                { name: "asuransi_email", title:"email", type: "text", width: 170, validate: "" , align:"center"},
                { name: "asuransi_keterangan", title:"Keterangan", type: "text", width: 120, align:"center" },
                { type: "control", width:70 }
            ]
        });
    });
};
asuransi();
</script>