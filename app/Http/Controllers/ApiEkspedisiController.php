<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Ekspedisi;

class ApiEkspedisiController extends Controller
{
    function index(){
    	$data = DB::table('tb_ekspedisi')
                -> get();
		$result = $data->filter(function ($data) {
		    return 
		    	(!request("ekspedisi_nama") || strrpos(strtolower($data->ekspedisi_nama), strtolower(request("ekspedisi_nama"))) > -1) &&
				 (!request("ekspedisi_kodepos") || strrpos(strtolower($data->ekspedisi_kodepos), strtolower(request("ekspedisi_kodepos"))) > -1) &&
				 (!request("ekspedisi_alamat") || strrpos(strtolower($data->ekspedisi_alamat), strtolower(request("ekspedisi_alamat"))) > -1) &&
				 (!request("ekspedisi_kota") || strrpos(strtolower($data->ekspedisi_kota), strtolower(request("ekspedisi_kota"))) > -1) &&
				 (!request("ekspedisi_telepon") || strrpos(strtolower($data->ekspedisi_telepon), strtolower(request("ekspedisi_telepon"))) > -1) &&
				 (!request("ekspedisi_email") || strrpos(strtolower($data->ekspedisi_email), strtolower(request("ekspedisi_email"))) > -1);
		});

    	return json_encode($result);
    }

    function store(){
    	$this->validate(request(), [
            "ekspedisi_nama"      => "required",
            "ekspedisi_kota"     => "required",
            "ekspedisi_telepon"     => "required"
        ]);

	    $insert = array(
	        "ekspedisi_nama"     		=>  request("ekspedisi_nama"),
            "ekspedisi_kota"          =>  request("ekspedisi_kota"),
            "ekspedisi_alamat"        =>  request("ekspedisi_alamat"),
            "ekspedisi_kodepos"        =>  request("ekspedisi_kodepos"),
            "ekspedisi_email"        =>  request("ekspedisi_email"),
            "ekspedisi_telepon"          =>  request("ekspedisi_telepon")
	    );

        $id= DB::table('tb_ekspedisi')-> insertGetId($insert,'ekspedisi_id');

        return json_encode(DB::table('tb_ekspedisi')->where("ekspedisi_id",$id)->first());
    }

    function update(){
    	$this->validate(request(), [
            "ekspedisi_nama"      => "required",
            "ekspedisi_kota"     => "required",
            "ekspedisi_telepon"     => "required"
        ]);

	    DB::table('tb_ekspedisi')->where("ekspedisi_id",request("ekspedisi_id"))->update([
	        "ekspedisi_nama"     		=>  request("ekspedisi_nama"),
            "ekspedisi_kota"          =>  request("ekspedisi_kota"),
            "ekspedisi_alamat"        =>  request("ekspedisi_alamat"),
            "ekspedisi_kodepos"        =>  request("ekspedisi_kodepos"),
            "ekspedisi_email"        =>  request("ekspedisi_email"),
            "ekspedisi_telepon"          =>  request("ekspedisi_telepon")
	    ]);

	    return json_encode(DB::table('tb_ekspedisi')->where("ekspedisi_id",request("ekspedisi_id"))->first());
    }

    function destroy(){
		return DB::table('tb_ekspedisi')->where('ekspedisi_id', request("ekspedisi_id"))->delete();
    }    
}
