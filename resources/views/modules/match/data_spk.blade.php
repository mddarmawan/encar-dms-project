<div id="dataSpk">

</div>


<script>
function detail(id){
        $("#spk_id").html(id);

        $.ajax({
            type: "GET",
            url: "{{url('api/spk/')}}/"+id
        }).done(function(json) {
            var pemesan = json.pemesan;
            var kendaraan = json.kendaraan;
            var leasing = json.leasing;
            setNoDH();

            $("#pel_nama").html(pemesan.spk_pel_nama);
            $("#cb_matching").modal("open");
        });
    };

$("#filter").click(function(e){
        e.preventDefault();
        loadData();
    });

    $("#reset_variant").click(function(e){
        e.preventDefault();
        $("#filter_awal").val("");
        $("#filter_akhir").val("");
        status="";
        $(".tab li").removeClass("active");
        $(".tab li a[data-id='']").parent().addClass("active");
        loadData();
    });


    $("#export_variant").click('click', function (event) {
        var args = [$('#dataVariant'), 'DATA_VARIANT_<?php echo date('dmY') ?>.xls'];   
        exportTableToExcel.apply(this, args);
    });

function loadDataSPK() {
    var db = {
        loadData: function(filter) {

            return $.ajax({
                        type: "GET",
                        url: "{{url('api/manual_spk/matching')}}",
                        data: filter
                    });
        },
    };

    $("#dataSpk").jsGrid({
        height: "calc(100% - 40px)",
        width: "100%",
        
        filtering: true,
        sorting: true,
        autoload: true,
        paging: true,
        pageSize: 30,
        pageButtonCount: 5,
        deleteConfirm: "Anda yakin akan menghapus data ini?",
        rowClick:function(data){
            var item = data.item;
            detail(item.spk_id);
        },
        controller: db,
 
        fields: [
            { name: "spk_tgl", title:"Tgl. SPk", type: "text", width: 90, validate: "required" },
            { name: "spk_id", title:"No. spk", type: "text", width: 100 },
            { name: "spk_pel_nama", title:"pelanggan", type: "text", width: 140, validate: "required" },
            { name: "spk_variant", title:"Kendaraan", type: "number", width: 120, validate: "" },
            { name: "spk_warna", title:"Warna", type: "text", width: 100, validate: "required", align:"right" },
            { name: "spk_sales", title:"Sales", type: "text", width: 100, validate: "" },
            { name: "spk_team", title:"Team", type: "text", width: 100, validate: "" },
            { name: "spk_kota", title:"Kota", type: "text", width: 100, validate: "" },
        ]
    });
}

loadData();
</script>