<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class ApiNotifActivity extends Controller
{
    public function index() {
    	$data = DB::table('tb_notif_activity')->get();
    	
    	return json_encode($data);
    }

    public function get($kategori, $status, $spk = NULL) {
    	$data = DB::table('tb_notif_activity')
    		->where('na_kategori', $kategori)
    		->where('na_status', $status)
    		->first();

    	$data->created_at = date('Y-m-d H:i:s');
    	unset($data->na_id);
    	unset($data->updated_at);

    	if ($spk != NULL) {
    		$data->na_ref = $spk;
    		$data->na_judul = str_replace('%spk%', $spk, $data->na_judul);
    		$data->na_keterangan = str_replace('%spk%', $spk, $data->na_keterangan);
    	}

    	if ($data->na_jenis == 'notif') {
    		foreach ($data as $key => $value) {
    			if ($key == 'na_jenis') {
    				$key = 'jenis';
    				$item[$key] = $value;

    				continue;
    			}

    			if ($key == 'created_at') {
    				$key = 'na_tgl';
    			}
    			
    			$key = str_replace('na_', 'notif_', $key);
    			$item[$key] = $value;
    		}
    	} else {
    		foreach ($data as $key => $value) {
    			if ($key == 'na_jenis') {
    				$key = 'jenis';
    				$item[$key] = $value;
    				
    				continue;
    			}

    			if ($key == 'created_at') {
    				$key = 'na_tgl';
    			}

    			$key = str_replace('na_', 'activity_', $key);
    			$item[$key] = $value;
    		}
    	}


    	return json_encode($item);
    }
}
