<div id="dataType">

</div>


<script>
$("#filter").click(function(e){
        e.preventDefault();
        loadData();
    });

    $("#reset_type").click(function(e){
        e.preventDefault();
        $("#filter_awal").val("");
        $("#filter_akhir").val("");
        status="";
        $(".tab li").removeClass("active");
        $(".tab li a[data-id='']").parent().addClass("active");
        loadData();
    });


    $("#export_type").click('click', function (event) {
        var args = [$('#dataType'), 'DATA_TYPE_<?php echo date('dmY') ?>.xls'];   
        exportTableToExcel.apply(this, args);
    });
$(function() {
        var db = {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/kendaraan/type')}}",
                    data: filter
                });
            },

            insertItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "POST",
                    url: "{{url('/api/kendaraan/type')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/kendaraan/type/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].type_id] = {
                                type_id: item[i].type_id,
                                type_hapus: item[i].type_hapus,
                                type_nama: item[i].type_nama,
                                type_status : item[i].type_status,
                            };
                        }
                        var db = firebase.database().ref("data/tb_type/");
                        db.set(data);
                    });
                });    
            },

            updateItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/kendaraan/type')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/kendaraan/type/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].type_id] = {
                                type_id: item[i].type_id,
                                type_hapus: item[i].type_hapus,
                                type_nama: item[i].type_nama,
                                type_status : item[i].type_status,
                            };
                        }
                        var db = firebase.database().ref("data/tb_type/");
                        db.set(data);
                    });
                });
            },

            deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/kendaraan/type')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(response){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/kendaraan/type/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].type_id] = {
                                type_id: item[i].type_id,
                                type_hapus: item[i].type_hapus,
                                type_nama: item[i].type_nama,
                                type_status : item[i].type_status,
                            };
                        }
                        var db = firebase.database().ref("data/tb_type/");
                        db.set(data);
                    });              
                });
            }
        };

        $("#dataType").jsGrid({
            height: "calc(100% - 40px)",
            width: "100%",
     
            filtering: true,
            editing: true,
            inserting: true,
            sorting: true,
            autoload: true,
     
            deleteConfirm: "Anda yakin akan menghapus data ini?",
     
            controller: db,
     
            fields: [
                { name: "type_nama", title:"Type", type: "text", width: 120, validate: "required" },
                { name: "type_poin", title:"Point", type: "number", width: 80, validate: "required" },
                { type: "control", width:70 }
            ]
        });
});
</script>