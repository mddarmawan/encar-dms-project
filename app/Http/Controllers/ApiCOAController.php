<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class ApiCOAController extends Controller
{
    function akun_kasbank(){
    	$data = DB::table('tb_akun')->select("akun_id","akun_nama")->whereNull("akun_hapus")->where("akun_status","1")->where("akun_induk","1.1.01")->orwhere("akun_induk","1.1.02")
                -> get();

    	return json_encode($data);
    }

    function akun_bank(){
    	$data = DB::table('tb_akun')->select("akun_id","bank_rek","akun_nama")
    		->join("tb_bank","bank_akun","=","akun_id")
    		->join("tb_bank_kategori","kategori_id","=","bank_kategori")
    		->whereNull("akun_hapus")->where("akun_status","1")->where("akun_induk","1.1.02")
            -> get();

    	return json_encode($data);
    }
}
