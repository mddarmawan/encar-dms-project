@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Penjualan</small>
            Pelanggan
        </h6>
    </div>

<div id="data_grid" class="wrapper">
        <div class="nav-wrapper">
            <div class="nav-right">
                <i style="margin-right: 5px" class="fa fa-filter" aria-hidden="true"></i>
                <span class="bold" style="font-size: 13px"> Periode :</span>
                <div class="input-group">
                    <input type="text" class="input-filter datepicker" id="filter_awal" placeholder="Tanggal Awal" required="" />
                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                </div>
                <span class="bold" style="font-size: 13px">s.d</span>
                <div class="input-group">
                    <input type="text" class="input-filter datepicker"  id="filter_akhir" placeholder="Tanggal Akhir"  required="" />
                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                </div>
                <a class="waves-effect waves-light btn-flat btn-filter" id="filter">Cari</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="reset">Reset</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
            </div>

            

        </div>

        <div id="data">

        </div>  

</div>

@include("modules.pelanggan.detail")  
 

<script>
    function detail(id){
        $("#pel_id").html(id);

        $.ajax({
            type: "GET",
            url: "{{url('api/pelanggan/')}}/"+id
        }).done(function(json) {
            var pemesan = json.pemesan;
            console.log(json);
            $("#pel_nama").html(pemesan.pel_nama);
            $("#pel_lahir").html(pemesan.pel_lahir);
            $("#pel_kota").html(pemesan.pel_kota);
            $("#pel_alamat").html(pemesan.pel_alamat);
            $("#pel_pos").html(pemesan.pel_pos);
            $("#pel_telp").html(pemesan.pel_telp);
            $("#pel_ponsel").html(pemesan.pel_ponsel);
            $("#pel_email").html(pemesan.pel_email);
           
            var db_spk = {

                loadData: function(filter) {
                        return json.spk;
                },

            };

            db_spk.pembayaran = [
            {
                "pembayaran_id": "",
                "pembayaran_nama": "",           
            },

            {
                "pembayaran_id": 1,
                "pembayaran_nama": "CREDIT",           
            },

            {
                "pembayaran_id": 2,
                "pembayaran_nama": "CASH",           
            },
        ];

            $("#dataSPK").jsGrid({
                height: "150px",
                width: "100%",
         
                filtering: false,
                sorting: true,
                autoload: true,
         
                controller: db_spk,
         
                fields: [
                    { name: "spk_tgl", title:"Tanggal", type: "text", width:100, align:"center" },
                    { name: "spk_id", title:"NO SPK", type: "text", width: 80, align:"center" },
                    { name: "spk_stnk_nama", title:"Nama STNK", type: "text", width: 140 , align:"center"},
                    { name: "karyawan_nama", title:"Sales", type: "text", width: 140, align:"center" },
                    { name: "variant_nama", title:"Type", type: "text", width: 180, align:"center" },
                    { name: "spk_pembayaran", title:"Pembayaran", type: "select", items: db_spk.pembayaran, valueField: "pembayaran_id", textField: "pembayaran_nama", width: 100, align:"center" },
                    { name: "leasing_nama", title:"Leasing", type: "text", width: 100, align:"center" },
                    { name: "spk_do", title:"Tgl. Do", type: "text", width: 100, align:"center" }
                ]
            });

            $("#detail").modal("open");
        });
    };

    $("#filter").click(function(e){
        e.preventDefault();
        loadData();
    });

    $("#reset").click(function(e){
        e.preventDefault();
        $("#filter_awal").val("");
        $("#filter_akhir").val("");
        status="";
        $(".tab li").removeClass("active");
        $(".tab li a[data-id='']").parent().addClass("active");
        loadData();
    });

    $("#export").click('click', function (event) {
        var args = [$('#data'), 'PELANGGAN_<?php echo date('dmY') ?>.xls'];   
        exportTableToExcel.apply(this, args);
    });

   function loadData() {
    var db_pelanggan = {
        loadData: function(filter) {
            var filter_awal = $("#filter_awal").val().trim();
            var filter_akhir = $("#filter_akhir").val().trim();
            if (filter_awal != ""){
                filter['filter_awal'] = filter_awal;
            }
            if (filter_akhir != ""){
                filter['filter_akhir'] = filter_akhir;
            }
            filter['spk_status'] = status;
            return $.ajax({
                type: "GET",
                url: "{{url('api/pelanggan')}}",
                data: filter
            });
        }
    };

        $("#data").jsGrid({
            height: "calc(100% - 40px)",
            width: "100%",
     
            sorting: true,
            filtering: true,
            autoload: true,
            paging: true,
            pageSize: 30,
            pageButtonCount: 5,
            noDataContent: "Tidak Ada Data",

            rowClick:function(data){
            var item = data.item;
            detail(item.pel_id);
            },
     
     
            controller: db_pelanggan,
     
            fields: [
                { name: "pel_daftar", title:"TGL. Daftar", type: "text", width: 120, align:"center" },
                { name: "pel_nama", title:"Nama", type: "text", width: 120, align:"" },
                { name: "pel_alamat", title:"Alamat", type: "text", width: 260, align:"" },
                { name: "pel_lahir", title:"Tgl. Lahir", type: "text", width: 100, align:"center"},
                { name: "pel_kota", title:"Kota", type: "text", width: 100},
                { name: "pel_pos", title:"Kode Pos", type: "text", width: 100, align: "center" },
                { name: "pel_telp", title:"No. Telp", type: "number", width: 100, align:""},
                { name: "pel_ponsel", title:"Ponsel", type: "number", width: 100, align:"" },
                { name: "pel_email", title:"Email", type: "text", width: 150, align:""},
                { name: "pel_sales", title:"Sales", type: "text", width: 170, align:"" }
            ]
        });
     
    };
    loadData();

    $(".tab li a").click(function(e){
    e.preventDefault();

    status = $(this).data("id");
    
    loadData();
    $(".tab li").removeClass("active");
    $(this).parent().addClass("active");
});
</script>

@endsection