-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 23, 2017 at 12:36 PM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `encar_system.fix`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(4, '2017_04_10_040338_create_ekspedisi_table', 1),
(5, '2017_04_10_071514_create_bank_kategori_table', 2),
(6, '2017_04_10_071640_create_bank_table', 2),
(7, '2017_04_10_082236_create_users_table', 3),
(8, '2017_04_13_084447_create_tb_leashing', 4),
(9, '2017_04_14_132038_create_cmo_table', 5),
(10, '2017_04_15_050321_create_spk_table', 6),
(11, '2017_04_15_053403_create_warna_table', 6),
(12, '2017_04_15_053525_create_pelanggan_table', 6),
(13, '2017_04_15_055203_create_v_spk_table', 7),
(14, '2017_04_15_055219_create_v_diskon_table', 7),
(15, '2017_04_18_041034_create_tb_variant_table', 7),
(16, '2017_04_18_041535_create_tb_type_table', 7),
(17, '2017_04_15_054946_create_tb_refferal_table', 8),
(18, '2017_04_18_053445_create_tb_jabatan', 9),
(19, '2017_04_18_053509_create_tb_karyawan', 9),
(20, '2017_04_18_053530_create_tb_team', 9),
(21, '2017_04_18_053545_create_tb_sales', 9),
(22, '2017_04_19_161422_create_tb_spk_faktur_table', 10),
(23, '2017_04_19_162741_create_tb_spk_pembayaran_table', 10),
(24, '2017_04_19_163433_create_tb_spk_leashing_table', 10),
(25, '2017_04_15_054101_create_tr_kendaraan_table', 11),
(26, '2017_04_25_060551_create_tb_spk_diskon_table', 11),
(28, '2017_04_25_060607_create_tb_spk_aksesoris_table', 12),
(29, '2017_04_25_064823_create_tb_aksesoris_table', 12),
(30, '2017_04_28_061346_create_tb_vendor_table', 13),
(31, '2017_05_11_102305_create_table_spk_referral', 14),
(35, '2017_05_11_155657_create_table_spk_ttbj', 15),
(36, '2017_05_11_170712_create_table_biro', 16),
(37, '2017_05_22_074206_create_tb_vendorAks_table', 17),
(38, '2017_05_17_080957_create_tb_asuransi_table', 18),
(40, '2017_06_11_133148_create_table_leasing_bayar', 19),
(42, '2017_06_12_051709_create_table_gudang', 20),
(43, '2017_06_14_160043_create_table_asuransi_jenis', 21),
(44, '2017_07_05_042258_create_stockmove_table', 22);

-- --------------------------------------------------------

--
-- Table structure for table `tb_aksesoris`
--

CREATE TABLE `tb_aksesoris` (
  `aksesoris_id` int(10) UNSIGNED NOT NULL,
  `aksesoris_vendor` int(11) NOT NULL,
  `aksesoris_kendaraan` int(11) DEFAULT NULL,
  `aksesoris_kode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aksesoris_nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aksesoris_harga` bigint(20) NOT NULL,
  `aksesoris_status` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_aksesoris`
--

INSERT INTO `tb_aksesoris` (`aksesoris_id`, `aksesoris_vendor`, `aksesoris_kendaraan`, `aksesoris_kode`, `aksesoris_nama`, `aksesoris_harga`, `aksesoris_status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'VL0001', 'Velg Racing', 15000000, 1, NULL, NULL),
(2, 1, 7, 'VL0006', 'Velg Racing Grand MAX', 11000000, 1, NULL, NULL),
(3, 1, 2, 'SJ0003', 'Sarung Jok Xenia', 250000, 1, NULL, NULL),
(4, 1, 5, 'AY0003', 'Sill plate Samping dan Belakang', 33, 1, NULL, NULL),
(5, 2, 9, 'AY0004', 'Over Fender Offroad/Spakbor Karet Daihatsu Ayla', 600, 1, NULL, NULL),
(6, 1, 6, 'AY0008', 'Outer dan Door Handle Polos JSL Daihatsu Ayla', 85, 1, NULL, NULL),
(7, 1, 6, 'asas', 'asdasda', 22, 1, NULL, NULL),
(8, 2, 3, 'VL0001', 'Velg Racing', 15000000, 1, NULL, NULL),
(9, 1, 7, 'VL0006', 'Velg Racing Grand MAX', 11000000, 1, NULL, NULL),
(10, 1, 2, 'SJ0003', 'Sarung Jok Xenia', 250000, 1, NULL, NULL),
(11, 1, 5, 'AY0003', 'Sill plate Samping dan Belakang', 33, 1, NULL, NULL),
(12, 1, 9, 'AY0004', 'Over Fender Offroad/Spakbor Karet Daihatsu Ayla', 600, 1, NULL, NULL),
(13, 1, 6, 'AY0008', 'Outer dan Door Handle Polos JSL Daihatsu Ayla', 85, 1, NULL, NULL),
(14, 1, 6, 'asas', 'asdasda', 22, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_akun`
--

CREATE TABLE `tb_akun` (
  `akun_id` varchar(10) NOT NULL,
  `akun_nama` varchar(100) DEFAULT NULL,
  `akun_induk` varchar(10) DEFAULT NULL,
  `akun_status` int(1) DEFAULT '1',
  `akun_lock` int(1) DEFAULT '0',
  `akun_hapus` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_akun`
--

INSERT INTO `tb_akun` (`akun_id`, `akun_nama`, `akun_induk`, `akun_status`, `akun_lock`, `akun_hapus`) VALUES
('1.1.01.01', 'KAS ENCARTHA', '1.1.01', 1, 0, NULL),
('1.1.01.02', 'KAS KECIL CABANG', '1.1.01', 1, 0, NULL),
('1.1.01.03', 'KAS BATURAJA', '1.1.01', 1, 0, NULL),
('1.1.01.04', 'KAS LUBUK LINGGAU', '1.1.01', 1, 0, NULL),
('1.1.01.05', 'SETORAN TUNAI', '1.1.01', 1, 0, NULL),
('1.1.02.01', 'BANK BCA ENCAR', '1.1.02', 1, 0, NULL),
('1.1.02.02', 'BANK PERMATA ENCAR', '1.1.02', 1, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_akun_induk`
--

CREATE TABLE `tb_akun_induk` (
  `induk_id` varchar(10) NOT NULL,
  `induk_nama` varchar(100) DEFAULT NULL,
  `induk_ref` varchar(10) DEFAULT NULL,
  `induk_status` int(1) DEFAULT '1',
  `induk_level` int(1) DEFAULT '1',
  `induk_hapus` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_akun_induk`
--

INSERT INTO `tb_akun_induk` (`induk_id`, `induk_nama`, `induk_ref`, `induk_status`, `induk_level`, `induk_hapus`) VALUES
('1.0.00', 'Aset', NULL, 1, 1, NULL),
('1.1.00', 'Aset Lancar', '1.0.00', 1, 2, NULL),
('1.1.01', 'Kas', '1.1.00', 1, 3, NULL),
('1.1.02', 'Bank', '1.1.00', 1, 3, NULL),
('1.2.00', 'Aset Tidak Lancar', '1.0.00', 1, 2, NULL),
('2.0.00', 'Kewajiban', NULL, 1, 1, NULL),
('3.0.00', 'Ekuitas', NULL, 1, 1, NULL),
('4.0.00', 'Pendapatan', NULL, 1, 1, NULL),
('5.0.00', 'Harga Pokok Penjualan', NULL, 1, 1, NULL),
('6.0.00', 'Biaya', NULL, 1, 1, NULL),
('7.0.00', 'Pendapatan Lainnya', NULL, 1, 1, NULL),
('8.0.00', 'Biaya Lainnya', NULL, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_asuransi`
--

CREATE TABLE `tb_asuransi` (
  `asuransi_id` int(10) UNSIGNED NOT NULL,
  `asuransi_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `asuransi_telp` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asuransi_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `asuransi_alamat` text COLLATE utf8mb4_unicode_ci,
  `asuransi_keterangan` text COLLATE utf8mb4_unicode_ci,
  `asuransi_status` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_asuransi`
--

INSERT INTO `tb_asuransi` (`asuransi_id`, `asuransi_nama`, `asuransi_telp`, `asuransi_email`, `asuransi_alamat`, `asuransi_keterangan`, `asuransi_status`, `created_at`, `updated_at`) VALUES
(5, 'PT. Asuransi Raksa Pratikara', '(0711) 368811', 'reksa@gmail.com', 'Jl. Radial No. 129/2878 24 Ilir, Bukit Kecil, 24 Ilir, Bukit Kecil, Palembang City, South Sumatra 30134', 'http://www.araksa.com', 1, '2017-08-11 03:10:01', '2017-08-11 03:10:01'),
(6, 'Garda Oto', '(0711) 351900', 'support@gardaoto.com', 'Jl. Veteran No.2, 9 Ilir, Ilir Tim. II, Kota Palembang, Sumatera Selatan 30114', NULL, 1, '2017-08-11 03:11:19', '2017-08-11 03:11:19'),
(7, 'PT. Asuransi Sinar Mas', '(0711) 316966', 'cs@sinarmas.co.id', 'Jl. Jend Sudirman No 2937 I-J, Kelurahan 20 Ilir II Kecamatan Ilir Timur I, 20 Ilir D. III, Ilir Timur I, Palembang City, South Sumatra 30129', NULL, 1, '2017-08-11 03:12:00', '2017-08-11 03:12:00'),
(8, 'Asuransi Mitra Maparya. PT', '(0711) 446084', 'support@mitramaparya.co.id', 'Jl. Demang Lebar Daun No.3, Demang Lebar Daun, Ilir Bar. I, Kota Palembang, Sumatera Selatan 30137', 'ini adalah keterangan', 1, '2017-08-11 03:13:00', '2017-08-11 03:13:00'),
(9, 'Adira Insurance', '1 500 456', 'leasing@adira.co.id', 'Jl. Residen Abdul Rozak, Bukit Sangkal, Kalidoni, Kota Palembang, Sumatera Selatan 30163', NULL, 1, '2017-08-11 03:13:56', '2017-08-11 03:13:56');

-- --------------------------------------------------------

--
-- Table structure for table `tb_asuransi_jenis`
--

CREATE TABLE `tb_asuransi_jenis` (
  `ajenis_id` int(10) UNSIGNED NOT NULL,
  `ajenis_nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_asuransi_jenis`
--

INSERT INTO `tb_asuransi_jenis` (`ajenis_id`, `ajenis_nama`) VALUES
(1, 'ALL RISK'),
(2, 'TLO'),
(3, 'KOMBINASI');

-- --------------------------------------------------------

--
-- Table structure for table `tb_bank`
--

CREATE TABLE `tb_bank` (
  `bank_id` int(10) UNSIGNED NOT NULL,
  `bank_kategori` int(11) NOT NULL,
  `bank_an` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_rek` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bank_akun` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_bank`
--

INSERT INTO `tb_bank` (`bank_id`, `bank_kategori`, `bank_an`, `bank_rek`, `bank_akun`, `created_at`, `updated_at`) VALUES
(1, 1, 'PT. Encartha Indonesia', '021.272727.8', '1.1.02.01', '2017-09-14 14:58:10', NULL),
(2, 2, 'PT. Encartha Indonesia', '1.311.005.835', '1.1.02.02', '2017-09-14 14:58:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_bank_kategori`
--

CREATE TABLE `tb_bank_kategori` (
  `kategori_id` int(10) UNSIGNED NOT NULL,
  `kategori_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_bank_kategori`
--

INSERT INTO `tb_bank_kategori` (`kategori_id`, `kategori_nama`) VALUES
(1, 'BCA'),
(2, 'Permata');

-- --------------------------------------------------------

--
-- Table structure for table `tb_biro`
--

CREATE TABLE `tb_biro` (
  `biro_id` int(10) UNSIGNED NOT NULL,
  `biro_nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biro_alamat` text COLLATE utf8mb4_unicode_ci,
  `biro_kodepos` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `biro_kota` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biro_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `biro_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_biro`
--

INSERT INTO `tb_biro` (`biro_id`, `biro_nama`, `biro_alamat`, `biro_kodepos`, `biro_kota`, `biro_telp`, `biro_email`, `created_at`, `updated_at`) VALUES
(6, 'SAMSAT', 'Jl. Kampus', '39920', 'Palembang', '0711-38828', 'samsat@gmail.com', '2017-08-11 10:43:00', '2017-08-11 10:43:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_cmo`
--

CREATE TABLE `tb_cmo` (
  `cmo_id` int(10) UNSIGNED NOT NULL,
  `cmo_leasing` int(11) NOT NULL,
  `cmo_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cmo_alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `cmo_kota` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cmo_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_config`
--

CREATE TABLE `tb_config` (
  `config_id` int(11) NOT NULL,
  `config_name` varchar(50) DEFAULT NULL,
  `config_value` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_config`
--

INSERT INTO `tb_config` (`config_id`, `config_name`, `config_value`) VALUES
(1, 'logo', NULL),
(2, 'company_name', NULL),
(3, 'company_address', NULL),
(4, 'company_phone', NULL),
(5, 'company_fax', NULL),
(6, 'company_email', NULL),
(7, 'match_limit_personal', '3500000'),
(8, 'match_limit_fleet', '0'),
(9, 'branch_code', '1061'),
(10, 'company_city', NULL),
(11, 'company_province', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_ekspedisi`
--

CREATE TABLE `tb_ekspedisi` (
  `ekspedisi_id` int(10) UNSIGNED NOT NULL,
  `ekspedisi_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ekspedisi_alamat` text COLLATE utf8mb4_unicode_ci,
  `ekspedisi_kota` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ekspedisi_kodepos` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ekspedisi_email` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ekspedisi_telepon` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_ekspedisi`
--

INSERT INTO `tb_ekspedisi` (`ekspedisi_id`, `ekspedisi_nama`, `ekspedisi_alamat`, `ekspedisi_kota`, `ekspedisi_kodepos`, `ekspedisi_email`, `ekspedisi_telepon`, `created_at`, `updated_at`) VALUES
(4, 'PT. ANTAR PULAU', 'Jl. angkasa III', 'Palembang', '38882', 'support@antarpulau.co.id', '711888871', '2017-08-11 03:20:26', '2017-08-11 03:20:26'),
(5, 'PT. Cargonesia Utama Trans', 'Jl. Pos Pengumben Raya No. 13 B', 'Jakarta', '1200', 'cs@cargonesia.co.id', '2153665404', '2017-08-11 03:21:36', '2017-08-11 03:21:36'),
(6, 'PT. MULTI ANDALAN SEJAHTERA', 'JL. CAKUNG CILINCING, NO. 50, KEL. CAKUNG BARAT, KEC. CAKUNG, JAKARTA TIMUR, 13910', 'Jakarta', '13910', 'support@muldiandalan.co.id', '2129844941', '2017-08-11 03:24:09', '2017-08-11 03:24:09');

-- --------------------------------------------------------

--
-- Table structure for table `tb_gudang`
--

CREATE TABLE `tb_gudang` (
  `gudang_id` int(10) UNSIGNED NOT NULL,
  `gudang_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gudang_alamat` text COLLATE utf8mb4_unicode_ci,
  `gudang_kota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gudang_telp` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gudang_email` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_jabatan`
--

CREATE TABLE `tb_jabatan` (
  `jabatan_id` int(10) UNSIGNED NOT NULL,
  `jabatan_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_jabatan`
--

INSERT INTO `tb_jabatan` (`jabatan_id`, `jabatan_nama`, `created_at`, `updated_at`) VALUES
(1, 'Sales', '2017-04-18 06:19:01', '2017-04-18 06:19:45'),
(2, 'Admin Keuangan', '2017-04-18 06:19:26', '2017-04-25 17:00:00'),
(3, 'Supervisor', '2017-04-11 17:00:00', '2017-04-20 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_karyawan`
--

CREATE TABLE `tb_karyawan` (
  `karyawan_id` int(10) UNSIGNED NOT NULL,
  `karyawan_nip` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `karyawan_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `karyawan_jabatan` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_karyawan`
--

INSERT INTO `tb_karyawan` (`karyawan_id`, `karyawan_nip`, `karyawan_nama`, `karyawan_jabatan`, `created_at`, `updated_at`) VALUES
(1, 'P-009', 'Okta Reza', 1, '2017-04-02 17:00:00', '2017-04-20 17:00:00'),
(2, 'P-012', 'Deny Ramdhan', 1, '2017-04-09 17:00:00', '2017-04-27 17:00:00'),
(3, 'P-005', 'Windi Dwi Lestari', 1, '2017-07-20 05:50:39', '2017-07-20 05:50:39'),
(4, 'P-006', 'Nilam Permata Sari', 1, '2017-07-20 05:50:39', '2017-07-20 05:50:39'),
(5, 'P-007', 'Uba Kiri Kanan', 1, '2017-07-20 05:51:59', '2017-07-20 05:51:59'),
(6, 'P-008', 'Mang Boy', 1, '2017-07-20 05:51:59', '2017-07-20 05:51:59');

-- --------------------------------------------------------

--
-- Table structure for table `tb_konfirmasi_pembayaran`
--

CREATE TABLE `tb_konfirmasi_pembayaran` (
  `konfirmasi_id` int(11) NOT NULL,
  `konfirmasi_spk` varchar(10) DEFAULT NULL,
  `konfirmasi_tgl` timestamp NULL DEFAULT NULL,
  `konfirmasi_jumlah` bigint(20) DEFAULT NULL,
  `konfirmasi_tujuan` varchar(20) DEFAULT NULL,
  `konfirmasi_bank` varchar(30) DEFAULT NULL,
  `konfirmasi_rek` varchar(20) DEFAULT NULL,
  `konfirmasi_an` varchar(30) DEFAULT NULL,
  `konfirmasi_bukti` text,
  `konfirmasi_ket` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_kota`
--

CREATE TABLE `tb_kota` (
  `kota_id` char(4) NOT NULL,
  `kota_provinsi` char(2) DEFAULT NULL,
  `kota_nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kota`
--

INSERT INTO `tb_kota` (`kota_id`, `kota_provinsi`, `kota_nama`) VALUES
('1101', '11', 'KABUPATEN SIMEULUE'),
('1102', '11', 'KABUPATEN ACEH SINGKIL'),
('1103', '11', 'KABUPATEN ACEH SELATAN'),
('1104', '11', 'KABUPATEN ACEH TENGGARA'),
('1105', '11', 'KABUPATEN ACEH TIMUR'),
('1106', '11', 'KABUPATEN ACEH TENGAH'),
('1107', '11', 'KABUPATEN ACEH BARAT'),
('1108', '11', 'KABUPATEN ACEH BESAR'),
('1109', '11', 'KABUPATEN PIDIE'),
('1110', '11', 'KABUPATEN BIREUEN'),
('1111', '11', 'KABUPATEN ACEH UTARA'),
('1112', '11', 'KABUPATEN ACEH BARAT DAYA'),
('1113', '11', 'KABUPATEN GAYO LUES'),
('1114', '11', 'KABUPATEN ACEH TAMIANG'),
('1115', '11', 'KABUPATEN NAGAN RAYA'),
('1116', '11', 'KABUPATEN ACEH JAYA'),
('1117', '11', 'KABUPATEN BENER MERIAH'),
('1118', '11', 'KABUPATEN PIDIE JAYA'),
('1171', '11', 'KOTA BANDA ACEH'),
('1172', '11', 'KOTA SABANG'),
('1173', '11', 'KOTA LANGSA'),
('1174', '11', 'KOTA LHOKSEUMAWE'),
('1175', '11', 'KOTA SUBULUSSALAM'),
('1201', '12', 'KABUPATEN NIAS'),
('1202', '12', 'KABUPATEN MANDAILING NATAL'),
('1203', '12', 'KABUPATEN TAPANULI SELATAN'),
('1204', '12', 'KABUPATEN TAPANULI TENGAH'),
('1205', '12', 'KABUPATEN TAPANULI UTARA'),
('1206', '12', 'KABUPATEN TOBA SAMOSIR'),
('1207', '12', 'KABUPATEN LABUHAN BATU'),
('1208', '12', 'KABUPATEN ASAHAN'),
('1209', '12', 'KABUPATEN SIMALUNGUN'),
('1210', '12', 'KABUPATEN DAIRI'),
('1211', '12', 'KABUPATEN KARO'),
('1212', '12', 'KABUPATEN DELI SERDANG'),
('1213', '12', 'KABUPATEN LANGKAT'),
('1214', '12', 'KABUPATEN NIAS SELATAN'),
('1215', '12', 'KABUPATEN HUMBANG HASUNDUTAN'),
('1216', '12', 'KABUPATEN PAKPAK BHARAT'),
('1217', '12', 'KABUPATEN SAMOSIR'),
('1218', '12', 'KABUPATEN SERDANG BEDAGAI'),
('1219', '12', 'KABUPATEN BATU BARA'),
('1220', '12', 'KABUPATEN PADANG LAWAS UTARA'),
('1221', '12', 'KABUPATEN PADANG LAWAS'),
('1222', '12', 'KABUPATEN LABUHAN BATU SELATAN'),
('1223', '12', 'KABUPATEN LABUHAN BATU UTARA'),
('1224', '12', 'KABUPATEN NIAS UTARA'),
('1225', '12', 'KABUPATEN NIAS BARAT'),
('1271', '12', 'KOTA SIBOLGA'),
('1272', '12', 'KOTA TANJUNG BALAI'),
('1273', '12', 'KOTA PEMATANG SIANTAR'),
('1274', '12', 'KOTA TEBING TINGGI'),
('1275', '12', 'KOTA MEDAN'),
('1276', '12', 'KOTA BINJAI'),
('1277', '12', 'KOTA PADANGSIDIMPUAN'),
('1278', '12', 'KOTA GUNUNGSITOLI'),
('1301', '13', 'KABUPATEN KEPULAUAN MENTAWAI'),
('1302', '13', 'KABUPATEN PESISIR SELATAN'),
('1303', '13', 'KABUPATEN SOLOK'),
('1304', '13', 'KABUPATEN SIJUNJUNG'),
('1305', '13', 'KABUPATEN TANAH DATAR'),
('1306', '13', 'KABUPATEN PADANG PARIAMAN'),
('1307', '13', 'KABUPATEN AGAM'),
('1308', '13', 'KABUPATEN LIMA PULUH KOTA'),
('1309', '13', 'KABUPATEN PASAMAN'),
('1310', '13', 'KABUPATEN SOLOK SELATAN'),
('1311', '13', 'KABUPATEN DHARMASRAYA'),
('1312', '13', 'KABUPATEN PASAMAN BARAT'),
('1371', '13', 'KOTA PADANG'),
('1372', '13', 'KOTA SOLOK'),
('1373', '13', 'KOTA SAWAH LUNTO'),
('1374', '13', 'KOTA PADANG PANJANG'),
('1375', '13', 'KOTA BUKITTINGGI'),
('1376', '13', 'KOTA PAYAKUMBUH'),
('1377', '13', 'KOTA PARIAMAN'),
('1401', '14', 'KABUPATEN KUANTAN SINGINGI'),
('1402', '14', 'KABUPATEN INDRAGIRI HULU'),
('1403', '14', 'KABUPATEN INDRAGIRI HILIR'),
('1404', '14', 'KABUPATEN PELALAWAN'),
('1405', '14', 'KABUPATEN S I A K'),
('1406', '14', 'KABUPATEN KAMPAR'),
('1407', '14', 'KABUPATEN ROKAN HULU'),
('1408', '14', 'KABUPATEN BENGKALIS'),
('1409', '14', 'KABUPATEN ROKAN HILIR'),
('1410', '14', 'KABUPATEN KEPULAUAN MERANTI'),
('1471', '14', 'KOTA PEKANBARU'),
('1473', '14', 'KOTA D U M A I'),
('1501', '15', 'KABUPATEN KERINCI'),
('1502', '15', 'KABUPATEN MERANGIN'),
('1503', '15', 'KABUPATEN SAROLANGUN'),
('1504', '15', 'KABUPATEN BATANG HARI'),
('1505', '15', 'KABUPATEN MUARO JAMBI'),
('1506', '15', 'KABUPATEN TANJUNG JABUNG TIMUR'),
('1507', '15', 'KABUPATEN TANJUNG JABUNG BARAT'),
('1508', '15', 'KABUPATEN TEBO'),
('1509', '15', 'KABUPATEN BUNGO'),
('1571', '15', 'KOTA JAMBI'),
('1572', '15', 'KOTA SUNGAI PENUH'),
('1601', '16', 'KABUPATEN OGAN KOMERING ULU'),
('1602', '16', 'KABUPATEN OGAN KOMERING ILIR'),
('1603', '16', 'KABUPATEN MUARA ENIM'),
('1604', '16', 'KABUPATEN LAHAT'),
('1605', '16', 'KABUPATEN MUSI RAWAS'),
('1606', '16', 'KABUPATEN MUSI BANYUASIN'),
('1607', '16', 'KABUPATEN BANYU ASIN'),
('1608', '16', 'KABUPATEN OGAN KOMERING ULU SELATAN'),
('1609', '16', 'KABUPATEN OGAN KOMERING ULU TIMUR'),
('1610', '16', 'KABUPATEN OGAN ILIR'),
('1611', '16', 'KABUPATEN EMPAT LAWANG'),
('1612', '16', 'KABUPATEN PENUKAL ABAB LEMATANG ILIR'),
('1613', '16', 'KABUPATEN MUSI RAWAS UTARA'),
('1671', '16', 'KOTA PALEMBANG'),
('1672', '16', 'KOTA PRABUMULIH'),
('1673', '16', 'KOTA PAGAR ALAM'),
('1674', '16', 'KOTA LUBUKLINGGAU'),
('1701', '17', 'KABUPATEN BENGKULU SELATAN'),
('1702', '17', 'KABUPATEN REJANG LEBONG'),
('1703', '17', 'KABUPATEN BENGKULU UTARA'),
('1704', '17', 'KABUPATEN KAUR'),
('1705', '17', 'KABUPATEN SELUMA'),
('1706', '17', 'KABUPATEN MUKOMUKO'),
('1707', '17', 'KABUPATEN LEBONG'),
('1708', '17', 'KABUPATEN KEPAHIANG'),
('1709', '17', 'KABUPATEN BENGKULU TENGAH'),
('1771', '17', 'KOTA BENGKULU'),
('1801', '18', 'KABUPATEN LAMPUNG BARAT'),
('1802', '18', 'KABUPATEN TANGGAMUS'),
('1803', '18', 'KABUPATEN LAMPUNG SELATAN'),
('1804', '18', 'KABUPATEN LAMPUNG TIMUR'),
('1805', '18', 'KABUPATEN LAMPUNG TENGAH'),
('1806', '18', 'KABUPATEN LAMPUNG UTARA'),
('1807', '18', 'KABUPATEN WAY KANAN'),
('1808', '18', 'KABUPATEN TULANGBAWANG'),
('1809', '18', 'KABUPATEN PESAWARAN'),
('1810', '18', 'KABUPATEN PRINGSEWU'),
('1811', '18', 'KABUPATEN MESUJI'),
('1812', '18', 'KABUPATEN TULANG BAWANG BARAT'),
('1813', '18', 'KABUPATEN PESISIR BARAT'),
('1871', '18', 'KOTA BANDAR LAMPUNG'),
('1872', '18', 'KOTA METRO'),
('1901', '19', 'KABUPATEN BANGKA'),
('1902', '19', 'KABUPATEN BELITUNG'),
('1903', '19', 'KABUPATEN BANGKA BARAT'),
('1904', '19', 'KABUPATEN BANGKA TENGAH'),
('1905', '19', 'KABUPATEN BANGKA SELATAN'),
('1906', '19', 'KABUPATEN BELITUNG TIMUR'),
('1971', '19', 'KOTA PANGKAL PINANG'),
('2101', '21', 'KABUPATEN KARIMUN'),
('2102', '21', 'KABUPATEN BINTAN'),
('2103', '21', 'KABUPATEN NATUNA'),
('2104', '21', 'KABUPATEN LINGGA'),
('2105', '21', 'KABUPATEN KEPULAUAN ANAMBAS'),
('2171', '21', 'KOTA B A T A M'),
('2172', '21', 'KOTA TANJUNG PINANG'),
('3101', '31', 'KABUPATEN KEPULAUAN SERIBU'),
('3171', '31', 'KOTA JAKARTA SELATAN'),
('3172', '31', 'KOTA JAKARTA TIMUR'),
('3173', '31', 'KOTA JAKARTA PUSAT'),
('3174', '31', 'KOTA JAKARTA BARAT'),
('3175', '31', 'KOTA JAKARTA UTARA'),
('3201', '32', 'KABUPATEN BOGOR'),
('3202', '32', 'KABUPATEN SUKABUMI'),
('3203', '32', 'KABUPATEN CIANJUR'),
('3204', '32', 'KABUPATEN BANDUNG'),
('3205', '32', 'KABUPATEN GARUT'),
('3206', '32', 'KABUPATEN TASIKMALAYA'),
('3207', '32', 'KABUPATEN CIAMIS'),
('3208', '32', 'KABUPATEN KUNINGAN'),
('3209', '32', 'KABUPATEN CIREBON'),
('3210', '32', 'KABUPATEN MAJALENGKA'),
('3211', '32', 'KABUPATEN SUMEDANG'),
('3212', '32', 'KABUPATEN INDRAMAYU'),
('3213', '32', 'KABUPATEN SUBANG'),
('3214', '32', 'KABUPATEN PURWAKARTA'),
('3215', '32', 'KABUPATEN KARAWANG'),
('3216', '32', 'KABUPATEN BEKASI'),
('3217', '32', 'KABUPATEN BANDUNG BARAT'),
('3218', '32', 'KABUPATEN PANGANDARAN'),
('3271', '32', 'KOTA BOGOR'),
('3272', '32', 'KOTA SUKABUMI'),
('3273', '32', 'KOTA BANDUNG'),
('3274', '32', 'KOTA CIREBON'),
('3275', '32', 'KOTA BEKASI'),
('3276', '32', 'KOTA DEPOK'),
('3277', '32', 'KOTA CIMAHI'),
('3278', '32', 'KOTA TASIKMALAYA'),
('3279', '32', 'KOTA BANJAR'),
('3301', '33', 'KABUPATEN CILACAP'),
('3302', '33', 'KABUPATEN BANYUMAS'),
('3303', '33', 'KABUPATEN PURBALINGGA'),
('3304', '33', 'KABUPATEN BANJARNEGARA'),
('3305', '33', 'KABUPATEN KEBUMEN'),
('3306', '33', 'KABUPATEN PURWOREJO'),
('3307', '33', 'KABUPATEN WONOSOBO'),
('3308', '33', 'KABUPATEN MAGELANG'),
('3309', '33', 'KABUPATEN BOYOLALI'),
('3310', '33', 'KABUPATEN KLATEN'),
('3311', '33', 'KABUPATEN SUKOHARJO'),
('3312', '33', 'KABUPATEN WONOGIRI'),
('3313', '33', 'KABUPATEN KARANGANYAR'),
('3314', '33', 'KABUPATEN SRAGEN'),
('3315', '33', 'KABUPATEN GROBOGAN'),
('3316', '33', 'KABUPATEN BLORA'),
('3317', '33', 'KABUPATEN REMBANG'),
('3318', '33', 'KABUPATEN PATI'),
('3319', '33', 'KABUPATEN KUDUS'),
('3320', '33', 'KABUPATEN JEPARA'),
('3321', '33', 'KABUPATEN DEMAK'),
('3322', '33', 'KABUPATEN SEMARANG'),
('3323', '33', 'KABUPATEN TEMANGGUNG'),
('3324', '33', 'KABUPATEN KENDAL'),
('3325', '33', 'KABUPATEN BATANG'),
('3326', '33', 'KABUPATEN PEKALONGAN'),
('3327', '33', 'KABUPATEN PEMALANG'),
('3328', '33', 'KABUPATEN TEGAL'),
('3329', '33', 'KABUPATEN BREBES'),
('3371', '33', 'KOTA MAGELANG'),
('3372', '33', 'KOTA SURAKARTA'),
('3373', '33', 'KOTA SALATIGA'),
('3374', '33', 'KOTA SEMARANG'),
('3375', '33', 'KOTA PEKALONGAN'),
('3376', '33', 'KOTA TEGAL'),
('3401', '34', 'KABUPATEN KULON PROGO'),
('3402', '34', 'KABUPATEN BANTUL'),
('3403', '34', 'KABUPATEN GUNUNG KIDUL'),
('3404', '34', 'KABUPATEN SLEMAN'),
('3471', '34', 'KOTA YOGYAKARTA'),
('3501', '35', 'KABUPATEN PACITAN'),
('3502', '35', 'KABUPATEN PONOROGO'),
('3503', '35', 'KABUPATEN TRENGGALEK'),
('3504', '35', 'KABUPATEN TULUNGAGUNG'),
('3505', '35', 'KABUPATEN BLITAR'),
('3506', '35', 'KABUPATEN KEDIRI'),
('3507', '35', 'KABUPATEN MALANG'),
('3508', '35', 'KABUPATEN LUMAJANG'),
('3509', '35', 'KABUPATEN JEMBER'),
('3510', '35', 'KABUPATEN BANYUWANGI'),
('3511', '35', 'KABUPATEN BONDOWOSO'),
('3512', '35', 'KABUPATEN SITUBONDO'),
('3513', '35', 'KABUPATEN PROBOLINGGO'),
('3514', '35', 'KABUPATEN PASURUAN'),
('3515', '35', 'KABUPATEN SIDOARJO'),
('3516', '35', 'KABUPATEN MOJOKERTO'),
('3517', '35', 'KABUPATEN JOMBANG'),
('3518', '35', 'KABUPATEN NGANJUK'),
('3519', '35', 'KABUPATEN MADIUN'),
('3520', '35', 'KABUPATEN MAGETAN'),
('3521', '35', 'KABUPATEN NGAWI'),
('3522', '35', 'KABUPATEN BOJONEGORO'),
('3523', '35', 'KABUPATEN TUBAN'),
('3524', '35', 'KABUPATEN LAMONGAN'),
('3525', '35', 'KABUPATEN GRESIK'),
('3526', '35', 'KABUPATEN BANGKALAN'),
('3527', '35', 'KABUPATEN SAMPANG'),
('3528', '35', 'KABUPATEN PAMEKASAN'),
('3529', '35', 'KABUPATEN SUMENEP'),
('3571', '35', 'KOTA KEDIRI'),
('3572', '35', 'KOTA BLITAR'),
('3573', '35', 'KOTA MALANG'),
('3574', '35', 'KOTA PROBOLINGGO'),
('3575', '35', 'KOTA PASURUAN'),
('3576', '35', 'KOTA MOJOKERTO'),
('3577', '35', 'KOTA MADIUN'),
('3578', '35', 'KOTA SURABAYA'),
('3579', '35', 'KOTA BATU'),
('3601', '36', 'KABUPATEN PANDEGLANG'),
('3602', '36', 'KABUPATEN LEBAK'),
('3603', '36', 'KABUPATEN TANGERANG'),
('3604', '36', 'KABUPATEN SERANG'),
('3671', '36', 'KOTA TANGERANG'),
('3672', '36', 'KOTA CILEGON'),
('3673', '36', 'KOTA SERANG'),
('3674', '36', 'KOTA TANGERANG SELATAN'),
('5101', '51', 'KABUPATEN JEMBRANA'),
('5102', '51', 'KABUPATEN TABANAN'),
('5103', '51', 'KABUPATEN BADUNG'),
('5104', '51', 'KABUPATEN GIANYAR'),
('5105', '51', 'KABUPATEN KLUNGKUNG'),
('5106', '51', 'KABUPATEN BANGLI'),
('5107', '51', 'KABUPATEN KARANG ASEM'),
('5108', '51', 'KABUPATEN BULELENG'),
('5171', '51', 'KOTA DENPASAR'),
('5201', '52', 'KABUPATEN LOMBOK BARAT'),
('5202', '52', 'KABUPATEN LOMBOK TENGAH'),
('5203', '52', 'KABUPATEN LOMBOK TIMUR'),
('5204', '52', 'KABUPATEN SUMBAWA'),
('5205', '52', 'KABUPATEN DOMPU'),
('5206', '52', 'KABUPATEN BIMA'),
('5207', '52', 'KABUPATEN SUMBAWA BARAT'),
('5208', '52', 'KABUPATEN LOMBOK UTARA'),
('5271', '52', 'KOTA MATARAM'),
('5272', '52', 'KOTA BIMA'),
('5301', '53', 'KABUPATEN SUMBA BARAT'),
('5302', '53', 'KABUPATEN SUMBA TIMUR'),
('5303', '53', 'KABUPATEN KUPANG'),
('5304', '53', 'KABUPATEN TIMOR TENGAH SELATAN'),
('5305', '53', 'KABUPATEN TIMOR TENGAH UTARA'),
('5306', '53', 'KABUPATEN BELU'),
('5307', '53', 'KABUPATEN ALOR'),
('5308', '53', 'KABUPATEN LEMBATA'),
('5309', '53', 'KABUPATEN FLORES TIMUR'),
('5310', '53', 'KABUPATEN SIKKA'),
('5311', '53', 'KABUPATEN ENDE'),
('5312', '53', 'KABUPATEN NGADA'),
('5313', '53', 'KABUPATEN MANGGARAI'),
('5314', '53', 'KABUPATEN ROTE NDAO'),
('5315', '53', 'KABUPATEN MANGGARAI BARAT'),
('5316', '53', 'KABUPATEN SUMBA TENGAH'),
('5317', '53', 'KABUPATEN SUMBA BARAT DAYA'),
('5318', '53', 'KABUPATEN NAGEKEO'),
('5319', '53', 'KABUPATEN MANGGARAI TIMUR'),
('5320', '53', 'KABUPATEN SABU RAIJUA'),
('5321', '53', 'KABUPATEN MALAKA'),
('5371', '53', 'KOTA KUPANG'),
('6101', '61', 'KABUPATEN SAMBAS'),
('6102', '61', 'KABUPATEN BENGKAYANG'),
('6103', '61', 'KABUPATEN LANDAK'),
('6104', '61', 'KABUPATEN MEMPAWAH'),
('6105', '61', 'KABUPATEN SANGGAU'),
('6106', '61', 'KABUPATEN KETAPANG'),
('6107', '61', 'KABUPATEN SINTANG'),
('6108', '61', 'KABUPATEN KAPUAS HULU'),
('6109', '61', 'KABUPATEN SEKADAU'),
('6110', '61', 'KABUPATEN MELAWI'),
('6111', '61', 'KABUPATEN KAYONG UTARA'),
('6112', '61', 'KABUPATEN KUBU RAYA'),
('6171', '61', 'KOTA PONTIANAK'),
('6172', '61', 'KOTA SINGKAWANG'),
('6201', '62', 'KABUPATEN KOTAWARINGIN BARAT'),
('6202', '62', 'KABUPATEN KOTAWARINGIN TIMUR'),
('6203', '62', 'KABUPATEN KAPUAS'),
('6204', '62', 'KABUPATEN BARITO SELATAN'),
('6205', '62', 'KABUPATEN BARITO UTARA'),
('6206', '62', 'KABUPATEN SUKAMARA'),
('6207', '62', 'KABUPATEN LAMANDAU'),
('6208', '62', 'KABUPATEN SERUYAN'),
('6209', '62', 'KABUPATEN KATINGAN'),
('6210', '62', 'KABUPATEN PULANG PISAU'),
('6211', '62', 'KABUPATEN GUNUNG MAS'),
('6212', '62', 'KABUPATEN BARITO TIMUR'),
('6213', '62', 'KABUPATEN MURUNG RAYA'),
('6271', '62', 'KOTA PALANGKA RAYA'),
('6301', '63', 'KABUPATEN TANAH LAUT'),
('6302', '63', 'KABUPATEN KOTA BARU'),
('6303', '63', 'KABUPATEN BANJAR'),
('6304', '63', 'KABUPATEN BARITO KUALA'),
('6305', '63', 'KABUPATEN TAPIN'),
('6306', '63', 'KABUPATEN HULU SUNGAI SELATAN'),
('6307', '63', 'KABUPATEN HULU SUNGAI TENGAH'),
('6308', '63', 'KABUPATEN HULU SUNGAI UTARA'),
('6309', '63', 'KABUPATEN TABALONG'),
('6310', '63', 'KABUPATEN TANAH BUMBU'),
('6311', '63', 'KABUPATEN BALANGAN'),
('6371', '63', 'KOTA BANJARMASIN'),
('6372', '63', 'KOTA BANJAR BARU'),
('6401', '64', 'KABUPATEN PASER'),
('6402', '64', 'KABUPATEN KUTAI BARAT'),
('6403', '64', 'KABUPATEN KUTAI KARTANEGARA'),
('6404', '64', 'KABUPATEN KUTAI TIMUR'),
('6405', '64', 'KABUPATEN BERAU'),
('6409', '64', 'KABUPATEN PENAJAM PASER UTARA'),
('6411', '64', 'KABUPATEN MAHAKAM HULU'),
('6471', '64', 'KOTA BALIKPAPAN'),
('6472', '64', 'KOTA SAMARINDA'),
('6474', '64', 'KOTA BONTANG'),
('6501', '65', 'KABUPATEN MALINAU'),
('6502', '65', 'KABUPATEN BULUNGAN'),
('6503', '65', 'KABUPATEN TANA TIDUNG'),
('6504', '65', 'KABUPATEN NUNUKAN'),
('6571', '65', 'KOTA TARAKAN'),
('7101', '71', 'KABUPATEN BOLAANG MONGONDOW'),
('7102', '71', 'KABUPATEN MINAHASA'),
('7103', '71', 'KABUPATEN KEPULAUAN SANGIHE'),
('7104', '71', 'KABUPATEN KEPULAUAN TALAUD'),
('7105', '71', 'KABUPATEN MINAHASA SELATAN'),
('7106', '71', 'KABUPATEN MINAHASA UTARA'),
('7107', '71', 'KABUPATEN BOLAANG MONGONDOW UTARA'),
('7108', '71', 'KABUPATEN SIAU TAGULANDANG BIARO'),
('7109', '71', 'KABUPATEN MINAHASA TENGGARA'),
('7110', '71', 'KABUPATEN BOLAANG MONGONDOW SELATAN'),
('7111', '71', 'KABUPATEN BOLAANG MONGONDOW TIMUR'),
('7171', '71', 'KOTA MANADO'),
('7172', '71', 'KOTA BITUNG'),
('7173', '71', 'KOTA TOMOHON'),
('7174', '71', 'KOTA KOTAMOBAGU'),
('7201', '72', 'KABUPATEN BANGGAI KEPULAUAN'),
('7202', '72', 'KABUPATEN BANGGAI'),
('7203', '72', 'KABUPATEN MOROWALI'),
('7204', '72', 'KABUPATEN POSO'),
('7205', '72', 'KABUPATEN DONGGALA'),
('7206', '72', 'KABUPATEN TOLI-TOLI'),
('7207', '72', 'KABUPATEN BUOL'),
('7208', '72', 'KABUPATEN PARIGI MOUTONG'),
('7209', '72', 'KABUPATEN TOJO UNA-UNA'),
('7210', '72', 'KABUPATEN SIGI'),
('7211', '72', 'KABUPATEN BANGGAI LAUT'),
('7212', '72', 'KABUPATEN MOROWALI UTARA'),
('7271', '72', 'KOTA PALU'),
('7301', '73', 'KABUPATEN KEPULAUAN SELAYAR'),
('7302', '73', 'KABUPATEN BULUKUMBA'),
('7303', '73', 'KABUPATEN BANTAENG'),
('7304', '73', 'KABUPATEN JENEPONTO'),
('7305', '73', 'KABUPATEN TAKALAR'),
('7306', '73', 'KABUPATEN GOWA'),
('7307', '73', 'KABUPATEN SINJAI'),
('7308', '73', 'KABUPATEN MAROS'),
('7309', '73', 'KABUPATEN PANGKAJENE DAN KEPULAUAN'),
('7310', '73', 'KABUPATEN BARRU'),
('7311', '73', 'KABUPATEN BONE'),
('7312', '73', 'KABUPATEN SOPPENG'),
('7313', '73', 'KABUPATEN WAJO'),
('7314', '73', 'KABUPATEN SIDENRENG RAPPANG'),
('7315', '73', 'KABUPATEN PINRANG'),
('7316', '73', 'KABUPATEN ENREKANG'),
('7317', '73', 'KABUPATEN LUWU'),
('7318', '73', 'KABUPATEN TANA TORAJA'),
('7322', '73', 'KABUPATEN LUWU UTARA'),
('7325', '73', 'KABUPATEN LUWU TIMUR'),
('7326', '73', 'KABUPATEN TORAJA UTARA'),
('7371', '73', 'KOTA MAKASSAR'),
('7372', '73', 'KOTA PAREPARE'),
('7373', '73', 'KOTA PALOPO'),
('7401', '74', 'KABUPATEN BUTON'),
('7402', '74', 'KABUPATEN MUNA'),
('7403', '74', 'KABUPATEN KONAWE'),
('7404', '74', 'KABUPATEN KOLAKA'),
('7405', '74', 'KABUPATEN KONAWE SELATAN'),
('7406', '74', 'KABUPATEN BOMBANA'),
('7407', '74', 'KABUPATEN WAKATOBI'),
('7408', '74', 'KABUPATEN KOLAKA UTARA'),
('7409', '74', 'KABUPATEN BUTON UTARA'),
('7410', '74', 'KABUPATEN KONAWE UTARA'),
('7411', '74', 'KABUPATEN KOLAKA TIMUR'),
('7412', '74', 'KABUPATEN KONAWE KEPULAUAN'),
('7413', '74', 'KABUPATEN MUNA BARAT'),
('7414', '74', 'KABUPATEN BUTON TENGAH'),
('7415', '74', 'KABUPATEN BUTON SELATAN'),
('7471', '74', 'KOTA KENDARI'),
('7472', '74', 'KOTA BAUBAU'),
('7501', '75', 'KABUPATEN BOALEMO'),
('7502', '75', 'KABUPATEN GORONTALO'),
('7503', '75', 'KABUPATEN POHUWATO'),
('7504', '75', 'KABUPATEN BONE BOLANGO'),
('7505', '75', 'KABUPATEN GORONTALO UTARA'),
('7571', '75', 'KOTA GORONTALO'),
('7601', '76', 'KABUPATEN MAJENE'),
('7602', '76', 'KABUPATEN POLEWALI MANDAR'),
('7603', '76', 'KABUPATEN MAMASA'),
('7604', '76', 'KABUPATEN MAMUJU'),
('7605', '76', 'KABUPATEN MAMUJU UTARA'),
('7606', '76', 'KABUPATEN MAMUJU TENGAH'),
('8101', '81', 'KABUPATEN MALUKU TENGGARA BARAT'),
('8102', '81', 'KABUPATEN MALUKU TENGGARA'),
('8103', '81', 'KABUPATEN MALUKU TENGAH'),
('8104', '81', 'KABUPATEN BURU'),
('8105', '81', 'KABUPATEN KEPULAUAN ARU'),
('8106', '81', 'KABUPATEN SERAM BAGIAN BARAT'),
('8107', '81', 'KABUPATEN SERAM BAGIAN TIMUR'),
('8108', '81', 'KABUPATEN MALUKU BARAT DAYA'),
('8109', '81', 'KABUPATEN BURU SELATAN'),
('8171', '81', 'KOTA AMBON'),
('8172', '81', 'KOTA TUAL'),
('8201', '82', 'KABUPATEN HALMAHERA BARAT'),
('8202', '82', 'KABUPATEN HALMAHERA TENGAH'),
('8203', '82', 'KABUPATEN KEPULAUAN SULA'),
('8204', '82', 'KABUPATEN HALMAHERA SELATAN'),
('8205', '82', 'KABUPATEN HALMAHERA UTARA'),
('8206', '82', 'KABUPATEN HALMAHERA TIMUR'),
('8207', '82', 'KABUPATEN PULAU MOROTAI'),
('8208', '82', 'KABUPATEN PULAU TALIABU'),
('8271', '82', 'KOTA TERNATE'),
('8272', '82', 'KOTA TIDORE KEPULAUAN'),
('9101', '91', 'KABUPATEN FAKFAK'),
('9102', '91', 'KABUPATEN KAIMANA'),
('9103', '91', 'KABUPATEN TELUK WONDAMA'),
('9104', '91', 'KABUPATEN TELUK BINTUNI'),
('9105', '91', 'KABUPATEN MANOKWARI'),
('9106', '91', 'KABUPATEN SORONG SELATAN'),
('9107', '91', 'KABUPATEN SORONG'),
('9108', '91', 'KABUPATEN RAJA AMPAT'),
('9109', '91', 'KABUPATEN TAMBRAUW'),
('9110', '91', 'KABUPATEN MAYBRAT'),
('9111', '91', 'KABUPATEN MANOKWARI SELATAN'),
('9112', '91', 'KABUPATEN PEGUNUNGAN ARFAK'),
('9171', '91', 'KOTA SORONG'),
('9401', '94', 'KABUPATEN MERAUKE'),
('9402', '94', 'KABUPATEN JAYAWIJAYA'),
('9403', '94', 'KABUPATEN JAYAPURA'),
('9404', '94', 'KABUPATEN NABIRE'),
('9408', '94', 'KABUPATEN KEPULAUAN YAPEN'),
('9409', '94', 'KABUPATEN BIAK NUMFOR'),
('9410', '94', 'KABUPATEN PANIAI'),
('9411', '94', 'KABUPATEN PUNCAK JAYA'),
('9412', '94', 'KABUPATEN MIMIKA'),
('9413', '94', 'KABUPATEN BOVEN DIGOEL'),
('9414', '94', 'KABUPATEN MAPPI'),
('9415', '94', 'KABUPATEN ASMAT'),
('9416', '94', 'KABUPATEN YAHUKIMO'),
('9417', '94', 'KABUPATEN PEGUNUNGAN BINTANG'),
('9418', '94', 'KABUPATEN TOLIKARA'),
('9419', '94', 'KABUPATEN SARMI'),
('9420', '94', 'KABUPATEN KEEROM'),
('9426', '94', 'KABUPATEN WAROPEN'),
('9427', '94', 'KABUPATEN SUPIORI'),
('9428', '94', 'KABUPATEN MAMBERAMO RAYA'),
('9429', '94', 'KABUPATEN NDUGA'),
('9430', '94', 'KABUPATEN LANNY JAYA'),
('9431', '94', 'KABUPATEN MAMBERAMO TENGAH'),
('9432', '94', 'KABUPATEN YALIMO'),
('9433', '94', 'KABUPATEN PUNCAK'),
('9434', '94', 'KABUPATEN DOGIYAI'),
('9435', '94', 'KABUPATEN INTAN JAYA'),
('9436', '94', 'KABUPATEN DEIYAI'),
('9471', '94', 'KOTA JAYAPURA');

-- --------------------------------------------------------

--
-- Table structure for table `tb_leasing`
--

CREATE TABLE `tb_leasing` (
  `leasing_id` int(10) UNSIGNED NOT NULL,
  `leasing_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `leasing_nick` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `leasing_alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `leasing_kota` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `leasing_telp` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `leasing_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_leasing`
--

INSERT INTO `tb_leasing` (`leasing_id`, `leasing_nama`, `leasing_nick`, `leasing_alamat`, `leasing_kota`, `leasing_telp`, `leasing_status`, `created_at`, `updated_at`) VALUES
(8, 'Dipo Star Finance', 'DSF', 'Jalan Jendral Sudirman No.132C, 20 Ilir I, Ilir Timur I, 20 Ilir D. I, Ilir Tim. I, Kota Palembang, Sumatera Selatan 30126', 'Palembang', '711353955', 1, '2017-08-11 03:01:44', '2017-08-11 03:01:44'),
(9, 'Adira Finance', 'AF', 'Jl. Jend. Sudirman, Ario Kemuning, Kemuning, Kota Palembang, Sumatera Selatan 30128', 'Palembang', '7115612793', 1, '2017-08-11 03:02:30', '2017-08-11 03:02:30'),
(10, 'Mandiri Tunas Finance', 'MTF', 'JL. Veteran, Ruko Rajawali No.931-932, Palembang, Kepandean Baru, Ilir Timur I, Palembang City, South Sumatra 30111', 'Palembang', '711378476', 1, '2017-08-11 03:03:59', '2017-08-11 03:03:59'),
(11, 'Sinar Mas', 'SM', 'Jalan R Sukamto Komplek Ruko PTC Blok H No 1, 8 Ilir, Ilir Tim. II, Kota Palembang, Sumatera Selatan 30164', 'Palembang', '711379758', 1, '2017-08-11 03:05:15', '2017-08-11 03:05:15'),
(12, 'Bess Finance', 'BF', 'Jalan Sumpah Pemuda Blok I No.7 - 7A, Lorok Pakjo, Ilir Barat I, Lorok Pakjo, Ilir Bar. I, Kota Palembang, Sumatera Selatan 30137', 'Palembang', '711311828', 1, '2017-08-11 03:05:56', '2017-08-11 03:05:56'),
(13, 'BAF', 'BAF', 'Jl. Jend. Sudirman, Ario Kemuning, Kemuning, Kota Palembang, Sumatera Selatan 30128', 'Palembang', '7115610702', 1, '2017-08-11 03:06:30', '2017-08-11 03:06:30'),
(14, 'Indomobil Finance', 'IF', 'Jl. Basuki Rahmat No. 24F, RT. 024/009, Pahlawan, Kemuning, Pahlawan, Kemuning, Kota Palembang, Sumatera Selatan 30127', 'Palembang', '711319934', 1, '2017-08-11 03:07:01', '2017-08-11 03:07:01'),
(15, 'Mitsui Leasing', 'ML', 'Jl. Ptc No.59, 8 Ilir, Ilir Tim. II, Kota Palembang, Sumatera Selatan 30114', 'Palembang', '711382460', 1, '2017-08-11 03:08:11', '2017-08-11 03:08:11');

-- --------------------------------------------------------

--
-- Table structure for table `tb_leasing_bayar`
--

CREATE TABLE `tb_leasing_bayar` (
  `lbayar_id` int(10) UNSIGNED NOT NULL,
  `lbayar_spkl` int(11) NOT NULL,
  `lbayar_tgl` date NOT NULL,
  `lbayar_nominal` bigint(20) NOT NULL,
  `lbayar_ket` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_leasing_bayar`
--

INSERT INTO `tb_leasing_bayar` (`lbayar_id`, `lbayar_spkl`, `lbayar_tgl`, `lbayar_nominal`, `lbayar_ket`, `created_at`, `updated_at`) VALUES
(19, 19, '2017-08-09', 34750000, 'DP ', NULL, NULL),
(20, 19, '2017-08-12', 100000000, 'Pelunasan', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_notif_activity`
--

CREATE TABLE `tb_notif_activity` (
  `na_id` int(11) NOT NULL,
  `na_judul` varchar(255) NOT NULL,
  `na_keterangan` text,
  `na_kategori` varchar(20) NOT NULL,
  `na_jenis` varchar(10) NOT NULL,
  `na_status` varchar(2) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_notif_activity`
--

INSERT INTO `tb_notif_activity` (`na_id`, `na_judul`, `na_keterangan`, `na_kategori`, `na_jenis`, `na_status`, `created_at`, `updated_at`) VALUES
(1, 'Diskon %spk% Revisi  ', 'Cashback untuk kendaraan %spk%.', 'diskon', 'activity', '', '2017-09-10 00:00:00', '2017-09-10 00:00:00'),
(2, 'SPK %spk% telah disetujui oleh Supervisor. Harap lakukan pembayaran.', NULL, 'spk', 'notif', '9', '2017-09-09 00:00:00', '2017-09-09 00:00:00'),
(3, 'SPK %spk% telah ditolak oleh Supervisor.', NULL, 'spk', 'activity', '90', '2017-09-11 00:00:00', '2017-09-11 00:00:00'),
(4, 'SPK %spk% telah disetujui oleh Supervisor. Harap lakukan pelunasan. ', NULL, 'spk', 'notif', '1', '2017-09-11 00:00:00', '2017-09-11 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_pelanggan`
--

CREATE TABLE `tb_pelanggan` (
  `pel_id` int(10) UNSIGNED NOT NULL,
  `pel_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pel_identitas` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pel_npwp` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pel_provinsi` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pel_kategori` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pel_alamat` text COLLATE utf8mb4_unicode_ci,
  `pel_pos` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pel_kota` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pel_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pel_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pel_ponsel` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pel_lahir` date DEFAULT NULL,
  `pel_fotoid` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `pel_sales` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_pelanggan`
--

INSERT INTO `tb_pelanggan` (`pel_id`, `pel_nama`, `pel_identitas`, `pel_npwp`, `pel_provinsi`, `pel_kategori`, `pel_alamat`, `pel_pos`, `pel_kota`, `pel_email`, `pel_telp`, `pel_ponsel`, `pel_lahir`, `pel_fotoid`, `pel_sales`, `created_at`, `updated_at`) VALUES
(48, 'Rahmat Latif', '1671022809900009', NULL, '', '', 'jl. kh wahid hasyim lr berdikari', '30257', 'Palembang', 'rahmatlatif@gmail.com', '0', '081369342713', '1990-09-28', '', 4, '2017-08-11 02:44:12', '2017-08-11 20:04:09'),
(49, 'Windi Dwi Lestari', '3303884730001', NULL, '', '', 'Jl. Dempo Raya Ilir Barat I', '30344', 'Palembang', 'windidwilestari@gmail.com', '0711-899919', '08787777888', '1992-02-03', '', 4, '2017-08-11 02:53:47', '2017-08-11 02:53:47'),
(50, 'Mang Cek Boy', '109912211011', NULL, '', '', 'Plaju Ulu', '301192', 'Palembang', 'haaha@gmail.com', '07118817211', '089987721311', '1977-08-11', '', 1, '2017-08-11 03:04:38', '2017-08-11 03:04:38'),
(51, 'Rahmat Siagian', '33031778920001', NULL, '', '', 'Jl. Musi Raya Barat, Baung IV no.236', '30163', 'Palembang', 'rahmat@gmail.com', '0711823888', '08131111010', '1990-07-12', '', 4, '2017-08-11 03:50:38', '2017-08-11 03:51:00'),
(52, 'Muhammad Yusuf Gumay', '33031132850003', NULL, '', '', 'Jl. Nibung 4 no.98', '30167', 'Palembang', 'ucupz@gmail.com', '0711-882781', '08527777758', '1991-10-17', '', 2, '2017-08-11 03:54:00', '2017-08-11 03:54:00'),
(53, 'Ardie Wiratama Cibay', '1671012919900002', NULL, '', '', 'Komp Pusri Sako', '301991', 'Palembang', 'ardicibay@gmail.com', '0711899281', '081344552582', '1993-08-11', '', 6, '2017-08-11 03:54:59', '2017-08-11 03:54:59'),
(54, 'CV. Karya Siber Indonesia', '017087098374', NULL, '', '', 'Jl. Prajurit KMS Ali', '30111', 'Palembang', 'karyasiber@gmail.com', '0711-700012', '0852776685', '2017-01-01', '', 1, '2017-08-11 04:14:54', '2017-08-11 04:14:54'),
(55, 'Fajar Fernanda', '66067772380001', NULL, '', '', 'Jl. Trikora 2 No.89 RT. 09 RW.', '400142', 'Palembang', 'fajarfernanda@gmail.com', '0711-774812', '081300090009', '1978-03-14', '', 1, '2017-08-11 05:22:19', '2017-08-11 05:23:23'),
(56, 'Tegar Purnama', '88900029838001', NULL, '', '', 'Jl. Tanjung Api api', '30090', 'Palembang', 'tegarpurnama@gmail.com', '0711-788782', '0898200200', '1990-10-19', '', 3, '2017-08-11 10:12:10', '2017-08-11 10:12:27'),
(57, 'DONI DARMAWAN', '98848923929', NULL, '', '', 'JL. SULTAN', '39992', 'PALEMBANG', 'donidarmawan@gmail.com', '0711-239919', '0821777482', '1988-08-11', '', 5, '2017-08-11 10:49:58', '2017-08-11 10:49:58'),
(58, 'qweqweq', '123123', NULL, '', '', 'qweqweqw', '123123', 'wqeqweq', 'qweqwe', '12312', '1231231', '2017-08-12', '', 3, '2017-08-11 20:05:22', '2017-08-11 20:05:22'),
(59, 'DICKY WIJAYA KUSUMA', '1671101408750010', NULL, '', '', 'Jl. TAKWA LORONG JAKARTA PALEMBANG', '30020', 'PALEMBANG', 'DICKYWIJAYA@GMAIL.COM', '0711-822282', '085279574682', '1975-08-14', '', 3, '2017-08-11 20:33:43', '2017-08-11 20:34:06'),
(61, 'DIAN ARIANTI', '1600800092148567', NULL, 'SUMATERA SELATAN', '0', 'JL. JALAN JALAN', '31540', 'KOTA PALEMBANG', 'DISITA@GMAIL.COM', '071147889', '082282676924', '2017-08-07', '2c36adc7080cedb14782f16a7863d309.jpg', NULL, NULL, NULL),
(62, 'AMAT', '800546587543000', NULL, 'SUMATERA SELATAN', '0', 'JL. PANJAITAN', '30163', 'KOTA PALEMBANG', 'AMAT@GMAIL.COM', '071188008', '085279574682', '1990-08-23', '225fa41e347c56aa6edd105158f346e5.jpg', NULL, NULL, NULL),
(63, 'JUMAR FIKRI', '6689500976640001', NULL, 'SUMATERA SELATAN', '0', 'JL. YUKA BERSIH', '66430', 'KOTA PALEMBANG', 'JUMARFIKRI@GMAIL.COM', '0711522459', '0852467361845', '1985-03-24', '225fa41e347c56aa6edd105158f346e5.jpg', NULL, NULL, NULL),
(64, 'RIKO', '4669781512222', NULL, 'SUMATERA SELATAN', '0', 'JL. GOTONG ROYONG', '30448', 'KOTA PALEMBANG', 'RIKO@GMAIL.COM', '0711888866', '085464643168', '1991-02-18', 'dbd528f45c84099c6f7412345a1706cc.jpg', NULL, NULL, NULL),
(65, 'ANGGARI AYU', '3666536499990001', NULL, 'SUMATERA SELATAN', '0', 'JL. PLAJU', '66400', 'KOTA PALEMBANG', 'ANGGARI@GMAIL.COM', '0711133466', '085464643198', '1994-01-19', '16a67df40638f79cb3b325f805ca13a6.jpg', NULL, NULL, NULL),
(66, 'ANDRE D ASTA', '1234556789', '', 'SUMATERA SELATAN', '0', 'JL. SRIJAYA NO 1345 RT/RW 026/008 KEL. SRIJAYA KEC. ALANG ALANG LEBAR', '30114', 'KOTA PALEMBANG', 'ANDRE@GMAIL.COM', '', '081955555555', '1982-02-01', '3a471806e3239e880e6728e9ae8752c2.jpg', NULL, NULL, NULL),
(67, 'WAK IYENG', '08885466900003', NULL, 'ACEH', '0', 'THANKS', '80085', 'KABUPATEN ACEH BARAT', 'CRED@GMAIL.COM', '071111225', '008580555', '1987-08-21', '8dcd5edb525f812247b31cc449ad013d.jpg', NULL, NULL, NULL),
(68, 'HARIS ADNAN', '35546000645000', NULL, 'SUMATERA SELATAN', '0', 'JL. SEKIP PANGKAL NO.33 ', '30450', 'KOTA PALEMBANG', 'HARISADN12@GMAIL.COM', '', '081233388880', '1985-04-26', 'd415a44209f80a12787204d61b24aa2c.jpg', NULL, NULL, NULL),
(69, 'TIARAPUSPA', '36699966663333', NULL, 'SUMATERA SELATAN', '0', 'JL. SEADAYA', '69998', 'KOTA PALEMBANG', 'TIARAPUSPA@GMAIL.COM', '', '081342225555', '1992-09-08', 'bea013a947e2432b4b0fb0e1891703d0.jpg', NULL, NULL, NULL),
(70, 'LULUS SETIAWAN', '65646464646', NULL, 'SUMATERA SELATAN', '0', 'JL. BABABAB', '66868', 'KOTA PALEMBANG', '', '', '0686863834', '1979-09-08', '3f16035b825cabe9d3f8d8929914d063.jpg', NULL, NULL, NULL),
(71, 'PT. ASRO PAMOR', '', '369369369', 'SUMATERA SELATAN', '1', 'JL. BUNGA MAS KIKIM SELATAN', '70211', 'KOTA PALEMBANG', 'ASROPAMOR@GMAIL.COM', '0711823823', '08124584545', NULL, '', NULL, NULL, NULL),
(72, 'TYAR BUNJAHI', '9000056662333333', NULL, 'SUMATERA SELATAN', '0', 'JL. BAJSBSKSBDHDJSBSH', '84800', 'KOTA PALEMBANG', 'HAAAAG@GMAIL.COM', '', '081554887466', '1981-03-05', 'bea013a947e2432b4b0fb0e1891703d0.jpg', NULL, NULL, NULL),
(73, 'PAK UCIK', '80028888881333', NULL, 'SUMATERA SELATAN', '0', 'JL. HHHHHHBBB', '69000', 'KOTA PALEMBANG', '', '', '081346667888', '1983-09-11', 'f1646a1aac4bf0fd904e8ecb302c83ad.jpg', NULL, NULL, NULL),
(74, 'TDZUNAL OBARAKARTI', '1651447888000888', '', 'SUMATERA SELATAN', '0', 'JALANIN AJA DULU KALI YAAA', '31045', 'KOTA PALEMBANG', 'TDZUNAL23@GMAIL.COM ', '0714244888', '08127318321', '1951-09-11', '21dc2e6727c10264f93bfccd7411e34c.jpg', NULL, NULL, NULL),
(75, 'HINAYAH', '580088466663333', '', 'SUMATERA SELATAN', '0', 'JL. PUNIKAN NO.23', '64943', 'KOTA PALEMBANG', 'GIUAKK@GMAIL.COM', '0711840008', '08546943696', '1983-02-20', '7d3ca6d213ac76a77bde0aa7b08fe6ed.jpg', NULL, NULL, NULL),
(76, 'TDZUNAL OKBAEAKARTI', '1672258880085125', '', 'PAPUA BARAT', '0', 'JALANIN AJA DULU', '40008', 'KABUPATEN FAKFAK', 'TDZUNAL23@GMAIL.COM ', '022788569', '0889558885588', '1986-09-12', '2e1376ad08d2b34351e28b8989d404b8.jpg', NULL, NULL, NULL),
(77, 'WAHYU', '949484849494', '', 'ACEH', '0', 'JL. HAGAGAVGAHAH', '57007', 'KABUPATEN ACEH BARAT', 'GGHGGFF', '071197948456', '085764644', '2007-04-10', 'ee4d0d28fd52ef56ac87d53e85943971.jpg', NULL, NULL, NULL),
(78, 'TDZUNAL', '1675111188455788', '', 'DI YOGYAKARTA', '0', 'JALANIN AJA DULU', '20548', 'KABUPATEN BANTUL', 'OKHDHHS@GMAIL', '082282676924', '082282676924', '2006-09-12', '07ad95adfc4854bd78e0ac38029bf2ee.jpg', NULL, NULL, NULL),
(79, 'EDO KONDOM LOGIT', '1670048840078844', '', 'ACEH', '0', 'JALANI AJA DLU', '30521', 'KABUPATEN ACEH BARAT', 'KSB@JDHDHD', '081275488487', '08215884887444', '1988-09-15', 'ca5d4319b35c0670d54f7f1163fae741.jpg', NULL, NULL, NULL),
(80, 'YUDHA PRATAMA PUTRA', '8058884666133300', '', 'SUMATERA SELATAN', '0', 'JL. MERDEKA', '80548', 'KOTA PALEMBANG', 'MERDEKA@GMAIL.COM', '0711588096', '0812548464980', '1983-05-16', 'b26a87b196c98b227f6649602868d600.jpg', NULL, NULL, NULL),
(81, 'DANDY', '1671118755888588', '', 'GORONTALO', '0', 'JALAN TG. TAKAT NO 67 RT. 42 RW 009 PALEMBANG ', '47718', 'KABUPATEN BOALEMO', 'DND@GMAIL.COM', '', '085888800990', '1999-09-09', 'b3d899b51341e38d8e428c02a49a08e4.jpg', NULL, NULL, NULL),
(82, 'WINDA PREMASWA', '9039000008453316', '', 'SUMATERA SELATAN', '0', 'JL. USAHA KARYA', '30968', 'KOTA PALEMBANG', 'WINDA@GMAIL.COM', '0711845269', '081354848490', '1989-09-20', 'f3b0859a56717732985810f3febf358b.jpg', NULL, NULL, NULL),
(83, 'AMRIL GIRING', '908500086399', '', 'SUMATERA SELATAN', '0', 'JL. KIJANG MAS NO.657', '58000', 'KOTA PALEMBANG', 'GHIJO@GMAIL.COM', '', '081355569877', '1988-01-24', '9fcc3b510ce17b50f1f54e7732d1e7f6.jpg', NULL, NULL, NULL),
(84, 'HARIS ADNAN', '1671118808899888', '', 'SUMATERA SELATAN', '0', 'KOMP PJKA PALEMBANG NO 89 RT 4 RW 98 PALEMBANG', '30155', 'KOTA PALEMBANG', 'HRA@GMAIL.COM', '', '08136745212', '1994-09-20', '25faed668a079d01858c6c9d19304d34.jpg', NULL, NULL, NULL),
(85, 'ADITYA PURNAMA', '805000463154000', '', 'SUMATERA SELATAN', '0', 'JL. GERILYA NO.34', '80540', 'KOTA PALEMBANG', 'PURNAMA@GMAIL.COM', '', '0856248888854', '1987-03-24', '54a0f06629c56a5e9cadd45118b7eca6.jpg', NULL, NULL, NULL),
(86, 'LOLOK', '1758888858809589', '', 'SUMATERA SELATAN', '0', 'JL PUNCAK SEKUNING NO 67 RT 89 RW 67 PALEMBANG', '30154', 'KOTA PALEMBANG', 'OKTACAC123@GMAIL.COM ', '', '081367409950', '1941-09-20', '352d2435217fc94de7a6f3fee67b290d.jpg', NULL, NULL, NULL),
(87, 'PT. ASRO PAMOR', '', '369369369', 'SUMATERA SELATAN', '1', 'JL. BUNGA MAS KIKIM SELATAN', '70211', 'KOTA PALEMBANG', 'ASROPAMOR@GMAIL.COM', '0711823823', '08124584545', NULL, '', NULL, NULL, NULL),
(88, 'ZUNAL ', '1671158885888558', '', 'SUMATERA UTARA', '0', 'PALEMBANG, JLN ASIA AFRIKA NO 89 RT 67 KELURAHAN ULU 2', '50812', 'KABUPATEN ASAHAN', 'HBD@IRMANSYAH805 ', '071558885', '08124578515', '1974-09-20', '794dbca5eb9dc23441e43523d136c070.jpg', NULL, NULL, NULL),
(89, 'RENDHY ENGGARIAWAN', '1671111800099888', '', 'SUMATERA SELATAN', '0', 'JLN. PIPA REJA NO 89 RT 76 RW 30 KELURAHAN TALANG AMAN KECAMATAN 8 ILIR', '31115', 'KOTA PALEMBANG', 'REND@GMAIL.COM', '', '081354978529', '1994-06-09', '51ccced4c28ffd9b5bc1a346772aa02d.jpg', NULL, NULL, NULL),
(90, 'JANURIAN GLIKO', '690000008880466', '', 'SUMATERA SELATAN', '0', 'JL. BIUSJJ', '30899', 'KOTA PALEMBANG', 'GKILAS@GMAIL.COM', '0711258099', '085234584949', '1985-01-28', 'ba2637e13708e4a00f01ba49848a522e.jpg', NULL, NULL, NULL),
(91, 'KILOID HERO', '6930880800088864', '', 'NUSA TENGGARA TIMUR', '0', 'JL. HHSJJSKNB', '94646', 'KABUPATEN ALOR', 'BSHSHS@GMAIL.COM', '07118846764', '08138454545', '1982-11-18', 'b28fd2282c758b699f291df6574c16e5.jpg', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_provinsi`
--

CREATE TABLE `tb_provinsi` (
  `provinsi_id` char(2) NOT NULL,
  `provinsi_nama` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_provinsi`
--

INSERT INTO `tb_provinsi` (`provinsi_id`, `provinsi_nama`) VALUES
('11', 'ACEH'),
('12', 'SUMATERA UTARA'),
('13', 'SUMATERA BARAT'),
('14', 'RIAU'),
('15', 'JAMBI'),
('16', 'SUMATERA SELATAN'),
('17', 'BENGKULU'),
('18', 'LAMPUNG'),
('19', 'KEPULAUAN BANGKA BELITUNG'),
('21', 'KEPULAUAN RIAU'),
('31', 'DKI JAKARTA'),
('32', 'JAWA BARAT'),
('33', 'JAWA TENGAH'),
('34', 'DI YOGYAKARTA'),
('35', 'JAWA TIMUR'),
('36', 'BANTEN'),
('51', 'BALI'),
('52', 'NUSA TENGGARA BARAT'),
('53', 'NUSA TENGGARA TIMUR'),
('61', 'KALIMANTAN BARAT'),
('62', 'KALIMANTAN TENGAH'),
('63', 'KALIMANTAN SELATAN'),
('64', 'KALIMANTAN TIMUR'),
('65', 'KALIMANTAN UTARA'),
('71', 'SULAWESI UTARA'),
('72', 'SULAWESI TENGAH'),
('73', 'SULAWESI SELATAN'),
('74', 'SULAWESI TENGGARA'),
('75', 'GORONTALO'),
('76', 'SULAWESI BARAT'),
('81', 'MALUKU'),
('82', 'MALUKU UTARA'),
('91', 'PAPUA BARAT'),
('94', 'PAPUA');

-- --------------------------------------------------------

--
-- Table structure for table `tb_referral`
--

CREATE TABLE `tb_referral` (
  `referral_id` int(10) UNSIGNED NOT NULL,
  `referral_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referral_alamat` text COLLATE utf8mb4_unicode_ci,
  `referral_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referral_bank` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referral_rek` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referral_an` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_referral`
--

INSERT INTO `tb_referral` (`referral_id`, `referral_nama`, `referral_alamat`, `referral_telp`, `referral_bank`, `referral_rek`, `referral_an`, `created_at`, `updated_at`) VALUES
(7, 'Harry Purmanta Siagian', 'Jalan Baung', '08115123441', 'BCA', '1233221', 'Harry Purmanta Siagian', '2017-08-11 02:46:12', '2017-08-11 02:46:12'),
(8, 'Rahmi Liza', 'Jl. Bukit Lama', '085200002391', 'BNI', '0178305704', 'Rahmi Liza', '2017-08-11 02:46:30', '2017-08-11 02:46:30'),
(9, 'Alek Santoso', 'Kertapati', '08992348112', 'BNI', '1122131141', 'Alek Santoso', '2017-08-11 02:46:49', '2017-08-11 02:46:49'),
(10, 'Nurdin', 'Jl. Pakjo sebelah kiri', '0813777783', 'BRI', '700601003614530', 'Nurdin', '2017-08-11 02:47:38', '2017-08-11 02:47:38'),
(11, 'Nanda Pratama Putra', 'Jl. Anggrek raya no.23', '0889222213', 'Mandiri', '1560009861578', 'Nanda Pratama Putra', '2017-08-11 02:48:50', '2017-08-11 02:48:50'),
(12, 'Mohammad Muhaimin', 'Jl. sekip pangkal', '08571988192', 'Danamon', '1430049736', 'Mohammad Muhaimin', '2017-08-11 02:50:44', '2017-08-11 02:50:44');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sales`
--

CREATE TABLE `tb_sales` (
  `sales_id` int(10) UNSIGNED NOT NULL,
  `sales_salesUid` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sales_karyawan` int(10) UNSIGNED DEFAULT NULL,
  `sales_pwd` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `karyawan_usr` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sales_team` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_sales`
--

INSERT INTO `tb_sales` (`sales_id`, `sales_salesUid`, `sales_karyawan`, `sales_pwd`, `karyawan_usr`, `sales_team`, `created_at`, `updated_at`) VALUES
(2, '9Lci6Fq57ZPZCdZMdaadsn8VwVL2', 1, 'sales', 'okta', 1, '2017-04-26 17:00:00', '2017-04-28 17:00:00'),
(3, 'P7PplzcOEhXwvrWZaYc18Jd688i1', 5, '123456', 'ubakiri', 1, '2017-07-20 05:54:58', '2017-07-20 05:54:58'),
(4, '', 4, '123456', 'Nilam', 1, '2017-07-20 05:53:51', '2017-07-20 05:53:51'),
(6, 'xr4rWSWImNedeksUA5a6fNRXJBg1', 6, '123456', 'Yantok', 1, '2017-07-20 05:54:58', '2017-07-20 05:54:58'),
(33, '9Lci6Fq57ZPZCdZMdaadsn8VwVL2', 3, '123456', 'Windi', 1, '2017-07-20 05:53:51', '2017-07-20 05:53:51');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sales_uid`
--

CREATE TABLE `tb_sales_uid` (
  `salesUid_id` int(11) NOT NULL,
  `salesUid_sales` varchar(255) NOT NULL,
  `salesUid_spk` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_sales_uid`
--

INSERT INTO `tb_sales_uid` (`salesUid_id`, `salesUid_sales`, `salesUid_spk`) VALUES
(1, 'P7PplzcOEhXwvrWZaYc18Jd688i1', '17-00372'),
(2, '9Lci6Fq57ZPZCdZMdaadsn8VwVL2', '17-00104');

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk`
--

CREATE TABLE `tb_spk` (
  `spk_id` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_tgl` date NOT NULL,
  `spk_lat` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_lng` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_pajak` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spk_pel_npwp` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spk_fleet` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spk_pel_id` int(11) NOT NULL,
  `spk_pel_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_pel_identitas` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_pel_lahir` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_pel_alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_pel_provinsi` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_pel_kota` varchar(33) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_pel_pos` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_pel_telp` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spk_pel_ponsel` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_pel_email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_pel_kategori` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_pel_fotoid` text COLLATE utf8mb4_unicode_ci,
  `spk_stnk_nama` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_stnk_identitas` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_stnk_alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_stnk_provinsi` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_stnk_kota` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_stnk_pos` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_stnk_alamatd` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_stnk_provinsid` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_stnk_kotad` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_stnk_posd` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_stnk_telp` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spk_stnk_ponsel` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_stnk_email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spk_stnk_fotoid` text COLLATE utf8mb4_unicode_ci,
  `spk_pembayaran` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `spk_leasing` int(11) DEFAULT NULL,
  `spk_kendaraan` int(11) NOT NULL,
  `spk_ket_harga` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spk_warna` int(11) DEFAULT NULL,
  `spk_dp` int(10) UNSIGNED DEFAULT NULL,
  `spk_harga` bigint(20) DEFAULT '0',
  `spk_bbn` bigint(20) NOT NULL DEFAULT '0',
  `spk_dh` int(10) UNSIGNED DEFAULT NULL,
  `spk_match` date DEFAULT NULL,
  `spk_waktu_match` date DEFAULT NULL,
  `spk_sales` int(11) NOT NULL,
  `spk_status` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '90',
  `spk_do` date DEFAULT NULL,
  `spk_ket` text COLLATE utf8mb4_unicode_ci,
  `spk_catt_permintaan` text COLLATE utf8mb4_unicode_ci,
  `spk_ket_permintaanDo` text COLLATE utf8mb4_unicode_ci,
  `spk_catt_permintaanDo` text COLLATE utf8mb4_unicode_ci,
  `spk_ket_cancel` text COLLATE utf8mb4_unicode_ci,
  `spk_catt_cancel` text COLLATE utf8mb4_unicode_ci,
  `spk_automatching` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_spk`
--

INSERT INTO `tb_spk` (`spk_id`, `spk_tgl`, `spk_lat`, `spk_lng`, `spk_pajak`, `spk_pel_npwp`, `spk_fleet`, `spk_pel_id`, `spk_pel_nama`, `spk_pel_identitas`, `spk_pel_lahir`, `spk_pel_alamat`, `spk_pel_provinsi`, `spk_pel_kota`, `spk_pel_pos`, `spk_pel_telp`, `spk_pel_ponsel`, `spk_pel_email`, `spk_pel_kategori`, `spk_pel_fotoid`, `spk_stnk_nama`, `spk_stnk_identitas`, `spk_stnk_alamat`, `spk_stnk_provinsi`, `spk_stnk_kota`, `spk_stnk_pos`, `spk_stnk_alamatd`, `spk_stnk_provinsid`, `spk_stnk_kotad`, `spk_stnk_posd`, `spk_stnk_telp`, `spk_stnk_ponsel`, `spk_stnk_email`, `spk_stnk_fotoid`, `spk_pembayaran`, `spk_leasing`, `spk_kendaraan`, `spk_ket_harga`, `spk_warna`, `spk_dp`, `spk_harga`, `spk_bbn`, `spk_dh`, `spk_match`, `spk_waktu_match`, `spk_sales`, `spk_status`, `spk_do`, `spk_ket`, `spk_catt_permintaan`, `spk_ket_permintaanDo`, `spk_catt_permintaanDo`, `spk_ket_cancel`, `spk_catt_cancel`, `spk_automatching`, `created_at`, `updated_at`) VALUES
('17-00000', '2017-09-23', '-2.9826783', '104.7394717', '0', '', '', 90, 'JANURIAN GLIKO', '690000008880466', '1985-01-28', 'JL. BIUSJJ', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '30899', '0711258099', '085234584949', 'GKILAS@GMAIL.COM', '0', 'ba2637e13708e4a00f01ba49848a522e.jpg', 'JANURIAN GLIKO', '690000008880466', 'JL. BIUSJJ', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '30899', 'JL. BIUSJJ', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '30899', '0711258099', '085234584949', 'GKILAS@GMAIL.COM', '', 0, NULL, 1, '0', 1, 20000000, 204100000, 0, NULL, NULL, NULL, 3, '9', NULL, 'KETETANGAN SPK', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
('17-00001', '2017-09-23', '-2.9827333', '104.7394833', '0', '', '', 91, 'KILOID HERO', '6930880800088864', '1982-11-18', 'JL. HHSJJSKNB', 'NUSA TENGGARA TIMUR', 'KABUPATEN ALOR', '94646', '07118846764', '08138454545', 'BSHSHS@GMAIL.COM', '0', 'b28fd2282c758b699f291df6574c16e5.jpg', 'KILOID HERO', '6930880800088864', 'JL. HHSJJSKNB', 'NUSA TENGGARA TIMUR', 'KABUPATEN ALOR', '94646', 'JL. HHSJJSKNB', 'NUSA TENGGARA TIMUR', 'KABUPATEN ALOR', '94646', '07118846764', '08138454545', 'BSHSHS@GMAIL.COM', '', 0, NULL, 1, '0', 7, 20000000, 108900000, 0, NULL, NULL, NULL, 3, '9', NULL, 'KETERANGAN DISKON', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
('17-00384', '2017-09-20', '-2.9827428', '104.7395549', '0', '', '', 86, 'LOLOK', '1758888858809589', '1941-09-20', 'JL PUNCAK SEKUNING NO 67 RT 89 RW 67 PALEMBANG', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '30154', '', '081367409950', 'OKTACAC123@GMAIL.COM ', '0', 'a1135c11794bcad2d685f9173ec66299.jpg', 'LOLOK', '1758888858809589', 'JL PUNCAK SEKUNING NO 67 RT 89 RW 67 PALEMBANG', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '30154', 'JL PUNCAK SEKUNING NO 67 RT 89 RW 67 PALEMBANG', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '30154', '', '081367409950', 'OKTACAC123@GMAIL.COM', '', 1, 9, 2, '0', 9, 80000000, 244000000, 0, NULL, NULL, NULL, 2, '9', NULL, 'Ini kawan saya', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
('17-00385', '2017-09-20', '-2.9827428', '104.7395549', '0', '', '', 88, 'ZUNAL ', '1671158885888558', '1974-09-20', 'PALEMBANG, JLN ASIA AFRIKA NO 89 RT 67 KELURAHAN ULU 2', 'SUMATERA UTARA', 'KABUPATEN ASAHAN', '50812', '071558885', '08124578515', 'HBD@IRMANSYAH805 ', '0', '699147dc4bb75ba02de122963cdf623f.jpg', 'ZUNAL', '1671158885888558', 'PALEMBANG, JLN ASIA AFRIKA NO 89 RT 67 KELURAHAN ULU 2', 'SUMATERA UTARA', 'KABUPATEN ASAHAN', '50812', 'PALEMBANG, JLN ASIA AFRIKA NO 89 RT 67 KELURAHAN ULU 2', 'SUMATERA UTARA', 'KABUPATEN ASAHAN', '50812', '071558885', '08124578515', 'HBD@IRMANSYAH805', '', 1, 9, 4, '0', 11, 60000000, 184750000, 2000000, 201700063, '2017-09-20', '2017-09-27', 2, '3', '2017-09-20', 'Ini teman Saya', 'berhasil', 'ok', 'OKE', NULL, NULL, 1, NULL, '2017-09-20 13:12:47'),
('17-00387', '2017-09-20', '-2.9827428', '104.7395549', '0', '', '', 84, 'HARIS ADNAN', '1671118808899888', '1994-09-20', 'KOMP PJKA PALEMBANG NO 89 RT 4 RW 98 PALEMBANG', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '30155', '', '08136745212', 'HRA@GMAIL.COM', '0', '25faed668a079d01858c6c9d19304d34.jpg', 'HARIS ADNAN', '1671118808899888', 'KOMP PJKA PALEMBANG NO 89 RT 4 RW 98 PALEMBANG', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '30155', 'KOMP PJKA PALEMBANG NO 89 RT 4 RW 98 PALEMBANG', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '30155', '', '08136745212', 'HRA@GMAIL.COM', '', 1, 11, 8, '0', 1, 40000000, 195700000, 0, 20170002, '2017-09-20', '2017-09-23', 2, '2', NULL, 'Ini teman saya', 'berhasil', NULL, NULL, NULL, NULL, 1, NULL, NULL),
('17-00388', '2017-09-20', '-2.9826983', '104.739575', '0', '', '', 85, 'ADITYA PURNAMA', '805000463154000', '1987-03-24', 'JL. GERILYA NO.34', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '80540', '', '0856248888854', 'PURNAMA@GMAIL.COM', '0', 'bb23ada1feea686f2ba996e655a50bd9.jpg', 'ADITYA PURNAMA', '805000463154000', 'JL. GERILYA NO.34', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '80540', 'JL. GERILYA NO.34', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '80540', '', '0856248888854', 'PURNAMA@GMAIL.COM', '', 1, 15, 9, '0', 1, 30000000, 184750000, 20000000, 201700073, '2017-09-22', '2017-09-26', 3, '3', '2017-09-22', 'KETERANGAN SPK', NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-09-22 07:23:29'),
('17-00389', '2017-09-20', '-2.9827067', '104.7394933', '1', '369369369', 'DONNY ', 87, 'PT. ASRO PAMOR', '', '', 'JL. BUNGA MAS KIKIM SELATAN', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '70211', '0711823823', '08124584545', 'ASROPAMOR@GMAIL.COM', '1', '', 'PT. ASRO PAMOR', '8054222158', 'JL. BUNGA MAS KIKIM SELATAN', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '70211', 'JL. BUNGA MAS KIKIM SELATAN', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '70211', '0711823823', '08124584545', 'ASROPAMOR@GMAIL.COM', '2c78cf65d4b649f17c614fdf64e9ad0d.jpg', 1, 10, 11, '0', 16, 60000000, 205500000, 0, NULL, NULL, NULL, 3, '1', NULL, 'KETERANGAN SPK', NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
('17-00428', '2017-09-22', '-2.982725', '104.7395564', '0', '', '', 89, 'RENDHY ENGGARIAWAN', '1671111800099888', '1994-06-09', 'JLN. PIPA REJA NO 89 RT 76 RW 30 KELURAHAN TALANG AMAN KECAMATAN 8 ILIR', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '31115', '', '081354978529', 'REND@GMAIL.COM', '0', '51ccced4c28ffd9b5bc1a346772aa02d.jpg', 'RENDHY ENGGARIAWAN', '1671111800099888', 'JLN. PIPA REJA NO 89 RT 76 RW 30 KELURAHAN TALANG AMAN KECAMATAN 8 ILIR', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '31115', 'JLN. PIPA REJA NO 89 RT 76 RW 30 KELURAHAN TALANG AMAN KECAMATAN 8 ILIR', 'SUMATERA SELATAN', 'KOTA PALEMBANG', '31115', '', '081354978529', 'REND@GMAIL.COM', '', 1, 9, 4, '0', 12, 50000000, 184750000, 2000000, 201700042, '2017-09-22', '2017-09-26', 2, '3', '2017-09-23', 'Ini teman saya pak', NULL, 'ok', 'Setuju.', NULL, NULL, 1, NULL, '2017-09-23 07:14:40');

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_aksesoris`
--

CREATE TABLE `tb_spk_aksesoris` (
  `spka_id` int(11) NOT NULL,
  `spka_diskon` int(11) NOT NULL,
  `spka_kode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spka_nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spka_harga` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_spk_aksesoris`
--

INSERT INTO `tb_spk_aksesoris` (`spka_id`, `spka_diskon`, `spka_kode`, `spka_nama`, `spka_harga`, `created_at`, `updated_at`) VALUES
(3, 2, 'VL0001', 'Velg Racing', 15000000, '2017-09-20 13:26:45', '2017-09-20 13:26:45'),
(4, 1, 'SJ0003', 'Sarung Jok Xenia', 250000, '2017-09-22 06:56:51', '2017-09-22 06:56:51');

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_diskon`
--

CREATE TABLE `tb_spk_diskon` (
  `spkd_id` int(10) UNSIGNED NOT NULL,
  `spkd_spk` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkd_cashback` bigint(20) NOT NULL,
  `spkd_komisi` bigint(20) NOT NULL,
  `spkd_ket` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spkd_status` int(1) DEFAULT NULL,
  `spkd_catatan` text COLLATE utf8mb4_unicode_ci,
  `spkd_ref_an` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spkd_ref_bank` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkd_ref_alamat` text COLLATE utf8mb4_unicode_ci,
  `spkd_ref_nama` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spkd_ref_hubungan` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spkd_ref_rek` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spkd_ref_telp` varchar(13) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_spk_diskon`
--

INSERT INTO `tb_spk_diskon` (`spkd_id`, `spkd_spk`, `spkd_cashback`, `spkd_komisi`, `spkd_ket`, `spkd_status`, `spkd_catatan`, `spkd_ref_an`, `spkd_ref_bank`, `spkd_ref_alamat`, `spkd_ref_nama`, `spkd_ref_hubungan`, `spkd_ref_rek`, `spkd_ref_telp`, `created_at`, `updated_at`) VALUES
(1, '17-00388', 3000000, 0, 'keterangan cashback', 1, 'Diskon di setujui.', '', '', '', '', '', '', '', NULL, NULL),
(2, '17-00389', 3000000, 0, 'keterangan diskon', 1, NULL, '', '', '', '', '', '', '', NULL, NULL),
(3, '17-00385', 5000000, 0, 'ada cashbaxk', 1, NULL, '', '', '', '', '', '', '', NULL, NULL),
(4, '17-00428', 10000000, 0, 'Ada cash back om', 1, NULL, '', '', '', '', '', '', '', NULL, NULL),
(5, '17-00000', 3000000, 0, '', 1, 'Diskon telah di setujui.', '', '', '', '', '', '', '', NULL, NULL),
(6, '17-00001', 1000000, 0, '', 99, 'Diskon telah di tolak.', '', '', '', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_faktur`
--

CREATE TABLE `tb_spk_faktur` (
  `spkf_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkf_spk` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkf_tgl` date NOT NULL,
  `spkf_cetak` int(11) DEFAULT NULL,
  `spkf_user` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_spk_faktur`
--

INSERT INTO `tb_spk_faktur` (`spkf_id`, `spkf_spk`, `spkf_tgl`, `spkf_cetak`, `spkf_user`, `created_at`, `updated_at`) VALUES
('F-20170001', '17-00385', '2017-09-20', 2, NULL, '2017-09-20 13:12:47', '2017-09-20 13:12:47'),
('F-20170002', '17-00388', '2017-09-22', 2, NULL, '2017-09-22 07:23:29', '2017-09-22 07:23:29'),
('F-20170003', '17-00428', '2017-09-23', 1, NULL, '2017-09-23 07:14:21', '2017-09-23 07:14:21'),
('F-20170004', '17-00387', '2017-09-23', NULL, NULL, '2017-09-23 07:28:03', '2017-09-23 07:28:03');

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_leasing`
--

CREATE TABLE `tb_spk_leasing` (
  `spkl_id` int(10) UNSIGNED NOT NULL,
  `spkl_spk` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkl_leasing` int(10) UNSIGNED NOT NULL,
  `spkl_asuransi` int(11) DEFAULT NULL,
  `spkl_jenis_asuransi` int(11) DEFAULT NULL,
  `spkl_dp` bigint(20) UNSIGNED NOT NULL,
  `spkl_droping` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkl_waktu` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spkl_angsuran` bigint(20) UNSIGNED DEFAULT NULL,
  `spkl_cetak` date DEFAULT NULL,
  `spkl_tagihan` date DEFAULT NULL,
  `spkl_lunas` date DEFAULT NULL,
  `spkl_refund` date DEFAULT NULL,
  `spkl_jumlah_refund` bigint(20) DEFAULT NULL,
  `spkl_doc` char(1) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `spkl_user` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_spk_leasing`
--

INSERT INTO `tb_spk_leasing` (`spkl_id`, `spkl_spk`, `spkl_leasing`, `spkl_asuransi`, `spkl_jenis_asuransi`, `spkl_dp`, `spkl_droping`, `spkl_waktu`, `spkl_angsuran`, `spkl_cetak`, `spkl_tagihan`, `spkl_lunas`, `spkl_refund`, `spkl_jumlah_refund`, `spkl_doc`, `spkl_user`, `created_at`, `updated_at`) VALUES
(2, '17-00385', 9, 7, 1, 40000000, '144750000', '60 BULAN', 5000000, '2017-09-20', '2017-09-22', '2017-09-22', '2017-09-22', 500000, '1', NULL, NULL, NULL),
(3, '17-00388', 15, NULL, 1, 40000000, '144750000', '60 BLN', 3500000, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL),
(4, '17-00428', 9, NULL, 1, 30000000, '144750000', '60 BLN', 3500000, NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_no`
--

CREATE TABLE `tb_spk_no` (
  `spkNo_id` int(10) UNSIGNED NOT NULL,
  `spk_id` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkNo_sales` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_spk_no`
--

INSERT INTO `tb_spk_no` (`spkNo_id`, `spk_id`, `spkNo_sales`, `created_at`, `updated_at`) VALUES
(5, '17-00005', 1, NULL, NULL),
(6, '17-00006', 1, NULL, NULL),
(7, '17-00007', 1, NULL, NULL),
(8, '17-00008', 1, NULL, NULL),
(9, '17-00009', 1, NULL, NULL),
(10, '17-00010', 1, NULL, NULL),
(11, '17-00011', 2, NULL, NULL),
(12, '17-00012', 2, NULL, NULL),
(13, '17-00013', 2, NULL, NULL),
(14, '17-00014', 2, NULL, NULL),
(15, '17-00015', 2, NULL, NULL),
(16, '17-00016', 2, NULL, NULL),
(17, '17-00017', 2, NULL, NULL),
(18, '17-00018', 2, NULL, NULL),
(19, '17-00019', 2, NULL, NULL),
(20, '17-00020', 2, NULL, NULL),
(21, '17-00040', 3, NULL, NULL),
(22, '17-00041', 3, NULL, NULL),
(23, '17-00042', 3, NULL, NULL),
(24, '17-00043', 3, NULL, NULL),
(25, '17-00044', 3, NULL, NULL),
(26, '17-00045', 3, NULL, NULL),
(27, '17-00046', 3, NULL, NULL),
(28, '17-00047', 3, NULL, NULL),
(29, '17-00048', 3, NULL, NULL),
(30, '17-00049', 3, NULL, NULL),
(31, '17-00050', 3, NULL, NULL),
(32, '17-00100', 4, NULL, NULL),
(33, '17-00101', 4, NULL, NULL),
(34, '17-00102', 4, NULL, NULL),
(35, '17-00103', 4, NULL, NULL),
(36, '17-00104', 4, NULL, NULL),
(37, '17-00105', 4, NULL, NULL),
(38, '17-00106', 4, NULL, NULL),
(39, '17-00107', 4, NULL, NULL),
(40, '17-00108', 4, NULL, NULL),
(41, '17-00109', 4, NULL, NULL),
(42, '17-00110', 4, NULL, NULL),
(43, '17-00120', 5, NULL, NULL),
(44, '17-00121', 5, NULL, NULL),
(45, '17-00122', 5, NULL, NULL),
(46, '17-00123', 5, NULL, NULL),
(47, '17-00124', 5, NULL, NULL),
(48, '17-00125', 5, NULL, NULL),
(49, '17-00126', 5, NULL, NULL),
(50, '17-00127', 5, NULL, NULL),
(51, '17-00128', 5, NULL, NULL),
(52, '17-00129', 5, NULL, NULL),
(53, '17-00130', 5, NULL, NULL),
(54, '17-00131', 5, NULL, NULL),
(55, '17-00132', 5, NULL, NULL),
(56, '17-00133', 5, NULL, NULL),
(57, '17-00134', 5, NULL, NULL),
(58, '17-00135', 5, NULL, NULL),
(59, '17-00136', 5, NULL, NULL),
(60, '17-00137', 5, NULL, NULL),
(61, '17-00138', 5, NULL, NULL),
(62, '17-00139', 5, NULL, NULL),
(63, '17-00140', 5, NULL, NULL),
(64, '17-00160', 2, NULL, NULL),
(65, '17-00161', 2, NULL, NULL),
(66, '17-00162', 2, NULL, NULL),
(67, '17-00163', 2, NULL, NULL),
(68, '17-00164', 2, NULL, NULL),
(69, '17-00165', 2, NULL, NULL),
(70, '17-00166', 2, NULL, NULL),
(71, '17-00167', 2, NULL, NULL),
(72, '17-00168', 2, NULL, NULL),
(73, '17-00169', 2, NULL, NULL),
(74, '17-00170', 2, NULL, NULL),
(75, '17-00170', 6, NULL, NULL),
(76, '17-00171', 6, NULL, NULL),
(77, '17-00172', 6, NULL, NULL),
(78, '17-00173', 6, NULL, NULL),
(79, '17-00174', 6, NULL, NULL),
(80, '17-00175', 6, NULL, NULL),
(81, '17-00176', 6, NULL, NULL),
(82, '17-00177', 6, NULL, NULL),
(83, '17-00178', 6, NULL, NULL),
(84, '17-00179', 6, NULL, NULL),
(85, '17-00180', 6, NULL, NULL),
(86, '17-00190', 4, NULL, NULL),
(87, '17-00191', 4, NULL, NULL),
(88, '17-00192', 4, NULL, NULL),
(89, '17-00193', 4, NULL, NULL),
(90, '17-00194', 4, NULL, NULL),
(91, '17-00195', 4, NULL, NULL),
(92, '17-00196', 4, NULL, NULL),
(93, '17-00197', 4, NULL, NULL),
(94, '17-00198', 4, NULL, NULL),
(95, '17-00199', 4, NULL, NULL),
(96, '17-00200', 4, NULL, NULL),
(97, '17-00201', 1, NULL, NULL),
(98, '17-00202', 1, NULL, NULL),
(99, '17-00203', 1, NULL, NULL),
(100, '17-00204', 1, NULL, NULL),
(101, '17-00205', 1, NULL, NULL),
(102, '17-00206', 1, NULL, NULL),
(103, '17-00207', 1, NULL, NULL),
(104, '17-00208', 1, NULL, NULL),
(105, '17-00209', 1, NULL, NULL),
(106, '17-00210', 1, NULL, NULL),
(107, '17-00210', 1, NULL, NULL),
(108, '17-00211', 1, NULL, NULL),
(109, '17-00212', 1, NULL, NULL),
(110, '17-00213', 1, NULL, NULL),
(111, '17-00214', 1, NULL, NULL),
(112, '17-00215', 1, NULL, NULL),
(113, '17-00216', 1, NULL, NULL),
(114, '17-00217', 1, NULL, NULL),
(115, '17-00218', 1, NULL, NULL),
(116, '17-00219', 1, NULL, NULL),
(117, '17-00220', 1, NULL, NULL),
(118, '17-00221', 1, NULL, NULL),
(119, '17-00222', 1, NULL, NULL),
(120, '17-00223', 1, NULL, NULL),
(121, '17-00224', 1, NULL, NULL),
(122, '17-00225', 1, NULL, NULL),
(123, '17-00226', 1, NULL, NULL),
(124, '17-00227', 1, NULL, NULL),
(125, '17-00228', 1, NULL, NULL),
(126, '17-00229', 1, NULL, NULL),
(127, '17-00230', 1, NULL, NULL),
(128, '17-00231', 1, NULL, NULL),
(129, '17-00232', 1, NULL, NULL),
(130, '17-00233', 1, NULL, NULL),
(131, '17-00234', 1, NULL, NULL),
(132, '17-00235', 1, NULL, NULL),
(133, '17-00236', 1, NULL, NULL),
(134, '17-00237', 1, NULL, NULL),
(135, '17-00238', 1, NULL, NULL),
(136, '17-00239', 1, NULL, NULL),
(137, '17-00240', 1, NULL, NULL),
(138, '17-00240', 4, NULL, NULL),
(139, '17-00241', 4, NULL, NULL),
(140, '17-00242', 4, NULL, NULL),
(141, '17-00243', 4, NULL, NULL),
(142, '17-00244', 4, NULL, NULL),
(143, '17-00245', 4, NULL, NULL),
(144, '17-00246', 4, NULL, NULL),
(145, '17-00247', 4, NULL, NULL),
(146, '17-00248', 4, NULL, NULL),
(147, '17-00249', 4, NULL, NULL),
(148, '17-00250', 4, NULL, NULL),
(149, '17-00250', 2, NULL, NULL),
(150, '17-00251', 2, NULL, NULL),
(151, '17-00252', 2, NULL, NULL),
(152, '17-00253', 2, NULL, NULL),
(153, '17-00254', 2, NULL, NULL),
(154, '17-00255', 2, NULL, NULL),
(155, '17-00255', 5, NULL, NULL),
(156, '17-00256', 5, NULL, NULL),
(157, '17-00257', 5, NULL, NULL),
(158, '17-00258', 5, NULL, NULL),
(159, '17-00259', 5, NULL, NULL),
(160, '17-00260', 5, NULL, NULL),
(161, '17-00261', 5, NULL, NULL),
(162, '17-00262', 5, NULL, NULL),
(163, '17-00263', 5, NULL, NULL),
(164, '17-00264', 5, NULL, NULL),
(165, '17-00265', 5, NULL, NULL),
(166, '17-00266', 5, NULL, NULL),
(167, '17-00267', 5, NULL, NULL),
(168, '17-00268', 5, NULL, NULL),
(169, '17-00269', 5, NULL, NULL),
(170, '17-00270', 5, NULL, NULL),
(171, '17-00271', 5, NULL, NULL),
(172, '17-00272', 5, NULL, NULL),
(173, '17-00273', 5, NULL, NULL),
(174, '17-00274', 5, NULL, NULL),
(175, '17-00275', 5, NULL, NULL),
(176, '17-00276', 5, NULL, NULL),
(177, '17-00277', 5, NULL, NULL),
(178, '17-00278', 5, NULL, NULL),
(179, '17-00279', 5, NULL, NULL),
(180, '17-00280', 5, NULL, NULL),
(181, '17-00281', 5, NULL, NULL),
(182, '17-00282', 5, NULL, NULL),
(183, '17-00283', 5, NULL, NULL),
(184, '17-00284', 5, NULL, NULL),
(185, '17-00285', 5, NULL, NULL),
(186, '17-00286', 5, NULL, NULL),
(187, '17-00287', 5, NULL, NULL),
(188, '17-00288', 5, NULL, NULL),
(189, '17-00289', 5, NULL, NULL),
(190, '17-00290', 5, NULL, NULL),
(191, '17-00291', 5, NULL, NULL),
(192, '17-00292', 5, NULL, NULL),
(193, '17-00293', 5, NULL, NULL),
(194, '17-00294', 5, NULL, NULL),
(195, '17-00295', 5, NULL, NULL),
(196, '17-00296', 5, NULL, NULL),
(197, '17-00297', 5, NULL, NULL),
(198, '17-00298', 5, NULL, NULL),
(199, '17-00299', 5, NULL, NULL),
(200, '17-00300', 5, NULL, NULL),
(201, '17-00301', 5, NULL, NULL),
(202, '17-00302', 5, NULL, NULL),
(203, '17-00303', 5, NULL, NULL),
(204, '17-00304', 5, NULL, NULL),
(205, '17-00305', 5, NULL, NULL),
(206, '17-00306', 5, NULL, NULL),
(207, '17-00307', 5, NULL, NULL),
(208, '17-00308', 5, NULL, NULL),
(209, '17-00309', 5, NULL, NULL),
(210, '17-00310', 5, NULL, NULL),
(211, '17-00311', 5, NULL, NULL),
(212, '17-00312', 5, NULL, NULL),
(213, '17-00313', 5, NULL, NULL),
(214, '17-00314', 5, NULL, NULL),
(215, '17-00315', 5, NULL, NULL),
(216, '17-00316', 5, NULL, NULL),
(217, '17-00317', 5, NULL, NULL),
(218, '17-00318', 5, NULL, NULL),
(219, '17-00319', 5, NULL, NULL),
(220, '17-00320', 5, NULL, NULL),
(221, '17-00321', 5, NULL, NULL),
(222, '17-00322', 5, NULL, NULL),
(223, '17-00323', 5, NULL, NULL),
(224, '17-00324', 5, NULL, NULL),
(225, '17-00325', 5, NULL, NULL),
(226, '17-00326', 5, NULL, NULL),
(227, '17-00327', 5, NULL, NULL),
(228, '17-00328', 5, NULL, NULL),
(229, '17-00329', 5, NULL, NULL),
(230, '17-00330', 5, NULL, NULL),
(231, '17-00331', 5, NULL, NULL),
(232, '17-00332', 5, NULL, NULL),
(233, '17-00333', 5, NULL, NULL),
(234, '17-00334', 5, NULL, NULL),
(235, '17-00335', 5, NULL, NULL),
(236, '17-00336', 5, NULL, NULL),
(237, '17-00337', 5, NULL, NULL),
(238, '17-00338', 5, NULL, NULL),
(239, '17-00339', 5, NULL, NULL),
(240, '17-00340', 5, NULL, NULL),
(241, '17-00341', 5, NULL, NULL),
(242, '17-00342', 5, NULL, NULL),
(243, '17-00343', 5, NULL, NULL),
(244, '17-00344', 5, NULL, NULL),
(245, '17-00345', 5, NULL, NULL),
(246, '17-00346', 5, NULL, NULL),
(247, '17-00347', 5, NULL, NULL),
(248, '17-00348', 5, NULL, NULL),
(249, '17-00349', 5, NULL, NULL),
(250, '17-00350', 5, NULL, NULL),
(251, '17-00351', 5, NULL, NULL),
(252, '17-00352', 5, NULL, NULL),
(253, '17-00353', 5, NULL, NULL),
(254, '17-00354', 5, NULL, NULL),
(255, '17-00355', 5, NULL, NULL),
(256, '17-00355', 5, NULL, NULL),
(257, '17-00356', 5, NULL, NULL),
(258, '17-00357', 5, NULL, NULL),
(259, '17-00358', 5, NULL, NULL),
(260, '17-00359', 5, NULL, NULL),
(261, '17-00360', 5, NULL, NULL),
(262, '17-00361', 5, NULL, NULL),
(263, '17-00362', 5, NULL, NULL),
(264, '17-00363', 5, NULL, NULL),
(265, '17-00364', 5, NULL, NULL),
(266, '17-00365', 5, NULL, NULL),
(267, '17-00365', 2, NULL, NULL),
(268, '17-00366', 2, NULL, NULL),
(269, '17-00367', 2, NULL, NULL),
(270, '17-00367', 2, NULL, NULL),
(271, '17-00368', 2, NULL, NULL),
(272, '17-00369', 2, NULL, NULL),
(273, '17-00370', 2, NULL, NULL),
(274, '17-00370', 2, NULL, NULL),
(275, '17-00371', 2, NULL, NULL),
(277, '17-00372', 2, NULL, NULL),
(278, '17-00372', 5, NULL, NULL),
(279, '17-00373', 5, NULL, NULL),
(280, '17-00374', 5, NULL, NULL),
(281, '17-00375', 5, NULL, NULL),
(282, '17-00376', 5, NULL, NULL),
(283, '17-00377', 5, NULL, NULL),
(284, '17-00378', 5, NULL, NULL),
(285, '17-00379', 5, NULL, NULL),
(286, '17-00380', 5, NULL, NULL),
(287, '17-00381', 5, NULL, NULL),
(288, '17-00382', 5, NULL, NULL),
(289, '17-00372', 3, NULL, NULL),
(290, '17-00373', 3, NULL, NULL),
(291, '17-00374', 3, NULL, NULL),
(292, '17-00375', 3, NULL, NULL),
(293, '17-00376', 3, NULL, NULL),
(294, '17-00377', 3, NULL, NULL),
(295, '17-00377', 1, NULL, NULL),
(296, '17-00378', 1, NULL, NULL),
(297, '17-00378', 3, NULL, NULL),
(298, '17-00379', 3, NULL, NULL),
(299, '17-00380', 3, NULL, NULL),
(300, '17-00381', 3, NULL, NULL),
(301, '17-00382', 3, NULL, NULL),
(302, '17-00383', 3, NULL, NULL),
(303, '17-00383', 3, NULL, NULL),
(304, '17-00384', 3, NULL, NULL),
(305, '17-00385', 3, NULL, NULL),
(306, '17-00386', 3, NULL, NULL),
(307, '17-00387', 3, NULL, NULL),
(308, '17-00388', 3, NULL, NULL),
(309, '17-00389', 3, NULL, NULL),
(310, '17-00390', 3, NULL, NULL),
(311, '17-00391', 3, NULL, NULL),
(312, '17-00392', 3, NULL, NULL),
(313, '17-00393', 3, NULL, NULL),
(344, '17-00393', 1, NULL, NULL),
(345, '17-00394', 1, NULL, NULL),
(346, '17-00394', 1, NULL, NULL),
(347, '17-00395', 1, NULL, NULL),
(348, '17-00395', 1, NULL, NULL),
(349, '17-00396', 1, NULL, NULL),
(350, '17-00396', 1, NULL, NULL),
(351, '17-00397', 1, NULL, NULL),
(352, '17-00398', 1, NULL, NULL),
(353, '17-00399', 1, NULL, NULL),
(354, '17-00400', 1, NULL, NULL),
(355, '17-00401', 1, NULL, NULL),
(356, '17-00402', 1, NULL, NULL),
(357, '17-00403', 1, NULL, NULL),
(358, '17-00404', 1, NULL, NULL),
(359, '17-00405', 1, NULL, NULL),
(360, '17-00406', 1, NULL, NULL),
(361, '17-00406', 1, NULL, NULL),
(362, '17-00407', 1, NULL, NULL),
(363, '17-00408', 1, NULL, NULL),
(364, '17-00409', 1, NULL, NULL),
(365, '17-00410', 1, NULL, NULL),
(366, '17-00411', 1, NULL, NULL),
(367, '17-00412', 1, NULL, NULL),
(368, '17-00413', 1, NULL, NULL),
(369, '17-00414', 1, NULL, NULL),
(370, '17-00415', 1, NULL, NULL),
(371, '17-00416', 1, NULL, NULL),
(372, '17-00416', 3, NULL, NULL),
(373, '17-00417', 3, NULL, NULL),
(374, '17-00418', 3, NULL, NULL),
(375, '17-00419', 3, NULL, NULL),
(376, '17-00420', 3, NULL, NULL),
(377, '17-00421', 3, NULL, NULL),
(378, '17-00422', 3, NULL, NULL),
(379, '17-00423', 3, NULL, NULL),
(380, '17-00424', 3, NULL, NULL),
(381, '17-00425', 3, NULL, NULL),
(382, '17-00426', 3, NULL, NULL),
(383, '17-00426', 1, NULL, NULL),
(384, '17-00427', 1, NULL, NULL),
(385, '17-00427', 3, NULL, NULL),
(386, '17-00428', 3, NULL, NULL),
(387, '17-00427', 3, NULL, NULL),
(388, '17-00428', 3, NULL, NULL),
(389, '17-00428', 2, NULL, NULL),
(390, '17-00429', 2, NULL, NULL),
(391, '17-00430', 2, NULL, NULL),
(392, '17-00431', 2, NULL, NULL),
(393, '17-00432', 2, NULL, NULL),
(394, '17-00433', 2, NULL, NULL),
(395, '17-00434', 2, NULL, NULL),
(396, '17-00435', 2, NULL, NULL),
(397, '17-00436', 2, NULL, NULL),
(398, '17-00437', 2, NULL, NULL),
(399, '17-00438', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_pembayaran`
--

CREATE TABLE `tb_spk_pembayaran` (
  `spkp_id` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkp_spk` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkp_tgl` date NOT NULL,
  `spkp_akun` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkp_jumlah` bigint(20) NOT NULL,
  `spkp_ket` text COLLATE utf8mb4_unicode_ci,
  `spkp_user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_spk_pembayaran`
--

INSERT INTO `tb_spk_pembayaran` (`spkp_id`, `spkp_spk`, `spkp_tgl`, `spkp_akun`, `spkp_jumlah`, `spkp_ket`, `spkp_user`, `created_at`, `updated_at`) VALUES
('BP17-00004', '17-00370', '2017-09-13', '1.1.02.01', 35000000, 'UANG MUKA', NULL, '2017-09-14 18:48:05', NULL),
('BP17-00005', '17-00370', '2017-09-15', '1.1.01.01', 5000000, 'UANG MUKA 1', NULL, '2017-09-14 18:57:28', NULL),
('BP17-00006', '17-00370', '2017-09-15', '1.1.01.01', 64100000, 'UANG MUKA 2', NULL, '2017-09-14 19:02:30', NULL),
('BP17-00007', '17-00370', '2017-09-15', '1.1.01.01', 30000000, 'CICILAN 1', NULL, '2017-09-14 19:03:25', NULL),
('BP17-00008', '17-00370', '2017-09-15', '1.1.02.01', 10000000, 'CICILAN 2', NULL, '2017-09-14 19:06:15', NULL),
('BP17-00009', '17-00370', '2017-09-15', '1.1.01.01', 10000000, 'CICILAN 3', NULL, '2017-09-14 19:08:28', NULL),
('BP17-00010', '17-00370', '2017-09-15', '1.1.02.01', 15000000, 'CICILAN 4', NULL, '2017-09-14 19:12:54', NULL),
('BP17-00011', '17-00370', '2017-09-15', '1.1.01.01', 29750000, 'PELUNASAN', NULL, '2017-09-14 20:21:07', NULL),
('BP17-00012', '17-00377', '2017-09-15', '1.1.01.01', 44000000, 'UANG MUKA', NULL, '2017-09-15 01:52:32', NULL),
('BP17-00013', '17-00382', '2017-09-15', '1.1.02.01', 45000000, 'UANG MUKA', NULL, '2017-09-15 03:33:57', NULL),
('BP17-00014', '17-00378', '2017-09-15', '1.1.01.01', 20000000, 'UANG MUKA', NULL, '2017-09-15 12:05:54', NULL),
('BP17-00015', '17-00386', '2017-09-20', '1.1.01.01', 40000000, 'UANG MUKA', NULL, '2017-09-20 09:50:07', NULL),
('BP17-00016', '17-00386', '2017-09-20', '1.1.01.01', 1000000, 'PELUNASAN DP', NULL, '2017-09-20 10:14:14', NULL),
('BP17-00017', '17-00387', '2017-09-20', '1.1.01.01', 30000000, 'UANG MUKA', NULL, '2017-09-20 12:17:56', NULL),
('BP17-00018', '17-00385', '2017-09-20', '1.1.01.01', 35000000, 'UANG MUKA', NULL, '2017-09-20 12:45:41', NULL),
('BP17-00019', '17-00388', '2017-09-22', '1.1.01.01', 37000000, 'UANG MUKA', NULL, '2017-09-22 06:58:28', NULL),
('BP17-00020', '17-00428', '2017-09-22', '1.1.01.01', 30000000, 'UANG MUKA', NULL, '2017-09-22 09:40:21', NULL),
('BP17-00022', '17-00384', '2017-09-23', '1.1.01.01', 44000000, 'UANG MUKA', NULL, '2017-09-23 07:11:12', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_referral`
--

CREATE TABLE `tb_spk_referral` (
  `spkr_id` int(10) UNSIGNED NOT NULL,
  `spkr_diskon` int(11) NOT NULL,
  `spkr_tgl` date NOT NULL,
  `spkr_ket` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tb_spk_ttbj`
--

CREATE TABLE `tb_spk_ttbj` (
  `spkt_id` int(10) UNSIGNED NOT NULL,
  `spkt_spk` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `spkt_biro` int(11) DEFAULT NULL,
  `spkt_tglfaktur` date DEFAULT NULL,
  `spkt_stck` text COLLATE utf8mb4_unicode_ci,
  `spkt_ujitype` date DEFAULT NULL,
  `spkt_nopol` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spkt_plat` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `spkt_notis` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `spkt_stnk` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `spkt_waktu_stnk` int(10) UNSIGNED DEFAULT NULL,
  `spkt_nobpkb` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spkt_waktu_bpkb` int(10) UNSIGNED DEFAULT NULL,
  `spkt_status` int(1) NOT NULL DEFAULT '0',
  `spkt_terima_nama` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `spkt_terima_alamat` text COLLATE utf8mb4_unicode_ci,
  `spkt_terima_tgl` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_spk_ttbj`
--

INSERT INTO `tb_spk_ttbj` (`spkt_id`, `spkt_spk`, `spkt_biro`, `spkt_tglfaktur`, `spkt_stck`, `spkt_ujitype`, `spkt_nopol`, `spkt_plat`, `spkt_notis`, `spkt_stnk`, `spkt_waktu_stnk`, `spkt_nobpkb`, `spkt_waktu_bpkb`, `spkt_status`, `spkt_terima_nama`, `spkt_terima_alamat`, `spkt_terima_tgl`, `created_at`, `updated_at`) VALUES
(3, '17-00385', 6, '2017-09-21', 'ini surat sementara', '2017-09-20', 'Bg 9093 Ui', 1, 1, 1, 0, 'ugit868hkh86uhu', 0, 1, NULL, NULL, NULL, '2017-09-20 13:12:47', '2017-09-20 13:12:47'),
(4, '17-00388', NULL, NULL, NULL, NULL, NULL, 0, 0, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, '2017-09-22 07:23:29', '2017-09-22 07:23:29');

-- --------------------------------------------------------

--
-- Table structure for table `tb_stockmove`
--

CREATE TABLE `tb_stockmove` (
  `sm_id` int(10) UNSIGNED NOT NULL,
  `sm_trk` int(11) NOT NULL,
  `sm_ekspedisi` int(11) DEFAULT NULL,
  `sm_lokasi` int(11) NOT NULL,
  `sm_tglout` date DEFAULT NULL,
  `sm_tglin` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_stockmove`
--

INSERT INTO `tb_stockmove` (`sm_id`, `sm_trk`, `sm_ekspedisi`, `sm_lokasi`, `sm_tglout`, `sm_tglin`, `created_at`, `updated_at`) VALUES
(1, 2, 2, 2, '2017-07-05', NULL, '2017-07-05 04:48:06', '2017-07-05 04:48:06'),
(4, 3, 2, 2, NULL, NULL, '2017-07-05 09:44:10', '2017-07-05 09:44:10');

-- --------------------------------------------------------

--
-- Table structure for table `tb_team`
--

CREATE TABLE `tb_team` (
  `team_id` int(10) UNSIGNED NOT NULL,
  `team_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `team_spv` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_team`
--

INSERT INTO `tb_team` (`team_id`, `team_nama`, `team_spv`, `created_at`, `updated_at`) VALUES
(1, 'Jaguar', 2, '2017-04-11 17:00:00', '2017-04-19 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_test`
--

CREATE TABLE `tb_test` (
  `id` int(11) NOT NULL,
  `val` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_test`
--

INSERT INTO `tb_test` (`id`, `val`) VALUES
(2, 'ok'),
(3, 'ok'),
(4, 'ok'),
(5, 'ok');

-- --------------------------------------------------------

--
-- Table structure for table `tb_tr_kendaraan`
--

CREATE TABLE `tb_tr_kendaraan` (
  `trk_id` int(11) NOT NULL,
  `trk_ref` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trk_vendor` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trk_invoice` bigint(20) DEFAULT NULL,
  `trk_tgl` date DEFAULT NULL,
  `trk_dh` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trk_indent` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trk_rrn` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trk_variantid` int(11) DEFAULT NULL,
  `trk_warna` int(11) DEFAULT NULL,
  `trk_tahun` int(10) UNSIGNED DEFAULT NULL,
  `trk_rangka` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trk_mesin` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trk_hrg_unit` bigint(20) DEFAULT NULL,
  `trk_hrg_diskon` bigint(20) DEFAULT NULL,
  `trk_hrg_pbm` bigint(20) DEFAULT NULL,
  `trk_hrg_dpp` bigint(20) DEFAULT NULL,
  `trk_hrg_ppn` bigint(20) DEFAULT NULL,
  `trk_hrg_total` bigint(20) DEFAULT NULL,
  `trk_tgl_tempo` datetime DEFAULT NULL,
  `trk_interest` bigint(20) DEFAULT NULL,
  `trk_promes` bigint(20) DEFAULT NULL,
  `trk_tgl_fisik` datetime DEFAULT NULL,
  `trk_ekspedisi` int(11) DEFAULT NULL,
  `trk_masuk` date DEFAULT NULL,
  `trk_keluar` date DEFAULT NULL,
  `trk_lokasi` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trk_encarid` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `trk_automatching` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_tr_kendaraan`
--

INSERT INTO `tb_tr_kendaraan` (`trk_id`, `trk_ref`, `trk_vendor`, `trk_invoice`, `trk_tgl`, `trk_dh`, `trk_indent`, `trk_rrn`, `trk_variantid`, `trk_warna`, `trk_tahun`, `trk_rangka`, `trk_mesin`, `trk_hrg_unit`, `trk_hrg_diskon`, `trk_hrg_pbm`, `trk_hrg_dpp`, `trk_hrg_ppn`, `trk_hrg_total`, `trk_tgl_tempo`, `trk_interest`, `trk_promes`, `trk_tgl_fisik`, `trk_ekspedisi`, `trk_masuk`, `trk_keluar`, `trk_lokasi`, `trk_encarid`, `trk_automatching`, `created_at`, `updated_at`) VALUES
(1, 'P-00002', 'PT. ASTRA INTERNATIONAL', 5751545017, '2017-08-02', '20170002', NULL, '5751545017', 8, 1, 2017, 'MHKV5EA1JHK027808', '1NRF315734', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-08-28 00:00:00', 1027776, 142627222, '2017-08-02 11:33:50', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(2, 'P-00003', 'PT. ASTRA INTERNATIONAL', 5751545016, '2017-08-02', '20170003', NULL, '5751545016', 9, 2, 2017, 'MHKV5EA2JHK025824', '1NRF313872', 133422018, 32694740, 9059800, 109787078, 10978708, 120765786, '2017-08-28 00:00:00', 876558, 121642344, '2017-08-02 11:33:49', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(3, 'P-00004', 'PT. ASTRA INTERNATIONAL', 5751545018, '2017-08-02', '20170004', NULL, '5751545018', 9, 3, 2017, 'MHKV5EA2JHK025967', '1NRF315144', 133422018, 32694740, 9059800, 109787078, 10978708, 120765786, '2017-08-28 00:00:00', 876558, 121642344, '2017-08-02 11:33:49', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(4, 'P-00006', 'PT. ASTRA INTERNATIONAL', 5751545019, '2017-08-02', '20170006', NULL, '5751545019', 4, 4, 2017, 'MHKS6GJ6JHJ026183', '3NRH158451', 110100000, 24411425, 0, 85688575, 8568858, 94257433, '2017-08-28 00:00:00', 684152, 94941585, '2017-08-02 11:33:48', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(5, 'P-00008', 'PT. ASTRA INTERNATIONAL', 5751545238, '2017-08-02', '20170008', NULL, '5751545238', 4, 5, 2017, 'MHKS6GJ6JHJ026214', '3NRH158808', 110100000, 6373624, 0, 103726376, 10372638, 114099014, '2017-08-28 00:00:00', 828169, 114927183, '2017-08-02 16:02:21', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(6, 'P-00009', 'PT. ASTRA INTERNATIONAL', 5751545239, '2017-08-02', '20170009', NULL, '5751545239', 4, 6, 2017, 'MHKS6GJ6JHJ026215', '3NRH158685', 110100000, 6373624, 0, 103726376, 10372638, 114099014, '2017-08-28 00:00:00', 828169, 114927183, '2017-08-02 16:02:21', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(7, 'P-00010', 'PT. ASTRA INTERNATIONAL', 5751545240, '2017-08-02', '201700010', NULL, '5751545240', 8, 7, 2017, 'MHKV5EA1JHK027793', '1NRF315611', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-08-28 00:00:00', 1027776, 142627222, '2017-08-02 16:02:20', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(8, 'P-00012', 'PT. ASTRA INTERNATIONAL', 5751544978, '2017-08-03', '201700012', NULL, '5751544978', 8, 2, 2017, 'MHKV5EA1JHK027852', '1NRF314784', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-08-29 00:00:00', 1027776, 142627222, '2017-08-03 14:07:04', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(9, 'P-00013', 'PT. ASTRA INTERNATIONAL', 5751544979, '2017-08-03', '201700013', NULL, '5751544979', 17, 8, 2017, 'MHKP3CA1JHK145445', '3SZDGH5973', 94686364, 16082135, 0, 78604229, 7860423, 86464652, '2017-08-29 00:00:00', 627589, 87092241, '2017-08-03 14:07:03', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(10, 'P-00014', 'PT. ASTRA INTERNATIONAL', 5751545426, '2017-08-03', '201700014', NULL, '5751545426', 2, 9, 2017, 'MHKG2CJ1JHK037019', '3SZDGH5800', 143829482, 30526007, 9508700, 122812175, 12281217, 135093392, '2017-08-29 00:00:00', 980553, 136073945, '2017-08-03 15:23:31', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(11, 'P-00015', 'PT. ASTRA INTERNATIONAL', 5751545427, '2017-08-03', '201700015', NULL, '5751545427', 17, 10, 2017, 'MHKP3CA1JHK145454', '3SZDGH6225', 94686364, 7965125, 0, 86721239, 8672124, 95393363, '2017-08-29 00:00:00', 692397, 96085760, '2017-08-03 15:23:30', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(12, 'P-00032', 'PT. ASTRA INTERNATIONAL', 5751545445, '2017-08-03', '201700032', NULL, '5751545445', 6, 11, 2017, 'MHKS6GJ3JHJ010562', '3NRH159488', 105100000, 6153641, 0, 98946359, 9894636, 108840995, '2017-08-29 00:00:00', 790004, 109630999, '2017-08-03 15:22:45', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(13, 'P-00034', 'PT. ASTRA INTERNATIONAL', 5751545669, '2017-08-04', '201700034', NULL, '5751545669', 6, 5, 2017, 'MHKS6GJ3JHJ010521', '3NRH158669', 105100000, 6153641, 0, 98946359, 9894636, 108840995, '2017-08-30 00:00:00', 790004, 109630999, '2017-08-05 15:02:45', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(14, 'P-00035', 'PT. ASTRA INTERNATIONAL', 5751545670, '2017-08-04', '201700035', NULL, '5751545670', 6, 11, 2017, 'MHKS6GJ3JHJ010523', '3NRH158890', 105100000, 6153641, 0, 98946359, 9894636, 108840995, '2017-08-30 00:00:00', 790004, 109630999, '2017-08-05 15:02:45', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(15, 'P-00036', 'PT. ASTRA INTERNATIONAL', 5751545671, '2017-08-04', '201700036', NULL, '5751545671', 6, 6, 2017, 'MHKS6GJ3JHJ010524', '3NRH158843', 105100000, 6153641, 0, 98946359, 9894636, 108840995, '2017-08-30 00:00:00', 790004, 109630999, '2017-08-05 15:02:02', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(16, 'P-00037', 'PT. ASTRA INTERNATIONAL', 5751545672, '2017-08-04', '201700037', NULL, '5751545672', 6, 11, 2017, 'MHKS6GJ3JHJ010527', '3NRH158944', 105100000, 6153641, 0, 98946359, 9894636, 108840995, '2017-08-30 00:00:00', 790004, 109630999, '2017-08-05 15:02:02', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(17, 'P-00038', 'PT. ASTRA INTERNATIONAL', 5751545673, '2017-08-04', '201700038', NULL, '5751545673', 6, 5, 2017, 'MHKS6GJ3JHJ010529', '3NRH158827', 105100000, 6153641, 0, 98946359, 9894636, 108840995, '2017-08-30 00:00:00', 790004, 109630999, '2017-08-05 15:02:01', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(18, 'P-00039', 'PT. ASTRA INTERNATIONAL', 5751545674, '2017-08-04', '201700039', NULL, '5751545674', 6, 12, 2017, 'MHKS6GJ3JHJ010531', '3NRH158820', 105100000, 6153641, 0, 98946359, 9894636, 108840995, '2017-08-30 00:00:00', 790004, 109630999, '2017-08-05 15:02:01', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(19, 'P-00042', 'PT. ASTRA INTERNATIONAL', 5751545678, '2017-08-04', '201700042', NULL, '5751545678', 4, 12, 2017, 'MHKS6GJ6JHJ026295', '3NRH159243', 110100000, 6373624, 0, 103726376, 10372638, 114099014, '2017-08-30 00:00:00', 828169, 114927183, '2017-08-05 15:01:59', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(20, 'P-00043', 'PT. ASTRA INTERNATIONAL', 5751545677, '2017-08-04', '201700043', NULL, '5751545677', 4, 4, 2017, 'MHKS6GJ6JHJ026742', '3NRH159002', 110100000, 6373624, 0, 103726376, 10372638, 114099014, '2017-08-30 00:00:00', 828169, 114927183, '2017-08-05 15:01:59', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(21, 'P-00044', 'PT. ASTRA INTERNATIONAL', 5751545679, '2017-08-04', '201700044', NULL, '5751545679', 6, 4, 2017, 'MHKS6GJ3JHJ010541', '3NRH159146', 105100000, 6153641, 0, 98946359, 9894636, 108840995, '2017-08-30 00:00:00', 790004, 109630999, '2017-08-05 15:01:59', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(22, 'P-00045', 'PT. ASTRA INTERNATIONAL', 5751545680, '2017-08-04', '201700045', NULL, '5751545680', 4, 4, 2017, 'MHKS6GJ6JHJ026744', '3NRH159029', 110100000, 6373624, 0, 103726376, 10372638, 114099014, '2017-08-30 00:00:00', 828169, 114927183, '2017-08-05 15:01:58', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(23, 'P-00048', 'PT. ASTRA INTERNATIONAL', 5751545683, '2017-08-04', '201700048', NULL, '5751545683', 4, 12, 2017, 'MHKS6GJ6JHJ026226', '3NRH158699', 110100000, 6373624, 0, 103726376, 10372638, 114099014, '2017-08-30 00:00:00', 828169, 114927183, '2017-08-05 15:00:35', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(24, 'P-00049', 'PT. ASTRA INTERNATIONAL', 5751545685, '2017-08-04', '201700049', NULL, '5751545685', 4, 6, 2017, 'MHKS6GJ6JHJ026217', '3NRH158660', 110100000, 6373624, 0, 103726376, 10372638, 114099014, '2017-08-30 00:00:00', 828169, 114927183, '2017-08-05 15:00:35', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(25, 'P-00050', 'PT. ASTRA INTERNATIONAL', 5751545684, '2017-08-04', '201700050', NULL, '5751545684', 4, 11, 2017, 'MHKS6GJ6JHJ026225', '3NRH158701', 110100000, 6373624, 0, 103726376, 10372638, 114099014, '2017-08-30 00:00:00', 828169, 114927183, '2017-08-05 15:00:35', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(26, 'P-00051', 'PT. ASTRA INTERNATIONAL', 5751545686, '2017-08-04', '201700051', NULL, '5751545686', 4, 5, 2017, 'MHKS6GJ6JHJ026218', '3NRH158686', 110100000, 6373624, 0, 103726376, 10372638, 114099014, '2017-08-30 00:00:00', 828169, 114927183, '2017-08-05 15:00:34', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(27, 'P-00052', 'PT. ASTRA INTERNATIONAL', 5751545687, '2017-08-04', '201700052', NULL, '5751545687', 4, 12, 2017, 'MHKS6GJ6JHJ026220', '3NRH158689', 110100000, 6373624, 0, 103726376, 10372638, 114099014, '2017-08-30 00:00:00', 828169, 114927183, '2017-08-05 15:00:34', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(28, 'P-00053', 'PT. ASTRA INTERNATIONAL', 5751545688, '2017-08-04', '201700053', NULL, '5751545688', 6, 4, 2017, 'MHKS6GJ3JHJ010520', '3NRH158926', 105100000, 6153641, 0, 98946359, 9894636, 108840995, '2017-08-30 00:00:00', 790004, 109630999, '2017-08-05 15:00:33', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(29, 'P-00056', 'PT. ASTRA INTERNATIONAL', 5751545690, '2017-08-04', '201700056', NULL, '5751545690', 6, 5, 2017, 'MHKS6GJ3JHJ010517', '3NRH158976', 105100000, 6153641, 0, 98946359, 9894636, 108840995, '2017-08-30 00:00:00', 790004, 109630999, '2017-08-04 14:44:48', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(30, 'P-00058', 'PT. ASTRA INTERNATIONAL', 5751545693, '2017-08-04', '201700058', NULL, '5751545693', 6, 6, 2017, 'MHKS6GJ3JHJ010514', '3NRH158932', 105100000, 6153641, 0, 98946359, 9894636, 108840995, '2017-08-30 00:00:00', 790004, 109630999, '2017-08-04 14:44:47', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(31, 'P-00063', 'PT. ASTRA INTERNATIONAL', 5751545698, '2017-08-04', '201700063', NULL, '5751545698', 4, 11, 2017, 'MHKS6GJ6JHJ026230', '3NRH158887', 110100000, 6373624, 0, 103726376, 10372638, 114099014, '2017-08-30 00:00:00', 828169, 114927183, '2017-08-04 14:44:45', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(32, 'P-00066', 'PT. ASTRA INTERNATIONAL', 5751546060, '2017-08-04', '201700066', NULL, '5751546060', 17, 13, 2017, 'MHKP3CA1JHK145503', '3SZDGH6095', 94686364, 7965125, 0, 86721239, 8672124, 95393363, '2017-08-30 00:00:00', 692397, 96085760, '2017-08-04 14:45:29', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(33, 'P-00067', 'PT. ASTRA INTERNATIONAL', 5751546061, '2017-08-04', '201700067', NULL, '5751546061', 9, 2, 2017, 'MHKV5EA2JHK026031', '1NRF316118', 133422018, 10147488, 9059800, 132334330, 13233433, 145567763, '2017-08-30 00:00:00', 1056579, 146624342, '2017-08-04 14:45:28', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(34, 'P-00068', 'PT. ASTRA INTERNATIONAL', 5751546062, '2017-08-04', '201700068', NULL, '5751546062', 7, 12, 2017, 'MHKS6DJ2JHJ006166', '1KRA415446', 95872727, 5629607, 0, 90243120, 9024312, 99267432, '2017-08-30 00:00:00', 720516, 99987948, '2017-08-04 14:45:28', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(35, 'P-00070', 'PT. ASTRA INTERNATIONAL', 5751546064, '2017-08-04', '201700070', NULL, '5751546064', 8, 14, 2017, 'MHKV5EA1JHK023905', '1NRF279633', 130058354, 10118685, 8787100, 128726769, 12872677, 141599446, '2017-08-30 00:00:00', 1027776, 142627222, '2017-08-04 14:45:27', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(36, 'P-00072', 'PT. ASTRA INTERNATIONAL', 5751546066, '2017-08-04', '201700072', NULL, '5751546066', 8, 14, 2017, 'MHKV5EA1JHK022689', '1NRF265819', 129969854, 10118685, 8875600, 128726769, 12872677, 141599446, '2017-08-30 00:00:00', 1027776, 142627222, '2017-08-04 14:45:25', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(37, 'P-00073', 'PT. ASTRA INTERNATIONAL', 5751546067, '2017-08-04', '201700073', NULL, '5751546067', 9, 1, 2017, 'MHKV5EA2JHK026037', '1NRF316250', 133422018, 10147488, 9059800, 132334330, 13233433, 145567763, '2017-08-30 00:00:00', 1056579, 146624342, '2017-08-04 14:45:25', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(38, 'P-00075', 'PT. ASTRA INTERNATIONAL', 5751546069, '2017-08-04', '201700075', NULL, '5751546069', 17, 8, 2017, 'MHKP3CA1JHK145525', '3SZDGH6267', 94686364, 7965125, 0, 86721239, 8672124, 95393363, '2017-08-30 00:00:00', 692397, 96085760, '2017-08-04 14:45:24', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(39, 'P-00077', 'PT. ASTRA INTERNATIONAL', 5751546453, '2017-08-07', '201700077', NULL, '5751546453', 8, 15, 2017, 'MHKV5EA1JHK027965', '1NRF316798', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-02 00:00:00', 1027776, 142627222, '2017-08-07 13:00:56', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:15', NULL),
(40, 'P-00078', 'PT. ASTRA INTERNATIONAL', 5751546454, '2017-08-07', '201700078', NULL, '5751546454', 8, 15, 2017, 'MHKV5EA1JHK027961', '1NRF316797', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-02 00:00:00', 1027776, 142627222, '2017-08-07 13:00:56', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(41, 'P-00080', 'PT. ASTRA INTERNATIONAL', 5751546456, '2017-08-07', '201700080', NULL, '5751546456', 8, 15, 2017, 'MHKV5EA1JHK027958', '1NRF316534', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-02 00:00:00', 1027776, 142627222, '2017-08-07 13:00:51', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(42, 'P-00081', 'PT. ASTRA INTERNATIONAL', 5751546458, '2017-08-07', '201700081', NULL, '5751546458', 8, 15, 2017, 'MHKV5EA1JHK027950', '1NRF316557', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-02 00:00:00', 1027776, 142627222, '2017-08-07 13:00:50', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(43, 'P-00082', 'PT. ASTRA INTERNATIONAL', 5751546457, '2017-08-07', '201700082', NULL, '5751546457', 8, 15, 2017, 'MHKV5EA1JHK027953', '1NRF316487', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-02 00:00:00', 1027776, 142627222, '2017-08-07 13:00:49', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(44, 'P-00083', 'PT. ASTRA INTERNATIONAL', 5751546574, '2017-08-07', '201700083', NULL, '5751546574', 8, 3, 2017, 'MHKV5EA1JHK027994', '1NRF317185', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-02 00:00:00', 1027776, 142627222, '2017-08-07 16:25:03', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(45, 'P-00084', 'PT. ASTRA INTERNATIONAL', 5751546180, '2017-08-08', '201700084', NULL, '5751546180', 2, 16, 2017, 'MHKG2CJ1JHK037004', '3SZDGH5313', 143829481, 15193876, 9508700, 138144305, 13814431, 151958736, '2017-09-03 00:00:00', 1102967, 153061703, '2017-08-08 11:05:56', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(46, 'P-00085', 'PT. ASTRA INTERNATIONAL', 5751546181, '2017-08-08', '201700085', NULL, '5751546181', 18, 17, 2017, 'MHKT3BA1JHK040723', 'K3MH04156', 84595455, 7885195, 0, 76710260, 7671026, 84381286, '2017-09-03 00:00:00', 612468, 84993754, '2017-08-08 11:05:56', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(47, 'P-00086', 'PT. ASTRA INTERNATIONAL', 5751546182, '2017-08-08', '201700086', NULL, '5751546182', 2, 16, 2017, 'MHKG2CJ1JHK037001', '3SZDGH5187', 143829482, 7527811, 9508700, 145810371, 14581037, 160391408, '2017-09-03 00:00:00', 1164174, 161555582, '2017-08-08 11:05:55', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(48, 'P-00087', 'PT. ASTRA INTERNATIONAL', 5751546183, '2017-08-08', '201700087', NULL, '5751546183', 17, 13, 2017, 'MHKP3CA1JHK145688', '3SZDGH6712', 94686364, 7965125, 0, 86721239, 8672124, 95393363, '2017-09-03 00:00:00', 692397, 96085760, '2017-08-08 11:05:55', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(49, 'P-00089', 'PT. ASTRA INTERNATIONAL', 5751546185, '2017-08-08', '201700089', NULL, '5751546185', 8, 3, 2017, 'MHKV5EA1JHK027987', '1NRF317106', 130058454, 29960266, 8787000, 108885188, 10888519, 119773707, '2017-09-03 00:00:00', 869357, 120643064, '2017-08-08 11:05:54', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(50, 'P-00090', 'PT. ASTRA INTERNATIONAL', 5751546849, '2017-08-08', '201700090', NULL, '5751546849', 8, 2, 2017, 'MHKV5EA1JHK027876', '1NRF316014', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-03 00:00:00', 1027776, 142627222, '2017-08-09 10:33:11', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(51, 'P-00091', 'PT. ASTRA INTERNATIONAL', 5751546848, '2017-08-08', '201700091', NULL, '5751546848', 8, 2, 2017, 'MHKV5EA1JHK027879', '1NRF316184', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-03 00:00:00', 1027776, 142627222, '2017-08-09 10:33:11', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(52, 'P-00092', 'PT. ASTRA INTERNATIONAL', 5751546847, '2017-08-08', '201700092', NULL, '5751546847', 8, 2, 2017, 'MHKV5EA1JHK027892', '1NRF316330', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-03 00:00:00', 1027776, 142627222, '2017-08-09 10:33:11', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(53, 'P-00093', 'PT. ASTRA INTERNATIONAL', 5751546845, '2017-08-08', '201700093', NULL, '5751546845', 8, 2, 2017, 'MHKV5EA1JHK027895', '1NRF315049', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-03 00:00:00', 1027776, 142627222, '2017-08-09 10:33:10', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(54, 'P-00094', 'PT. ASTRA INTERNATIONAL', 5751546846, '2017-08-08', '201700094', NULL, '5751546846', 3, 2, 2017, 'MHKV5EA2JHK026153', '1NRF317081', 153694746, 11660903, 9059800, 151093643, 15109364, 166203007, '2017-09-03 00:00:00', 1206357, 167409364, '2017-08-09 10:33:10', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(55, 'P-00098', 'PT. ASTRA INTERNATIONAL', 5751547073, '2017-08-09', '201700098', NULL, '5751547073', 8, 2, 2017, 'MHKV5EA1JHK028035', '1NRF317716', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-04 00:00:00', 1027776, 142627222, '2017-08-09 16:32:53', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(56, 'P-00103', 'PT. ASTRA INTERNATIONAL', 5751547366, '2017-08-09', '2017000103', NULL, '5751547366', 7, 12, 2017, 'MHKS6DJ2JHJ006301', '1KRA416058', 95872727, 5629607, 0, 90243120, 9024312, 99267432, '2017-09-04 00:00:00', 720516, 99987948, '2017-08-09 16:32:50', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(57, 'P-00104', 'PT. ASTRA INTERNATIONAL', 5751547367, '2017-08-09', '2017000104', NULL, '5751547367', 3, 15, 2017, 'MHKV5EA2JHK026247', '1NRF318270', 153694746, 11660903, 9059800, 151093643, 15109364, 166203007, '2017-09-04 00:00:00', 1206357, 167409364, '2017-08-09 16:32:50', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(58, 'P-00108', 'PT. ASTRA INTERNATIONAL', 5751547653, '2017-08-10', '2017000108', NULL, '5751547653', 7, 11, 2017, 'MHKS6DJ2JHJ006355', '1KRA416335', 95872727, 5629607, 0, 90243120, 9024312, 99267432, '2017-09-05 00:00:00', 720516, 99987948, '2017-08-10 15:01:09', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(59, 'P-00109', 'PT. ASTRA INTERNATIONAL', 5751547655, '2017-08-10', '2017000109', NULL, '5751547655', 17, 10, 2017, 'MHKP3CA1JHK145801', '3SZDGH7168', 94686364, 7965125, 0, 86721239, 8672124, 95393363, '2017-09-05 00:00:00', 692397, 96085760, '2017-08-10 15:01:09', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(60, 'P-00111', 'PT. ASTRA INTERNATIONAL', 5751547656, '2017-08-10', '2017000111', NULL, '5751547656', 8, 15, 2017, 'MHKV5EA1JHK028188', '1NRF318614', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-05 00:00:00', 1027776, 142627222, '2017-08-10 15:01:08', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(61, 'P-00115', 'PT. ASTRA INTERNATIONAL', 5751548085, '2017-08-11', '2017000115', NULL, '5751548085', 8, 3, 2017, 'MHKV5EA1JHK028207', '1NRF318766', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-06 00:00:00', 1027776, 142627222, '2017-08-11 10:53:02', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(62, 'P-00116', 'PT. ASTRA INTERNATIONAL', 5751548086, '2017-08-11', '2017000116', NULL, '5751548086', 8, 15, 2017, 'MHKV5EA1JHK028224', '1NRF318840', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-06 00:00:00', 1027776, 142627222, '2017-08-11 10:53:02', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(63, 'P-00117', 'PT. ASTRA INTERNATIONAL', 5751548087, '2017-08-11', '2017000117', NULL, '5751548087', 8, 15, 2017, 'MHKV5EA1JHK028227', '1NRF319058', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-06 00:00:00', 1027776, 142627222, '2017-08-11 10:53:02', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL),
(64, 'P-00118', 'PT. ASTRA INTERNATIONAL', 5751548088, '2017-08-11', '2017000118', NULL, '5751548088', 8, 15, 2017, 'MHKV5EA1JHK028231', '1NRF317984', 130058454, 10118685, 8787000, 128726769, 12872677, 141599446, '2017-09-06 00:00:00', 1027776, 142627222, '2017-08-11 10:53:01', NULL, NULL, NULL, NULL, NULL, 1, '2017-09-19 22:58:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_type`
--

CREATE TABLE `tb_type` (
  `type_id` int(10) UNSIGNED NOT NULL,
  `type_nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_poin` int(11) NOT NULL,
  `type_status` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_type`
--

INSERT INTO `tb_type` (`type_id`, `type_nama`, `type_poin`, `type_status`, `created_at`, `updated_at`) VALUES
(2, 'XENIA', 9, 1, NULL, NULL),
(3, 'TERIOS', 7, 1, NULL, NULL),
(4, 'GM PU 1.5', 5, 1, NULL, NULL),
(5, 'SIGRA', 9, 1, NULL, NULL),
(6, 'AYLA', 7, 1, NULL, NULL),
(7, 'HI-MAX', 6, 1, NULL, NULL),
(8, 'GM PU 1.3', 8, 1, NULL, NULL),
(9, 'GM FAN 1.3', 10, 1, NULL, NULL),
(10, 'GM FAN 1.5', 8, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_users`
--

CREATE TABLE `tb_users` (
  `nip` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_users`
--

INSERT INTO `tb_users` (`nip`, `nama`, `password`, `created_at`, `updated_at`) VALUES
('101', 'Admin', '$2y$10$DcG6hy6UHlyhyvUCaDFL3.L3LU8halUxVBDghJ.KWHnbexTUvV8Ki', '2017-04-10 02:13:00', '2017-04-10 02:13:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_variant`
--

CREATE TABLE `tb_variant` (
  `variant_id` int(11) NOT NULL,
  `variant_serial` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `variant_type` int(11) NOT NULL,
  `variant_nama` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `variant_ket` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `variant_off` bigint(20) NOT NULL DEFAULT '0',
  `variant_on` bigint(20) NOT NULL DEFAULT '0',
  `variant_bbn` bigint(20) NOT NULL DEFAULT '0',
  `variant_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_variant`
--

INSERT INTO `tb_variant` (`variant_id`, `variant_serial`, `variant_type`, `variant_nama`, `variant_ket`, `variant_off`, `variant_on`, `variant_bbn`, `variant_status`, `created_at`, `updated_at`) VALUES
(1, 'F650RV-GMDFJ', 2, 'FMC M MT 1.0', 'XENIA FMC M MT 1.0', 0, 108900000, 2100000, 1, '2017-04-17 15:19:33', '2017-08-10 20:40:52'),
(2, 'F700RGGMRFJE', 3, 'X MT EXTRA', 'TERIOS X MT EXTRA', 242000000, 244000000, 2000000, 1, '2017-07-19 14:45:27', '2017-08-10 20:40:22'),
(3, 'F653MD-R-SPT', 2, 'R MT 1.3 SPORTY', 'GREAT NEW XENIA R MT 1.3 SPORTY', 0, 204100000, 2000000, 1, '2017-07-19 16:12:59', '2017-08-10 20:42:12'),
(4, 'B401RS-GMZFJ', 5, '1.2 R MT', 'SIGRA 1.2 R MT', 182750000, 184750000, 2000000, 1, '2017-07-19 16:14:25', '2017-08-10 20:38:22'),
(5, 'B401RS-GQZFJ', 5, '1.2 R AT', 'SIGRA 1.2 R AT', 0, 132650000, 2000000, 1, '2017-07-19 16:15:39', '2017-08-10 20:38:06'),
(6, 'B401RS-GMQFJ', 5, '1.2 X MT', 'SIGRA 1.2 X MT', 0, 132650000, 1500000, 1, '2017-07-19 16:17:20', '2017-08-10 20:38:49'),
(7, 'B400RS-GMDEJ', 5, '1.0 M MT', 'SIGRA 1.0 M MT', 0, 117800000, 1000000, 1, '2017-07-19 16:19:16', '2017-08-10 20:37:17'),
(8, 'F653MR-X', 2, 'X MT 1.3 STD', 'GREAT NEW XENIA X MT 1.3 STD', 0, 195700000, 23000000, 1, '2017-07-19 16:20:21', '2017-08-10 20:42:51'),
(9, 'F653MD-R', 2, 'R MT 1.3', 'GREAT NEW XENIA R MT 1.3', 164750000, 184750000, 20000000, 1, '2017-07-19 16:21:59', '2017-08-10 20:41:54'),
(10, 'F700RGGMRFJO', 3, 'X AT EXTRA', 'TERIOS X AT EXTRA', 0, 216000000, 23000000, 1, '2017-07-19 16:25:36', '2017-08-10 20:40:13'),
(11, 'F700RGGMRFJS', 3, 'STD MT EXTRA', 'TERIOS STD MT EXTRA', 0, 205500000, 20000000, 1, '2017-07-19 16:26:46', '2017-08-10 20:39:59'),
(12, 'F700RGGMRWJS', 3, 'STD AT EXTRA', 'TERIOS STD AT EXTRA', 0, 235000000, 23000000, 1, '2017-07-19 16:27:35', '2017-08-10 20:39:33'),
(13, 'F700RGGADFJS', 3, 'STD MT', 'TERIOS STD MT', 0, 195200000, 21000000, 1, '2017-07-19 16:28:05', '2017-08-10 20:39:46'),
(14, 'F700RGGFGFJS', 3, 'STD AT', 'TERIOS STD AT', 0, 216000000, 21000000, 1, '2017-07-19 16:28:42', '2017-08-10 20:39:23'),
(15, 'F653AR-X', 2, 'X AT 1.3 STD', 'GREAT NEW XENIA X AT 1.3 STD', 0, 191300000, 17000000, 1, '2017-07-19 16:29:27', '2017-08-10 20:42:27'),
(16, 'F653AD-R', 2, 'R AT 1.3', 'GREAT NEW XENIA R AT 1.3', 0, 195000000, 18000000, 1, '2017-07-19 16:30:47', '2017-08-10 20:41:23'),
(17, 'GMRP-PMRFJJ-KJFH', 4, 'AC PS 1.5 FH', 'GRAN MAX PU AC PS 1.5 FH', 0, 146300000, 17000000, 1, '2017-07-19 16:31:52', '2017-08-10 20:36:21'),
(18, 'GMRP-TMREJJ-HCFH', 8, '1.3 3W FH', 'GRAND MAX PU 1.3 3W FH', 0, 134600000, 13000000, 1, '2017-07-19 16:36:18', '2017-08-10 20:36:06'),
(19, 'GMRF-PMRFJJ-KJFH', 10, 'AC PS 1.5 FH', 'GRAN MAX PU AC PS 1.5 FH', 0, 119900000, 17000000, 1, '2017-07-19 16:42:50', '2017-08-10 20:35:38'),
(20, 'GMRF-TMREJJ-HCFH', 9, '1.3 3W FH', 'GRAND MAX FAN 1.3 3W FH', 0, 115200000, 17000000, 1, '2017-07-19 16:43:24', '2017-08-10 20:35:19'),
(21, 'AYLA-GMDFJ-PN', 6, '1.2 M MT MI', 'AYLA 1.2 M MT MI', 0, 104750000, 12000000, 1, '2017-07-19 16:45:57', '2017-08-10 20:33:02'),
(22, 'AYLA-GADFJ-PN', 6, '1.2 M AT MI', 'AYLA 1.2 M AT MI', 0, 125450000, 11000000, 1, '2017-07-19 16:52:05', '2017-08-10 20:33:53'),
(23, 'AYLA-GADFJ-LK', 6, '1.0 M AT MI', 'AYLA 1.0 M AT MI', 0, 118050000, 20000000, 1, '2017-07-19 16:55:29', '2017-08-10 20:33:22'),
(24, 'AYLA-GADFJ-GK', 6, '1.0 M MT MI', 'AYLA 1.0 M MT MI', 0, 92550000, 10000000, 1, '2017-07-19 16:57:15', '2017-08-10 20:32:32'),
(25, 'AYLA-GMDFJ-S', 6, '1.2 M MT SPORTY', 'AYLA 1.2 M MT SPORTY', 0, 125250000, 10000000, 1, '2017-07-19 16:58:27', '2017-08-10 20:31:53'),
(26, 'AYLA-GMDFJ-S', 6, '1.2 M AT SPORTY', 'AYLA 1.2 M AT SPORTY', 0, 123750000, 10000000, 1, '2017-07-19 16:59:48', '2017-08-10 20:34:15'),
(27, 'S501RP-PMRFJ-YD', 7, 'PU 1.0 STD', 'HI MAX PU 1.0 STD', 0, 96550000, 0, 1, '2017-07-19 17:00:36', '2017-08-10 20:36:56'),
(28, 'S501RP-PMRFJ-BH', 7, 'PU 1.0 STD AC PS', 'HI MAX PU 1.0 STD AC PS', 0, 104550000, 0, 1, '2017-07-19 17:01:05', '2017-08-10 20:36:49');

-- --------------------------------------------------------

--
-- Table structure for table `tb_vendor`
--

CREATE TABLE `tb_vendor` (
  `vendor_id` int(10) UNSIGNED NOT NULL,
  `vendor_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor_alamat` text COLLATE utf8mb4_unicode_ci,
  `vendor_kota` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor_kodepos` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendor_notelp` varchar(13) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendor_email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_vendor`
--

INSERT INTO `tb_vendor` (`vendor_id`, `vendor_nama`, `vendor_alamat`, `vendor_kota`, `vendor_kodepos`, `vendor_notelp`, `vendor_email`, `created_at`, `updated_at`) VALUES
(3, 'PT. Encar Daihatsu', 'Jakarta Pusat', 'Jakarta', '30112', '218119119', 'encardaihatsu@daihatsu.com', '2017-08-11 02:50:49', '2017-08-11 03:26:09'),
(4, 'PT. Karya Siber Indonesia', 'Palembang Pusat', 'Palembang', '30163', '711877721', 'karyasiber@gmail.com', '2017-08-11 03:26:49', '2017-08-11 03:26:49');

-- --------------------------------------------------------

--
-- Table structure for table `tb_vendoraks`
--

CREATE TABLE `tb_vendoraks` (
  `vendorAks_id` int(10) UNSIGNED NOT NULL,
  `vendorAks_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendorAks_alamat` text COLLATE utf8mb4_unicode_ci,
  `vendorAks_kota` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendorAks_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `vendorAks_email` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendorAks_kodepos` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vendorAks_status` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_vendoraks`
--

INSERT INTO `tb_vendoraks` (`vendorAks_id`, `vendorAks_nama`, `vendorAks_alamat`, `vendorAks_kota`, `vendorAks_telp`, `vendorAks_email`, `vendorAks_kodepos`, `vendorAks_status`, `created_at`, `updated_at`) VALUES
(2, 'PT. AKSESORIS INDONESIA', 'Jl. Panjaitan kedua', 'Jakarta', '021999921', 'support@aksesoris.co.id', '100022', 1, '2017-08-11 03:28:24', '2017-08-11 03:28:24'),
(3, 'PT. NOTIFIKASI AKSESORIS', 'Jl. KMS ALI no.23', 'Palembang', '0711877727', 'cs@notifikasi.co.id', '30990', 1, '2017-08-11 03:30:36', '2017-08-11 03:30:36');

-- --------------------------------------------------------

--
-- Table structure for table `tb_warna`
--

CREATE TABLE `tb_warna` (
  `warna_id` int(10) UNSIGNED NOT NULL,
  `warna_nama` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `warna_type` int(11) NOT NULL,
  `warna_status` int(11) DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tb_warna`
--

INSERT INTO `tb_warna` (`warna_id`, `warna_nama`, `warna_type`, `warna_status`, `created_at`, `updated_at`) VALUES
(1, 'SCARLET RED METALLIC DSO', 2, 1, '2017-09-20 10:58:18', NULL),
(2, 'BLACK METALIC DSO', 2, 1, '2017-09-20 10:58:18', NULL),
(3, 'WHITE DSO', 2, 1, '2017-09-20 10:58:18', NULL),
(4, 'WHITE DSO', 5, 1, '2017-09-20 10:58:19', NULL),
(5, 'CLASSIC SILVER DSO', 5, 1, '2017-09-20 10:58:19', NULL),
(6, 'ROCK GREY METALLIC DSO', 5, 1, '2017-09-20 10:58:19', NULL),
(7, 'ROCK GREY METALLIC DSO', 2, 1, '2017-09-20 10:58:19', NULL),
(8, 'ROCK GREY METALLIC DSO', 4, 1, '2017-09-20 10:58:19', NULL),
(9, 'CLASSIC SILVER DSO', 3, 1, '2017-09-20 10:58:19', NULL),
(10, 'ULTRA BLACK DSO', 4, 1, '2017-09-20 10:58:19', NULL),
(11, 'ULTRA BLACK SOLID DSO', 5, 1, '2017-09-20 10:58:19', NULL),
(12, 'RED SOLID DSO', 5, 1, '2017-09-20 10:58:19', NULL),
(13, 'WHITE DSO', 4, 1, '2017-09-20 10:58:19', NULL),
(14, 'DARK BROWN METALLIC DSO', 2, 1, '2017-09-20 10:58:19', NULL),
(15, 'CLASSIC SILVER DSO', 2, 1, '2017-09-20 10:58:19', NULL),
(16, 'WHITE DSO', 3, 1, '2017-09-20 10:58:19', NULL),
(17, 'ULTRA BLACK DSO', 8, 1, '2017-09-20 10:58:19', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_bayar`
-- (See below for the actual view)
--
CREATE TABLE `vw_bayar` (
`bayar_spk` varchar(8)
,`bayar` decimal(41,0)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_diskon`
-- (See below for the actual view)
--
CREATE TABLE `vw_diskon` (
`diskon_spk` varchar(8)
,`diskon` bigint(20)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `vw_stock_kendaraan`
-- (See below for the actual view)
--
CREATE TABLE `vw_stock_kendaraan` (
`trk_id` int(11)
,`trk_ref` varchar(10)
,`trk_vendor` varchar(50)
,`trk_invoice` bigint(20)
,`trk_tgl` date
,`trk_dh` varchar(10)
,`trk_indent` varchar(15)
,`trk_rrn` varchar(30)
,`trk_variantid` int(11)
,`trk_warna` int(11)
,`trk_tahun` int(10) unsigned
,`trk_rangka` varchar(25)
,`trk_mesin` varchar(25)
,`trk_hrg_dpp` bigint(20)
,`trk_ekspedisi` int(11)
,`trk_masuk` date
,`trk_lokasi` varchar(50)
,`trk_encarid` varchar(20)
,`trk_automatching` int(11)
,`created_at` timestamp
,`updated_at` timestamp
,`warna_nama` varchar(50)
);

-- --------------------------------------------------------

--
-- Structure for view `vw_bayar`
--
DROP TABLE IF EXISTS `vw_bayar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_bayar`  AS  select `tb_spk`.`spk_id` AS `bayar_spk`,if(isnull(sum(`tb_spk_pembayaran`.`spkp_jumlah`)),0,sum(`tb_spk_pembayaran`.`spkp_jumlah`)) AS `bayar` from (`tb_spk` left join `tb_spk_pembayaran` on((`tb_spk`.`spk_id` = `tb_spk_pembayaran`.`spkp_spk`))) group by `tb_spk`.`spk_id` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_diskon`
--
DROP TABLE IF EXISTS `vw_diskon`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_diskon`  AS  select `tb_spk_diskon`.`spkd_spk` AS `diskon_spk`,`tb_spk_diskon`.`spkd_cashback` AS `diskon` from (`tb_spk_diskon` left join `tb_spk_aksesoris` on((`tb_spk_diskon`.`spkd_id` = `tb_spk_aksesoris`.`spka_diskon`))) where (`tb_spk_diskon`.`spkd_status` = 1) group by `tb_spk_aksesoris`.`spka_diskon`,`tb_spk_diskon`.`spkd_spk`,`tb_spk_diskon`.`spkd_cashback` ;

-- --------------------------------------------------------

--
-- Structure for view `vw_stock_kendaraan`
--
DROP TABLE IF EXISTS `vw_stock_kendaraan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vw_stock_kendaraan`  AS  select `tb_tr_kendaraan`.`trk_id` AS `trk_id`,`tb_tr_kendaraan`.`trk_ref` AS `trk_ref`,`tb_tr_kendaraan`.`trk_vendor` AS `trk_vendor`,`tb_tr_kendaraan`.`trk_invoice` AS `trk_invoice`,`tb_tr_kendaraan`.`trk_tgl` AS `trk_tgl`,`tb_tr_kendaraan`.`trk_dh` AS `trk_dh`,`tb_tr_kendaraan`.`trk_indent` AS `trk_indent`,`tb_tr_kendaraan`.`trk_rrn` AS `trk_rrn`,`tb_tr_kendaraan`.`trk_variantid` AS `trk_variantid`,`tb_tr_kendaraan`.`trk_warna` AS `trk_warna`,`tb_tr_kendaraan`.`trk_tahun` AS `trk_tahun`,`tb_tr_kendaraan`.`trk_rangka` AS `trk_rangka`,`tb_tr_kendaraan`.`trk_mesin` AS `trk_mesin`,`tb_tr_kendaraan`.`trk_hrg_dpp` AS `trk_hrg_dpp`,`tb_tr_kendaraan`.`trk_ekspedisi` AS `trk_ekspedisi`,`tb_tr_kendaraan`.`trk_masuk` AS `trk_masuk`,`tb_tr_kendaraan`.`trk_lokasi` AS `trk_lokasi`,`tb_tr_kendaraan`.`trk_encarid` AS `trk_encarid`,`tb_tr_kendaraan`.`trk_automatching` AS `trk_automatching`,`tb_tr_kendaraan`.`created_at` AS `created_at`,`tb_tr_kendaraan`.`updated_at` AS `updated_at`,`tb_warna`.`warna_nama` AS `warna_nama` from ((`tb_tr_kendaraan` left join `tb_spk` on((`tb_spk`.`spk_dh` = `tb_tr_kendaraan`.`trk_dh`))) join `tb_warna` on((`tb_tr_kendaraan`.`trk_warna` = `tb_warna`.`warna_id`))) where isnull(`tb_spk`.`spk_dh`) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_aksesoris`
--
ALTER TABLE `tb_aksesoris`
  ADD PRIMARY KEY (`aksesoris_id`);

--
-- Indexes for table `tb_akun`
--
ALTER TABLE `tb_akun`
  ADD PRIMARY KEY (`akun_id`);

--
-- Indexes for table `tb_akun_induk`
--
ALTER TABLE `tb_akun_induk`
  ADD PRIMARY KEY (`induk_id`);

--
-- Indexes for table `tb_asuransi`
--
ALTER TABLE `tb_asuransi`
  ADD PRIMARY KEY (`asuransi_id`);

--
-- Indexes for table `tb_asuransi_jenis`
--
ALTER TABLE `tb_asuransi_jenis`
  ADD PRIMARY KEY (`ajenis_id`);

--
-- Indexes for table `tb_bank`
--
ALTER TABLE `tb_bank`
  ADD PRIMARY KEY (`bank_id`);

--
-- Indexes for table `tb_bank_kategori`
--
ALTER TABLE `tb_bank_kategori`
  ADD PRIMARY KEY (`kategori_id`);

--
-- Indexes for table `tb_biro`
--
ALTER TABLE `tb_biro`
  ADD PRIMARY KEY (`biro_id`);

--
-- Indexes for table `tb_cmo`
--
ALTER TABLE `tb_cmo`
  ADD PRIMARY KEY (`cmo_id`);

--
-- Indexes for table `tb_config`
--
ALTER TABLE `tb_config`
  ADD PRIMARY KEY (`config_id`);

--
-- Indexes for table `tb_ekspedisi`
--
ALTER TABLE `tb_ekspedisi`
  ADD PRIMARY KEY (`ekspedisi_id`);

--
-- Indexes for table `tb_gudang`
--
ALTER TABLE `tb_gudang`
  ADD PRIMARY KEY (`gudang_id`);

--
-- Indexes for table `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  ADD PRIMARY KEY (`jabatan_id`);

--
-- Indexes for table `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  ADD PRIMARY KEY (`karyawan_id`);

--
-- Indexes for table `tb_konfirmasi_pembayaran`
--
ALTER TABLE `tb_konfirmasi_pembayaran`
  ADD PRIMARY KEY (`konfirmasi_id`);

--
-- Indexes for table `tb_kota`
--
ALTER TABLE `tb_kota`
  ADD PRIMARY KEY (`kota_id`);

--
-- Indexes for table `tb_leasing`
--
ALTER TABLE `tb_leasing`
  ADD PRIMARY KEY (`leasing_id`);

--
-- Indexes for table `tb_leasing_bayar`
--
ALTER TABLE `tb_leasing_bayar`
  ADD PRIMARY KEY (`lbayar_id`);

--
-- Indexes for table `tb_notif_activity`
--
ALTER TABLE `tb_notif_activity`
  ADD PRIMARY KEY (`na_id`);

--
-- Indexes for table `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  ADD PRIMARY KEY (`pel_id`);

--
-- Indexes for table `tb_provinsi`
--
ALTER TABLE `tb_provinsi`
  ADD PRIMARY KEY (`provinsi_id`);

--
-- Indexes for table `tb_referral`
--
ALTER TABLE `tb_referral`
  ADD PRIMARY KEY (`referral_id`);

--
-- Indexes for table `tb_sales`
--
ALTER TABLE `tb_sales`
  ADD PRIMARY KEY (`sales_id`);

--
-- Indexes for table `tb_sales_uid`
--
ALTER TABLE `tb_sales_uid`
  ADD PRIMARY KEY (`salesUid_id`);

--
-- Indexes for table `tb_spk`
--
ALTER TABLE `tb_spk`
  ADD PRIMARY KEY (`spk_id`);

--
-- Indexes for table `tb_spk_aksesoris`
--
ALTER TABLE `tb_spk_aksesoris`
  ADD PRIMARY KEY (`spka_id`);

--
-- Indexes for table `tb_spk_diskon`
--
ALTER TABLE `tb_spk_diskon`
  ADD PRIMARY KEY (`spkd_id`);

--
-- Indexes for table `tb_spk_faktur`
--
ALTER TABLE `tb_spk_faktur`
  ADD PRIMARY KEY (`spkf_id`);

--
-- Indexes for table `tb_spk_leasing`
--
ALTER TABLE `tb_spk_leasing`
  ADD PRIMARY KEY (`spkl_id`);

--
-- Indexes for table `tb_spk_no`
--
ALTER TABLE `tb_spk_no`
  ADD PRIMARY KEY (`spkNo_id`);

--
-- Indexes for table `tb_spk_pembayaran`
--
ALTER TABLE `tb_spk_pembayaran`
  ADD PRIMARY KEY (`spkp_id`);

--
-- Indexes for table `tb_spk_referral`
--
ALTER TABLE `tb_spk_referral`
  ADD PRIMARY KEY (`spkr_id`);

--
-- Indexes for table `tb_spk_ttbj`
--
ALTER TABLE `tb_spk_ttbj`
  ADD PRIMARY KEY (`spkt_id`);

--
-- Indexes for table `tb_stockmove`
--
ALTER TABLE `tb_stockmove`
  ADD PRIMARY KEY (`sm_id`);

--
-- Indexes for table `tb_team`
--
ALTER TABLE `tb_team`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexes for table `tb_test`
--
ALTER TABLE `tb_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_tr_kendaraan`
--
ALTER TABLE `tb_tr_kendaraan`
  ADD PRIMARY KEY (`trk_id`);

--
-- Indexes for table `tb_type`
--
ALTER TABLE `tb_type`
  ADD PRIMARY KEY (`type_id`);

--
-- Indexes for table `tb_users`
--
ALTER TABLE `tb_users`
  ADD PRIMARY KEY (`nip`);

--
-- Indexes for table `tb_variant`
--
ALTER TABLE `tb_variant`
  ADD PRIMARY KEY (`variant_id`);

--
-- Indexes for table `tb_vendor`
--
ALTER TABLE `tb_vendor`
  ADD PRIMARY KEY (`vendor_id`);

--
-- Indexes for table `tb_vendoraks`
--
ALTER TABLE `tb_vendoraks`
  ADD PRIMARY KEY (`vendorAks_id`);

--
-- Indexes for table `tb_warna`
--
ALTER TABLE `tb_warna`
  ADD PRIMARY KEY (`warna_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `tb_aksesoris`
--
ALTER TABLE `tb_aksesoris`
  MODIFY `aksesoris_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `tb_asuransi`
--
ALTER TABLE `tb_asuransi`
  MODIFY `asuransi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tb_asuransi_jenis`
--
ALTER TABLE `tb_asuransi_jenis`
  MODIFY `ajenis_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_bank`
--
ALTER TABLE `tb_bank`
  MODIFY `bank_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_bank_kategori`
--
ALTER TABLE `tb_bank_kategori`
  MODIFY `kategori_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_biro`
--
ALTER TABLE `tb_biro`
  MODIFY `biro_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_cmo`
--
ALTER TABLE `tb_cmo`
  MODIFY `cmo_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_config`
--
ALTER TABLE `tb_config`
  MODIFY `config_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tb_ekspedisi`
--
ALTER TABLE `tb_ekspedisi`
  MODIFY `ekspedisi_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_gudang`
--
ALTER TABLE `tb_gudang`
  MODIFY `gudang_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_jabatan`
--
ALTER TABLE `tb_jabatan`
  MODIFY `jabatan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_karyawan`
--
ALTER TABLE `tb_karyawan`
  MODIFY `karyawan_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_konfirmasi_pembayaran`
--
ALTER TABLE `tb_konfirmasi_pembayaran`
  MODIFY `konfirmasi_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_leasing`
--
ALTER TABLE `tb_leasing`
  MODIFY `leasing_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tb_leasing_bayar`
--
ALTER TABLE `tb_leasing_bayar`
  MODIFY `lbayar_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tb_notif_activity`
--
ALTER TABLE `tb_notif_activity`
  MODIFY `na_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_pelanggan`
--
ALTER TABLE `tb_pelanggan`
  MODIFY `pel_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `tb_referral`
--
ALTER TABLE `tb_referral`
  MODIFY `referral_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_sales`
--
ALTER TABLE `tb_sales`
  MODIFY `sales_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `tb_sales_uid`
--
ALTER TABLE `tb_sales_uid`
  MODIFY `salesUid_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_spk_aksesoris`
--
ALTER TABLE `tb_spk_aksesoris`
  MODIFY `spka_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_spk_diskon`
--
ALTER TABLE `tb_spk_diskon`
  MODIFY `spkd_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_spk_leasing`
--
ALTER TABLE `tb_spk_leasing`
  MODIFY `spkl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_spk_no`
--
ALTER TABLE `tb_spk_no`
  MODIFY `spkNo_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=400;
--
-- AUTO_INCREMENT for table `tb_spk_referral`
--
ALTER TABLE `tb_spk_referral`
  MODIFY `spkr_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_spk_ttbj`
--
ALTER TABLE `tb_spk_ttbj`
  MODIFY `spkt_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_stockmove`
--
ALTER TABLE `tb_stockmove`
  MODIFY `sm_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_team`
--
ALTER TABLE `tb_team`
  MODIFY `team_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_test`
--
ALTER TABLE `tb_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_tr_kendaraan`
--
ALTER TABLE `tb_tr_kendaraan`
  MODIFY `trk_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;
--
-- AUTO_INCREMENT for table `tb_type`
--
ALTER TABLE `tb_type`
  MODIFY `type_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_variant`
--
ALTER TABLE `tb_variant`
  MODIFY `variant_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `tb_vendor`
--
ALTER TABLE `tb_vendor`
  MODIFY `vendor_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_vendoraks`
--
ALTER TABLE `tb_vendoraks`
  MODIFY `vendorAks_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tb_warna`
--
ALTER TABLE `tb_warna`
  MODIFY `warna_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
