<style>
	body{
		margin:0;
		padding:0;
		font-size:14px;
		/*font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;*/
	}
	td, th{
		font-size:14px;
	}
	h5{
		margin: 5px 0 20px;
		font-weight:400;
	}
	.text-center{
		text-align:center;
	}
	.text-right{
		text-align:right;
	}
	table{
		width:100%;		
	}
	table, td, th, tr{
		border:0;
		border-spacing: 0;
		border-color:#fff;
		padding:0;
		border-collapse: collapse;
		vertical-align:top;
	}
	table th{
		border-top:1px solid #ddd;
		border-left:1px solid #ddd;
		border-right:1px solid #ddd;
		border-bottom:4px solid #ddd;
		padding:3px 7px;
		vertical-align:middle;
		background:#f5f5f5;
		border-spacing: 0;
		border-collapse: collapse;
	}
	
	table tfoot th{
		border-bottom:1px solid #ddd;
		border-top:4px solid #ddd;
	}
	
	.table td{
		padding:3px 7px;
		border-top:1px solid #ddd;
		border-left:1px solid #ddd;
		border-right:1px solid #ddd;
		border-bottom:1px solid #ddd;		
	}

	.bold{
		font-weight:bold;
	}

</style>

<page>
	<body style="margin: 50px; font-family: arial;">
		<div class="text-center" style="margin-bottom: 20px;"><b><u><h3>SURAT PERNYATAAN</h3></u></b></div>
		<p style="margin-bottom: 10px;">Yang bertanda tangan dibawah ini :</p>
		<table style="margin-bottom: 20px;">
			<tr>
				<td style="width: 120px;">Nama</td>
				<td style="width: 5px;"> : &nbsp;&nbsp;</td>
				<td> {{ $data->spk_sales_nama }} </td>
			</tr>
			<tr>
				<td>Jabatan</td>
				<td> : &nbsp;&nbsp;</td>
				<td> {{ $data->spk_sales_jabatan }} </td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td> : &nbsp;&nbsp;</td>
				<td> Jl. Angkatan 45 Palembang</td>
			</tr>				
		</table>
		<br>
		<div style="margin-bottom: 10px;">Dengan ini menyatakan bahwa BPKB (Buku Pemilik Kendaraan Bermotor) dan Faktur serta dokumen lainnya untuk kendaraan bermotor sbb: </div>
		<table style="margin-bottom: 20px;">
			<tr>
				<td style="width: 120px;">Merk / Type</td>
				<td style="width: 5px;"> : &nbsp;&nbsp;</td>
				<td> {{ $data->spk_type }} / {{ $data->spk_variant }} </td>
			</tr>
			<tr>
				<td>Rangka</td>
				<td> : &nbsp;&nbsp;</td>
				<td> {{ $data->spk_rangka }} </td>
			</tr>
			<tr>
				<td>Mesin</td>
				<td> : &nbsp;&nbsp;</td>
				<td> {{ $data->spk_no_mesin }} </td>
			</tr>
			<tr>
				<td>Warna</td>
				<td> : &nbsp;&nbsp;</td>
				<td> {{ $data->spk_warna }} </td>
			</tr>
			<tr>
				<td>Tahun/CC</td>
				<td> : &nbsp;&nbsp;</td>
				<td> {{ $data->spk_tahun }} / 1500 </td>
			</tr>	
			<tr>
				<td>Nama BPKB</td>
				<td> : &nbsp;&nbsp;</td>
				<td> {{ $data->spk_stnk_nama }} </td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td> : &nbsp;&nbsp;</td>
				<td> {{ $data->spk_stnk_alamat }} </td>
			</tr>					
		</table>
		<br><br>
		<p style="text-align: justify;">
			Surat-surat kendaraan tersebut (BPKB, FAKTUR, NOTICE PAJAK, NIK) masih dalam proses pengurusan di Kantor SAMSAT oleh kami sebagai penjual kendaraan bermotor dan apabila BPKB serta dokumen-dokumen yang dimaksud selesai akan segera kami serahkan langsung kepada {{ $data->spk_leasing_nama }}, selambat-lambatnya 3 ( <b>Tiga</b> ) bulan sejak surat pernyataan ini dibuat.
		<br>
		<br>
			Dan surat penyataan BPKB ini wajib dikembalikan kepada PT. ENCARTHA INDONESIA pada saat serah terima BPKB.
		<br>
		<br>
			Selanjutnya dengan ini kami nyatakan bahwa di samping BPKB dan dokumen-dokumen dimaksud tidak akan kami berikan kepada siapapun / pihak manapun dengan dalih dan alsan apapun, kecuali kepada {{ $data->spk_leasing_nama }}, Cab. {{ $data->spk_leasing_kota }}.
		<br>
		<br>
			Demikianlah surat penyataan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.
		</p>
		<br>
		<table style="text-align: center; margin-left: 500px;">
			<tr>
				<td>
					Palembang, 16 Januari 2017
					<br>
					<br>
					<br>
					<br>
					<b><u>{{ $data->spk_sales_nama }}</u></b>
					<br>
					<b>{{ $data->spk_sales_jabatan }}</b>
				</td>
			</tr>
		</table>
	</body>
</page>