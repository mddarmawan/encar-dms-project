@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Penjualan</small>
            Data Aksesoris
        </h6>
        <ul class="header-tools right" style="margin-left: 20px">
            <li><a href="#vendorAks" class="chip"> Data Vendor Aksesoris</a></li>
        </ul>
    </div>

<div id="data_grid" class="wrapper">
        <div class="nav-wrapper">
            
            <div class="nav-right"> 
                <form action="" method="get" style="display: inline-block;">
                    <button type="submit" class="waves-effect waves-light btn-flat btn-filter" style="margin:10px;"><i class="fa fa-refresh" aria-hidden="true" style="font-size: 15px;"></i> &nbsp;Refresh</button>
                </form>
                <a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
            </div>

            

        </div>

        <div id="dataAksesoris">

        </div>  

</div>

<div id="vendorAks" class="modal" style="width:1000px;">  
    <h6 class="modal-title blue-grey darken-1">
        Data Vendor Aksesoris
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:0;position: relative;">
        @include("modules.vendorAks.data")  
    </div>
</div>


<script>

    $("#filter").click(function(e){
        e.preventDefault();
        loadData();
    });

    $("#reset").click(function(e){
        e.preventDefault();
        $("#filter_awal").val("");
        $("#filter_akhir").val("");
        status="";
        $(".tab li").removeClass("active");
        $(".tab li a[data-id='']").parent().addClass("active");
        loadData();
    });


    $("#export").click('click', function (event) {
        var args = [$('#dataAksesoris'), 'DATA_AKSESORIS_{{ date('dmY') }}.xls'];   
        exportTableToExcel.apply(this, args);
    });

    function vendor() {
    $.ajax({
        type: "GET",
        url: "{{url('api/vendorAks')}}"
    }).done(function(vendorAks) {
        vendorAks.unshift({ vendorAks_id: "0", vendorAks_nama: "" });

    function type() {
    $.ajax({
        type: "GET",
        url: "{{url('api/kendaraan/type')}}"
    }).done(function(type) {
        type.unshift({ type_id: "0", type_nama: "" });

        var db = {
           loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/aksesoris')}}",
                    data: filter
                });
            },

            insertItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "POST",
                    url: "{{url('/api/aksesoris')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                    alert(response);
                }).done(function(response){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/aksesoris/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].aksesoris_id] = {
                                aksesoris_id: item[i].aksesoris_kode,
                                aksesoris_hapus: item[i].aksesoris_hapus,
                                aksesoris_type: item[i].aksesoris_kendaraan,
                                aksesoris_nama : item[i].aksesoris_nama,
                                aksesoris_harga : item[i].aksesoris_harga
                            };
                        }
                        var db = firebase.database().ref("data/tb_aksesoris/");
                        db.set(data);
                    })
                });
            },

            updateItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/aksesoris')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/aksesoris/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].aksesoris_id] = {
                                aksesoris_id: item[i].aksesoris_kode,
                                aksesoris_hapus: item[i].aksesoris_hapus,
                                aksesoris_type: item[i].aksesoris_kendaraan,
                                aksesoris_nama : item[i].aksesoris_nama,
                                aksesoris_harga : item[i].aksesoris_harga
                            };
                        }
                        var db = firebase.database().ref("data/tb_aksesoris/");
                        db.set(data);
                    });
                });
            },

            deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/aksesoris')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(response){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/aksesoris/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].aksesoris_id] = {
                                aksesoris_id: item[i].aksesoris_kode,
                                aksesoris_hapus: item[i].aksesoris_hapus,
                                aksesoris_type: item[i].aksesoris_kendaraan,
                                aksesoris_nama : item[i].aksesoris_nama,
                                aksesoris_harga : item[i].aksesoris_harga
                            };
                        }
                        var db = firebase.database().ref("data/tb_aksesoris/");
                        db.set(data);
                    });                  
                });
            }
        };

   db.status = [
            {
                "status_id": "",
                "status_nama": "",     
            },
            {
                "status_id": 0,
                "status_nama": "<span class='box-span red'>NON. ACTIVED</span>",          
            },
            {
                "status_id": 1,
                "status_nama": "<span class='box-span orange'>ACTIVED</span>",           
            },
            
        ];

    $("#dataAksesoris").jsGrid({
        height: "calc(100% - 40px)",
        width: "100%",
        filtering: true,
        editing: true,
        inserting: true,
        sorting: true,
        autoload: true,
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
        onItemEditing: function(args){
            var harga = args.item.aksesoris_harga; 
            args.item.aksesoris_harga= string_format(harga);
        },
        controller: db,
 
        fields: [
                { name: "aksesoris_status", title:"Status", type: "select", width: 80, items: db.status, valueField: "status_id", textField: "status_nama", align:"center", filtering:false, editing: false,  },
                { name: "aksesoris_kode", title:"Kode Aksesoris", type: "text", width: 80, validate: "required", align:"center" },
                { name: "aksesoris_nama", title:"Nama Aksesoris", type: "text", width: 200, validate: "required", align:"" },
                { name: "aksesoris_kendaraan", title:"Type Kendaraan", type: "select", items: type, valueField: "type_id", textField: "type_nama", width: 70, align:"left" },
                { name: "aksesoris_harga", title:"Harga", type: "text", width: 70, validate: "required" , align:"right"},
                { name: "aksesoris_vendor", title:"Vendor", type: "select", items: vendorAks, valueField: "vendorAks_id", textField: "vendorAks_nama", width: 100, align:"left", validate:  { message: "Vendor harus diisi !", validator: function(value) { return value > 0; } }},
                { type: "control", width:70 }
            ]
        });
      });
 
};

type();
    });
 
};
vendor();
$(".tab li a").click(function(e){
    e.preventDefault();

    status = $(this).data("id");
    
    loadData();
    $(".tab li").removeClass("active");
    $(this).parent().addClass("active");
});
</script>

@endsection