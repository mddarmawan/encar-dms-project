<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Pelanggan;

class ApiPelangganController extends Controller
{
    function index(){
    	$data = DB::table('tb_pelanggan')
                -> leftjoin('tb_sales','tb_pelanggan.pel_sales', '=','tb_sales.sales_id')
                -> leftjoin('tb_karyawan','tb_sales.sales_karyawan', '=','tb_karyawan.karyawan_id')
                -> get();

    	$result = array();
        foreach($data as $r){
            $item = array();
            $item['pel_id'] = $r->pel_id;
            $item['pel_nama'] = $r->pel_nama;
            $item['pel_alamat'] = $r->pel_alamat;
            $item['pel_lahir'] = date_format(date_create($r->pel_lahir),"d/m/Y");
            $item['pel_kota'] = $r->pel_kota;
            $item['pel_pos'] = $r->pel_pos;
            $item['pel_telp'] = $r->pel_telp;
            $item['pel_ponsel'] = $r->pel_ponsel;
            $item['pel_email'] = $r->pel_email;
            $item['pel_sales'] = $r->karyawan_nama;
            $item['pel_daftar'] = date_format(date_create($r->created_at),"d/m/Y");
            
            if((!request("pel_id") || strrpos(strtolower($item['pel_id']), strtolower(request("pel_id"))) > -1) &&
                (!request("pel_daftar") || strrpos(strtolower($item['pel_daftar']), strtolower(request("pel_daftar"))) > -1) &&
                (!request("pel_nama") || strrpos(strtolower($item['pel_nama']), strtolower(request("pel_nama"))) > -1) &&
                (!request("pel_lahir") || strrpos(strtolower($item['pel_lahir']), strtolower(request("pel_lahir"))) > -1) &&
                (!request("pel_kota") || strrpos(strtolower($item['pel_kota']), strtolower(request("pel_kota"))) > -1) &&
                (!request("pel_pos") || strrpos(strtolower($item['pel_pos']), strtolower(request("pel_pos"))) > -1) &&
                (!request("pel_telp") || strrpos(strtolower($item['pel_telp']), strtolower(request("pel_telp"))) > -1) &&
                (!request("pel_ponsel") || strrpos(strtolower($item['pel_ponsel']), strtolower(request("pel_ponsel"))) > -1) &&
                 (!request("pel_email") || strrpos(strtolower($item['pel_email']), strtolower(request("pel_email"))) > -1) &&
                 (!request("pel_sales") || strtolower($item['pel_sales'])== strtolower(request("pel_sales")))) {

                $tgl = strtotime(str_replace("/","-",$item['pel_daftar']));
                if (request("filter_awal") && request("filter_akhir")){
                    $filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
                    $filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
                    if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
                        array_push($result, $item);                     
                    }
                }else if (request("filter_awal")){
                    $filter_awal = strtotime(request("filter_awal"));
                    if ($filter_awal<=$tgl){
                        array_push($result, $item);                     
                    }
                }else if (request("filter_akhir")){
                    $filter_akhir = strtotime(request("filter_akhir"));
                    if ($filter_akhir>=$tgl){
                        array_push($result, $item);                     
                    }
                }else{
                    array_push($result, $item);
                }
            }
        }

    	return json_encode($result);
    }

    function destroy(){
		return Pelanggan::where('pel_id', request("pel_id"))->delete();
    }

     function detail($id){
        $data['pemesan'] = DB::table('tb_pelanggan')->where("pel_id",$id)->first();
        $data['spk'] =DB::table('tb_spk')
            ->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
            ->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
            ->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
            ->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
            ->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
            ->leftjoin('tb_leasing', 'tb_spk.spk_leasing', '=', 'tb_leasing.leasing_id')
            ->where("spk_id",$id)
            ->get();


        return json_encode($data);
    }
}
