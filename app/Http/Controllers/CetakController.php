<?php

namespace App\Http\Controllers;

require_once('mpdf/vendor/autoload.php');

use Illuminate\Http\Request;
use Response;
use mPDF;
use App\Http\Controllers\ApiLaporanController;

class CetakController extends Controller
{
	public function index(){
		$mpdf = new mPDF();

		//load the view and saved it into $html variable
        $html = view('modules.cetak.f_kendaraan');
 
        //PDF filename that user will get to download
        $pdfFilePath = "Invoice RentalMoCar.pdf";
 
       //generate the PDF from html
        $mpdf->WriteHTML($html);
		$mpdf->debug = true;
 
        //download PDF
		$mpdf->Output($pdfFilePath, "D");
	}

	public function cetak($aksi, $id){
	    /**
	     * Create a new mPDF instance.
	     *
	     * @param class
	     */
		$mpdf = new mPDF();

	    /**
	     * Load the required data and store it to an array.
	     *
	     * @param json
	     * @param array
	     */
	    $data = json_decode((new ApiLaporanController)->$aksi($id));
		$data = $data[0];

	    /**
	     * load the view and saved it into $html variable.
	     *
	     * @param view
	     */
        $html = view('modules.cetak.'.$aksi, compact('data'));
 
	    /**
	     * PDF filename that user will get to download.
	     *
	     * @param string
	     */
        $pdfFilePath = $aksi.'_'.date('h:i:s').'.pdf';
 
	    /**
	     * Generate the PDF from html.
	     *
	     * @return void
	     */
        $mpdf->WriteHTML($html);
 
	    /**
	     * Download the PDF file.
	     *
	     * @return file
	     */
		$mpdf->Output($pdfFilePath, "D");
	}

	public function t_tagihan_leasing($id){
		$mpdf = new mPDF();
	    $data = json_decode((new ApiLaporanController)->t_tagihan_leasing($id));
		$data = $data[0];
        $html = view('modules.cetak.t_tagihan_leasing', compact('data'));
        $pdfFilePath = 't_tagihan_leasing_'.$id.'.pdf';
        $mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "D");
	}

	public function t_kwitansi_dp($id){
		try {
			$mpdf = new mPDF();
		    $data = json_decode((new ApiLaporanController)->t_pembayaran_leasing($id));
			$data = $data[0];
	        $html = view('modules.cetak.t_kwitansi', compact('data'));
	        $pdfFilePath = 'kwitansi_dp_'.$id.'.pdf';
	        $mpdf->WriteHTML($html);
			$mpdf->Output($pdfFilePath, "D");
		} catch (Exception $e) {
			echo 'Error: ' . $e;
		}
	}

	public function t_kwitansi_lunas($id){
		$mpdf = new mPDF();
	    $data = json_decode((new ApiLaporanController)->t_pembayaran_leasing($id));
		$data = $data[1];
        $html = view('modules.cetak.t_kwitansi', compact('data'));
        $pdfFilePath = 'kwitansi_lunas_'.$id.'.pdf';
        $mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "D");
	}

	public function t_terima_bpkb($id){
		$mpdf = new mPDF();
	    $data = json_decode((new ApiLaporanController)->t_terima_bpkb($id));
		$data = $data[1];
        $html = view('modules.cetak.t_terima', compact('data'));
        $pdfFilePath = 'tanda_terima_bpkb_'.$id.'.pdf';
        $mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "D");
	}

	public function t_pembayaran($id){
		$mpdf = new mPDF();
	    $data =json_decode((new ApiLaporanController)->t_kwitansi_pembayaran($id));
	    $data = $data[0];
        $html = view('modules.cetak.t_kwitansi_pembayaran', compact('data'));
        $pdfFilePath = 'Kwitansi_Pembayaran_'.$id.'.pdf';
        $mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "D");
	}

	public function f_BAST($id){
		$mpdf = new mPDF();
	    $data =json_decode((new ApiLaporanController)->f_BAST($id));
	    $data = $data[0];
        $html = view('modules.cetak.f_BAST', compact('data'));
        $pdfFilePath = 'Faktur_BAST_'.$id.'.pdf';
        $mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "D");
	}

	public function f_kendaraan($id){
		$mpdf = new mPDF();
	    $data =json_decode((new ApiLaporanController)->f_kendaraan($id));
	    $data = $data[0];
        $html = view('modules.cetak.f_kendaraan', compact('data'));
        $pdfFilePath = 'Faktur_Kendaraan_'.$id.'.pdf';
        $mpdf->WriteHTML($html);
		$mpdf->Output($pdfFilePath, "D");
	}
}