function date_format(mdate){
	var d = new Date(mdate);
	var day = d.getDate();
	var month = d.getMonth();
	var year = d.getFullYear();
	
	if (day <10){
		day = "0" + day;
	}
	
	if (month <10){
		month = "0" + month;
	}
	
	return day + "/" + month + "/" + year;
}