<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use DateTime;

class ApiFirebaseController extends Controller
{
    /**
     * Get the Table data.
     *
     * @param mixed
     * @return \Illuminate\Auth\Access\Response
     *
     */
	public function getTableData($table){
		return json_encode(DB::table($table)->get());
	}

    /**
     * Get the Sales data.
     *
     * @param mixed
     * @return \Illuminate\Auth\Access\Response
     *
     */
	public function getSales($uid = null){
		if (is_null($uid)){
			$data = DB::table('tb_sales')->select("sales_id","sales_salesUid","team_nama","karyawan_nama")
				->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
				->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
				->whereNotNull("sales_salesUid")
				->where("sales_salesUid","!=","")
				->get();
		}else{
			$data = DB::table('tb_sales')->select("sales_id","sales_salesUid","team_nama","karyawan_nama")
				->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
				->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
				->where("sales_salesUid", $uid)
				->first();
		}
		
		if (count($data)>0){
			return json_encode($data);
		}

		return FALSE;		
	}

    /**
     * Get the UID of Sales data.
     *
     * @param mixed
     * @return \Illuminate\Auth\Access\Response
     *
     */
	public function getSalesUid($spk = null) {
		if (is_null($spk)) {
			$data = DB::table('tb_spk')
				->select("sales_salesUid","spk_id")
				->join('tb_sales', 'tb_sales.sales_id', '=', 'tb_spk.spk_sales')
				->where('tb_sales.sales_salesUid', '!=', "")
				->Where('tb_sales.sales_salesUid', '!=', NULL)
				->get();

			$items = array();
			foreach ($data as $r) {
				$item['salesUid_sales'] = $r->sales_salesUid;
				$item['salesUid_spk'] = $r->spk_id;

				array_push($items, $item);
			}
		} else {
			$data = DB::table('tb_spk')
				->select("sales_salesUid")
				->join('tb_sales', 'tb_sales.sales_id', '=', 'tb_spk.spk_sales')
				->where('spk_id', $spk)
				->where('tb_sales.sales_salesUid', '!=', "")
				->Where('tb_sales.sales_salesUid', '!=', NULL)
				->first();
			$items = array();
			$items['salesUid_sales'] = $data->sales_salesUid;
			$items['salesUid_spk'] = $spk;
		}

		if (count($items)>0){
			return json_encode($items);
		}

		return FALSE;
	}

    /**
     * Store SPK's data.
     *
     * @return \Illuminate\Auth\Access\Response
     *
     */
	public function storeSPK() {
		$proses = 0;
		$item = $_POST;
		$item["created_at"] = date("Y-m-d H:i:s");
		
		unset($item['_token']);
		unset($item['spk_hapus']);
		unset($item['spk_sales_nama']);
		unset($item['spk_tim']);
		unset($item['spk_tim_id']);

		if (!isset($item['spk_pel_nama'])){
			return DB::table('tb_spk')-> where('spk_id', request('spk_id'))->update([
				"spk_status"			=> request('spk_status'),
				"spk_ket_cancel"		 =>  request("spk_ket_cancel"),
				"spk_catt_cancel"		 =>  request("spk_catt_cancel"),
			]);	
		}

		if ($item['spk_pembayaran'] == 0) {
			$item['spk_leasing'] = NULL;
		}

		$foldername = 'public/data/identitas/';

		foreach ($item as $key => $value) {
			if (strpos($key, '_pel_') !== false) {
				$column = str_replace("spk_", "", $key);
				$pelanggan[$column] = $value;
			}
		}

		if ($item['spk_pel_kategori'] == 0) {
			$source = $item['spk_pel_lahir'];
			$date = DateTime::createFromFormat('d/m/Y', $source);

			$item['spk_pel_lahir'] = $date->format('Y-m-d');
			$pelanggan['pel_lahir'] = $date->format('Y-m-d');
		} else {
			$pelanggan['pel_lahir'] = NULL;
		}

		if($item['spk_pel_fotoid']!=""){
			$spk_pel_fotoid = md5(date('Y-m-d H:i:s')) . '.jpg';
			if(Storage::append($foldername . $spk_pel_fotoid, base64_decode($item['spk_pel_fotoid']))){				
				$pelanggan['pel_fotoid'] = $spk_pel_fotoid;
				$item['spk_pel_fotoid'] = $spk_pel_fotoid;
			}
		}
		if($item['spk_stnk_fotoid']!=""){
			$spk_stnk_fotoid = md5(date('Y-m-d H:i:s')) . '.jpg';
			if(Storage::append($foldername . $spk_stnk_fotoid, base64_decode($item['spk_stnk_fotoid']))){
				$item['spk_stnk_fotoid'] = $spk_stnk_fotoid;
			}
		}


		$result = DB::table('tb_pelanggan')->where("pel_identitas", $item["spk_pel_identitas"])->where("pel_npwp", $item["spk_pel_npwp"])->get();
		if (count($result) == 0){
			$insert = DB::table('tb_pelanggan')->insert($pelanggan);
		} else {
			$insert = DB::table('tb_pelanggan')->where("pel_identitas", $item["spk_pel_identitas"])->update($pelanggan);
		}

		$result = DB::table('tb_pelanggan')->where("pel_identitas", $item["spk_pel_identitas"])->where("pel_npwp", $item["spk_pel_npwp"])->select("pel_id")->first();
		if (count($result) > 0){
			$item['spk_pel_id'] = $result->pel_id;
		}

		$result =  DB::table('tb_spk')->where("spk_id", $item["spk_id"])->get();
		if (count($result) == 0){
			$insert = DB::table('tb_spk')->insert($item);
		} else {
			$insert = DB::table('tb_spk')-> where('spk_id', request('spk_id'))->where("spk_status",95)->update($item);
		}

		$result =  DB::table('tb_spk')->where("spk_id", $item["spk_id"])->get();
		if (count($result) > 0){
			$proses = 1;
		} else {
			$proses = 0;
		}

		return $proses;
	}


    /**
     * Store Discount's data.
     *
     * @param mixed
     * @return \Illuminate\Auth\Access\Response
     *
     */
	public function storeDiskon() {
		$proses = 0;
		$item = $_POST;
		$item["created_at"] = date("Y-m-d H:i:s");

		if (!empty($item['aksesoris'])) {
			foreach ($item['aksesoris'] as $key => $value) {
				$allAksesoris[$key] = $value;
			}
		}

		unset($item['_token']);
		unset($item['aksesoris']);
		unset($item['diskon_hapus']);
		unset($item['diskon_sales_nama']);
		unset($item['diskon_tim']);
		unset($item['diskon_tim_id']);
		
		foreach ($item as $key => $value) {
			$column = str_replace("diskon_", "spkd_", $key);
			$diskon[$column] = $value;
		}

		$diskon['spkd_komisi'] = (int) $diskon['spkd_komisi'];

		$result =  DB::table('tb_spk_diskon')-> where("spkd_spk", $diskon["spkd_spk"])->get();
		if (count($result) == 0){
			$insert = DB::table('tb_spk_diskon')->insert($diskon);
		} else {
			$insert = DB::table('tb_spk_diskon')-> where("spkd_spk", $diskon["spkd_spk"])->update($diskon);
		}

		$diskon_id = DB::table('tb_spk_diskon')->where("spkd_spk", $diskon["spkd_spk"])->first();

		if (!empty($item['aksesoris'])) {
			foreach ($allAksesoris as $a) {
				$aksesoris = array(
					"spka_diskon"	=> $diskon_id->spkd_id,
					"spka_kode"		=> $a['diskona_aksesoris'],
					"spka_nama"		=> $a['diskona_nama'],
					"spka_harga"	=> $a['diskona_harga'],
					"created_at"	=> date('Y-m-d H:i:s'),
					"updated_at"	=> date('Y-m-d H:i:s')
				);

				$result =  DB::table('tb_spk_aksesoris')
					->where("spka_kode", $a['diskona_aksesoris'])
					->where("spka_diskon", $diskon_id->spkd_id)
					->get();
				if (count($result) == 0){
					$insert = DB::table('tb_spk_aksesoris')->insert($aksesoris);
				} else {
					$insert = DB::table('tb_spk_aksesoris')
					->where("spka_kode", $a['diskona_aksesoris'])
					->where("spka_diskon", $diskon_id->spkd_id)
					->update($aksesoris);
				}
			}
		}

		$result =  DB::table('tb_spk_diskon')-> where("spkd_spk", $diskon["spkd_spk"])->get();
		if (count($result) > 0){
			// $proses = 1;
			return "BERHASIL!";
		} else {
			// $proses = 0;
			// return $insert;
			return "GAGAL!!";
		}

		return $proses;
	}


	public function storePembayaran() {
		$proses = 0;
		$item = $_POST;

		$spk = $item['spk'];
		unset($item['_token']);
		unset($item['spk']);

		$foldername = 'public/data/bukti/';
		
		$err = 0;
		foreach ($item as $key => $value) {
			$konfirmasi = array();

			foreach ($value as $itemKey => $itemValue) {
				$column = str_replace("konfirm_", "konfirmasi_", $itemKey);
				$konfirmasi[$column] = $itemValue;
			}
			$konfirmasi['konfirmasi_spk'] = $spk;
			$konfirmasi['konfirmasi_tgl'] = $key;

			if($konfirmasi['konfirmasi_bukti']!=""){
				$konfirmasi_bukti = md5(date('Y-m-d H:i:s')) . '.jpg';
				if(Storage::append($foldername . $konfirmasi_bukti, base64_decode($konfirmasi['konfirmasi_bukti']))){				
					$konfirmasi['konfirmasi_bukti'] = $konfirmasi_bukti;
				}else{
					unset($konfirmasi['konfirmasi_bukti']);
				}
			}

			if(!DB::table('tb_konfirmasi_pembayaran')->insert($konfirmasi)){
				$err++;
			}
		}
		if ($err>0){
			return 0;
		}else{
			return 1;
		}
	}

    /**
     * Store Sales data.
     *
     * @param mixed
     * @return \Illuminate\Auth\Access\Response
     *
     */
	public function sales_input(){
		$validator = Validator::make(request()->all(), [
			"sales_Uid"		=> "required",
			"sales_id"		=> "required",
		]);

		$proses['result'] = false;
		$proses['msg'] = "";

		if ($validator->fails()) {
			$proses['msg'] =  $validator->messages();

		} else {
			if (DB::table('tb_sales')-> where("sales_Uid",request("sales_Uid"))->first()) {
				$proses['result'] = DB::table('tb_sales')-> where("sales_Uid",request("sales_Uid"))->update([
					"sales_id"	=>  request("akun_id"),
					"sales_usr"	=>  request("akun_nama"),
				]);

				$proses['result'] = 1;  
			} else {
				$proses['result'] = DB::table('tb_sales')->insert([
					"sales_Uid"	 =>  request("sales_Uid"),
					"sales_id"	=>  request("akun_id"),
					"sales_usr"	=>  request("akun_nama"),
				]); 

				$proses['result'] = 1;	   
			}

		}
		
		return json_encode($proses);
	}

    /**
     * Store Sales UID data.
     *
     * @param mixed
     * @return \Illuminate\Auth\Access\Response
     *
     */
	public function salesUid_input(){
		$this->validate(request(), [
			"salesUid_id"		=> "required",
			"salesUid_spk"		=> "required",
			"salesUid_spkd"		=> "required",
		]);

		
		$insert = array(
			"salesUid_id"	 =>  request("salesUid_id"),
			"salesUid_spk"	 =>  request("salesUid_spk"),
			"salesUid_spkd"   =>  request("salesUid_spkd")
			);

		$id = DB::table('tb_sales_Uid')->insertGetId($insert, 'salesUid_id');
		return json_encode(DB::table('tb_sales_Uid')->where("salesUid_id", $id)->first());
	}

    /**
     * Get Discount data.
     *
     * @param mixed
     * @return \Illuminate\Auth\Access\Response
     *
     */
	public function app(){
		$this->validate(request(), [
			"diskon_id"	  	=> "required",
			"diskon_status"	  => "required"
		]);

		
		$insert = array(
			 "spkd_cashback"	 	=>  request("diskon_cashback"),
			"spkd_komisi"	 		=>  request("diskon_komisi"),
			"spkd_status"	 		=>  request("diskon_status"),
			"spkd_catatan"	 		=>  request("diskon_catatan"),
		);

		$id = DB::table('tb_spk_aksesoris')-> insertGetId($insert,'diskon_id');
		return json_encode(DB::table('tb_spk_aksesoris') -> where("diskon_id", $id)->first());

	 }
}

