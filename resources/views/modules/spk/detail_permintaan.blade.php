<div id="detail_permintaan" class="modal" style="width:1180px; overflow-x: hidden;">  
    <h6 class="modal-title blue-grey darken-1">
        <span id="spk_id" style="margin-right:20px"></span>
        <b>Salesman/Tim: </b>
        <span id="spk_sales" style="margin-right:20px"></span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:0;position: relative;">
        <div class="row" style="margin:0">
                        <div class="col s12 m6">
                              <div style="display:none" id="uid"></div>
                              <table class="info">
                                    <tr>
                                          <td width="180px">Nama Pemesan</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_nama"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Alamat Domisili/Usaha</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_alamat"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode Pos</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_pos"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Telepon</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_telp"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Ponsel</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_ponsel"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Email</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_email"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode PPN</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_ppn"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">NPWP</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_npwp"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Faktur Pajak</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_pajak"></td>
                                    </tr>
                                    <tr>
                                          <td colspan="3">Nama dan Jabatan Contact Person Customer Corporate/Fleet:</td>
                                    </tr>
                                    <tr>
                                          <td colspan="3" id="spk_fleet"></td>
                                    </tr>
                                    <tr>
                                          <td colspan="3" id="spk_pel_fotoid"></td>
                                    </tr>
                              </table>
                        </div>                        
                        <div class="col s12 m6" >
                              <table class="info">
                                    <tr>
                                          <td width="180px">Faktur STNK a/n</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_nama"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">No. KTP</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_identitas"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Alamat KTP/KIMS</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_alamat"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode Pos</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_pos"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Alamat Tempat Tinggal</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_alamatd"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode Pos</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_posd"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Telepon</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_telp"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Ponsel</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_ponsel"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Email</td>
                                          <td width="10px">:</td>
                                          <td class="bold" style="text-center" id="spk_stnk_email"></td>
                                    </tr>
                                    <tr>
                                          <td colspan="3" style="text-center" id="spk_stnk_fotoid"></td>
                                    </tr>
                              </table>
                        </div>
                      </div>
                    <div class="row" style="border-top:1px solid #eee">
                      <div class="col s12 m6"  style="border-bottom:1px solid #eee;font-size:12px; padding:5px 20px;">
                        KETERANGAN
                      </div>
                      <div class="col s12 m6"  style="border-bottom:1px solid #eee;font-size:12px; padding:5px 20px;">
                        CATATAN SALES
                      </div>
                        <div class="col s12 m6">
                            <table class="info" style="margin-top:0">
                                    <tr>
                                          <td width="180px">Variant</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="variant_nama"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Warna</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_warna"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Keterangan Harga</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_kategori"></td>
                                    </tr>
                              </table>
                        </div>

                        <div class="col s12 m6">
                            <i><p id="spk_ket_permintaan" style="padding-left:10px"><p></i>
                        </div>

                  </div>  
      </div>
                  <div class="modal-footer text-right">
                      <a class="waves-effect waves-light btn green" id="setujui" title="Konfirmasi SPK">
                          <i class="material-icons left" >done</i>
                          <span>Konfirmasi</span>
                      </a>
                      <a class="waves-effect waves-light btn orange" id="konfirmasi" title="Kirim Tanggapan" style="margin-right:15px">
                          <i class="material-icons left" >error_outline</i>
                          <span>Tanggapi</span>
                      </a>
                  </div>  
</div>

<div id="konfirmasi_spk" class="modal"  style="width:450px;">  
    <h6 class="modal-title blue-grey darken-1">
        <span>Tanggapi SPK</span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:15px; position: relative;">
        <div class="row" style="margin:0;margin-bottom:10px;">
          <span style="color: red">*</span> <span style="color: black">Berikan Catatan/Tanggapan Anda.</span> 
                  <div class="col m12" style="font-size:12px">
                         <textarea id="spk_catt_permintaan" class="" style="height: 120px" placeholder="Berikan Catatan"></textarea>
                  </div>
                  <div class="col m12">
                    <a id="tanggapi" target="_blank" class="waves-effect waves-light orange btn-large" style="width:100%"><i class="material-icons left">send</i> Kirim Tanggapan</a>
                  </div>
        </div>
    </div>
</div>


<div id="setujui_spk" class="modal"  style="width:450px;">  
    <h6 class="modal-title blue-grey darken-1">
        <span>Konfirmasi SPK</span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:15px; position: relative;">
        <div class="row" style="margin:0;margin-bottom:10px;">
          <div class="col m12" style="color: black">Apakah SPK Ini Auto Matching ?</div>
        </div>
    </div>
     <div class="modal-footer row" style="margin:0">
           <div class="col m6">
              <a id="tidak" target="_blank" class="waves-effect waves-light red btn" style="width:100%"><i class="material-icons left">remove</i> <span>Tidak</span></a>
          </div>
           <div class="col m6">
              <a id="ya" target="_blank" class="waves-effect waves-light green btn" style="width:100%"><i class="material-icons left">done</i> <span>Ya</span></a>
          </div>
      </div>
</div>
