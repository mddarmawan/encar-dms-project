<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\TrKendaraan;
use App\StockMove;

class ApiStockMoveController extends Controller
{
    function index(){
		$data = DB::table('tb_stockmove')
			->select("tb_stockmove.*","trk_dh","trk_tahun", "trk_mesin","trk_rangka","variant_serial","warna_nama","trk_tgl", "g_dari.gudang_nama as dari","g_ke.gudang_nama as ke", "ekspedisi_nama", "type_nama", "variant_nama")
    		->join('tb_tr_kendaraan', 'tb_tr_kendaraan.trk_id', '=', 'tb_stockmove.sm_trk')
    		->join('tb_variant', 'tb_variant.variant_id', '=', 'tb_tr_kendaraan.trk_variantid')
    		->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
    		->join('tb_warna', 'tb_warna.warna_id', '=', 'tb_tr_kendaraan.trk_warna')
    		->join(DB::raw('tb_gudang g_dari'), 'g_dari.gudang_id', '=', 'tb_tr_kendaraan.trk_lokasi')
    		->join(DB::raw('tb_gudang g_ke'), 'g_ke.gudang_id', '=', 'tb_stockmove.sm_lokasi')
    		->leftjoin('tb_ekspedisi', 'tb_ekspedisi.ekspedisi_id', '=', 'tb_stockmove.sm_ekspedisi')
			->orderBy('sm_id', 'desc')
			->get();
		$result = $data->filter(function ($data) {
		    return 
		    	(!request("trk_tahun") || strrpos(strtolower($data->trk_tahun), strtolower(request("trk_tahun"))) > -1) && (!request("trk_dh") || strrpos(strtolower($data->trk_dh), strtolower(request("trk_dh"))) > -1) && (!request("trk_mesin") || strrpos(strtolower($data->trk_mesin), strtolower(request("trk_mesin"))) > -1) && (!request("trk_rangka") || strrpos(strtolower($data->trk_rangka), strtolower(request("trk_rangka"))) > -1) && (!request("variant_serial") || strrpos(strtolower($data->variant_serial), strtolower(request("variant_serial"))) > -1) && (!request("variant_nama") || strrpos(strtolower($data->type_nama." ".$data->variant_nama), strtolower(request("variant_nama"))) > -1) &&
				(!request("warna_nama") || strrpos(strtolower($data->warna_nama), strtolower(request("warna_nama"))) > -1) &&
				(!request("sm_dari") || strrpos(strtolower($data->dari), strtolower(request("sm_dari"))) > -1) &&
				(!request("sm_ke") || strrpos(strtolower($data->ke), strtolower(request("sm_ke"))) > -1) &&
				(!request("ekspedisi_nama") || strrpos(strtolower($data->ekspedisi_nama), strtolower(request("ekspedisi_nama"))) > -1);
		});
		$data = array();
        foreach($result as $r){
            $item = array();
            $item['sm_id'] = $r->sm_id;
            $item['trk_tahun'] = $r->trk_tahun;
            $item['trk_tgl'] = date_format(date_create($r->trk_tgl),"d/m/Y");
            $item['trk_dh'] = $r->trk_dh;
            $item['trk_rangka'] = $r->trk_rangka;
            $item['trk_mesin'] = $r->trk_mesin;
            $item['variant_serial'] = $r->variant_serial;
            $item['variant_nama'] = $r->type_nama." ".$r->variant_nama;
            $item['warna_nama'] = $r->warna_nama;
            $item['ekspedisi_nama'] = $r->ekspedisi_nama;
            $item['sm_dari'] = $r->dari;
            $item['sm_ke'] = $r->ke;
			$item['sm_tglout'] = "";
			$item['sm_tglin'] = "";
			$item['sm_status'] = "WAITING";
			if (!is_null($r->sm_tglout)){
				$item['sm_tglout'] = date_format(date_create($r->sm_tglout),"d/m/Y");
				$item['sm_status'] = "INTRANSIT";
			}
			if (!is_null($r->sm_tglin)){
				$item['sm_tglin'] = date_format(date_create($r->sm_tglin),"d/m/Y");
				$item['sm_status'] = "GI";			
			}
			
			if ((!request("trk_tgl") || strrpos(strtolower($item['trk_tgl']), strtolower(request("trk_tgl"))) > -1) &&
				 (!request("sm_tglout") || strrpos(strtolower($item['sm_tglout']), strtolower(request("sm_tglout"))) > -1) && 
				 (!request("sm_tglin") || strrpos(strtolower($item['sm_tglin']), strtolower(request("sm_tglin"))) > -1) &&
				 (!request("sm_status") || strrpos(strtolower($item['sm_status']), strtolower(request("sm_status"))) > -1)
				){			
				array_push($data,$item);
			}
		}
		
		return json_encode($data);
	}
	
	
    function store(){
    	$this->validate(request(), [
            "sm_trk"      => "required",
            "sm_gudang"      => "required",
        ]);


        
        $insert = StockMove::create([
            "sm_trk"     =>  request("sm_trk"),
            "sm_lokasi"     =>  request("sm_gudang"),
            "sm_ekspedisi"     =>  request("sm_ekspedisi")
        ]);
        $result =  StockMove::where("sm_id",$insert->id)->first();

        if (count($result)>0){
            return 1;
        }
        return 0;
    }

	
	function getstock(){
		$data = DB::table('vw_stock_kendaraan')
			->select(DB::raw("trk_id as id"),DB::raw("trk_dh as value"),DB::raw("gudang_nama as trk_lokasi"),"trk_tahun", "trk_mesin","trk_rangka","variant_serial","warna_nama","trk_tgl", "type_nama", "variant_nama")
    		->join('tb_variant', 'tb_variant.variant_id', '=', 'vw_stock_kendaraan.trk_variantid')
    		->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
    		->join('tb_warna', 'tb_warna.warna_id', '=', 'vw_stock_kendaraan.trk_warna')
    		->join('tb_gudang', 'tb_gudang.gudang_id', '=', 'vw_stock_kendaraan.trk_lokasi')
    		->leftjoin('tb_stockmove', 'tb_stockmove.sm_trk', '=', 'vw_stock_kendaraan.trk_id')
			->where("trk_lokasi",1)
			->whereNotNull("sm_tglin")
			->orwhereNull("sm_id")
			->get();
			
		return json_encode($data);
	}
	
	function getgudang($id){
		$data = DB::table('tb_gudang')->where("gudang_id","!=",1)->get();
			
		return json_encode($data);
	}
}
