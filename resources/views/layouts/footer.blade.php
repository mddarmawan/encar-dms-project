
<div class="nav-footer" style="display:none">
	<ul class="left">
		<li><strong>Logged As</strong> : <span id="app-logged">Administrator</span></li>
	</ul>
	<ul class="right">
		<li><strong>A-DMS</strong> : <span id="app-adms">Disconnected</span></li>
		<li><strong><span class="fa fa-calendar"></span></strong> <span id="app-calendar">00/00/0000</span></li>
		<li><strong><span class="fa fa-clock-o"></span></strong> <span id="app-timer">00/00/00</span></li>

		<li><a class="fullscreen" title="Fullscreen"><span class="material-icons">aspect_ratio</span></a></li>
	</ul>
</div>

<script src="/assets/js/materialize.js"></script>
<script src="/assets/js/prism.js"></script>
<script>
function number_format(x) {
	if (isNaN(x)){
		return 0;
	}
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
}
function string_format(x) {
	return parseInt(x.toString().replace(/\./g, ''));
}
function toInt(x){
	x = string_format(x);
	if (isNaN(x)){
		return 0;
	}
	return x;
}
function selected(param1, param2){
	if (param1==param2){
		return ' selected ';
	}
	return '';
}
function checked(param1, param2){
	if (param1==param2){
		return ' checked ';
	}
	return '';
}
function date_format(x){
	var y = x.substr(0,4);
	var m = x.substr(5,2);
	var d = x.substr(8,2);
	
	return d+"/"+m+"/"+y;
}

 $(document).ready(function() {
 	var full = false;
			$(".fullscreen").click(function(){
				$(this).toggleClass("active");
				
				if(!full){
					full=true;
					var elem = document.getElementById("apps");
					if (elem.requestFullscreen) {
					  elem.requestFullscreen();
					} else if (elem.msRequestFullscreen) {
					  elem.msRequestFullscreen();
					} else if (elem.mozRequestFullScreen) {
					  elem.mozRequestFullScreen();
					} else if (elem.webkitRequestFullscreen) {
					  elem.webkitRequestFullscreen();
					}
					$(this).attr("title","Exit Fullscreen");
				}else{
					full=false;
					if (document.exitFullscreen) {
						document.exitFullscreen();
					} else if (document.webkitExitFullscreen) {
						document.webkitExitFullscreen();
					} else if (document.mozCancelFullScreen) {
						document.mozCancelFullScreen();
					} else if (document.msExitFullscreen) {
						document.msExitFullscreen();
					}
					$(this).attr("title","Fullscreen");
				}
			});

	$(".number").focusin(function(){
		var num = string_format($(this).val());
		if (num=="0") num="";
		$(this).attr("type","number");
		$(this).val(num);
	});

	$(".number").focusout(function(){
		var num = number_format($(this).val());
		$(this).attr("type","text");
		$(this).val(num);
	});
	


 	function getDate(){
 		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1;
		var yyyy = today.getFullYear();

		if(dd<10) {
		    dd='0'+dd
		} 

		if(mm<10) {
		    mm='0'+mm
		} 

		today = mm+'/'+dd+'/'+yyyy;
		$("#app-calendar").html(today);
 	}
 	function startTime() {
	    var today = new Date();
	    var h = today.getHours();
	    var m = today.getMinutes();
	    var s = today.getSeconds();
	    m = checkTime(m);
	    s = checkTime(s);
	    $('#app-timer').html(h + ":" + m + ":" + s)
	    var t = setTimeout(startTime, 500);
	}
	function checkTime(i) {
	    if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
	    return i;
	}
 	getDate();startTime();

 	var nav_active = false;
 	var id_tmp="";
 	$(".app-menu li a").click(function(e){
 		e.preventDefault(); 		
 		var id = $(this).attr("href").replace("#","");
 		$(".app-menu li").removeClass("active");
 		if (id!=id_tmp){
 			id_tmp=id; 			
 			$(".nav-sub>div").removeClass("active");
 			$("#"+id).toggleClass("active");
	 		$(this).parent().addClass("active");
 			if (!nav_active){
	 			nav_active = true;
	 			$(".nav-sub").addClass("active");

	 		}
 		}else{
 			id_tmp="";
 			nav_active = false;
 			$(".nav-sub").removeClass("active");

 		}
 		
 	});

  });

    $('select.material').material_select();
    $('.collapse').collapsible();
    $('.modal').modal({dismissible: false});
    $('.tooltip').tooltip({delay: 50});
    $('.datepicker').datepicker({ dateFormat: 'dd/mm/yy' });
	
</script>
