<div id="dataVariant">

</div>


<script>
$("#filter").click(function(e){
        e.preventDefault();
        loadData();
    });

    $("#reset_variant").click(function(e){
        e.preventDefault();
        $("#filter_awal").val("");
        $("#filter_akhir").val("");
        status="";
        $(".tab li").removeClass("active");
        $(".tab li a[data-id='']").parent().addClass("active");
        loadData();
    });


    $("#export_variant").click('click', function (event) {
        var args = [$('#dataVariant'), 'DATA_VARIANT_<?php echo date('dmY') ?>.xls'];   
        exportTableToExcel.apply(this, args);
    });

function type() {
    $.ajax({
        type: "GET",
        url: "{{url('api/kendaraan/type')}}"
    }).done(function(type) {
        type.unshift({ type_id: "0", type_nama: "" });

function variant() {


           var db = {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/kendaraan/variant')}}",
                    data: filter
                });
            },

           insertItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "POST",
                    url: "{{url('/api/kendaraan/variant')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/kendaraan/variant/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].variant_id] = {
                                variant_id: item[i].variant_id,
                                variant_hapus: item[i].variant_hapus,
                                variant_nama : item[i].variant_nama,
                                variant_type: item[i].variant_type,
                                variant_on : item[i].variant_on,
                                variant_status : item[i].variant_status,
                            };
                        }
                        var db = firebase.database().ref("data/tb_variant/");
                        db.set(data);
                    });
                });    
            },

            updateItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/kendaraan/variant')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/kendaraan/variant/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].variant_id] = {
                                variant_id: item[i].variant_id,
                                variant_hapus: item[i].variant_hapus,
                                variant_nama : item[i].variant_nama,
                                variant_type: item[i].variant_type,
                                variant_on : item[i].variant_on,
                                variant_status : item[i].variant_status,
                            };
                        }
                        var db = firebase.database().ref("data/tb_variant/");
                        db.set(data);
                    });
                });
            },

            deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/kendaraan/variant')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(response){
                    $.ajax({
                        type: "GET",
                        url: "{{url('/api/kendaraan/variant/all')}}"
                    }).done(function(item) {
                        var data = [];
                        for (var i in item) {
                            data[item[i].variant_id] = {
                                variant_id: item[i].variant_id,
                                variant_hapus: item[i].variant_hapus,
                                variant_nama : item[i].variant_nama,
                                variant_type: item[i].variant_type,
                                variant_on : item[i].variant_on,
                                variant_status : item[i].variant_status,
                            };
                        }
                        var db = firebase.database().ref("data/tb_variant/");
                        db.set(data);
                    });                
                });
            }
        };

        db.status = [
            {
                "status_id": "",
                "status_nama": "",     
            },
            {
                "status_id": 0,
                "status_nama": "<span class='box-span red'>NON. ACTIVED</span>",          
            },
            {
                "status_id": 1,
                "status_nama": "<span class='box-span green'>ACTIVED</span>",           
            },
            
        ];

        $("#dataVariant").jsGrid({
            height: "calc(100% - 40px)",
            width: "100%",
     
            filtering: true,
            editing: true,
            inserting: true,
            sorting: true,
            autoload: true,
     
            deleteConfirm: "Anda yakin akan menghapus data ini?",
     
            controller: db,
     
            fields: [
                { name: "variant_status", title:"Status", type: "select", items: db.status, valueField: "status_id", textField: "status_nama", width: 80, align:"center", filtering:false, editing:false},
                { name: "variant_type", title:"Type", type: "select", items: type, valueField: "type_id", textField: "type_nama", width: 100, align:"left" },
                { name: "variant_serial", title:"ID", type: "text", width: 100, validate: "required" },
                { name: "variant_nama", title:"Nama", type: "text", width: 130, validate: "required" },
                { name: "variant_ket", title:"Keterangan", type: "text", width: 170, validate: "required" },
                { name: "type_poin", title:"Point", type: "number", width: 50, inserting: false, editing:false },
                { type: "control", width:70 }
            ]

        });

    
};
variant();

});
};
type();
</script>