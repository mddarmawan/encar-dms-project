<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Gudang;

class ApiGudangController extends Controller
{
    function index(){
    	$data = DB::table('tb_gudang')
                -> get();
		$result = $data->filter(function ($data) {
		    return 
		    	(!request("gudang_nama") || strrpos(strtolower($data->gudang_nama), strtolower(request("gudang_nama"))) > -1) &&
				 (!request("gudang_alamat") || strrpos(strtolower($data->gudang_alamat), strtolower(request("gudang_alamat"))) > -1) &&
				 (!request("gudang_kota") || strrpos(strtolower($data->gudang_kota), strtolower(request("gudang_kota"))) > -1) &&
				 (!request("gudang_telp") || strrpos(strtolower($data->gudang_telp), strtolower(request("gudang_telp"))) > -1) &&
				 (!request("gudang_email") || strrpos(strtolower($data->gudang_email), strtolower(request("gudang_email"))) > -1);
		});

    	return json_encode($result);
    }

    function store(){
    	$this->validate(request(), [
            "gudang_nama"      => "required",
            "gudang_kota"     => "required"
        ]);

	    $insert=array(
	        "gudang_nama"     		=>  request("gudang_nama"),
            "gudang_kota"          =>  request("gudang_kota"),
            "gudang_alamat"        =>  request("gudang_alamat"),
            "gudang_email"        =>  request("gudang_email"),
            "gudang_telp"          =>  request("gudang_telp")
	    );

       $id = DB::table('tb_gudang')->insertGetId($insert,'gudang_id');

        return json_encode(DB::table('tb_gudang')->where("gudang_id",$id)->first());
    }

    function update(){
    	$this->validate(request(), [
            "gudang_nama"      => "required",
            "gudang_kota"     => "required"
        ]);

	    DB::table('tb_gudang')-> where("gudang_id",request("gudang_id"))->update([
	        "gudang_nama"     		=>  request("gudang_nama"),
            "gudang_kota"          =>  request("gudang_kota"),
            "gudang_alamat"        =>  request("gudang_alamat"),
            "gudang_email"        =>  request("gudang_email"),
            "gudang_telp"          =>  request("gudang_telp")
	    ]);

	    return json_encode(DB::table('tb_gudang')->where("gudang_id",request("gudang_id"))->first()) ;
    }

    function destroy(){
		return DB::table('tb_gudang')-> where('gudang_id', request("gudang_id"))->delete();
    }    
}
