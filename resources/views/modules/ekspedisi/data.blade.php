@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Pembelian</small>
            Ekspedisi Kendaraan
        </h6>
    </div>

<div id="data_grid" class="wrapper">
        <div class="nav-wrapper">
            
            <div class="nav-right">
                <a class="waves-effect waves-light btn-flat btn-filter" id="reset">Reset</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
            </div>

            

        </div>

        <div id="dataEkspedisi">

        </div>  

</div>
<script>

    $("#filter").click(function(e){
        e.preventDefault();
        loadData();
    });


    $("#reset").click(function(e){
        e.preventDefault();
        $("#filter_awal").val("");
        $("#filter_akhir").val("");
        status="";
        $(".tab li").removeClass("active");
        $(".tab li a[data-id='']").parent().addClass("active");
        loadData();
    });


    $("#export").click('click', function (event) {
        var args = [$('#dataEkspedisi'), 'DATA_EKSPEDISI_<?php echo date('dmY') ?>.xls'];   
        exportTableToExcel.apply(this, args);
    });
$(function() {

    var db_ekspedisi = {
        loadData: function(filter) {

            return $.ajax({
                        type: "GET",
                        url: "{{url('api/ekspedisi')}}",
                        data: filter
                    });
        },

        insertItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "POST",
                url: "{{url('/api/ekspedisi')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });    
        },

        updateItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "PUT",
                url: "{{url('/api/ekspedisi')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });
        },

        deleteItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "DELETE",
                url: "{{url('/api/ekspedisi')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });
        }
    };

    $("#dataEkspedisi").jsGrid({
        height: "100%",
        width: "100%",
 
        filtering: true,
        editing: true,
        inserting: true,
        sorting: true,
        autoload: true,
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db_ekspedisi,
 
        fields: [
            { name: "ekspedisi_nama", title:"Nama ekspedisi", type: "text", width: 150, validate: "required" },
            { name: "ekspedisi_alamat", title:"Alamat", type: "text", width: 200 },
            { name: "ekspedisi_kota", title:"Kota", type: "text", width: 100, validate: "required" },
            { name: "ekspedisi_kodepos", title:"Kode Pos", type: "number", width: 120, validate: "" },
            { name: "ekspedisi_telepon", title:"Telepon", type: "text", width: 100, validate: "required", align:"right" },
            { name: "ekspedisi_email", title:"Email", type: "text", width: 100, validate: "" },
            { type: "control", width:70 }
        ]
    });
 
});
</script>

@endsection