<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ApiPenjualanController extends Controller
{
    function index(){
    	$data = DB::table('tb_spk')
			->leftjoin('tb_spk_leasing', 'tb_spk_leasing.spkl_spk', '=', 'tb_spk.spk_id')
			->leftjoin('tb_spk_diskon', 'tb_spk_diskon.spkd_spk', '=', 'tb_spk.spk_id')
			->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
			->leftjoin('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk.spk_leasing')
    		->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
    		->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
    		->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
    		->join('tb_spk_faktur', 'tb_spk_faktur.spkf_spk', '=', 'tb_spk.spk_id')
    		->where("spk_do","!=",NULL)
    		->get();

    	
		$result = array();
		foreach($data as $r){
			$item = array();
			$item['spkf_id'] = $r->spkf_id;
			$item['spkf_tgl'] = date_format(date_create($r->spkf_tgl),"d/m/Y");
			$item['spk_id'] = $r->spk_id;
			$item['spk_tgl'] = date_format(date_create($r->spk_tgl),"d/m/Y");
			$item['spk_pel_nama'] = $r->spk_pel_nama;
			$item['karyawan_nama'] = $r->karyawan_nama;
			$item['team_nama'] = $r->team_nama;

			if ($r->spk_ket_harga ==0){
				$harga = number_format( $r->variant_on);	 
			}else{
				$harga = number_format($r->variant_on - $r->variant_bbn) ;			
			}

			
			$diskon = DB::table('tb_spk_diskon')
				->select("spkd_id",DB::raw("SUM(spkd_cashback) as diskon"))
				->groupBy("spkd_id")
	    		->where("spkd_status",1)
	    		->where("spkd_spk",$r->spk_id)->first();
	    	
	    	
	    	$bayar = DB::table('tb_spk_pembayaran')->select(DB::raw("SUM(spkp_jumlah) as bayar"))
    		->where("spkp_spk",$r->spk_id)->first()->bayar;
    		if (is_null($bayar)){
    			$bayar = 0;
    		}
			if ($r->spk_ket_harga == 1){
				$piutang = number_format ( $r->spkl_droping - ($r->variant_on   - $r->variant_bbn -$r->spkd_cashback - (DB::table('tb_spk_pembayaran')-> select(DB::raw("SUM(spkp_jumlah) as bayar"))->where("spkp_spk",$r->spk_id)->first()->bayar)),0,",",".") ;  
			}else{
				$piutang = number_format($r->spkl_droping - ($r->variant_on  - $r->spkd_cashback - ( DB::table('tb_spk_pembayaran')->select(DB::raw("SUM(spkp_jumlah) as bayar")) ->where("spkp_spk",$r->spk_id)->first()->bayar)),0,",",".");	 
			}


			$item['spk_harga'] =$harga;
			$item['spk_potongan'] = number_format($diskon-> diskon,0,',','.');
			$item['spk_bayar'] = number_format($bayar,0,',','.');
			$item['detail'] = "<a href='#detail' class='green-text detail' onclick='detail(this)'  data-id='".$r->spk_id."'  title='Detail'><span class='material-icons'  style='font-size:20px'>search</span></a>";
			$item['spk_via'] = $r->spk_pembayaran."";
			if ($r->spk_pembayaran == 1){
				$leasing = $this->get_leasing($r->spk_id);
				if ($leasing){
					$piutang_leasing = $this->get_piutang_leasing($leasing->spkl_id);
					if ($piutang_leasing){
						$item['spk_bayar'] = number_format($bayar + $piutang_leasing->jumlah,0,',','.');
						$piutang -= $piutang_leasing->jumlah;
					}
					$item['via_nama'] = $leasing->leasing_nama;
				}
			}else{
				$item['spk_cash'] = "CASH";				
			}

			if ($r->spk_pembayaran == 1) {
				$item['spk_leasing'] = $r->leasing_nama;
			}else{
				$item['spk_leasing'] = "TIDAK ADA";
			}


			$item['spk_metode'] = $r->spk_pembayaran+1;

			$item['spk_piutang'] = number_format($piutang,0,',','.');
			
			if ($piutang >0){
				$item['spk_status'] = "0";
			}else{
				$item['spk_status'] = "1";
			}
			if((!request("spk_id") || strrpos(strtolower($item['spk_id']), strtolower(request("spk_id"))) > -1) &&
				(!request("spkf_id") || strrpos(strtolower($item['spkf_id']), strtolower(request("spkf_id"))) > -1) &&
				(!request("spkf_tgl") || strrpos(strtolower($item['spkf_tgl']), strtolower(request("spkf_tgl"))) > -1) &&
				(!request("spk_tgl") || strrpos(strtolower($item['spk_tgl']), strtolower(request("spk_tgl"))) > -1) &&
				(!request("spk_pel_nama") || strrpos(strtolower($item['spk_pel_nama']), strtolower(request("spk_pel_nama"))) > -1) &&
				(!request("karyawan_nama") || strrpos(strtolower($item['karyawan_nama']), strtolower(request("karyawan_nama"))) > -1) &&
				(!request("team_nama") || strrpos(strtolower($item['team_nama']), strtolower(request("team_nama"))) > -1) &&
				 (!request("spk_harga") || strrpos(strtolower($item['spk_harga']), strtolower(request("spk_harga"))) > -1) &&
				 (!request("spk_leasing") || strrpos(strtolower($item['spk_leasing']), strtolower(request("spk_leasing"))) > -1) &&
				 (!request("spk_potongan") || strtolower($item['spk_potongan'])== strtolower(request("spk_potongan"))) &&
				 (!request("spk_bayar") || strtolower($item['spk_bayar'])== strtolower(request("spk_bayar")))&&
				 (!request("spk_status") || strtolower($item['spk_status'])== strtolower(request("spk_status"))))	{

				$tgl = strtotime(str_replace("/","-",$item['spk_tgl']));
				if (request("filter_awal") && request("filter_akhir")){
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
					if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_awal")){
					$filter_awal = strtotime(request("filter_awal"));
					if ($filter_awal<=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_akhir")){
					$filter_akhir = strtotime(request("filter_akhir"));
					if ($filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else{
					array_push($result, $item);
				}
			}
		}

		return json_encode($result);
    }

    public function detail_penjualan($id){
		$data['bayar'] = DB::table('tb_spk_pembayaran')
			->where("spkp_spk",$id)->get();

        for ($i = 0; $i < count($data['bayar']); $i++) {
            unset($data['bayar'][$i]->created_at);
            unset($data['bayar'][$i]->updated_at);
        }

		$data['diskon'] = DB::table('tb_spk_diskon')
			->where("spkd_status",1)
			->where("spkd_spk",$id)->first();

		$data['aksesoris'] = array();
		if ($data['diskon']){
		$data['aksesoris'] = DB::table('tb_spk_aksesoris')
			->where("spka_diskon",$data['diskon']->spkd_id)->get();
		}

		return json_encode($data);
	}

	function detail($id){
		$data['pemesan'] = DB::table('tb_spk')
			->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
			->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
			->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
			->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
			->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
			->leftjoin('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk.spk_leasing')
			->leftjoin("tb_warna", 'warna_id', '=', 'tb_spk.spk_warna')
			->where("spk_id",$id)
			->first();

		$data['kendaraan'] = DB::table('tb_spk')
			->join("tb_tr_kendaraan","tb_tr_kendaraan.trk_dh","=","tb_spk.spk_dh")
			->join('tb_warna', 'tb_warna.warna_id', '=', 'tb_tr_kendaraan.trk_warna')
			->where("spk_status","<",10)
			->where("spk_id",$id)->first();

		$data['bayar'] = DB::table('tb_spk_pembayaran')
			->where("spkp_spk",$id)->get();


		$data['faktur'] = DB::table('tb_spk_faktur')
			->where("spkf_spk",$id)->first();

		$faktur = DB::table('tb_spk_faktur')->orderBy("spkf_id","DESC")->first();

		$data['no_faktur'] = "F-".date("Y")."0001";
		if($faktur){
			$tmp = $faktur->spkf_id;
			$tahun = substr($tmp,2,4);
			$num = substr($tmp,6,strlen($tmp));
			if(date("Y") == $tahun){
				$num++;
				if(strlen($num)==1){
					$num = "000".$num;
				}elseif(strlen($num)==2){
					$num = "00".$num;
				}elseif(strlen($num)==3){
					$num = "0".$num;
				}
			}
			$data['no_faktur']="F-".date("Y").$num;
		};

		$data['diskon'] = DB::table('tb_spk_diskon')
			->where("spkd_status",1)
			->where("spkd_spk",$id)->first();

		$data['aksesoris'] = array();
		if ($data['diskon']){
		$data['aksesoris'] = DB::table('tb_spk_aksesoris')
			->where("spka_diskon",$data['diskon']->spkd_id)->get();
		}

		$data['d_leasing'] = DB::table('tb_leasing')
			->where("leasing_status",1)->get();

		$data['d_asuransi'] = DB::table('tb_asuransi_jenis')->get();

		$data['leasing'] = DB::table('tb_spk_leasing')
			->leftjoin('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk_leasing.spkl_leasing')
			->where("spkl_spk",$id)->first();

		return json_encode($data);
	}

    private function get_leasing($id){
    	$data = DB::table("tb_spk_leasing")
    		->join('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk_leasing.spkl_leasing')
    		->where("spkl_spk",$id)
    		->first();
    	return $data;
    }

     private function get_piutang_leasing($id){
    	$data = DB::table("tb_leasing_bayar")
    		->select(DB::raw('SUM(lbayar_nominal) as jumlah'), DB::raw("MAX(lbayar_tgl) as lunas"))
    		->where("lbayar_spkl",$id)
    		->first();
    	if (!is_null($data->jumlah)){
    		return $data;
    	}
    	return FALSE;
    }

}
