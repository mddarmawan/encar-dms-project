<div id="dataRef">

</div>

<script>
$(function() {
	var db_ref = {
        loadData: function(filter) {
            return $.ajax({
                        type: "GET",
                        url: "{{url('api/referral')}}",
                        data: filter
                    });
        },

        insertItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "POST",
                url: "{{url('/api/referral')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });    
        },

        updateItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "PUT",
                url: "{{url('/api/referral')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });
        },

        deleteItem: function(item) {
            item['_token'] = '{{csrf_token()}}';
            return $.ajax({
                type: "DELETE",
                url: "{{url('/api/referral')}}",
                data: item
            }).fail(function(response) {
                console.log(response);
            });
        }

    };

    $("#dataRef").jsGrid({
        height: "380px",
        width: "100%",
 
        filtering: true,
        editing: true,
        inserting: true,
        sorting: true,
        autoload: true,
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db_ref,
 
        fields: [
            { name: "referral_nama", title:"Nama Referral", type: "text", width: 150, validate: "required" },
            { name: "referral_alamat", title:"Alamat", type: "text", width: 200 },
            { name: "referral_telp", title:"Telepon", type: "text", width: 100, validate: "required" },
            { name: "referral_bank", title:"Bank", type: "text", width: 70, validate: "required" },
            { name: "referral_rek", title:"Rekening", type: "text", width: 100, validate: "required" },
            { name: "referral_an", title:"Atas Nama", type: "text", width: 150, validate: "required" },
            { type: "control", width:70 }
        ]
    });
 
});
</script>