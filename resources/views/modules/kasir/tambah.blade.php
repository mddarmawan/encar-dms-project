<div class="row" style="margin:0">
		<input type="hidden" id="konfirmasi_id" />
		<input type="hidden" id="spk_id" />
		<div style="display:none" id="uid"></div>
		<div class="col s12 m12" style="padding-bottom:10px;border-bottom: 1px solid #ddd">			
			<table class="info payment" style="width:auto;margin:0;margin-bottom:10px;">
				<tr>
					<td style="padding-right:15px!important;">
						<b>No. SPK: </b>					
					</td>
					<td style="padding-right:45px!important;">
						<div class="input-group" style="margin:0; width: 284px; position: relative;">
							<input type="text" id="spk_spk" class="input-filter uppercase" placeholder="Masukkan No. SPK/Nama Pemesan" value="" style="position: relative;" >
							<span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
						</div>
					</td>
					<td>
						<b>Total Invoice: </b>					
					</td>
					<td  style="padding-right:15px!important;" id="total_invoice">Rp 0,-</td>
					<td>
						<b>Total Bayar: </b>					
					</td>
					<td  style="padding-right:15px!important" id="total_bayar">Rp 0,-</td>
					<td>
						<b>Kurang Bayar: </b>					
					</td>
					<td style="padding-right:15px!important" id="total_kurang">Rp 0,-</td>
				</tr>
			</table>
			<table class="info payment" style="width:auto;margin:0;"> 
				<tr>
					<td><b>No. Kwitansi*:</b></td>
					<td><b>Tgl. Bayar*:</b></td>
					<td><b>Jumlah Bayar*:</b></td>
					<td><b>Bayar ke akun*:</b></td>
					<td>Keterangan:</td>
					<td></td>
				</tr>
					<td style="padding-right:10px!important">
						<div class="input-group" style="background:#f5f5f5">
							<input type="text" placeholder="0" id="no_faktur" class="input-filter" disabled style="padding:4px 6px 3px 6px;"/>
						</div>
					</td>
					<td style="padding-right:10px!important">
						<div class="input-group">
							<input type="text" class="input-filter datepicker" id="tanggal_bayar" placeholder="{{date('d/m/Y')}}" value="{{date('d/m/Y')}}" />
							<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
						</div>
					</td>					
					<td style="padding-right:10px!important">
						<div class="input-group">
							<input type="text" placeholder="0" id="jumlah" class="input-filter number" />
						</div>
					</td>
					<td style="padding-right:10px!important">
						<div class="input-group">
							<select type="text" id="akun" class="input-filter">
							</select>
						</div>
					</td>	
					<td style="padding-right:10px!important">
						<div class="input-group" style="width:320px">
							<input type="text" id="ket" class="input-filter uppercase" />
						</div>
					</td>
					<td>
						<a class="waves-effect waves-light btn-flat btn-filter" style="background: #009F61;color:#fff;border-radius:3px;border:0;border-bottom:2px solid #008E57;" id="save">Simpan &amp; Cetak</a>
					</td>			
			  </tr>
			</table>
		</div>
		<div class="col s12 m4" style="height:320px;padding-left:0;border-right: 1px solid #ddd">
			<table class="info" style="margin-left:10px;margin-top:0">
				<tr>
					<td width="180px">Nama Pemesan</td>
					<td width="10px">:</td>
					<td class="bold" id="pel_nama"></td>
				</tr>
				<tr>
					<td width="180px">Alamat Domisili/Usaha</td>
					<td width="10px">:</td>
					<td class="bold" id="pel_alamat"></td>
				</tr>
				<tr>
					<td width="180px">Kode Pos</td>
					<td width="10px">:</td>
					<td class="bold" id="pel_pos"></td>
				</tr>
				<tr>
					<td width="180px">Telepon</td>
					<td width="10px">:</td>
					<td class="bold" id="pel_telp"></td>
				</tr>
				<tr>
					<td width="180px">Ponsel</td>
					<td width="10px">:</td>
					<td class="bold" id="pel_ponsel"></td>
				</tr>
				<tr>
					<td width="180px">Email</td>
					<td width="10px">:</td>
					<td class="bold" id="pel_email"></td>
				</tr>
				<tr>
					<td width="180px">Kode PPN</td>
					<td width="10px">:</td>
					<td class="bold" id="spk_ppn"></td>
				</tr>
				<tr>
					<td width="180px">NPWP</td>
					<td width="10px">:</td>
					<td class="bold" id="spk_npwp"></td>
				</tr>
				<tr>
					<td width="180px">Faktur Pajak</td>
					<td width="10px">:</td>
					<td class="bold" id="spk_pajak"></td>
				</tr>
				<tr>
					<td colspan="3">Nama dan Jabatan Contact Person Customer Corporate/Fleet:</td>
				</tr>
				<tr>
					<td colspan="3" id="spk_fleet" style="padding-top:0!important;font-weight: bold"></td>
				</tr>
			</table>
		</div>
		<div class="col s12 m4" style="height:320px;padding-left:0;border-right: 1px solid #ddd">
			<table class="info" style="margin-left:10px;margin-top:0">
						<tr>
							<td width="180px">Variant</td>
							<td width="10px">:</td>
							<td class="bold" id="spk_kendaraan"></td>
						</tr>
						<tr>
							<td width="180px">ID</td>
							<td width="10px">:</td>
							<td class="bold" id="spk_kendaraan_serial"></td>
						</tr>
						<tr>
							<td width="180px">Warna</td>
							<td width="10px">:</td>
							<td class="bold" id="spk_warna"></td>
						</tr>
						<tr>
							<td width="180px">Keterangan</td>
							<td width="10px">:</td>
							<td class="bold" id="spk_ket_harga"></td>
						</tr>
			</table>
			<table class="info" style="margin-left:10px;margin-top:0">
						<tr >
							<td width="180px">Jenis Pembayaran</td>
							<td width="10px">:</td>
							<td class="bold" id="spk_pembayaran"></td>
						</tr>
						<tr class="pembayaran">
							<td width="180px">Leasing</td>
							<td width="10px">:</td>
							<td class="bold" id="spk_leasing"></td>
						</tr>
						<tr class="pembayaran">
							<td width="180px">DP</td>
							<td width="10px">:</td>
							<td class="bold" id="spk_dp"></td>
						</tr>
						<tr>
							<td width="180px">Sales</td>
							<td width="10px">:</td>
							<td class="bold" id="spk_sales"></td>
						</tr>
			</table>
		</div>
		<div class="col s12 m4" style="height:320px;padding:0;border-right: 1px solid #ddd">
			<div style="height: 320px;overflow:hidden;overflow-y:auto;background:#f9f9f9;">
				<table class="info payment" style="border:0;margin:0">
					<thead>
						<tr class="jsgrid-header-row">
							<th class="jsgrid-header-cell p5">Tanggal</th>
							<th class="jsgrid-header-cell p5">Keterangan</th>
							<th class="jsgrid-header-cell p5 text-right">Jumlah (Rp)</th>
						</tr>
					</thead>
					<tbody id="tabel_pembayaran">
					</tbody>
				</table>
			</div>
		</div>	
</div>
