function exportTableToExcel($table, filename) {
		var header;
		var body;
		tmpColDelim = String.fromCharCode(11);
		tmpRowDelim = String.fromCharCode(0);
	 	var $rows_header = $table.find('.jsgrid-grid-header .jsgrid-table tr:has(th)'),

            colDelim = "</td><th  bgcolor='#dddddd'>",
            rowDelim = "\r\n" + "<tr><th  bgcolor='#dddddd'>",
            header = "<tr><th bgcolor='#dddddd'>" + $rows_header.map(function (i, row) {
                var $row = $(row),
                    $cols = $row.find('th');

                return $cols.map(function (j, col) {
                    var $col = $(col),
                        text = $col.text();
                    return text.replace(/"/g, '""'); 
                }).get().join(tmpColDelim)+"</th></tr>";
            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim);

        var $rows = $table.find('.jsgrid-grid-body .jsgrid-table tr:has(td)'),
            colDelim = "</td><td>",
            rowDelim = "\r\n" + '<tr><td>',

            body = '<tr><td>' + $rows.map(function (i, row) {
                var $row = $(row),
                    $cols = $row.find('td');

                return $cols.map(function (j, col) {
                    var $col = $(col),
                        text = $col.text();
                    return text.replace(/"/g, '""'); 
                }).get().join(tmpColDelim)+"</td></tr>";

            }).get().join(tmpRowDelim)
                .split(tmpRowDelim).join(rowDelim)
                .split(tmpColDelim).join(colDelim);
        data = "<table border='1'>"+ header + body + "</table>";

        if (false && window.navigator.msSaveBlob) {
			var blob = new Blob([decodeURIComponent(data)], {
	            type: 'text/html;charset=utf8'
            });            
            window.navigator.msSaveBlob(blob, filename);
            
        } else if (window.Blob && window.URL) {
            var blob = new Blob([data], { type: 'text/html;charset=utf8' });
            var csvUrl = URL.createObjectURL(blob);

            $(this)
            		.attr({
                		'download': filename,
                		'href': csvUrl
		            });
				} else {
            var csvData = 'data:application/vnd.ms-excel' + encodeURIComponent(data);

			$(this).attr({
               	'download': filename,
                'href': csvData,
                'target': '_blank'
            });
        }
    }