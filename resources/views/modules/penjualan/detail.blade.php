<div id="detail" class="modal" style="width:1180px; overflow-x: hidden;">  
    <h6 class="modal-title blue-grey darken-1">
        Detail SPK
        <span id="spk_id">SPK</span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:0;position: relative;">
        <div class="row" style="margin:0">
                        <div class="col s12 m4">
                         <h6 style="padding:0 12px;font-weight:bold">Data Pemesan</h6>
                         <hr>
                              <table class="info" style="margin-top: -13px">
                                    <tr>
                                          <td width="180px">Nama Pemesan</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_nama"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Alamat Domisili/Usaha</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_alamat"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode Pos</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_pos"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Telepon</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_telp"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Ponsel</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_ponsel"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Email</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_email"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode PPN</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_ppn"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">NPWP</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_npwp"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Faktur Pajak</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_pajak"></td>
                                    </tr>
                                    <tr>
                                          <td colspan="3">Nama dan Jabatan Contact Person Customer Corporate/Fleet:</td>
                                    </tr>
                                    <tr>
                                          <td colspan="3" id="spk_fleet"></td>
                                    </tr>
                              </table>
                        </div>                        
                        <div class="col s12 m4"  style="border-left:1px solid #ddd">
                         <h6 style="padding:0 12px;font-weight:bold">Data STNK</h6>
                         <hr>
                              <table class="info" style="margin-top: -13px">
                                    <tr>
                                          <td width="180px">Faktur STNK a/n</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_nama"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">No. KTP</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_identitas"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Alamat KTP/KIMS</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_alamat"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode Pos</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_pos"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Alamat Tempat Tinggal</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_alamatd"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode Pos</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_posd"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Telepon</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_telp"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Ponsel</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_ponsel"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Email</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_email"></td>
                                    </tr>
                              </table>
                        </div>
                        <div class="col s12 m4"  style="border-left:1px solid #ddd">
                         <h6 style="padding:0 12px;font-weight:bold">Data Kendaraan</h6>
                         <hr>
                            <table class="info" style="margin-top: -13px">
                                    <tr>
                                          <td width="180px">Variant</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="variant_nama"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">ID</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="variant_id"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Warna</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_warna"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Keterangan</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="variant_ket"></td>
                                    </tr>
                              </table>
                              <hr>
                              <h6 style="padding:0 12px;font-weight:bold">Data Pembayaran</h6>
                               <hr>
                              <table class="info" style="margin-top: -13px">
                                    <tr>
                                          <td width="180px">Jenis Pembayaran</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_metode"></td>
                                    </tr>
                                    <tr class="leasing">
                                          <td width="180px">Nama Leasing</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="leasing"></td>
                                    </tr>
                                    <tr class="leasing">
                                          <td width="180px">Angsuran Perbulan</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="angsuran"></td>
                                    </tr>
                                    <tr class="leasing">
                                          <td width="180px">Downpayment</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="dp"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Keterangan Harga</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="harga"></td>
                                    </tr>

                              </table>
                        </div>

                  </div>
                  
      </div>
</div>

