<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<style type="text/css">
			body{
				font-family: Courier;
				font-size: 10px;
			}
			.head{
				width: 100%;
				border-top:1px solid #000;
				border-bottom:1px solid #000;
				font-size: 15px;
			}
			.bulat{
				background-color: #000;
				color: #fff;
				border-radius: 30px;
				width: 15px;
				height: 15px;
				-webkit-print-color-adjust: exact; 
				text-align: center;	
			}
			.col-3{
				width: 55%;
				display: inline-block; 	
			}

		</style>
	</head>
	<body>
		<div>
			<div style="float: right; top:0; position: fixed; width: 11%; margin-top: 2%;">
				<tt><h3 style="font-weight: normal;">No. 43931</h3></tt>
			</div>
			<div style="display: inline-block; margin-right:10px;">
				<img style=" margin-bottom: 20px; max-width: 100%; width: 150px;" src="./assets/images/logo.png">
				<br>
				<tt style="font-size: 13px;">PT. ENCARTHA INDONESIA<br>
				Palembang</tt>
			</div>
		</div> 
		<div style="position: absolute; top:120px; left:240px; width: 100%;">
				<tt><b>JL. Angkatan 45 Palembang 30137<br>
					SUMSEL - Indonesia
				</b><br>
				<span style="display: inline-block;widows: 100px;">
					<p style="margin-top: 5px;">
						<p class="bulat"><b>P</b></p>
					</p>
					<p style="margin-top: 5px;">
						<p class="bulat"><b>F</b></p>
					</p>
					<p style="margin-top: 5px;">
						<p class="bulat"><b>W</b></p>
					</p> 
				</span>
				<div style="display: inline-block;margin-left: 20px;float: right;margin-top:-60px;position: absolute;">
					<p style="margin-top: 6px;">
						0711-374500 (Showroom), 0711-374800 (Service & Part)
						<div style="margin-top: 19px;"></div>
						0711-374500
						<div style="margin-top: 6px;"></div>
						www.encar.co.id
					</p>
				</div>
			</tt>
		</div>
		<tt>
			<table style="width:100%; margin-top: 30px;">
				<tr>
					<td style="text-align: right; width: 50%;">
						<h2 style="font-weight: normal;">KWITANSI</h2>
					</td>
					<td style="width:29%;">&nbsp;</td>
					<td>
						<p style="margin-bottom: 2px;"> Nomor :  {{ $data->spkp_id }}/{{ $data->spk_id }}</p>
						<p style="margin-bottom: 2px;"> Tanggal : {{ $data->spkp_tgl }}  </p>
						<p style="margin-bottom: 2px;"> Akun :  {{ $data->spkp_akun_id }}/{{ $data->spkp_akun }}</p>
					</td>
				</tr>
			</table>
		</tt>
		<p style="margin:0;"><tt>Terima : {{ $data->spk_pel_nama }}</tt></p>		
		<tt>
			<div style="border-top: 1px solid #000;"></div>
			<table width="99%">
				<tr class="tr">
					<td style="width: 120px;">No.</td>
					<td style="width: 450px;">Keterangan</td>
					<td style="text-align: right;">Jumlah.</td>
				</tr>
			</table>
			<div style="border-bottom: 1px solid #000;"></div>
		</tt>
		<tt>
			<table>
				<tr>
					<td style="width: 120px;">
						<br>
						1.
						<br><br><br>
					</td>
					<td style="width: 450px;">
						<br>
						{{ $data->spkp_ket }} PEBELIAN KENDARAAN {{ $data->spk_variant }} :<br>
						{{ $data->spk_warna }}
						<br><br><br>
					</td>
					<td style="text-align: right;">
						{{ $data->spkp_nominal }}
						<br><br><br>
					</td>
				</tr>
			</table>
			<div style="border-bottom: 1px solid #000; margin-top: 100px; margin-bottom: 10px;"></div>
		</tt>
		<tt>
			<table>
				<tr>
					<td style="width: 500px;">
						Terbilang : {{ $data->spkp_terbilang }}
					</td>
					<td style="text-align: right;">
						Total &nbsp; {{ $data->spkp_nominal }}
					</td>
				</tr>
				<tr>
					<td></td>
					<td style="text-align: center;">
						<br>
						<br>
						<br>
						Palembang, {{ $data->spkp_tgl }}
						<br>
						<br>
						<br>
						<br>
						___________
						<br>
						Kasir
					</td>
				</tr>
			</table>
		</tt>
	</body>
</html>