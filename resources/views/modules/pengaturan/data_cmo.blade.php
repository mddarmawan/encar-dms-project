@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Pengaturan</small>
            Data CMO
        </h6>
    </div>

<div id="data_grid" class="wrapper">
        <div class="nav-wrapper">
            
            <div class="nav-right">
                <a class="waves-effect waves-light btn-flat btn-filter" id="reset">Reset</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
            </div>

            

        </div>

        <div id="dataCmo">

        </div>  

</div>

<script>

     $("#filter").click(function(e){
        e.preventDefault();
        loadData();
    });


    $("#reset").click(function(e){
        e.preventDefault();
        $("#filter_awal").val("");
        $("#filter_akhir").val("");
        status="";
        $(".tab li").removeClass("active");
        $(".tab li a[data-id='']").parent().addClass("active");
        loadData();
    });


    $("#export").click('click', function (event) {
       var args = [$('#dataCmo'), 'DATA_cmo_<?php echo date('dmY') ?>.xls'];    
        exportTableToExcel.apply(this, args);
    });
$(function() {
     $.ajax({
        type: "GET",
        url: "{{url('api/leasing')}}"
    }).done(function(leasing) {

        leasing.unshift({ leasing_id: 0, leasing_nama: "" });

        var db_cmo = {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/cmo')}}",
                    data: filter
                });
            },

            insertItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "POST",
                    url: "{{url('/api/cmo')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                });    
            },

            updateItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/cmo')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                });
            },

            deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/cmo')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                });
            }
        };

    $("#dataCmo").jsGrid({
        height: "calc(100% - 40px)",
        width: "100%",
 
        filtering: true,
        editing: true,
        inserting: true,
        sorting: true,
        autoload: true,
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db_cmo,
 
         fields: [
                 { name: "cmo_nama", title:"Nama CMO", type: "text", width: 150, validate: "required" },
                { name: "cmo_leasing", title:"Leashing", type: "select", items: leasing, valueField: "leasing_id", textField: "leasing_nama", width: 120, align:"left", validate:  { message: "Leashing harus diisi !", validator: function(value) { return value > 0; } }},
                { name: "cmo_alamat", title:"Alamat", type: "text", width: 200 },
                { name: "cmo_kota", title:"Kota", type: "text", width: 100, validate: "required" },
                { name: "cmo_telp", title:"Telepon", type: "number", width: 100, validate: "required" },
                { type: "control", width:70 }
            ]
    });
 
});
});
</script>

@endsection