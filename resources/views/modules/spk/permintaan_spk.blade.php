@extends('layout')

@section('content')
	<div class="content-header">
		<h6>
			<small>Penjualan</small>
			Permintaan SPK
		</h6>
		<ul class="header-tools right">
		   <li><a href="{{url('spk/no_spk')}}" class="chip">Pembagian No SPK</a></li>
		   <li><a href="{{url('spk')}}" class="chip ">Monitoring SPK</a></li>
			<li><a href="{{url('spk/permintaan_spk')}}" class="chip active">Permintaan SPK</a></li>
			<li><a href="{{url('spk/cancel')}}" class="chip">Batal SPK</a></li>
			<li><a href="{{url('spk/permintaan_do')}}" class="chip">Permintaan DO</a></li>
		</ul>
	</div>		

<div id="data_grid" class="wrapper">
		<div class="nav-wrapper">
			<div class="nav-left">
				<ul class="tab">
					<li class="active"><a data-id="" >Semua</a></li>
					<li><a data-id="90">Req</a></li>
					<li><a data-id="95">Revisi</a></li>
				</ul>
			</div>
			<div class="nav-right">
				<i style="margin-right: 5px" class="fa fa-filter" aria-hidden="true"></i>
				<span class="bold" style="font-size: 13px"> Periode :</span>
				<div class="input-group">
					<input type="text" class="input-filter datepicker" id="filter_awal" placeholder="Tanggal Awal" required="" />
					<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
				</div>
				<span class="bold" style="font-size: 13px">s.d</span>
				<div class="input-group">
					<input type="text" class="input-filter datepicker"  id="filter_akhir" placeholder="Tanggal Akhir"  required="" />
					<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
				</div>
				<a class="waves-effect waves-light btn-flat btn-filter" id="filter">Cari</a>
				<a class="waves-effect waves-light btn-flat btn-filter" id="reset">Reset</a>
				<a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
			</div>
		</div>

		<div id="dataSPK">

		</div>	

</div>
	@include("modules.spk.detail_permintaan")


<script>
	function detail(id){
		$("#spk_id").html(id);

		$.ajax({
			type: "GET",
			url: "{{url('api/spk/')}}/"+id
		}).done(function(json) {
		 	var pemesan = json.pemesan;
		 	var kendaraan = json.kendaraan;
		 	var leasing = json.leasing;
		 	
			 if(pemesan.spk_status==9){
			 	$("#konfirmasi").hide();
			 } else {
			 	$("#konfirmasi").show();
			 }

		 	if (pemesan.spk_ppn===1){
		 		$("#spk_ppn").html("YA");
		 	}
		 	if (pemesan.spk_pajak===1){
		 		$("#spk_pajak").html("DIMINTA");
		 	}else{
		 		$("#spk_pajak").html("TIDAK DIMINTA");
		 	}
		 	$("#uid").html(pemesan.sales_salesUid);
		 	$("#spk_tgl").html(date_format(pemesan.spk_tgl));
		 	$("#spk_sales").html(pemesan.karyawan_nama + " / " +pemesan.team_nama );

		 	$("#pel_nama").html(pemesan.spk_pel_nama);
		 	$("#pel_identitas").html(pemesan.spk_pel_identitas);
		 	$("#pel_alamat").html(pemesan.spk_pel_alamat);
		 	$("#pel_pos").html(pemesan.spk_pel_pos);
		 	$("#pel_telp").html(pemesan.spk_pel_telp);
		 	$("#pel_ponsel").html(pemesan.spk_pel_ponsel);
		 	$("#pel_email").html(pemesan.spk_pel_email);
		 	$("#pel_kategori").html(pemesan.spk_pel_kategori);
		 	$("#spk_npwp").html(pemesan.spk_npwp);
		 	$("#spk_fleet").html(pemesan.spk_fleet);
		 	$("#spk_ket_permintaan").html(pemesan.spk_ket);
		 	$("#spk_stnk_nama").html(pemesan.spk_stnk_nama);
		 	$("#spk_stnk_alamat").html(pemesan.spk_stnk_alamat);
		 	$("#spk_stnk_pos").html(pemesan.spk_stnk_pos);
		 	$("#spk_stnk_alamatd").html(pemesan.spk_stnk_alamatd);
		 	$("#spk_stnk_posd").html(pemesan.spk_stnk_posd);
		 	$("#spk_stnk_telp").html(pemesan.spk_stnk_telp);	
		 	$("#spk_stnk_ponsel").html(pemesan.spk_stnk_ponsel);
		 	$("#spk_stnk_email").html(pemesan.spk_stnk_email);
		 	$("#spk_stnk_identitas").html(pemesan.spk_stnk_identitas);
		 	$("#variant_nama").html(pemesan.type_nama+ " " +pemesan.variant_nama);
		 	$("#spk_warna").html(pemesan.warna_nama);
		 	$("#variant_id").html(pemesan.variant_serial);
		 	$("#spk_pel_fotoid").html("<img height='150px' src='{{url('/')}}/storage/data/identitas/"+pemesan.spk_pel_fotoid+"'/>");
		 	$("#spk_stnk_fotoid").html("<img height='150px' src='{{url('/')}}/storage/data/identitas/"+pemesan.spk_stnk_fotoid+"'/>");

		 	$("#spkt_stck").val(pemesan.spk_catt_permintaan);

		 	if (kendaraan!=null){
				$("#kendaraan .no-data").removeClass("show");
			 	$("#trk_dh").html(kendaraan.trk_dh);
			 	$("#trk_mesin").html(kendaraan.trk_mesin);
			 	$("#trk_rangka").html(kendaraan.trk_rangka);
			 	$("#trk_warna").html(kendaraan.warna_nama);		 		
		 	}else{
				$("#kendaraan .no-data").addClass("show");
			 	$("#trk_dh").html('');
			 	$("#trk_mesin").html('');
			 	$("#trk_rangka").html('');
			 	$("#trk_warna").html('');
		 	}

		 	if(pemesan.spk_kategori==1){	
		 		$("#spk_kategori").html("ON THE ROAD");
		 	}else{
		 		$("#spk_kategori").html("OFF THE ROAD");
		 	}
			
            $("#detail_permintaan").modal("open");

		});
	};
	
	
	

	$("#spk_automatching").click(function(){
		if (this.checked) {
			$("#spk_automatching").val("1");
		} else {
			$("#spk_automatching").val("0");
		}
	});

	$("#tanggapi").click(function(){
		var data = {
			"spk_id"	: $("#spk_id").html(),
			_token:'{{csrf_token()}}'
		};

		$.ajax({
			type: "POST",
			dataType:"json",
			url: "{{url('/api/spk/tanggapi_permintaan_spk')}}",
			data: data
		}).fail(function(response) {
			alert("ERR-42 Data Gagal Ditanggapi !");
		}).done(function(response){
			if (!response.result){
				alert(response.msg);
			} else {
						if (response.result){
							var ket = $("#spk_catt_permintaan").val();
							var spk = $("#spk_id").html();
							var uid = $("#uid").html();

							var data = {
								"activity_judul":"Revisi SPK " + spk,
								"activity_ket":ket,
								"activity_ref":spk,
								"activity_kategori":"spk",
								"activity_author":"ADH",
								"activity_tgl":response.tgl,
							};
							var db = firebase.database().ref('activity/' + uid).push();
							db.set(data);

							alert("SPK "+ spk +" Berhasil Ditanggapi !");

							loadData();
							$("#konfirmasi_spk").modal("close");
							$("#detail_permintaan").modal("close");
						}else{
							alert("ERR-00 Data Gagal Ditanggapi !");							
						}
			}

		});
	});

	$("#ya").click(function(){
		var data = {
			"spk_id"	: $("#spk_id").html(),
			"spk_catt_permintaan" : $("#spk_catt_permintaan").val(),
			"spk_automatching" : 1,
			_token:'{{csrf_token()}}'
		};

		konfirmasi(data);
	});

	$("#tidak").click(function(){
		var data = {
			"spk_id"	: $("#spk_id").html(),
			"spk_catt_permintaan" : $("#spk_catt_permintaan").val(),
			"spk_automatching" : 0,
			_token:'{{csrf_token()}}'
		};

		konfirmasi(data);
	});

	function konfirmasi(data){
		$.ajax({
			type: "POST",
			dataType:"json",
			url: "{{url('/api/spk/acc_permintaan_spk')}}",
			data: data
		}).fail(function(response) {
			alert("ERR-42 Data Gagal Disetujui !");
			}).done(function(response){
				if (!response.result){
					alert(response.msg);
				} else {
					var spk = $("#spk_id").html();
					var uid = $("#uid").html();

						var notif = {
							"notif_judul":"INVALID",
							"notif_ket":"SPK " + spk + " telah dinaikan ke status INVALID",
							"notif_ref":spk,
							"notif_kategori":"spk",
							"notif_tgl":response.tgl,
						};

						var activity = {
								"activity_judul":spk + " - INVALID",
								"activity_ket":"Harap segera follow up pembayaran Tunai atau uang muka pelanggan, dan konfirmasikan pada Kasir",
								"activity_kategori":"spk",
								"activity_ref":" ",
								"activity_author":"ADH",
								"activity_tgl":response.tgl
						};

						var db = firebase.database().ref('spk_update/' + uid + '/' + spk);
						db.set({"spk_status":9});

						var db = firebase.database().ref('activity/' + uid );
						db.push(activity);

						var db = firebase.database().ref('notif/' + uid);
						db.push(notif);

					alert("SPK "+ spk +" Berhasil Dikonfirmasi !");
							
					loadData();
					$("#setujui_spk").modal("close");
					$("#detail_permintaan").modal("close");
				}
			});
	}

	$("#konfirmasi").click(function(){
		$("#konfirmasi_spk").modal("open");
	});


	$("#setujui").click(function(){
		$("#setujui_spk").modal("open");
	});
	

$("#filter").click(function(e){
	e.preventDefault();
	loadData();
});

$("#reset").click(function(e){
	e.preventDefault();
	$("#filter_awal").val("");
	$("#filter_akhir").val("");
	status="";
	$(".tab li").removeClass("active");
	$(".tab li a[data-id='']").parent().addClass("active");
	loadData();
});


$("#export").click('click', function (event) {
    var args = [$('#dataSPK'), 'Permintaan_SPK_<?php echo date('dmY') ?>.xls'];   
    exportTableToExcel.apply(this, args);
});

var status="";
function loadData() {
	var db_spk = {
		loadData: function(filter) {
			var filter_awal = $("#filter_awal").val().trim();
			var filter_akhir = $("#filter_akhir").val().trim();
			if (filter_awal != ""){
				filter['filter_awal'] = filter_awal;
			}
			if (filter_akhir != ""){
				filter['filter_akhir'] = filter_akhir;
			}
			filter['spk_status'] = status;
			return $.ajax({
				type: "GET",
				url: "{{url('api/spk/permintaan_spk')}}",
				data: filter
			});
		}
	};
		
	db_spk.harga = [
			{
				"harga_id": "",
				"harga_nama": "",		   
			},
			{
				"harga_id": 1,
				"harga_nama": "ON-TR",		   
			},
			{
				"harga_id": 2,
				"harga_nama": "OFF-TR",		   
			},
			
		];
	
	db_spk.via = [
			{
				"via_id": "",
				"via_nama": "",		   
			},
			{
				"via_id": 1,
				"via_nama": "CASH",		   
			},
			{
				"via_id": 2,
				"via_nama": "CREDIT",		   
			},
			
		];


	db_spk.status = [
			{
				"status_id": "",
				"status_nama": "",	   
			},
			{
				"status_id": "90",
				"status_nama": "<span class='box-span green'>REQ</span>",		   
			},
			{
				"status_id": "95",
				"status_nama": "<span class='box-span orange'>REVISI</span>",		   
			},
			
		];

	$("#dataSPK").jsGrid({
		height: "calc(100% - 40px)",
		width: "100%",
 
		sorting: true,
		filtering: true,
		autoload: true,
		paging: true,
		pageSize: 30,
		pageButtonCount: 5,
		noDataContent: "Permintaan SPK masih Kosong",
		rowClick:function(data){
            var item = data.item;
            detail(item.spk_id);
        },
 
		controller: db_spk,
 
		fields: [
			{ name: "spk_status", title:"Status", type: "select", width: 90,items: db_spk.status, valueField: "status_id", textField: "status_nama", align:"center", filtering:false },
			{ name: "spk_tgl", title:"Tanggal", type: "text", width: 100, align:"center" },
			{ name: "spk_id", title:"No SPK", type: "text", width: 100, align:"center" },
			{ name: "spk_pel_nama", title:"Nama Pelanggan", type: "text", width: 190},
		   // { name: "spk_type", title:"Type", type: "text", width: 70, align:"center" },
			{ name: "spk_variant", title:"Varian", type: "text", width: 200 },			
			{ name: "spk_warna", title:"Warna", type: "text", width: 120, align:"center"},
			{ name: "spk_sales", title:"Sales", type: "text", width: 120},
			{ name: "spk_team", title:"Tim", type: "text", width: 120, align:"center" },
			{ name: "spk_kendaraan_harga", title:"Ket. Harga", type: "select", width:100, items: db_spk.harga, valueField: "harga_id", textField: "harga_nama", align:"center" },
			{ name: "spk_dp", title:"Permintaan Dp", type: "number", width: 140, align:"right" },
			{ name: "spk_metode", title:"CARA BAYAR", type: "select", width: 90,items: db_spk.via, valueField: "via_id", textField: "via_nama", align:"center" }
		]
	});
}
loadData();

$(".tab li a").click(function(e){
	e.preventDefault();

	status = $(this).data("id");
	
	loadData();
	$(".tab li").removeClass("active");
	$(this).parent().addClass("active");
});

</script>
@endsection