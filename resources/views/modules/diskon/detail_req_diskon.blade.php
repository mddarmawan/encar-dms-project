<div class="modal-content" style="padding:10px 14px;position: relative;">
        <div class="row" style="margin:0">
            <div class="col s4">
                <h6>
                    <span id="diskon_nama"></span><br/>
                    <small id="diskon_spk" class="orange-text lighten-1"></small>
                    <small class="right"><i class="fa fa-clock-o"></i> <span id="diskon_tgl"></span></small>
                </h6>
                 <hr/>
                <table class="info payment" style="margin:0">
                    <tr>
                        <td width="100">Sales</td><td id="sales"></td>
                     </tr>
                    <tr>
                        <td width="100">Type</td><td id="type"></td>
                    </tr>
                </table>

                 <hr/>

                <table class="info payment" style="margin-bottom:0">
                	<tr>
                        <td width="200">Cashback</td><td>Rp</td><td  class="right"><input type="text" min="0" step="1" id="cashback" class="text-right number"/></td>
                    </tr>
                    <tr>
                        <td colspan="3">Aksesoris</td>
                    </tr>
                </table>

                <table id="aksesoris" class="info payment" style="margin:0">
                </table>

                <table class="info payment" style="margin-top:0">
                    <tr style="font-weight:bold;border-top:1px solid #ddd;border-bottom:1px solid #ddd">
                        <td width="200">Total</td><td>Rp</td><td class="right"><input type="text" min="0" step="1" id="total" class="text-right number"/></td>
                    </tr>
                </table> 
            </div>

            <div class="col s4">
            	<h6> 
            		<span>DATA REFERRAL</span><br/>
            		<small id="diskon_spk" class="orange-text lighten-1"></small>
                    <small class="right"><i class="fa fa-clock-o"></i> <span id="diskon_tgl"></span></small>
            	</h6>
            </div>

            <dv class="col s4" style="padding-top:10px">
                <input type="hidden" id="diskon_id"/>
                <div class="input-field col s12">
                    <textarea id="catatan" class="materialize-textarea" style="height:80px"></textarea>
                    <label for="catatan">Catatan</label>
                </div>
                <div class="row">
                    <div class="col s6 center">
                        <button type="submit" value="1" class="btn-floating btn-large waves-effect waves-light green pulse app"><i class="material-icons">done</i></a>
                    </div>
                   	<div class="col s6 center">                     
                        <button type="submit"  value="99" class="btn-floating btn-large waves-effect waves-light red app"><i class="material-icons">close</i></a>
                    </div>
            	</div>
        	</div>
    	</div>
</div>