<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\TrKendaraan;
use App\SPK;

class ApiMatchController extends Controller
{
	function index(){
		$data = DB::table("tb_tr_kendaraan")
						->join('tb_variant', 'tb_tr_kendaraan.trk_variantid', '=', 'tb_variant.variant_id')
						->join('tb_type', 'tb_variant.variant_type', '=', 'tb_type.type_id')
						->join('tb_warna', 'tb_tr_kendaraan.trk_warna', '=', 'tb_warna.warna_id')
						->join('tb_spk', 'tb_tr_kendaraan.trk_dh', '=', 'tb_spk.spk_dh')
						->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
						->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
						->whereNull("spk_do")						
						->where("spk_status",2)
						->get();

		$result = array();
		foreach($data as $r){
			$item = array();
			$item['spk_id'] = $r->spk_id;
			$item['spk_tgl'] = date_format(date_create($r->spk_tgl),"d/m/Y");
			$item['spk_pel_nama'] = $r->spk_pel_nama;
			$item['spk_variant_id'] = $r->variant_serial;
			$item['spk_dh'] = $r->spk_dh;
			$item['spk_type'] = $r->type_nama;
			$item['spk_variant'] = $r->type_nama.' '.$r->variant_nama;
			$item['spk_warna'] = $r->warna_nama;
			$item['spk_nomesin'] = $r->trk_mesin;
			$item['spk_norangka'] = $r->trk_rangka;
			$item['spk_sales'] = $r->karyawan_nama;
			$item['spk_kota'] = $r->spk_pel_kota;
			$item['spk_match'] =  date_format(date_create($r->spk_match),"d/m/Y");
			$item['spk_waktu_match'] =  date_format(date_create($r->spk_waktu_match),"d/m/Y");
			$item['spk_status'] = $r->spk_status;
			$item['spk_ket'] = "-";

			if((!request("spk_id") || strrpos(strtolower($item['spk_id']), strtolower(request("spk_id"))) > -1) &&
				(!request("spk_tgl") || strrpos(strtolower($item['spk_tgl']), strtolower(request("spk_tgl"))) > -1) &&
				(!request("spk_pel_nama") || strrpos(strtolower($item['spk_pel_nama']), strtolower(request("spk_pel_nama"))) > -1) &&
				(!request("spk_variant_id") || strrpos(strtolower($item['spk_variant_id']), strtolower(request("spk_variant_id"))) > -1) &&
				(!request("spk_sales") || strrpos(strtolower($item['spk_sales']), strtolower(request("spk_sales"))) > -1) &&
				(!request("spk_team") || strrpos(strtolower($item['spk_team']), strtolower(request("spk_team"))) > -1) &&
				(!request("spk_variant") || strrpos(strtolower($item['spk_variant']), strtolower(request("spk_variant"))) > -1) &&
				 (!request("spk_dh") || strrpos(strtolower($item['spk_dh']), strtolower(request("spk_dh"))) > -1) &&
				 (!request("spk_type") || strrpos(strtolower($item['spk_type']), strtolower(request("spk_type"))) > -1) &&
				 (!request("spk_warna") || strrpos(strtolower($item['spk_warna']), strtolower(request("spk_warna"))) > -1) &&
				 (!request("spk_nomesin") || strrpos(strtolower($item['spk_nomesin']), strtolower(request("spk_nomesin"))) > -1) &&
				 (!request("spk_norangka") || strrpos(strtolower($item['spk_norangka']), strtolower(request("spk_norangka"))) > -1) &&
				 (!request("spk_sales") || strrpos(strtolower($item['spk_sales']), strtolower(request("spk_sales"))) > -1) &&
				 (!request("spk_kota") || strrpos(strtolower($item['spk_kota']), strtolower(request("spk_kota"))) > -1) &&
				 (!request("spk_match") || strrpos(strtolower($item['spk_match']), strtolower(request("spk_match"))) > -1) &&
				 (!request("spk_waktu_match") || strrpos(strtolower($item['spk_waktu_match']), strtolower(request("spk_waktu_match"))) > -1) &&
				 (!request("spk_variant_id") || strrpos(strtolower($item['spk_variant_id']), strtolower(request("spk_variant_id"))) > -1) &&
				 (!request("spk_status") || strtolower($item['spk_status'])== strtolower(request("spk_status"))))	{

				$tgl = strtotime(str_replace("/","-",$item['spk_tgl']));
				if (request("filter_awal") && request("filter_akhir")){
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
					if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_awal")){
					$filter_awal = strtotime(request("filter_awal"));
					if ($filter_awal<=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_akhir")){
					$filter_akhir = strtotime(request("filter_akhir"));
					if ($filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else{
					array_push($result, $item);
				}
			}
		}

		return json_encode($result);
	}

	function spk_manual(){
		$data = DB::table("tb_spk")
						->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
						->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
						->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
						->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
						->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
						->leftjoin("tb_warna", 'warna_id', '=', 'tb_spk.spk_warna')
						->whereNull("spk_do")
						->where("spk_status","=",1)
						->where("spk_automatching","=",0)
						->whereNull("spk_dh")
						->get();

		$result = array();
		foreach($data as $r){
			$item = array();
			$item['spk_id'] = $r->spk_id;
			$item['spk_tgl'] = date_format(date_create($r->spk_tgl),"d/m/Y");
			$item['spk_pel_nama'] = $r->spk_pel_nama;
			$item['spk_variant_id'] = $r->variant_serial;
			$item['spk_dh'] = $r->spk_dh;
			$item['spk_type'] = $r->type_nama;
			$item['spk_variant'] = $r->type_nama.' '.$r->variant_nama;
			$item['spk_warna'] = $r->warna_nama;
			$item['spk_team'] = $r->team_nama;
			$item['spk_sales'] = $r->karyawan_nama;
			$item['spk_kota'] = $r->spk_pel_kota;
			$item['spk_match'] =  date_format(date_create($r->spk_match),"d/m/Y");
			$item['spk_waktu_match'] =  date_format(date_create($r->spk_waktu_match),"d/m/Y");
			$item['spk_status'] = $r->spk_status;
			$item['spk_ket'] = "-";

			if((!request("spk_id") || strrpos(strtolower($item['spk_id']), strtolower(request("spk_id"))) > -1) &&
				(!request("spk_tgl") || strrpos(strtolower($item['spk_tgl']), strtolower(request("spk_tgl"))) > -1) &&
				(!request("spk_pel_nama") || strrpos(strtolower($item['spk_pel_nama']), strtolower(request("spk_pel_nama"))) > -1) &&
				(!request("spk_variant_id") || strrpos(strtolower($item['spk_variant_id']), strtolower(request("spk_variant_id"))) > -1) &&
				(!request("spk_sales") || strrpos(strtolower($item['spk_sales']), strtolower(request("spk_sales"))) > -1) &&
				(!request("spk_team") || strrpos(strtolower($item['spk_team']), strtolower(request("spk_team"))) > -1) &&
				(!request("spk_variant") || strrpos(strtolower($item['spk_variant']), strtolower(request("spk_variant"))) > -1) &&
				 (!request("spk_dh") || strrpos(strtolower($item['spk_dh']), strtolower(request("spk_dh"))) > -1) &&
				 (!request("spk_type") || strrpos(strtolower($item['spk_type']), strtolower(request("spk_type"))) > -1) &&
				 (!request("spk_warna") || strrpos(strtolower($item['spk_warna']), strtolower(request("spk_warna"))) > -1) &&
				 (!request("spk_nomesin") || strrpos(strtolower($item['spk_nomesin']), strtolower(request("spk_nomesin"))) > -1) &&
				 (!request("spk_norangka") || strrpos(strtolower($item['spk_norangka']), strtolower(request("spk_norangka"))) > -1) &&
				 (!request("spk_sales") || strrpos(strtolower($item['spk_sales']), strtolower(request("spk_sales"))) > -1) &&
				 (!request("spk_kota") || strrpos(strtolower($item['spk_kota']), strtolower(request("spk_kota"))) > -1) &&
				 (!request("spk_match") || strrpos(strtolower($item['spk_match']), strtolower(request("spk_match"))) > -1) &&
				 (!request("spk_waktu_match") || strrpos(strtolower($item['spk_waktu_match']), strtolower(request("spk_waktu_match"))) > -1) &&
				 (!request("spk_variant_id") || strrpos(strtolower($item['spk_variant_id']), strtolower(request("spk_variant_id"))) > -1) &&
				 (!request("spk_status") || strtolower($item['spk_status'])== strtolower(request("spk_status"))))	{

				$tgl = strtotime(str_replace("/","-",$item['spk_tgl']));
				if (request("filter_awal") && request("filter_akhir")){
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
					if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_awal")){
					$filter_awal = strtotime(request("filter_awal"));
					if ($filter_awal<=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_akhir")){
					$filter_akhir = strtotime(request("filter_akhir"));
					if ($filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else{
					array_push($result, $item);
				}
			}
		}

		return json_encode($result);
	}

	 function kendaraan_manual(){
        $data = DB::table('tb_tr_kendaraan')        
			->leftjoin('tb_spk', 'tb_tr_kendaraan.trk_dh', '=', 'tb_spk.spk_dh')    
            ->join('tb_variant', 'tb_variant.variant_id', '=', 'tb_tr_kendaraan.trk_variantid')
            ->join('tb_type', 'type_id', '=', 'variant_type')
            ->join('tb_warna', 'tb_warna.warna_id', '=', 'tb_tr_kendaraan.trk_warna')
			->where("trk_automatching","=",0)
			->where("spk_dh","=",NULL)
            ->get();
        
        $result = array();
        foreach($data as $r){
            $item = array();
            $item['trk_id'] = $r->trk_id;
            $item['trk_invoice'] = $r->trk_invoice;
            $item['trk_tahun'] = $r->trk_tahun;
            $item['trk_tgl'] = date_format(date_create($r->trk_tgl),"d/m/Y");
            $item['trk_dh'] = $r->trk_dh;
            $item['trk_rangka'] = $r->trk_rangka;
            $item['trk_mesin'] = $r->trk_mesin;
            $item['variant_serial'] = $r->variant_serial;
            $item['variant_type'] = $r->type_nama;
            $item['variant_nama'] = $r->type_nama." ".$r->variant_nama;
            $item['warna_nama'] = $r->warna_nama;
          
            if ((!request("trk_dh") || strrpos(strtolower($item['trk_dh']), strtolower(request("trk_dh"))) > -1) &&
                 (!request("trk_masuk") || strrpos(strtolower($item['trk_masuk']), strtolower(request("trk_masuk"))) > -1) && 
                 (!request("trk_stock") || strrpos(strtolower($item['trk_stock']), strtolower(request("trk_stock"))) > -1) && 
                 (!request("trk_tahun") || strrpos(strtolower($item['trk_tahun']), strtolower(request("trk_tahun"))) > -1) && 
                 (!request("variant_nama") || strrpos(strtolower($item['variant_nama']), strtolower(request("variant_nama"))) > -1) &&
                 (!request("variant_type") || strrpos(strtolower($item['variant_type']), strtolower(request("variant_type"))) > -1) &&
                 (!request("trk_rangka") || strrpos(strtolower($item['trk_rangka']), strtolower(request("trk_rangka"))) > -1)&&      
            
                (!request("trk_mesin") || strrpos(strtolower($item['trk_mesin']), strtolower(request("trk_mesin"))) > -1) &&
                (!request("variant_serial") || strrpos(strtolower($item['variant_serial']), strtolower(request("variant_serial"))) > -1) &&
                (!request("warna_nama") || strrpos(strtolower($item['warna_nama']), strtolower(request("warna_nama"))) > -1) &&
                (!request("trk_invoice") || strrpos(strtolower($item['trk_invoice']), strtolower(request("trk_invoice"))) > -1) &&
                (!request("trk_tgl") || strrpos(strtolower($item['trk_tgl']), strtolower(request("trk_tgl"))) > -1) &&
                (!request("ekspedisi_nama") || strrpos(strtolower($item['ekspedisi_nama']), strtolower(request("ekspedisi_nama"))) > -1) &&
                (!request("trk_status") || strrpos(strtolower($item['trk_status']), strtolower(request("trk_status"))) > -1) &&
                (!request("trk_lokasi") || strrpos(strtolower($item['trk_lokasi']), strtolower(request("trk_lokasi"))) > -1) &&
                (!request("spk_do") || strrpos(strtolower($item['spk_do']), strtolower(request("trk_rrn"))) > -1))   {

                $tgl = strtotime(str_replace("/","-",$item['trk_tgl']));
                if (request("filter_awal") && request("filter_akhir")){
                    $filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
                    $filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
                    if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
                        array_push($result, $item);                     
                    }
                }else if (request("filter_awal")){
                    $filter_awal = strtotime(request("filter_awal"));
                    if ($filter_awal<=$tgl){
                        array_push($result, $item);                     
                    }
                }else if (request("filter_akhir")){
                    $filter_akhir = strtotime(request("filter_akhir"));
                    if ($filter_akhir>=$tgl){
                        array_push($result, $item);                     
                    }
                }else{
                    array_push($result, $item);
                }
            }
        }


        return json_encode($result);
    }


	
	public function add_tempo(){
		$date = str_replace("/","-",request("date"));
		$days = request("days");
		$curDay = date('w', strtotime($date));
		
		$dt = date("Y-m-d", strtotime($date) + (86400*$days));
		if(($curDay + $days)>6){
			$dt = date("Y-m-d", strtotime($dt) + 86400);
		}
		return json_encode(date("d/m/Y",strtotime($dt)));
	}
	
	
    function update(){
    	$this->validate(request(), [
            "spk_id"      => "required",
            "spk_waktu_match"      => "required"
        ]);

	    $update = SPK::where("spk_id",request("spk_id"))->update([
	        "spk_waktu_match" =>  date_format(date_create(str_replace("/","-",request("spk_waktu_match"))),"Y-m-d"),
            ]);

	    if ($update){
	    	return 1;
	    }
	    return 0;
    }

    /**
     * Update the SPK's DH, SPK's Match, and SPK's Match Time.
     *
     * @param array
     * @return \Illuminate\Auth\Access\Response
     *
     */
    public function updateManualMatch() {
		$validator = Validator::make(request()->all(), [
			"spk_id"	=> "required",
			"trk_dh"	=> "required"
		]);

		$proses['result'] = false;
		$proses['msg'] = "";

		if ($validator->fails()) {
			$proses['msg'] = $validator->messages();
		} else {
			$proses['result'] = DB::table('tb_spk')
				->where("spk_id", request("spk_id"))->update([
					"spk_dh"			=> request("trk_dh"),
					"spk_match"			=> date("Y-m-d"),
					"spk_waktu_match"	=> $this->getMatchTime(date("Y-m-d"))
				]);

			$proses['result'] = 1;
		}
		
		return json_encode($proses);
    }

    /**
     * SPK's Match Tieme based on SPK's Match.
     *
     * @param string
     * @return string
     *
     */
    public function getMatchTime($date) {
    	$added_date = date("Y-m-d", strtotime($date) + (86400*3));
    	return $added_date;
    }
}
