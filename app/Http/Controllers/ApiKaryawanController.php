<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Karyawan;

class ApiKaryawanController extends Controller
{
     function index(){
    	$karyawan = DB::table('tb_karyawan')->get();
		$data = array();
		foreach($karyawan as $d){
    		$item = array();
    		$item['karyawan_nip'] = $d->karyawan_nip;
    		$item['karyawan_nama'] = $d->karyawan_nama;
    		$item['karyawan_jabatan'] = $d->karyawan_jabatan;
    		
    		array_push($data, $item);
    	}
    	return json_encode($data);
    }

     function sales_view(){
    	$karyawan = DB::table('tb_sales')
    			->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
    			->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
    			->get();
		$data = array();
		foreach($karyawan as $d){
    		$item = array();
    		$item['sales_id'] = $d->sales_id;
    		$item['sales_nip'] = $d->karyawan_nip;
    		$item['sales_karyawan'] = $d->karyawan_nama;
    		$item['sales_team'] = $d->team_nama;
    		
    		array_push($data, $item);
    	}
    	return json_encode($data);
    }
}
