<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\ApiSpkController;

class ApiKasirController extends Controller
{
    function index(){
		$result = DB::table('tb_spk_pembayaran')
    		->join('tb_spk', 'tb_spk_pembayaran.spkp_spk', '=', 'tb_spk.spk_id')
    		->join('tb_warna', 'tb_spk.spk_warna', '=', 'tb_warna.warna_id')
    		->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
    		->join('tb_type', 'tb_variant.variant_type', '=', 'tb_type.type_id')
    		->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
    		->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
    		->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
    		->orderBy('spkp_id','DESC')
			->get();

		$data = array();
		foreach($result as $r){
			$item = array();
			$item['spkp_id'] = $r->spkp_id;
			$item['spkp_spk'] = $r->spkp_spk;
			$item['spkp_tgl'] = date_format(date_create($r->spkp_tgl),"d/m/Y");
			$item['spk_pel_nama'] = $r->spk_pel_nama;
			$item['spkp_jumlah'] = number_format($r->spkp_jumlah,0,',','.');
			$item['spkp_ket'] = $r->spkp_ket;
            $item['spkp_via'] = strpos($r->spkp_akun,"1.1.01")>-1?1:2;
            $item['spkp_akun'] = $r->spkp_akun;
			$item['karyawan_nama'] = $r->karyawan_nama;
			$item['variant_nama'] = $r->type_nama." ".$r->variant_nama;
			$item['warna_nama'] = $r->warna_nama;
			$item['team_nama'] = $r->team_nama;
            if ((!request("spkp_tgl") || strrpos(strtolower($item['spkp_tgl']), strtolower(request("spkp_tgl"))) > -1)&&
                 (!request("spkp_spk") || strrpos(strtolower($item['spkp_spk']), strtolower(request("spkp_spk"))) > -1)&&
                 (!request("spk_pel_nama") || strrpos(strtolower($item['spk_pel_nama']), strtolower(request("spk_pel_nama"))) > -1)&&
                 (!request("spkp_id") || strrpos(strtolower($item['spkp_id']), strtolower(request("spkp_id"))) > -1)&&
                 (!request("spkp_ket") || strrpos(strtolower($item['spkp_ket']), strtolower(request("spkp_ket"))) > -1)&&
                 (!request("spkp_via") || strtolower($item['spkp_via']== strtolower(request("spkp_via"))))&&
                 (!request("spkp_akun") || strtolower($item['spkp_akun']== strtolower(request("spkp_akun"))))&&
                 (!request("karyawan_nama") || strrpos(strtolower($item['karyawan_nama']), strtolower(request("karyawan_nama"))) > -1)&&
                 (!request("variant_nama") || strrpos(strtolower($item['variant_nama']), strtolower(request("variant_nama"))) > -1)&&
                 (!request("team_nama") || strrpos(strtolower($item['team_nama']), strtolower(request("spkp_jumlah"))) > -1)&&
                 (!request("spkp_jumlah") || strrpos(strtolower($r->spkp_jumlah), strtolower(request("spkp_jumlah"))) > -1)){

                $tgl = strtotime(str_replace("/","-",$item['spkp_tgl']));
                if (request("filter_awal") && request("filter_akhir")){
                    $filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
                    $filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
                    if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
                        array_push($data, $item);                     
                    }
                }else if (request("filter_awal")){
                    $filter_awal = strtotime(request("filter_awal"));
                    if ($filter_awal<=$tgl){
                        array_push($data, $item);                     
                    }
                }else if (request("filter_akhir")){
                    $filter_akhir = strtotime(request("filter_akhir"));
                    if ($filter_akhir>=$tgl){
                        array_push($data, $item);                     
                    }
                }else{
                    array_push($data, $item);
                }
            }
		}

    	return json_encode($data);
    }

    public function getPiutang() {
       $result = DB::table('tb_spk')
            ->join('tb_warna', 'tb_spk.spk_warna', '=', 'tb_warna.warna_id')
            ->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
            ->join('tb_type', 'tb_variant.variant_type', '=', 'tb_type.type_id')
            ->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
            ->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
            ->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
            ->leftjoin('vw_bayar', 'bayar_spk', '=', 'spk_id')
            ->leftjoin('vw_diskon', 'diskon_spk', '=', 'spk_id')
            ->leftjoin('tb_spk_leasing', 'spkl_spk', '=', 'spk_id')
            ->whereRaw ("(COALESCE(bayar, 0) + COALESCE(spkl_droping, 0)) != COALESCE(spk_harga, 0) - COALESCE(diskon, 0)")
            ->orderBy('spk_id','DESC')            
            ->get();

        $data = array();
        foreach($result as $r){
            $item = array();
            $item['spk_id'] = $r->spk_id;
            $item['spk_tgl'] = date_format(date_create($r->spk_tgl),"d/m/Y");
            $item['spk_pel_nama'] = $r->spk_pel_nama;
            $item['karyawan_nama'] = $r->karyawan_nama;
            $item['variant_nama'] = $r->type_nama." ".$r->variant_nama;
            $item['warna_nama'] = $r->warna_nama;
            $item['team_nama'] = $r->team_nama;
            $item['spk_harga'] = number_format($r->spk_harga - $r->diskon,0,',','.');
            $item['spk_bayar'] = number_format($r->bayar + $r->spkl_droping,0,',','.');
            $item['spk_kurang'] = number_format(($r->spk_harga - $r->diskon) - ($r->bayar + $r->spkl_droping),0,',','.');
            if ((!request("spk_id") || strrpos(strtolower($item['spk_id']), strtolower(request("spk_id"))) > -1)&&
                 (!request("spk_tgl") || strrpos(strtolower($item['spk_tgl']), strtolower(request("spk_tgl"))) > -1)&&
                 (!request("spk_pel_nama") || strrpos(strtolower($item['spk_pel_nama']), strtolower(request("spk_pel_nama"))) > -1)&&
                 (!request("karyawan_nama") || strrpos(strtolower($item['karyawan_nama']), strtolower(request("karyawan_nama"))) > -1)&&
                 (!request("variant_nama") || strrpos(strtolower($item['variant_nama']), strtolower(request("variant_nama"))) > -1)&&
                 (!request("team_nama") || strrpos(strtolower($item['team_nama']), strtolower(request("spkp_jumlah"))) > -1)&&
                 (!request("spk_harga") || strrpos(strtolower($r->spk_harga - $r->diskon), strtolower(request("spk_harga"))) > -1)&&
                 (!request("spk_bayar") || strrpos(strtolower($r->bayar + $r->spkl_droping), strtolower(request("spk_bayar"))) > -1)&&
                 (!request("spk_kurang") || strrpos(strtolower(($r->spk_harga - $r->diskon) - ($r->bayar + $r->spkl_droping)), strtolower(request("spk_kurang"))) > -1)){

                $tgl = strtotime(str_replace("/","-",$item['spk_tgl']));
                if (request("filter_awal") && request("filter_akhir")){
                    $filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
                    $filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
                    if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
                        array_push($data, $item);                     
                    }
                }else if (request("filter_awal")){
                    $filter_awal = strtotime(request("filter_awal"));
                    if ($filter_awal<=$tgl){
                        array_push($data, $item);                     
                    }
                }else if (request("filter_akhir")){
                    $filter_akhir = strtotime(request("filter_akhir"));
                    if ($filter_akhir>=$tgl){
                        array_push($data, $item);                     
                    }
                }else{
                    array_push($data, $item);
                }
            }
        }

        return json_encode($data);
    }

    function daftarkonfirmasi(){
        $result = DB::table('tb_konfirmasi_pembayaran')
            ->join('tb_spk', 'konfirmasi_spk', '=', 'tb_spk.spk_id')
            ->join('tb_warna', 'tb_spk.spk_warna', '=', 'tb_warna.warna_id')
            ->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
            ->join('tb_type', 'tb_variant.variant_type', '=', 'tb_type.type_id')            
            ->join('tb_bank', 'tb_bank.bank_rek', '=', 'konfirmasi_tujuan')
            ->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
            ->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
            ->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
            ->orderBy('konfirmasi_id','DESC')
            ->get();

        $data = array();
        foreach($result as $r){
            $item = array();
            $item['konfirmasi_id'] = $r->konfirmasi_id;
            $item['konfirmasi_spk'] = $r->konfirmasi_spk;
            $item['konfirmasi_tgl'] = date_format(date_create($r->konfirmasi_tgl),"d/m/Y H:i:s");
            $item['konfirmasi_tujuan'] = $r->konfirmasi_tujuan;
            $item['bank_akun'] = $r->bank_akun;
            $item['konfirmasi_jumlah'] = number_format($r->konfirmasi_jumlah,0,',','.');
            $item['konfirmasi_ket'] = $r->konfirmasi_ket;
            $item['konfirmasi_bank'] = $r->konfirmasi_bank;
            $item['konfirmasi_rek'] = $r->konfirmasi_rek;
            $item['konfirmasi_an'] = $r->konfirmasi_an;
            $item['konfirmasi_bukti'] = "<a href='".url("/")."/storage/data/bukti/".$r->konfirmasi_bukti."' onclick='bukti(this, event)' data-id='".$r->konfirmasi_spk."' class='orange-text bukti' title='Lihat Bukti Transfer'><span class='material-icons'  style='font-size:20px'>search</span></a>";
            if ((!request("konfirmasi_spk") || strrpos(strtolower($item['konfirmasi_spk']), strtolower(request("konfirmasi_spk"))) > -1)&&
                 (!request("konfirmasi_tgl") || strrpos(strtolower($item['konfirmasi_tgl']), strtolower(request("konfirmasi_tgl"))) > -1)&&
                 (!request("konfirmasi_ket") || strrpos(strtolower($item['konfirmasi_ket']), strtolower(request("konfirmasi_ket"))) > -1)&&
                 (!request("konfirmasi_jumlah") || strrpos(strtolower($item['konfirmasi_jumlah']), strtolower(request("konfirmasi_jumlah"))) > -1)&&
                 (!request("konfirmasi_ket") || strrpos(strtolower($item['konfirmasi_ket']), strtolower(request("konfirmasi_ket"))) > -1)&&
                 (!request("konfirmasi_bank") || strrpos(strtolower($item['konfirmasi_bank']), strtolower(request("konfirmasi_bank"))) > -1)&&
                 (!request("konfirmasi_rek") || strrpos(strtolower($item['konfirmasi_rek']), strtolower(request("konfirmasi_rek"))) > -1)&&
                 (!request("konfirmasi_an") || strrpos(strtolower($item['konfirmasi_an']), strtolower(request("konfirmasi_an"))) > -1)&&
                 (!request("konfirmasi_tujuan") || strtolower($item['konfirmasi_tujuan']== strtolower(request("konfirmasi_tujuan"))))
                 ){

                $tgl = strtotime(str_replace("/","-",$item['konfirmasi_tgl']));
                if (request("filter_awal") && request("filter_akhir")){
                    $filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
                    $filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
                    if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
                        array_push($data, $item);                     
                    }
                }else if (request("filter_awal")){
                    $filter_awal = strtotime(request("filter_awal"));
                    if ($filter_awal<=$tgl){
                        array_push($data, $item);                     
                    }
                }else if (request("filter_akhir")){
                    $filter_akhir = strtotime(request("filter_akhir"));
                    if ($filter_akhir>=$tgl){
                        array_push($data, $item);                     
                    }
                }else{
                    array_push($data, $item);
                }
            }
        }

        return json_encode($data);
    }

    public function get_pembayaran($id){
        $result = DB::table('tb_spk_pembayaran')
            ->where("spkp_spk",$id)->get();

        $data = array();
        foreach($result as $r){
            $item = array();
            $item['spkp_tgl'] = date_format(date_create($r->spkp_tgl),"d/m/Y");
            $item['spkp_ket'] = $r->spkp_ket;
            $item['spkp_jumlah'] = $r->spkp_jumlah;
            array_push($data, $item);
        }
        return json_encode($data);
    }

    function generate_kode(){
        $data = DB::table('tb_spk_pembayaran')->select("spkp_id")->orderBy("spkp_id","DESC")->first();
        if (count($data)>0){
            $tahun = substr($data->spkp_id,2,2);
            $no = substr($data->spkp_id,5,5);
            if ($tahun != date("y")){
                return "BP".date("y")."-00001";
            }else{
                $no++;
                if (strlen($no)==1){
                    return "BP".$tahun."-0000".$no;
                }else if (strlen($no)==2){
                    return "BP".$tahun."-000".$no;
                }else if (strlen($no)==3){
                    return "BP".$tahun."-00".$no;
                }else if (strlen($no)==4){
                    return "BP".$tahun."-0".$no;
                }else{
                    return "BP".$tahun."-".$no;
                }
            }
        }

        return "BP".date("y")."-00001";
    }

    function store(){
        $validator =Validator::make(request()->all(), [
             "spkp_id"      => "required",
             "spkp_spk"      => "required",
             "spkp_tgl"     => "required",
             "spkp_jumlah"     => "required",
             "spkp_akun"     => "required",
         ]);

        $proses['result'] = 0;
    	$proses['msg'] = "";

    	 if ($validator->fails()) {
    	 	//$proses['msg'] = $validator->messages();
            $proses['msg'] = "Data yang diisi belum lengkap atau salah format!";
    	 } else {
            $jumlah = str_replace(".","",request("spkp_jumlah"));
            $check = DB::table("tb_spk_pembayaran")->where("spkp_spk", request("spkp_spk"))->get();
            
            $proses['tgl'] = date("Y-m-d H:i:s");
            $proses['statusPembayaran'] = 0;

            if (count($check) > 0) {
                $total = 0;

                foreach ($check as $pembayaran) {
                    $total += $pembayaran->spkp_jumlah;
                }

                if ($total < 3500000) {
                    if (($total + $jumlah) >= 3500000) {
                        $proses['statusPembayaran'] = 1;
                    }
                }
            } else {
                if ($jumlah >= 3500000) {
                    $proses['statusPembayaran'] = 1;
                }
            }

            $insert = DB::table('tb_spk_pembayaran')->insert([
                "spkp_id"     => request("spkp_id"),
                "spkp_spk"		=> request("spkp_spk"),
                "spkp_tgl"		=> date_format(date_create(str_replace("/","-", request("spkp_tgl"))), "Y-m-d"),
                "spkp_jumlah"   => str_replace(".","",request("spkp_jumlah")),
                "spkp_akun"		=> request("spkp_akun"),
                "spkp_ket"      => strtoupper(request("spkp_ket"))
            ]);

            $result =  DB::table('tb_spk_pembayaran')
            	->where("spkp_id", request("spkp_id"))
            	->first();

            if (count($result)>0){
            	$proses['result'] = 1;

                if (strlen(request("konfirmasi_id"))>0){
                    DB::table('tb_konfirmasi_pembayaran')->where("konfirmasi_id",request("konfirmasi_id"))->delete();
                }
            } else {
            	$proses['result'] = 0;
                $proses['msg'] = "Pembayaran Gagal Disimpan!";
            }

            $proses['result'] = 1;
    	 }

    	return json_encode($proses);  
    }

    public function getSPK($id = null) {
    	$result = json_decode((new ApiSpkController)->getSPK());


    	$data = array();
    	foreach ($result as $r) {

            if (($r->pemesan->spk_harga - $r->pemesan->diskon) - ($r->pemesan->bayar + $r->pemesan->spkl_droping) == 0){
                continue;
            }

    		if ($r->pemesan->spk_pajak == 0) {
    			$r->pemesan->spk_pajak = 'TIDAK ADA';
    		} else {
    			$r->pemesan->spk_pajak = 'ADA';
    		}

            if ($r->pemesan->spk_ket_harga == 0) {
                $r->pemesan->spk_ket_harga = 'ON THE ROAD';
            } else {
                $r->pemesan->spk_ket_harga = 'OF THE ROAD';
            }

            if ($r->pemesan->spk_pembayaran == 0) {
                $r->pemesan->spk_pembayaran = 'CASH';
            } else {
                $r->pemesan->spk_pembayaran = 'CREDIT';
            }

            if (!is_null($id)){
                if ($r->pemesan->spk_id == $id){
                    return json_encode($r);
                }
            }else{
        		array_push($data, $r);
            }
    	}

    	return json_encode($data);
    }
}