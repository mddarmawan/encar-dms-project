<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbSpkNoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tb_spk_no', function (Blueprint $table) {
            $table->increments('spkNo_id');
            $table->integer('spkNo_min',false,11);
            $table->integer('spkNo_max',false,11);
            $table->integer('spkNo_sales',false,11);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tb_spk_no');
    }
}
