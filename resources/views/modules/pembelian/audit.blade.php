<style>
    #progress-wrp {
        border: 1px solid #0099CC;
        padding: 1px;
        position: relative;
        height: 30px;
        border-radius: 3px;
        margin: 10px;
        text-align: left;
        background: #fff;
        box-shadow: inset 1px 3px 6px rgba(0, 0, 0, 0.12);
    }
    #progress-wrp .progress-bar{
        height: 100%;
        border-radius: 3px;
        background-color: #f39ac7;
        width: 0;
        box-shadow: inset 1px 1px 10px rgba(0, 0, 0, 0.11);
    }
    #progress-wrp .status{
        top:3px;
        left:50%;
        position:absolute;
        display:inline-block;
        color: #000000;
    }
</style>

<center>
    <input type="file" name="">
    <a class="btn btn-primary"><i class="fa fa-eye"></i> Preview</a>
    <br>
    <div id="progress-wrp">
        <div class="progress-bar"></div>
        <div class="status">0%</div>
    </div>
    <h5 style="float: left; margin: 20px;">Data Audit :</h5>
    <table style="margin-rigth: 10px; width: 100%;">
        <thead>
            <tr>
        		<th colspan='9' style="">Preview Data</th>
        	</tr>
            <tr>
                <th>No</th>
                <th>DH</th>
                <th>Tanggal</th>
                <th>RRN</th>
                <th>Vendor</th>
                <th>No. Mesin</th>
                <th>No. Kendaraan</th>
                <th>Auto Matching</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody id="table_audit">
        </tbody>
    </table>
</center>