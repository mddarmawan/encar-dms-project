<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Diskon;
use App\DiskonAks;
use App\DiskonRef;
use App\Aksesoris;

class ApiDiskonController extends Controller
{
    function index(){
    	$diskon = DB::table('tb_spk')
    		->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
    		->join("tb_spk_diskon","tb_spk_diskon.spkd_spk","=","tb_spk.spk_id")    		
    		->join("tb_sales","tb_sales.sales_id","=","tb_spk.spk_sales")
    		->join("tb_karyawan","tb_karyawan.karyawan_id","=","tb_sales.sales_karyawan")
    		->get();
    	$data = array();
    	foreach($diskon as $d){
    		$item = array();
    		$item['spk_diskon'] = $d->spkd_id;
    		$item['spk_id'] = $d->spk_id;
    		$item['spk_tgl'] = date_format(date_create($d->spk_tgl),"d/m/Y");
    		$item['spk_pel_nama'] = $d->spk_pel_nama;
    		$item['spk_sales'] = $d->karyawan_nama;
    		$item['spk_type'] = $d->variant_nama;
            if($d->spkd_ref_nama == NULL){
                $item['spk_ref'] = "Tidak Ada";
            }else{
                $item['spk_ref'] = $d->spkd_ref_nama;            
            }
    		$item['spk_ket'] = $d->spkd_ket;
    		$item['spk_cashback'] = number_format($d->spkd_cashback,0,',','.');
    		$item['spk_komisi'] = number_format($d->spkd_komisi,0,',','.');
    		$item['spk_aksesoris'] = DB::table('tb_spk_aksesoris')->where("spka_diskon",$d->spkd_id)->get();
    		$aksesoris=0;
    		foreach($item['spk_aksesoris'] as $a){
    			$aksesoris+=$a->spka_harga;
    		}
    		$item['spk_taksesoris'] = number_format($aksesoris,0,',','.');
    		$item['spk_total'] = number_format($d->spkd_cashback + $aksesoris,0,',','.');
    		array_push($data, $item);
    	}
    	return json_encode($data);
    }


    function referral(){
    	$ref = DB::table('tb_spk')    		
    		->join("tb_spk_diskon","tb_spk_diskon.spkd_spk","=","tb_spk.spk_id")    		
    		->join("tb_referral","tb_referral.referral_id","=","tb_spk_diskon.spkd_referral")
    		->join("tb_sales","tb_sales.sales_id","=","tb_spk.spk_sales")
    		->join("tb_karyawan","tb_karyawan.karyawan_id","=","tb_sales.sales_karyawan")
    		->where("spk_do","!=",null)
    		->where("spkd_status",1)
    		->whereRaw("NOT EXISTS(SELECT * FROM tb_spk_referral WHERE tb_spk_diskon.spkd_id = tb_spk_referral.spkr_diskon)")->get();
    	$data = array();
    	foreach($ref as $d){
    		$item = array();
    		$item['spkd_id'] = $d->spkd_id;
    		$item['spk_id'] = $d->spk_id;
    		$item['spk_tgl'] = date_format(date_create($d->spk_tgl),"d/m/Y");
    		$item['spk_pel_nama'] = $d->spk_pel_nama;
    		$item['sales_nama'] = $d->karyawan_nama;
    		$item['spkd_komisi'] = number_format($d->spkd_komisi,0,',','.');
    		$item['referral_nama'] = $d->referral_nama;
    		$item['referral_telp'] = $d->referral_telp;
    		$item['referral_info'] = $d->referral_bank." - ".$d->referral_rek."<br/>".$d->referral_an;
    		array_push($data, $item);
    	}
    	return json_encode($data);
    }


    function riwayat_referral(){
    	$ref = DB::table('tb_spk')    		
    		->join("tb_spk_diskon","tb_spk_diskon.spkd_spk","=","tb_spk.spk_id")    		
    		->join("tb_spk_referral","tb_spk_referral.spkr_diskon","=","tb_spk_diskon.spkd_id")    		
    		->join("tb_referral","tb_referral.referral_id","=","tb_spk_diskon.spkd_referral") 
    		->join("tb_sales","tb_sales.sales_id","=","tb_spk.spk_sales")
    		->join("tb_karyawan","tb_karyawan.karyawan_id","=","tb_sales.sales_karyawan")
    		->where("spkd_status",1)->get();
    	$data = array();
    	foreach($ref as $d){
    		$item = array();
    		$item['spkd_id'] = $d->spkd_id;
    		$item['spk_id'] = $d->spk_id;
    		$item['spk_tgl'] = date_format(date_create($d->spk_tgl),"d/m/Y");
    		$item['spk_pel_nama'] = $d->spk_pel_nama;
    		$item['sales_nama'] = $d->karyawan_nama;
    		$item['spkd_komisi'] = number_format($d->spkd_komisi,0,',','.');
    		$item['referral_nama'] = $d->referral_nama;
    		$item['referral_telp'] = $d->referral_telp;
    		$item['spkr_tgl'] = date_format(date_create($d->spkr_tgl),"d/m/Y");
    		array_push($data, $item);
    	}
    	return json_encode($data);
    }


    function req($id = NULL){
        if ($id == NULL) {
        	$diskon = DB::table('tb_spk')
        		->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
        		->join("tb_spk_diskon","tb_spk_diskon.spkd_spk","=","tb_spk.spk_id")    		
        		->join("tb_sales","tb_sales.sales_id","=","tb_spk.spk_sales")
        		->join("tb_karyawan","tb_karyawan.karyawan_id","=","tb_sales.sales_karyawan")
        		->where("spkd_status","=", 9)->get();
        	$data = array();
        	foreach($diskon as $d){
        		$item = array();
                $item['sales_salesUid'] = $d->sales_salesUid;
        		$item['spk_diskon'] = $d->spkd_id;
        		$item['spk_id'] = $d->spk_id;
        		$item['spk_tgl'] = $d->spk_tgl;
        		$item['spk_pel_nama'] = $d->spk_pel_nama;
        		$item['spk_sales'] = $d->karyawan_nama;
        		$item['spk_type'] = $d->variant_nama;
        		$item['spk_type_id'] = $d->variant_type;
        		$item['spk_ref'] = $d->spkd_ref_nama;
        		$item['spk_ket'] = $d->spkd_ket;
        		$item['spk_cashback'] = number_format($d->spkd_cashback,0,',','.');
        		$item['spk_komisi'] = number_format($d->spkd_komisi,0,',','.');
        		$item['spk_aksesoris'] = DB::table('tb_spk_aksesoris')->where("spka_diskon",$d->spkd_id)->get();
        		$aksesoris=0;
        		foreach($item['spk_aksesoris'] as $a){
        			$aksesoris+=$a->spka_harga;
        		}
        		$item['spk_taksesoris'] = number_format($aksesoris,0,',','.');
        		$item['spk_total'] = number_format($d->spkd_cashback + $d->spkd_komisi + $aksesoris,0,',','.');
        		array_push($data, $item);
        	}
        } else {
            $diskon = DB::table('tb_spk')
                ->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
                ->join("tb_spk_diskon","tb_spk_diskon.spkd_spk","=","tb_spk.spk_id")            
                ->join("tb_sales","tb_sales.sales_id","=","tb_spk.spk_sales")
                ->join("tb_karyawan","tb_karyawan.karyawan_id","=","tb_sales.sales_karyawan")
                ->where("spkd_status","=", 99)
                ->where("spk_id", $id)->get();
            $data = array();
            foreach($diskon as $d){
                $item = array();
                $item['sales_salesUid'] = $d->sales_salesUid;
                $item['spk_diskon'] = $d->spkd_id;
                $item['spk_id'] = $d->spk_id;
                $item['spk_tgl'] = $d->spk_tgl;
                $item['spk_pel_nama'] = $d->spk_pel_nama;
                $item['spk_sales'] = $d->karyawan_nama;
                $item['spk_type'] = $d->variant_nama;
                $item['spk_type_id'] = $d->variant_type;
                $item['spk_ref'] = $d->spkd_ref_nama;
                $item['spk_ket'] = $d->spkd_ket;
                $item['spk_cashback'] = number_format($d->spkd_cashback,0,',','.');
                $item['spk_komisi'] = number_format($d->spkd_komisi,0,',','.');
                $item['spk_aksesoris'] = DB::table('tb_spk_aksesoris')->where("spka_diskon",$d->spkd_id)->get();
                $aksesoris=0;
                foreach($item['spk_aksesoris'] as $a){
                    $aksesoris+=$a->spka_harga;
                }
                $item['spk_taksesoris'] = number_format($aksesoris,0,',','.');
                $item['spk_total'] = number_format($d->spkd_cashback + $d->spkd_komisi + $aksesoris,0,',','.');
                array_push($data, $item);
            }
        }
    	return json_encode($data);
    }



    function status(){
    	$data = DB::table('tb_referral')
    		->leftjoin("vw_referral_status",'vw_referral_status.spkd_referral', '=', 'tb_referral.referral_id')
	        ->select(DB::raw('spkd_jumlah as referral_jumlah, referral_nama'))->get();
    	
    	return json_encode($data);
    }

    function app(){
    	$this->validate(request(), [
            "diskon_id"      	=> "required",
            "diskon_status"      => "required"
        ]);

        try {
            $update = DB::table('tb_spk_diskon')->where("spkd_id",request("diskon_id"))->update([
                "spkd_cashback"         =>  request("diskon_cashback"),
                "spkd_komisi"           =>  request("diskon_komisi"),
                "spkd_status"           =>  request("diskon_status"),
                "spkd_catatan"          =>  request("diskon_catatan"),
                ]);

            DB::table('tb_spk_aksesoris')->where("spka_diskon",request("diskon_id"))->delete();

            foreach (json_decode(request("diskon")) as $key => $value) {
                $this->insert_aksesoris(request("diskon_id"), $value);
            }

            return 1;
        } catch (Exception $e) {
            return 0;
        }
    }


    function konfirmasi(){
    	$this->validate(request(), [
            "id"      	=> "required",
            "tgl"      => "required"
        ]);

	    $insert = DB::table('tb_spk_referral')->insert([
    	        "spkr_diskon"     	=>  request("id"),
    	        "spkr_tgl"     		=>  date_format(date_create(request("tgl")),'Y-m-d'),
    	        "spkr_ket"     		=>  request("ket"),
                "created_at"    => date("Y-m-d H:i:s"),
                "updated_at"    => date("Y-m-d H:i:s")
            ]);

	    return redirect("/referral");
    }

    function aksesoris($type){
    	$data = DB::table('tb_aksesoris')->where("aksesoris_kendaraan",$type)->orWhere("aksesoris_kendaraan",NULL)->orWhere("aksesoris_kendaraan",0)->get();

    	return json_decode($data);
    }

    private function insert_aksesoris($id,$kode){
    	if ($kode == "0"){
    		return false;
    	}
    	$data = DB::table('tb_aksesoris')->where("aksesoris_kode",$kode)->first();
    	
    	if (!is_null($data)){
    		DB::table('tb_spk_aksesoris')->insert([
	            "spka_diskon"     =>  $id,
	            "spka_kode"     =>  $kode,
	            "spka_nama"    =>  $data->aksesoris_nama,
	            "spka_harga"   =>  $data->aksesoris_harga,
                "created_at"    => date("Y-m-d H:i:s"),
                "updated_at"    => date("Y-m-d H:i:s")
	        ]);
    	}
    }
}
