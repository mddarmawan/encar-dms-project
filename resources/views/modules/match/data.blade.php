@extends('layout')

@section('content')

	<div class="content-header">
		<h6>
			<small>Penjualan</small>
			Matching Unit
		</h6>
        @include("modules.match.nav")
        <ul class="header-tools right">
            <li><a href="javascript:;" onclick="loadData();" class="chip"><i class="fa fa-refresh"></i> Refresh</a></li>
        </ul>
	</div>

<div class="wrapper">
	<div id="dataStock">

	</div>	
</div>



<div id="matching" class="modal"  style="width:800px;">  
    <h6 class="modal-title blue-grey darken-1">
        Matching Unit
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:0; position: relative;">
        <div class="row" style="margin:0">
            <div class="col m6" style="border-right:1px solid #ddd">
                <table class="info payment">
                    <tr>
                        <td width="120px">No DH</td>
                        <td width="10px">:</td>
                        <td><input type="text" id="dh" readonly="" /></td>
                    </tr>
                    <tr>
                        <td width="120px">ID</td>
                        <td width="10px">:</td>
                        <td><input type="text" id="id"   readonly="" /></td>
                    </tr>
                    <tr>
                        <td width="120px">Type</td>
                        <td width="10px">:</td>
                        <td><input type="text" id="type" readonly="" /></td>
                    </tr>
                    <tr>
                        <td width="120px">Variant</td>
                        <td width="10px">:</td>
                        <td><input type="text" id="variant"   readonly="" /></td>
                    </tr>
                    <tr>
                        <td width="120px">Warna</td>
                        <td width="10px">:</td>
                        <td><input type="text" id="warna" readonly="" /></td>
                    </tr>
                    <tr>
                        <td width="120px">No Rangka</td>
                        <td width="10px">:</td>
                        <td><input type="text" id="rangka"   readonly="" /></td>
                    </tr>
                    <tr>
                        <td width="120px">No Mesin</td>
                        <td width="10px">:</td>
                        <td><input type="text" id="mesin" readonly="" /></td>
                    </tr>
                    <tr>
                        <td width="120px">Tahun Produksi</td>
                        <td width="10px">:</td>
                        <td><input type="text" id="tahun"  readonly="" /></td>
                    </tr>
                    <tr>
                        <td width="120px">No Invoice</td>
                        <td width="10px">:</td>
                        <td><input type="text" id="invoice"   readonly="" /></td>
                    </tr>
                    <tr>
                        <td width="120px">Tgl. Invoice</td>
                        <td width="10px">:</td>
                        <td><input type="text"  id="invoice_tgl"readonly="" /></td>
                    </tr>
                    <tr>
                        <td width="120px">Tanggal Stock</td>
                        <td width="10px">:</td>
                        <td><input type="text" id="tgl_masuk"  readonly="" /></td>
                    </tr>
                </table>
            </div>
            <div class="col m6" >
                                    <table class="info payment">
                                        <tr>
                                            <td width="120px">Tanggal SPK</td>
                                            <td width="10px">:</td>
                                            <td><input type="text" id="spk_tgl" readonly="" /></td>
                                        </tr>
                                        <tr>
                                            <td width="120px">No SPK</td>
                                            <td width="10px">:</td>
                                            <td><input type="text" id="spk_no" readonly="" /></td>
                                        </tr>
                                        <tr>
                                            <td width="120px">Nama Pemesan</td>
                                            <td width="10px">:</td>
                                            <td><input type="text"  id="spk_pel_nama" readonly="" /></td>
                                        </tr>
                                        <tr>
                                            <td width="120px">Sales</td>
                                            <td width="10px">:</td>
                                            <td><input type="text"  id="spk_sales" readonly="" /></td>
                                        </tr>
                                        <tr>
                                            <td width="120px">Kota</td>
                                            <td width="10px">:</td>
                                            <td><input type="text" id="spk_kota" readonly="" /></td>
                                        </tr>
                                        <tr>
                                            <td width="180px">Tanggal Matching</td>
                                            <td width="10px">:</td>
                                            <td><input type="text" id="match" readonly /></td>
                                        </tr>
                                        <tr>
                                            <td width="180px">Jatuh Tempo</td>
                                            <td width="10px">:</td>
                                            <td><input type="text" id="tempo" readonly /></td>
                                        </tr>
                                        <tr>
                                            <td width="180px">Tambahan Waktu</td>
                                            <td width="10px">:</td>
                                            <td>
												<select id="added" class="pull-left" style="width:75px;height:24.41px;">
													<?php
														for($i=0;$i<7;$i++){
													?>
														<option value="<?php echo $i ?>"><?php echo $i ?> HARI</option>
													<?php } ?>
												</select>
												<input type="text" id="date_added" class="pull-left" style="width:109px;" readonly />
											</td>
                                        </tr>
                                    </table>
            </div>
        </div>
        
        <div style="padding:10px;text-align:right;background:#f5f5f5">
            <a class="waves-effect waves-light btn save"><i class="material-icons left">save</i>Simpan</a>
        </div>
    </div>
</div>

<script>
	$("#added").change(function(){
		var day = parseInt($(this).val());
		var myDate = $("#tempo").val();
		$("#date_added").val("");
			$.ajax({
                type: "GET",
                url: "{{url('/api/matching/addtempo')}}",
                data: {date:myDate, days:day},
				dataType:"json"
            }).done(function(data){
                $("#date_added").val(data); 
            });
	});
	
    $(".save").click(function(){
        var item = {
            spk_id:$("#spk_no").val(),
            spk_waktu_match:$("#date_added").val(),
            _token:'{{csrf_token()}}'
        }

        if ($("#spk_no").val() != "" && $("#date_added").val()!=""){

            $.ajax({
                type: "PUT",
                url: "{{url('/api/matching')}}",
                data: item
            }).fail(function(response) {
                alert("ERR-42 Matching gagal disimpan!, silahkan hubungi administrator");
                console.log(response);
            }).done(function(response){
                if (response==1){
                    loadData();
                    $("#matching").modal("close");
					document.getElementById("added").selectedIndex = 0;
					$("#date_added").val("");
                }else{
                    alert("ERR-00 Matching gagal disimpan!, silahkan hubungi administrator");
                }
            }); 
        }else{
            alert("SPK belum dipilih !");
        }
    });

    function loadData(){
    	var db = {
            loadData: function(filter) {
                    return $.ajax({
                        type: "GET",
                        url: "{{url('api/matching')}}",
                        data: filter
                    });
                },
        };
		
		function set_modal(item){			
			$("#dh").val(item.stock_dh);
			$("#id").val(item.trk_serial);
			$("#type").val(item.trk_type);
			$("#variant").val(item.stock_type);
			$("#warna").val(item.stock_warna);
			$("#rangka").val(item.stock_rangka);
			$("#mesin").val(item.stock_mesin);
			$("#tgl_masuk").val(item.trk_stock);
			$("#tahun").val(item.trk_tahun);
			$("#invoice").val(item.trk_invoice);
			$("#invoice_tgl").val(item.stock_tgl);
			$("#spk_tgl").val(item.spk_tgl);
			$("#spk_no").val(item.spk_id);
			$("#spk_pel_nama").val(item.spk_pel_nama);
			$("#spk_sales").val(item.spk_sales);
			$("#spk_kota").val(item.spk_kota);
			$("#tempo").val(item.stock_tempo);
			$("#match").val(item.stock_match);  

            $("#matching").modal("open");
		}

        $("#dataStock").jsGrid({
            height: "100%",
            width: "100%",
     
            sorting: true,
            filtering: true,
            autoload: true,
            noDataContent: "Tidak Ada Data",
			rowDoubleClick:function(data){
				var item = data.item;
				set_modal(item);
			},
            deleteConfirm: "Anda yakin akan menghapus data ini?",
     
            controller: db,
     
            fields: [
                { name: "stock_tgl", title:"Tanggal", type: "text", width: 80, align:"center" },
                { name: "stock_dh", title:"NO DH", type: "text", width: 80, align:"center" },
                { name: "stock_id", title:"ID", type: "text", width: 100 },
                { name: "stock_type", title:"Type", type: "text", width: 170 },
                { name: "stock_warna", title:"Warna", type: "text", width: 110},
                { name: "stock_rangka", title:"No Rangka", type: "text", width: 140},
                { name: "stock_mesin", title:"No Mesin", type: "text", width: 100},
                { name: "spk_id", title:"NO SPK", type: "text", width: 80, align:"center" },
                { name: "spk_pel_nama", title:"Nama Pemesan", type: "text", width: 120 },
                { name: "spk_sales", title:"Sales", type: "text", width: 120 },
                { name: "spk_kota", title:"Kota", type: "text", width: 80 },
                { name: "stock_match", title:"Tgl. Match", type: "text", width: 90, align:"center"},
                { name: "stock_tempo", title:"Jth. Tempo", type: "text", width: 90, align:"center"},
            ]
        });
    }


loadData(); 

</script>
@endsection