@extends('layout')

@section('content')

	<div class="content-header">
		<h6>
			<small>Penjualan</small>
			Batal SPK
		</h6>
	</div>		
<div id="data_grid" class="wrapper">
		<div class="nav-wrapper">
			<div class="nav-right">
				<i style="margin-right: 5px" class="fa fa-filter" aria-hidden="true"></i>
				<span class="bold" style="font-size: 13px"> Periode :</span>
				<div class="input-group">
					<input type="text" class="input-filter datepicker" id="filter_awal" placeholder="Tanggal Awal" required="" />
					<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
				</div>
				<span class="bold" style="font-size: 13px">s.d</span>
				<div class="input-group">
					<input type="text" class="input-filter datepicker"  id="filter_akhir" placeholder="Tanggal Akhir"  required="" />
					<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
				</div>
				<a class="waves-effect waves-light btn-flat btn-filter" id="filter">Cari</a>
				<a class="waves-effect waves-light btn-flat btn-filter" id="reset">Reset</a>
				<a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
			</div>
		</div>
		<div id="dataPenjualan">

		</div>	
</div>

@include("modules.penjualan.detail")

<script>

	function detail(id){
		$("#spk_id").html(id);

		$.ajax({
			type: "GET",
			url: "/api/penjualan/"+id
		}).done(function(json) {
		 	var pemesan = json.pemesan;
		 	var kendaraan = json.kendaraan;
		 	var leasing = json.leasing;
		 	
			 if(pemesan.spk_status==10){
			 	$("#konfirmasi").hide();
			 } else {
			 	$("#konfirmasi").show();
			 }

			 if(pemesan.spk_pembayaran == 0){
			 	$('#spk_metode').html("CASH");
			 	$(".leasing").hide();
			 }else{
			 	$('#spk_metode').html("CREDIT");
			 	$(".leasing").show();
			 }

		 	if (pemesan.spk_ppn===1){
		 		$("#spk_ppn").html("YA");
		 	}
		 	if (pemesan.spk_pajak===1){
		 		$("#spk_pajak").html("DIMINTA");
		 	}else{
		 		$("#spk_pajak").html("TIDAK DIMINTA");
		 	}
		 	$("#spk_tgl").html(date_format(pemesan.spk_tgl));
		 	$("#spk_sales").html(pemesan.karyawan_nama + " / " +pemesan.team_nama );
		 	$("#pel_nama").html(pemesan.spk_pel_nama);
		 	$("#pel_identitas").html(pemesan.spk_pel_identitas);
		 	$("#pel_alamat").html(pemesan.spk_pel_alamat);
		 	$("#pel_pos").html(pemesan.spk_pel_pos);
		 	$("#pel_telp").html(pemesan.spk_pel_telp);
		 	$("#pel_ponsel").html(pemesan.spk_pel_ponsel);
		 	$("#pel_email").html(pemesan.spk_pel_email);
		 	$("#pel_kategori").html(pemesan.spk_pel_kategori);
		 	$("#spk_npwp").html(pemesan.spk_npwp);
		 	$("#spk_fleet").html(pemesan.spk_fleet);
		 	$("#spk_ket_permintaan").html(pemesan.spk_ket);
		 	$("#spk_ket_cancel").html(pemesan.spk_ket_cancel);
		 	$("#spk_catt_cancel").html(pemesan.spk_catt_cancel);
		 	$("#spk_stnk_nama").html(pemesan.spk_stnk_nama);
		 	$("#spk_stnk_alamat").html(pemesan.spk_stnk_alamat);
		 	$("#spk_stnk_pos").html(pemesan.spk_stnk_pos);
		 	$("#spk_stnk_alamatd").html(pemesan.spk_stnk_alamatd);
		 	$("#spk_stnk_posd").html(pemesan.spk_stnk_posd);
		 	$("#spk_stnk_telp").html(pemesan.spk_stnk_telp);	
		 	$("#spk_stnk_ponsel").html(pemesan.spk_stnk_ponsel);
		 	$("#spk_stnk_email").html(pemesan.spk_stnk_email);
		 	$("#spk_stnk_identitas").html(pemesan.spk_stnk_identitas);
		 	$("#variant_nama").html(pemesan.type_nama+ " " +pemesan.variant_nama);
		 	$("#spk_warna").html(pemesan.warna_nama);
		 	$("#variant_id").html(pemesan.variant_serial);
		 	$("#variant_ket").html(pemesan.variant_ket);
		 	$("#spk_pel_fotoid").html("<img height='150px' src='http://localhost:8000/storage/data/identitas/"+pemesan.spk_pel_fotoid+"'/>");
		 	$("#spk_stnk_fotoid").html("<img height='150px' src='http://localhost:8000/storage/data/identitas/"+pemesan.spk_stnk_fotoid+"'/>");

		 	$("#spkt_stck").val(pemesan.spk_catt_permintaan);

		 	//leasing

		 	$("#leasing").html(leasing.leasing_nama);
		 	$("#angsuran").html(number_format(leasing.spkl_angsuran));
		 	$("#dp").html(number_format(leasing.spkl_dp));

		 	 if(pemesan.spk_ket_harga == 0){
			 	$('#harga').html("ON THE ROAD");
			 	
			 }else{
			 	$('#harga').html("OFF THE ROAD");
			 }

		 	if (kendaraan!=null){
				$("#kendaraan .no-data").removeClass("show");
			 	$("#trk_dh").html(kendaraan.trk_dh);
			 	$("#trk_mesin").html(kendaraan.trk_mesin);
			 	$("#trk_rangka").html(kendaraan.trk_rangka);
			 	$("#trk_warna").html(kendaraan.warna_nama);		 		
		 	}else{
				$("#kendaraan .no-data").addClass("show");
			 	$("#trk_dh").html('');
			 	$("#trk_mesin").html('');
			 	$("#trk_rangka").html('');
			 	$("#trk_warna").html('');
		 	}

		 	if(pemesan.spk_kategori==1){	
		 		$("#spk_kategori").html("ON THE ROAD");
		 	}else{
		 		$("#spk_kategori").html("OFF THE ROAD");
		 	}
			
            $("#detail").modal("open");

		});
	};
	

$("#filter").click(function(e){
	e.preventDefault();
	loadData();
});

$("#reset").click(function(e){
	e.preventDefault();
	$("#filter_awal").val("");
	$("#filter_akhir").val("");
	status="";
	$(".tab li").removeClass("active");
	$(".tab li a[data-id='']").parent().addClass("active");
	loadData();
});


$("#export").click('click', function (event) {
    var args = [$('#dataPenjualan'), 'DATA_PENJUALAN_<?php echo date('dmY') ?>.xls'];  
    exportTableToExcel.apply(this, args);
});

var status="";
function loadData() {
	var db = {
		loadData: function(filter) {
			var filter_awal = $("#filter_awal").val().trim();
			var filter_akhir = $("#filter_akhir").val().trim();
			if (filter_awal != ""){
				filter['filter_awal'] = filter_awal;
			}
			if (filter_akhir != ""){
				filter['filter_akhir'] = filter_akhir;
			}
			filter['spk_status'] = status;
			return $.ajax({
				type: "GET",
				url: "{{url('api/penjualan')}}",
				data: filter
			});
		}
	};

	 db.via = [
			{
				"via_id": "",
				"via_nama": "",		   
			},
			{
				"via_id": 1,
				"via_nama": "CASH",		   
			},
			{
				"via_id": 2,
				"via_nama": "CREDIT",		   
			},
			
		];

    db.harga = [
			{
				"harga_id": "",
				"harga_nama": "",		   
			},
			{
				"harga_id": 1,
				"harga_nama": "ON-TR",		   
			},
			{
				"harga_id": 2,
				"harga_nama": "OFF-TR",		   
			},
			
		];

    db.status = [
        {
				"status_id": "",
				"status_nama": "",	   
			},
			{
				"status_id": "0",
				"status_nama": "<span class='box-span red'>BELUM LUNAS</span>",		   
			},
			{
				"status_id": "1",
				"status_nama": "<span class='box-span orange'>LUNAS</span>",		   
			},
    ];
	

	$("#dataPenjualan").jsGrid({
		height: "calc(100% - 40px)",
		width: "100%",
 
		sorting: true,
		filtering: true,
		autoload: true,
		paging: true,
		pageSize: 30,
		pageButtonCount: 5,
		noDataContent: "Tidak Ada Data",
 		rowClick:function(data){
            var item = data.item;
            detail(item.spk_id);
        },
		controller: db,
 
		fields: [

			{ name: "spk_status", title:"Status", type: "select", items: db.status, valueField: "status_id", textField: "status_nama", width: 110, align:"center", filtering: false },
            { name: "spkf_tgl", title:"Tanggal Faktur", type: "text", width: 80, align:"center" },
            { name: "spkf_id", title:"No Faktur", type: "text", width: 110, align:"center" },
            { name: "spk_tgl", title:"Tanggal SPK", type: "text", width: 80, align:"center" },
            { name: "spk_id", title:"No SPK", type: "text", width: 80, align:"center" },
            { name: "spk_pel_nama", title:"Nama Pelanggan", type: "text", width: 170},
            { name: "karyawan_nama", title:"Sales", type: "text", width: 100 },
            { name: "team_nama", title:"Team", type: "text", width: 100, align:"center" },

			{ name: "spk_metode", title:"CARA BAYAR", type: "select", width: 90,items: db.via, valueField: "via_id", textField: "via_nama", align:"center" },
            { name: "spk_leasing", title:"Via", type: "text", width: 100 },
            
            { name: "spk_harga", title:"Harga", type: "text", width: 80, align:"right" },
            { name: "spk_potongan", title:"Potongan", type: "text", width: 80, align:"right" },

            { name: "spk_bayar", title:"Bayar", type: "text", width: 80, align:"right" },
            { name: "spk_piutang", title:"Piutang", type: "text", width: 80, align:"right" },
			
		]
	});
}
loadData();
$(".tab li a").click(function(e){
	e.preventDefault();

	status = $(this).data("id");
	
	loadData();
	$(".tab li").removeClass("active");
	$(this).parent().addClass("active");
});
</script>

@endsection