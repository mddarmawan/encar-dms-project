	<ul class="header-tools right">
		<div id="notiv_pdo" class="notiv">
			<b id="count_pdo"></b>
		</div>
		<li><a href="{{url('spk/permintaan_do')}}" class="chip">Permintaan DO</a></li>
	</ul>
	<ul class="header-tools right">
		<div id="notiv_bspk" class="notiv">
			<b id="count_bspk"></b>
		</div>
		<li><a href="{{url('spk/cancel')}}" class="chip">Batal SPK</a></li>
	</ul>
	<ul class="header-tools right">
		<div id="notiv_pspk" class="notiv">
			<b id="count_pspk"></b>
		</div>
		<li><a href="{{url('spk/permintaan_spk')}}" class="chip">Permintaan SPK</a></li>
	</ul>
	<ul class="header-tools right">
		<li><a href="{{url('spk')}}" class="chip">Monitoring SPK</a></li>
	</ul>
	<ul class="header-tools right">
     	<li><a href="{{url('spk/no_spk')}}" class="chip">Pembagian No SPK</a></li>
	</ul>
<script>
	$("#notiv_pdo").hide();
	$("#notiv_bspk").hide();
	$("#notiv_pspk").hide();

function loadNotif() {
	$.ajax({
		type: "GET",
		url: "/api/spk/permintaan_spk/"
	}).done(function(reponse) {
		if (reponse.length > 0) {
			$("#notiv_pspk").show();
			$("#count_pspk").html(reponse.length);
		} else {
			$("#notiv_pspk").hide();
			$("#count_pspk").html(reponse.length);
		}
	});

	$.ajax({
		type: "GET",
		url: "/api/spk/permintaan_do/"
	}).done(function(reponse) {
		if (reponse.length > 0) {
			$("#notiv_pdo").show();
			$("#count_pdo").html(reponse.length);
		} else {
			$("#notiv_pdo").hide();
			$("#count_pdo").html(reponse.length);
		}
	});

	$.ajax({
		type: "GET",
		url: "/api/spk/cancel/"
	}).done(function(reponse) {
		if (reponse.length > 0) {
			$("#notiv_bspk").show();
			$("#count_bspk").html(reponse.length);
		} else {
			$("#notiv_bspk").hide();
			$("#count_bspk").html(reponse.length);
		}
	});

	var  url = $(location).attr("href");
	var param = url.split('/');
	var currentURL = param[param.length-1];

	$(".header-tools li a").each(function(){
		var aURL = $(this).attr("href");
		var aparam = aURL.split('/');
		var currentaURL = aparam[aparam.length-1];

		if(currentURL == currentaURL){
			$(this).addClass("active");
		}
	});
}
</script>