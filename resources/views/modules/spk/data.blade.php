@extends('layout')

@section('content')

	<div class="content-header">
		<h6>
			<small>Penjualan</small>
			Monitoring SPK
		</h6>
		@include("modules.spk.nav")
	</div>

<div id="data_grid" class="wrapper">
		<div class="nav-wrapper">
			<div class="nav-left">
				<ul class="tab">
					<li class="active"><a data-id="" >Semua</a></li>
					<li><a data-id="9">Invalid</a></li>
					<li><a data-id="1">Valid</a></li>
					<li><a data-id="2">Matched</a></li>
				</ul>
			</div>
			<div class="nav-right">
				<i style="margin-right: 5px" class="fa fa-filter" aria-hidden="true"></i>
				<span class="bold" style="font-size: 13px"> Periode :</span>
				<div class="input-group">
					<input type="text" class="input-filter datepicker" id="filter_awal" placeholder="Tanggal Awal" required="" />
					<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
				</div>
				<span class="bold" style="font-size: 13px">s.d</span>
				<div class="input-group">
					<input type="text" class="input-filter datepicker"  id="filter_akhir" placeholder="Tanggal Akhir"  required="" />
					<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
				</div>
				<a class="waves-effect waves-light btn-flat btn-filter" id="filter">Cari</a>
				<a class="waves-effect waves-light btn-flat btn-filter" id="reset">Reset</a>
				<a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
			</div>

			

		</div>

		<div id="dataSPK">

		</div>	

</div>
@include("modules.spk.detail")

<script>
	var total_bayar = 0;
	var piutang = 0;
	var total_aksesoris = 0;
	var total_diskon = 0;
	var total_leasing = 0;
	var spkl_doc_val = 0;
	var hpp = 0;
	var bbn = 0;
	var off = 0;
	var dpp = 0;
	var ppn = 0;

	function clearPembayaran() {
		$("#hpp").val("");
		$("#cashback").val("");
		$("#total").val("");
		$("#total_bayar").val("");
		$("#piutang").val("");
	}

	function setPiutang() {
		cashback = toInt($("#cashback").val());
		hpp = toInt($("#hpp").val());
		total_bayar = toInt($("#total_bayar").val());
		dropping  = ($("#spk_droping").val() != null ? toInt($("#spk_droping").val()) : 0);
		piutang = (hpp - total_bayar - cashback - dropping);
		$("#piutang").val(number_format(piutang));
	}

	function setLeasingTotal() {
		dp  = ($("#spk_dp").val() != null ? toInt($("#spk_dp").val()) : 0);
		dropping  = ($("#spk_droping").val() != null ? toInt($("#spk_droping").val()) : 0);
		total_leasing = dp + dropping;
		$("#spk_tleasing").val(number_format(total_leasing));
	}

	function detail(id){
		$("#spk_id").html(id);
		$("#msg").html('');
		$("#msg_do").hide();
		
		$.ajax({
			type: "GET",
			url: "{{url('api/spk/')}}/"+id
		}).done(function(json) {

			/*
			 * --------------------------------------------------------------------------
			 * Variable-variable yang akan digunakan selanjutnya
			 * --------------------------------------------------------------------------
			 */
			 
			var pemesan = json.pemesan;
			var kendaraan = json.kendaraan;
			var diskon = json.diskon;
			var aksesoris = json.aksesoris;
			var leasing = json.leasing;
			var faktur = json.faktur;
			var bayar = json.bayar;
			var d_leasing = json.d_leasing;
			var d_asuransi = json.d_asuransi;
			var no_faktur = json.no_faktur;

			total_bayar=0;
			piutang = 0;
			total_aksesoris = 0;
			total_diskon = 0;
			total_leasing = 0;
			harga_kendaraan=0;
			hpp = 0;
			on = pemesan.variant_on;
			bbn = pemesan.variant_bbn;
			off = pemesan.variant_on - bbn;

			if (pemesan.spk_bbn != "" && pemesan.spk_bbn != null){
				bbn = pemesan.spk_bbn;
			}

			/*
			 * --------------------------------------------------------------------------
			 * SPK Kategori
			 * --------------------------------------------------------------------------
			 */
			if (pemesan.spk_ket_harga == "0") {
				//$("#spk_jenis_kendaraan").html("ON THE ROAD");
				$("#tr_bbn").show();
				$("#tr_on").show();
				$("#off").attr("readonly", "");
				$("#off").removeClass("number");
				$("#on").val(number_format(pemesan.variant_on));

				hpp = off + bbn;
			} else {
				//$("#spk_jenis_kendaraan").html("OFF THE ROAD");
				$("#tr_bbn").hide();
				$("#tr_on").hide();
				$("#off").removeAttr("readonly");
				$("#on").removeClass("number");

				hpp = off;
			}

			$("#off").val(number_format(off));

			/*
			 * --------------------------------------------------------------------------
			 * SPK Faktur
			 * --------------------------------------------------------------------------
			 */

			if (faktur != null) {
				$("#spk_faktur").val(faktur.spkf_id);
				$("#spk_tanggal").val(date_format(faktur.spkf_tgl));
				if (faktur.spkf_cetak == 0){
					$(".cetak").attr("disabled","true");
				}else if (faktur.spkf_cetak == 1){
					$(".cetak span").html("Cetak Faktur");
					$(".cetak").removeAttr("disabled");
				}
			} else {
				$("#spk_faktur").val(no_faktur);
				$("#pengajuan").removeAttr("disabled");
			}

			/*
			 * --------------------------------------------------------------------------
			 * SPK Tipe dan Status Pajak
			 * --------------------------------------------------------------------------
			 */
			 
			if (pemesan.spk_ppn === 1) {
				$("#spk_ppn").html("YA");
			}

			if (pemesan.spk_pajak === 1) {
				$("#spk_pajak").html("DIMINTA");
			} else {
				$("#spk_pajak").html("TIDAK DIMINTA");
			}

			/*
			 * --------------------------------------------------------------------------
			 * Assign Value pada Tampilan menggunakan data dari Database
			 * --------------------------------------------------------------------------
			 */
			 
			$("#spk_tgl").html(date_format(pemesan.spk_tgl));
			$("#spk_sales").html(pemesan.karyawan_nama + " / " +pemesan.team_nama );
			$("#pel_nama").html(pemesan.spk_pel_nama);
			$("#pel_identitas").html(pemesan.spk_pel_identitas);
			$("#pel_alamat").html(pemesan.spk_pel_alamat);
			$("#pel_pos").html(pemesan.spk_pel_pos);
			$("#pel_telp").html(pemesan.spk_pel_telp);
			$("#pel_ponsel").html(pemesan.spk_pel_ponsel);
			$("#pel_email").html(pemesan.spk_pel_email);
			$("#pel_kategori").html(pemesan.spk_pel_kategori);
			$("#spk_npwp").html(pemesan.spk_npwp);
			$("#spk_fleet").html(pemesan.spk_fleet);
			$("#spk_stnk_nama").html(pemesan.spk_stnk_nama);
			$("#spk_stnk_alamat").html(pemesan.spk_stnk_alamat);
			$("#spk_stnk_pos").html(pemesan.spk_stnk_pos);
			$("#spk_stnk_alamatd").html(pemesan.spk_stnk_alamatd);
			$("#spk_stnk_posd").html(pemesan.spk_stnk_posd);
			$("#spk_stnk_telp").html(pemesan.spk_stnk_telp);	
			$("#spk_stnk_ponsel").html(pemesan.spk_stnk_ponsel);
			$("#spk_stnk_email").html(pemesan.spk_stnk_email);
			$("#spk_stnk_identitas").html(pemesan.spk_stnk_identitas);
			$("#variant_nama").html(pemesan.type_nama+ " " +pemesan.variant_nama);
			$("#spk_warna").html(pemesan.warna_nama);
			$("#variant_id").html(pemesan.variant_serial);
			$("#variant_ket").html(pemesan.variant_ket);

			if (pemesan.spk_bbn == 0) {
				$("#bbn").val(pemesan.variant_bbn);
			} else {
				$("#bbn").val(pemesan.spk_bbn);
			}

			/*
			 * --------------------------------------------------------------------------
			 * SPK Kendaraan
			 * --------------------------------------------------------------------------
			 */
			 
			if (kendaraan != null) {
				$("#kendaraan .no-data").removeClass("show");
				$("#trk_dh").html(kendaraan.trk_dh);
				$("#trk_mesin").html(kendaraan.trk_mesin);
				$("#trk_rangka").html(kendaraan.trk_rangka);
				$("#trk_warna").html(kendaraan.warna_nama);				
			} else {
				$("#kendaraan .no-data").addClass("show");
				$("#trk_dh").html('');
				$("#trk_mesin").html('');
				$("#trk_rangka").html('');
				$("#trk_warna").html('');
			}

			/*
			 * --------------------------------------------------------------------------
			 * SPK Data Diskon
			 * --------------------------------------------------------------------------
			 */
			 
			if (diskon != null) {
				var total = on - diskon.spkd_cashback;

				$("#diskon .no-data").removeClass("show");
				$("#cashback").val(number_format(diskon.spkd_cashback));
				$("#diskon_cashback").html(number_format(diskon.spkd_cashback));
				$("#diskon_komisi").val(number_format(diskon.spkd_komisi));
				$("#komisi").val(number_format(diskon.spkd_komisi));
				$('#aksesoris').html('');
				$("#total").val(number_format(total));

				for (var i in aksesoris) {
					var no = parseInt(i)+1;
					var obj = aksesoris[i];
					total_aksesoris += obj.spka_harga;
					$('#aksesoris').append('<tr class="jsgrid-row"><td class="jsgrid-cell" width="180px" style="padding-left:30px!important">'+ (no) +'. '+ obj.spka_kode +' - '+ obj. spka_nama+'</td><td class="bold jsgrid-cell text-right">'+number_format(obj.spka_harga)+'</td></tr>');
				}

				total_diskon = diskon.spkd_cashback +  total_aksesoris;

				$("#diskon_ket").html(diskon.spkd_ket);
				$("#diskon_total").html(number_format(total_diskon));
				$("#potongan").val(number_format(total_diskon));

				dpp = Math.round((off - total_diskon)/1.1);
				ppn = Math.round(dpp * 0.1);
				hpp = on;

				$("#trk_dpp").val(number_format(dpp));
				$("#trk_ppn").val(number_format(ppn));
			} else {
				$("#diskon .no-data").addClass("show");
				$("#diskon_cashback").html('');
				$("#diskon_komisi").html('');
				$("#komisi").val('');
				$('#aksesoris').html('');
				$("#diskon_ket").html('');
				$("#diskon_total").html('');
				$("#potongan").val('');
				$("#hpp").val("");
				$("#trk_dpp").val("");
				$("#trk_ppn").val("");
				$("#total").val(number_format(off));
			}

			/*
			 * --------------------------------------------------------------------------
			 * SPK Jumlah Pembayaran
			 * --------------------------------------------------------------------------
			 */
			
			$("#hpp").val(number_format(hpp));
			$("#tabel_pembayaran").html('');

			if (bayar != null) {
				for(var i in bayar) {
					var item = bayar[i];
					$("#tabel_pembayaran").append('<tr class="jsgrid-row"><td class="jsgrid-cell">'+date_format(item.spkp_tgl)+'</td><td class="jsgrid-cell text-right">'+number_format(item.spkp_jumlah)+'</td></tr>');
					total_bayar +=item.spkp_jumlah;
				}
			}

			$("#total_bayar").val(number_format(total_bayar));

			/*
			 * --------------------------------------------------------------------------
			 * SPK Pembayaran
			 * --------------------------------------------------------------------------
			 *
			 * Jika SPK Pembayaran 0, CASH. 
			 * Jika SPK Pembayaran 1, CREDIT.
			 *
			 */

			if (pemesan.spk_pembayaran == 0) {
				$('#doc_po').hide();
				$('#spk_metode').html("CASH");
				$('.leasing').hide();
				$("#status_po_row").hide();
			} else {
				$('#doc_po').show();
				$('#spk_metode').html("CREDIT");
				$('.leasing').show();		
				$("#status_po_row").show();		
			}

			/*
			 * --------------------------------------------------------------------------
			 * SPK PO Leasing
			 * --------------------------------------------------------------------------
			 */

			var ls = pemesan.leasing_id;
			var as = '';

			$('#spk_waktu').val(pemesan.spk_waktu);
			$('#spk_leasing').val(pemesan.leasing_nama);

			if (leasing != null) {
				if (leasing.spkl_doc == 1) {
					spkl_doc_val = 1;
					$("#spkl_doc").prop("checked", true);
				} else {
					spkl_doc_val = 0;
					$("#spkl_doc").prop("checked", false);
				}

				ls = leasing.spkl_leasing;
				as = leasing.spkl_jenis_asuransi;

				$('#spk_waktu').val(leasing.spkl_waktu);
				$('#spk_angsuran').val(number_format(leasing.spkl_angsuran));
				$('#spk_dp').val(number_format(leasing.spkl_dp));
				$('#spk_droping').val(number_format(leasing.spkl_droping));
				total_leasing = toInt(leasing.spkl_dp) + toInt(leasing.spkl_droping);
				$('#spk_tleasing').val(number_format(total_leasing));
			} else {
				$("#spkl_doc").prop("checked", false);
				spkl_doc_val = 0;
				$('#spk_waktu').val('');
				$('#spk_angsuran').val('');
				$('#spk_dp').val('');
				$('#spk_droping').val('');
				$('#spk_tleasing').val('');
			}

			/*
			 * --------------------------------------------------------------------------
			 * SPK PO Leasing dropdown
			 * --------------------------------------------------------------------------
			 */

			$('#spk_leasing').html('');
			for (var i in d_leasing) {
				var item = d_leasing[i];
				$('#spk_leasing').append('<option value="'+item.leasing_id+'" '+selected(item.leasing_id,ls)+'>'+item.leasing_nama+'</option>');
			}

			/*
			 * --------------------------------------------------------------------------
			 * SPK Asuransi
			 * --------------------------------------------------------------------------
			 */

			$('#spk_asuransi').html('');
			for (var i in d_asuransi) {
				var item = d_asuransi[i];
				$('#spk_asuransi').append('<option value="'+item.ajenis_id+'" '+selected(item.ajenis_id,as)+'>'+item.ajenis_nama+'</option>');
			}

			/*
			 * --------------------------------------------------------------------------
			 * Status Faktur menggunakan ternary operator.
			 * --------------------------------------------------------------------------
			 *
			 * Contoh: 
			 *
			 * (pemesan.spk_status === "2"	? $("#status_matching").html("done") : "");
			 * if (pemesan.spk_status === "2") {
			 *	$("#status_matching").html("done")
			 * }
			 *
			 */

			$("#status_matching").html('');
			$("#status_diskon").html('');
			$("#status_po").html('');
			$("#status_pelunasan").html('')
			$("#keterangan").html('');

			(kendaraan != null && kendaraan.spk_dh	!= "" && (pemesan.spk_status === "2" || pemesan.spk_status === "3") ? $("#status_matching").html("done") : "");
			(diskon !== null && diskon.spkd_status	=== 1 ? $("#status_diskon").html("done") : "");
			(leasing != null && leasing.spkl_doc 	=== "1"	? $("#status_po").html("done") : "");
			(leasing != null && toInt($("#piutang").val()) 			=== 0	? $("#status_pelunasan").html("done") : "");

			if ((kendaraan != null && kendaraan.spk_dh != "") && (pemesan.spk_status >= 2 || pemesan.spk_status <= 4) && diskon !== null && diskon.spkd_status === 1 && (leasing != null && leasing.spkl_doc === "1") && toInt($("#piutang").val()) === 0) 
			{
				$("#keterangan").html("<p style='color:#e60000; margin: 0; margin-left: 8px;'>Faktur Penjualan Siap Diterbitkan!</p>");
				$("#pengajuan").attr("disabled", "true");
				$("#terbitkan").removeAttr("disabled");
			} else if ((kendaraan != null && kendaraan.spk_dh != "") && pemesan.spk_status === "2" && diskon !== null && diskon.spkd_status === 1 && toInt($("#piutang").val()) === 0)
			{
				$("#pengajuan").removeAttr("disabled");
			} else {
				$("#terbitkan").attr("disabled", "true");
				$("#pengajuan").attr("disabled", "true");
			}

			/*
			 * --------------------------------------------------------------------------
			 * Status Faktur 
			 * --------------------------------------------------------------------------
			 */

			 $("#spk_id_faktur").html(no_faktur);
			 $("#spk_tgl_faktur").html(pemesan.spk_tgl);

			/*
			 * --------------------------------------------------------------------------
			 * Tombol Cetak Faktur
			 * --------------------------------------------------------------------------
			 */

			$(".cetak").unbind().click(function(){
				if (faktur == null) {
					set_request(pemesan);
				} else {
					if (faktur.spkf_cetak == null) {
						set_request(pemesan);
					} else {
						$("#cetak").modal("open");
					}
				}
			});
			$("#detail").modal("open");
			setPiutang();
		});
	};
	
	function set_request(pemesan) {
		var faktur_id = $("#spk_faktur").val();
		$("#faktur_no").html(faktur_id);
		$(".app").click(function(){
			var value = $(this).val();				
			if (value == 1) {
				var data = {
					"spkf_id":faktur_id,
					"spkf_spk":pemesan.spk_id,
					_token:'{{csrf_token()}}'
				};
								
				$.ajax({
					type: "POST",
					dataType:"json",
					url: "{{url('/api/spk/reqfaktur')}}",
					data: data
				}).fail(function(response) {
					alert("ERR-42 Request Cetak Gagal Dikirim !");
				}).done(function(data){
					if (!data.result){
						alert(data.msg);
					}else{
						alert("Permintaan Cetak Faktur [" + faktur + "] telah dikirim, Silahkan menunggu Persetujuan untuk melanjutkan proses Cetak Faktur.");
						$(".cetak").attr("disabled","true");
					}
				});
			}
		});

		$("#request").modal("open");
	}
	
	$("#bbn").change(function(){
		var otr = toInt(string_format($("#on").val()));
		var off = otr - toInt($(this).val());
		var potongan = toInt(string_format($("#potongan").val()));
		var dpp = Math.round((off - potongan) / 1.1);
		var ppn = Math.round(dpp * 0.1);

		$("#off").val(number_format(off));
		$("#trk_dpp").val(number_format(dpp));
		$("#trk_ppn").val(number_format(ppn));
	});

	$('#bbn').keypress(function(e){
		if (e.keyCode == 13) {
			document.getElementById('on').focus();
		}
	});

	$("#spk_dp").change(function(){
		setLeasingTotal();
		setPiutang();
	});

	$('#spk_dp').keypress(function(e){
		if (e.keyCode == 13) {
			document.getElementById('spk_droping').focus();
		}
	});

	$("#spk_droping").change(function(){
		setLeasingTotal();
		setPiutang();
	});

	$('#spk_droping').keypress(function(e){
		if (e.keyCode == 13) {
			document.getElementById('spk_tleasing').focus();
		}
	});

	$('#spk_angsuran').keypress(function(e){
		if (e.keyCode == 13) {
			document.getElementById('spk_dp').focus();
		}
	});

	$('#spk_waktu').keypress(function(e){
		if (e.keyCode == 13) {
			document.getElementById('spk_angsuran').focus();
		}
	});

	$("#konfirmasi").click(function(){
		$("#konfirmasi").modal("open");
	});

	$("#cancel_button").click(function(){
		var value = $("#cancel_button").val();
						
		if (value == 1) {
			var data = {
				"spk_id":$("#spk_id").html(),
				_token:'{{csrf_token()}}'
			};

			$.ajax({
				type: "POST",
				dataType:"json",
				url: "{{url('/api/spk/cancel')}}",
				data: data
			}).fail(function(response) {
				alert("ERR-42 Data Gagal Dibatalkan !");
			}).done(function(data){
				if (!data.result){
					alert(data.msg);
				}else{
					$('#msg').html('Data Dibatalkan !');
				}
			});

			loadData();
		}
	});

	$("#spkl_doc").click(function() {
		if ($("#spkl_doc").is(":checked")) {
			spkl_doc_val = 1;
		} else {
			spkl_doc_val = 0;
		}
	});

	$("#save").click(function(){
		var off_var = toInt($("#off").val());
		var data = {
			"variant_id"	: $("#variant_id").html(),
			"variant_off"	: (off_var != number_format(off) ? off_var : 0),
			"variant_bbn"	: $("#bbn").val(),
			_token			: '{{csrf_token()}}'
		};

		$.ajax({
			type: "POST",
			dataType:"json",
			url: "{{url('/api/spk/variant_update')}}",
			data: data
		}).fail(function(response) {
			alert("ERR-42 Data Gagal Disimpan !");
		}).done(function(response){
			if (!response.result){
				alert(response.result);
			} else {
				if ($("#spk_pembayaran").val('CREDIT')){
					var data = {
						"spkl_spk":$("#spk_id").html(),
						"spkl_leasing":$("#spk_leasing").val(),
						"spkl_asuransi":$("#spk_asuransi").val(),
						"spkl_angsuran":toInt($("#spk_angsuran").val()),
						"spkl_waktu":$("#spk_waktu").val().toUpperCase(),
						"spkl_dp":toInt($("#spk_dp").val()),
						"spkl_droping":toInt($("#spk_droping").val()),
						"spkl_doc":spkl_doc_val,
						_token:'{{csrf_token()}}'
					};

					$.ajax({
						type: "POST",
						dataType:"json",
						url: "{{url('/api/spk/leasing_save')}}",
						data: data
					}).fail(function(response) {
						alert("ERR-42 Data Gagal Disimpan !");
					}).done(function(response){
						if (!response.result){
							alert(response.msg);
						}else{
							$('#msg').html('Tersimpan !');
							loadData();
						}
					});
				} else {
					$('#msg').html('Tersimpan !');
				}
			}
		});
	});

	$("#pengajuan").click(function(){
		var data = {
			"spk_id"	: $("#spk_id").html(),
			"spkf_id"	: $("#spk_id_faktur").html(),
			_token:'{{csrf_token()}}'
		};

		$.ajax({
			type: "POST",
			dataType:"json",
			url: "{{url('/api/spk/pengajuan_do')}}",
			data: data
		}).fail(function(response) {
			alert("ERR-42 Data Gagal Disimpan !");
		}).done(function(response){
			if (!response.result){
				alert(response.msg);
			} else {
				loadNotif();
				$('#pengajuan').attr("disabled", "true");
				$('#msg_do').show();
			}
		});
	});

	$("#terbitkan").click(function(){
		if (piutang != 0) {
			alert("Piutang belum 0 (NOL)\nFaktur Penjualan atas SPK " + $("#spk_id").html()+" belum bisa diterbitkan!");
		}else{
			var data = {
				"spk_id"	: $("#spk_id").html(),
				"spkf_id"	: $("#spk_id_faktur").html(),
				_token:'{{csrf_token()}}'
			};

			$.ajax({
				type: "POST",
				dataType:"json",
				url: "{{url('/api/spk/terbitkan')}}",
				data: data
			}).fail(function(response) {
				alert("ERR-42 Data Gagal Diterbitkan !");
			}).done(function(response){
				if (!response.result) {
					alert(response.msg);
				} else {
					// $.ajax({
					// 	type: "GET",
					// 	url: "{{url('cetak/f_kendaraan')}}/" + data.spkf_id
					// }).done(function() {
					// 	$.ajax({
					// 		type: "GET",
					// 		url: "{{url('cetak/f_BAST')}}/" + data.spkf_id
					// 	}).done(function() {
					// 		loadData();
					// 		$("#detail").modal("close");
					// 	});
					// });

					window.open("{{url('cetak/f_kendaraan')}}/" + data.spkf_id, "_blank");
					window.open("{{url('cetak/f_BAST')}}/" + data.spk_id, "_blank");

					loadData();
					$("#detail").modal("close");
				}
			});
		}
	})

	$("#cetak_faktur").click(function(){
		var data = {
			"spkf_id"	: $("#spk_id_faktur").html(),
			"spkf_spk"	: $("#spk_id").html(),
			_token:'{{csrf_token()}}'
		};

		$.ajax({
			type: "POST",
			url: "{{url('/api/spk/cetak_faktur')}}",
			data: data
		}).done(function(response){
			if (!response.result) {
				alert(response.msg);
			} else {
				// $.ajax({
				// 	type: "GET",
				// 	url: "{{url('cetak/f_kendaraan')}}/" + data.spkf_id
				// }).done(function() {
				// 	loadData();
				// 	$("#detail").modal("close");
				// });

				window.open("{{url('cetak/f_kendaraan')}}/" + data.spkf_spk, "_blank");

				loadData();
				$("#detail").modal("close");
			}
		});
	});


	$("#filter").click(function(e){
		e.preventDefault();
		loadData();
	});

	$("#reset").click(function(e){
		e.preventDefault();
		$("#filter_awal").val("");
		$("#filter_akhir").val("");
		status="";
		$(".tab li").removeClass("active");
		$(".tab li a[data-id='']").parent().addClass("active");
		loadData();
	});


	$("#export").click('click', function (event) {
	    var args = [$('#dataSPK'), 'MONITORING_SPK_<?php echo date('dmY') ?>.xls'];   
	    exportTableToExcel.apply(this, args);
	});

	var status="";
function loadData() {
	loadNotif();
	var db_spk = {
		loadData: function(filter) {
			var filter_awal = $("#filter_awal").val().trim();
			var filter_akhir = $("#filter_akhir").val().trim();
			if (filter_awal != ""){
				filter['filter_awal'] = filter_awal;
			}
			if (filter_akhir != ""){
				filter['filter_akhir'] = filter_akhir;
			}
			filter['spk_status'] = status;
			return $.ajax({
				type: "GET",
				url: "{{url('api/spk')}}",
				data: filter
			});
		}
	};

    db_spk.status = [
    		{
            	"status_id": "",
            	"status_nama": "",           
        	},
    		{
            	"status_id": "9",
            	"status_nama": "<span class='box-span red'>INVALID</span>",           
        	},
    		{
            	"status_id": "1",
            	"status_nama": "<span class='box-span orange'>VALID</span>",           
        	},
    		{
            	"status_id": "2",
            	"status_nama": "<span class='box-span green'>MATCHED</span>",           
        	},
    		{
            	"status_id": "3",
            	"status_nama": "<span class='box-span cyan'>DO</span>",           
        	},
        ];

    db_spk.via = [
			{
				"via_id": "",
				"via_nama": "",		   
			},
			{
				"via_id": 1,
				"via_nama": "CASH",		   
			},
			{
				"via_id": 2,
				"via_nama": "CREDIT",		   
			},
			
		];
	db_spk.harga = [
    		{
            	"harga_id": "",
            	"harga_nama": "",           
        	},
    		{
            	"harga_id": "0",
            	"harga_nama": "ON-TR",           
        	},
    		{
            	"harga_id": "1",
            	"harga_nama": "OFF-TR",           
        	},

    		
        ];

    $("#dataSPK").jsGrid({
        height: "calc(100% - 40px)",
        width: "100%",
        sorting: true,
        filtering: true,
        autoload: true,
        paging: true,
        pageSize: 30,
        pageButtonCount: 5,
        noDataContent: "Tidak Ada Data",
		rowClick:function(data){
            var item = data.item;
            detail(item.spk_id);
        },
 
        controller: db_spk,
 
        fields: [
            { name: "spk_status", title:"Status", type: "select", width: 100, items: db_spk.status, valueField: "status_id", textField: "status_nama", align:"center", filtering:false },
            { name: "spk_tgl", title:"Tanggal", type: "text", width: 80, align:"center" },
            { name: "spk_id", title:"No SPK", type: "text", width: 80, align:"center" },
            { name: "spk_pel_nama", title:"Nama Pelanggan",type: "text", width: 170},
           // { name: "spk_type", title:"Type", type: "text", width: 70, align:"center" },
            { name: "spk_variant", title:"Varian", type: "text", width: 150 },
            { name: "spk_warna", title:"Warna", type: "text", width: 100, align:"center"},

            { name: "spk_sales", title:"Sales", type: "text", width: 100},

            { name: "spk_team", title:"Team", type: "text", width: 100, align:"center" },
			{ name: "spk_kendaraan_harga", title:"Jenis Harga", type: "select", width: 70, items: db_spk.harga, valueField: "harga_id", textField: "harga_nama", align:"center" },
            { name: "spk_matching", title:"Matching", type: "checkbox", width: 80, align:"center"},
            { name: "spk_diskon", title:"Diskon", type: "checkbox", width: 80, align:"center"},
            { name: "spk_doc", title:"Po Leasing", type: "checkbox", width: 80, align:"center"},
            { name: "spk_pembayaran", title:"Pembayaran (Rp)", type: "text", width: 180, align:"right" },
            { name: "spk_piutang", title:"Piutang (Rp)", type: "text", width: 180, align:"right" },
			{ name: "spk_metode", title:"CARA BAYAR", type: "select", width: 90,items: db_spk.via, valueField: "via_id", textField: "via_nama", align:"center" },
            { name: "spk_pel_kota", title:"Kota", type: "text", width: 120, align:"center" },
        ]
    });
}
loadData();
$(".tab li a").click(function(e){
	e.preventDefault();

	status = $(this).data("id");
	
	loadData();
	$(".tab li").removeClass("active");
	$(this).parent().addClass("active");
});
</script>
@endsection