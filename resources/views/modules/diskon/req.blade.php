@extends('layout')

@section('content')

    <div class="content-header">
        <h6>
            <small>Penjualan</small>
            Request Diskon &amp; Approval
        </h6>

        <ul class="header-tools right">
            <li><a href="javascript:;" id="refresh" class="chip"><i class="fa fa-refresh"></i> Refresh</a></li>
        </ul>
    </div>
<div id="data_grid" class="wrapper">
        <div class="nav-wrapper">
            <div class="nav-right">
                <i style="margin-right: 5px" class="fa fa-filter" aria-hidden="true"></i>
                <span class="bold" style="font-size: 13px"> Periode :</span>
                <div class="input-group">
                    <input type="text" class="input-filter datepicker" id="filter_awal" placeholder="Tanggal Awal" required="" />
                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                </div>
                <span class="bold" style="font-size: 13px">s.d</span>
                <div class="input-group">
                    <input type="text" class="input-filter datepicker"  id="filter_akhir" placeholder="Tanggal Akhir"  required="" />
                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                </div>
                <a class="waves-effect waves-light btn-flat btn-filter" id="filter">Cari</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="reset">Reset</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
            </div>
        </div>
        <div id="dataDiskon">

        </div>  
</div>
<div id="approval" class="modal">    
    <input type="hidden" id="spk_type_id">
    <h6 class="modal-title blue-grey darken-1">
        Approval
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:10px 14px;position: relative;">
        <div class="row" style="margin:0">
            <div class="col s6">
                <div style="display:none" id="uid"></div>
                        <h6>
                            <span id="diskon_nama"></span><br/>
                            <small id="diskon_spk" class="orange-text lighten-1"></small>
                            <small class="right"><i class="fa fa-clock-o"></i> <span id="diskon_tgl"></span></small>
                        </h6>
                        <hr/>
                            <table class="info payment" style="margin:0">
                                <tr>
                                    <td width="100">Sales</td><td id="sales"></td>
                                </tr>
                                <tr>
                                    <td width="100">Type</td><td id="type"></td>
                                </tr>
                            </table>
                        <hr/>
                            <table class="info payment" style="margin-bottom:0">
                                <tr>
                                    <td width="200">Cashback</td>
                                    <td>Rp</td>
                                    <td class="right">
                                        <input type="text" min="0" step="1" id="cashback" class="text-right number"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">Aksesoris <span id="aksesoris_add" style="cursor: pointer;" class='material-icons' style='font-size:20px'>add</span></td>
                                </tr>
                            </table>
                            <table id="aksesoris" class="info payment" style="margin:0">
                            </table>
                            <table class="info payment" style="margin-top:3px">
                                <tr style="font-weight:bold;border-top:1px solid #ddd;border-bottom:1px solid #ddd">
                                    <td width="200">Total</td><td>Rp</td><td class="right"><input type="text" min="0" step="1" id="total" class="text-right number"/></td>
                                </tr>
                                <tr>
                                    <td width="200">Komisi</td><td>Rp</td><td class="right"><input type="text" min="0" step="1" id="komisi" class="text-right number"/></td>
                                </tr>
                                <tr>
                                    <td colspan="2">Referral</td><td id="referral" class="right" ></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <small>Keterangan</small>
                                        <p id="ket" style="margin:0"></p>
                                    </td>                                
                                </tr>
                            </table> 
            </div>
            <div class="col s6" style="padding-top:10px">
                    <input type="hidden" id="diskon_id"/>
                    <div class="input-field col s12">
                        <textarea id="catatan" class="materialize-textarea" style="height:80px"></textarea>
                        <label for="catatan">Catatan</label>
                    </div>
                    <div class="row">
                        <div class="col s6 center">
                            <button type="submit" value="1" class="btn-floating btn-large waves-effect waves-light green pulse app"><i class="material-icons">done</i></a>
                        </div>
                        <div class="col s6 center">                     
                            <button type="submit"  value="99" class="btn-floating btn-large waves-effect waves-light red app"><i class="material-icons">close</i></a>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<script>
$(function() {

    function detail(item) {
        $("#spk_type_id").val("");
        $("#diskon_id").val("");
        $("#diskon_tgl").html("");
        $("#diskon_spk").html("");
        $("#diskon_nama").html("");
        $("#sales").html("");
        $("#type").html("");
        $("#cashback").html("");
        $("#komisi").html("");
        $("#total").html("");
        $("#referral").html("");
        $("#ket").html("");

        $.ajax({
            type: "GET",
            url: "{{url('api/diskon/aksesoris/')}}/" + item.spk_type_id
        }).done(function(data){
            $("#uid").html(item.sales_salesUid);
            $("#spk_type_id").val(item.spk_type_id);
            $("#diskon_id").val(item.spk_diskon);
            $("#diskon_tgl").html(item.spk_tgl);
            $("#diskon_spk").html(item.spk_id);
            $("#diskon_nama").html(item.spk_pel_nama);
            $("#sales").html(item.spk_sales);
            $("#type").html(item.spk_type);
            $("#cashback").val(item.spk_cashback);
            set_aksesoris(data, item.spk_aksesoris);
            //$("#aksesoris").html(item.spk_taksesoris);
            total_aksesoris = 0;
            var aksesoris = item.spk_aksesoris;
            if (aksesoris.length>0){
                for (var i in aksesoris){
                    var item_a = aksesoris[i];
                    if(set_selected("aksesoris"+i, item_a.spka_kode)){
                        $("#harga"+i).html(number_format(item_a.spka_harga));

                        total_aksesoris += item_a.spka_harga;
                    }else{
                        $("#harga"+i).html('0');
                    }
                }
            }
            total= string_format(item.spk_cashback) + total_aksesoris;

            $("#komisi").val(item.spk_komisi);
            $("#total").val(number_format(total));
            $("#referral").html(item.spk_ref);
            $("#ket").html(item.spk_ket);

            $("#approval").modal("open");
        });
    }

    $(".app").click(function(){
        var this_status = $(this).val();
        var diskons = [];
        $("#aksesoris tr").each(function() {
            diskons.push($("#"+this.id).find("select").val());
        });
        diskon = JSON.stringify(diskons);

        var item = {
            diskon_id:$("#diskon_id").val(),
            diskon_cashback:string_format($("#cashback").val()),
            diskon_komisi:($("#komisi").val() != "" ? string_format($("#komisi").val()) : 0),
            diskon_status:this_status,
            diskon_catatan:$("#catatan").val(),
            diskon:diskon,
            _token:'{{csrf_token()}}'
        }

        if ($("#diskon_id").val() != ""){
            $.ajax({
                type: "POST",
                url: "{{url('/api/diskon/app')}}",
                data: item
            }).fail(function(response) {
                alert("ERR-42 Diskon gagal disimpan!, silahkan hubungi administrator");
                console.log(response);
            }).done(function(response){
                console.log(response);
                if (response==1){
                    var diskon = $("#diskon_spk").html();
                    var ket = $("#catatan").val();
                    var uid = $("#uid").html();
                    var tgl = "{{ date('Y-m-d H:i:s') }}";

                    if (this_status == 99) {
                        var activity = {
                            "activity_judul":"Revisi Diskon SPK " + diskon,
                            "activity_ket":ket,
                            "activity_kategori":"diskon",
                            "activity_author":"ADH",
                            "activity_tgl":tgl
                        };

                        var db = firebase.database().ref('spk_update/' + uid + '/' + diskon);
                        db.set({"spkd_status":99});

                        var db = firebase.database().ref('activity/' + uid );
                        db.push(activity);
                    } else {
                        var notif = {
                            "notif_judul":"ACC",
                            "notif_ket":"Diskon SPK " + diskon + " telah di setujui.",
                            "notif_ref":diskon,
                            "notif_kategori":"diskon",
                            "notif_tgl":tgl,
                        };

                        // var db = firebase.database().ref('spk_update/' + uid + '/' + diskon);
                        // db.set({"spkd_status":1});

                        var db = firebase.database().ref('notif/' + uid);
                        db.push(notif);
                    }

                    loadData();
                    $("#approval").modal("close");
                }else{
                    alert("ERR-00 Diskon gagal disimpan!, silahkan hubungi administrator");
                }
            }); 
        }else{
            alert("Diskon belum dipilih !");
            $("#approval").modal("close");
        }
    });

    var total = 0;
    var total_aksesoris = 0;

    $("#aksesoris_add").click(function() {     
        $.ajax({
            type: "GET",
            url: "{{url('api/diskon/aksesoris/')}}/" + $("#spk_type_id").val()
        }).done(function(data){
            add_aksesoris(data);
        }).fail(function() {
            alert('Gagal!');
        });
    });

    $("#refresh").click(function(){
        loadData();
    });
    
    function total_harga() {
        var aksesoris_harga = 0;
        $("#total").val("");

        $('[id^="harga"]').each(function() {
            aksesoris_harga += string_format($("#"+this.id).html());
        });

        $("#total").val(number_format(aksesoris_harga + string_format($('#cashback').val())));
    }

    function set_selected(id, selected){
        var data = document.getElementById(id).options;
        for(var i in data){
            var item = data[i];
            if (item.value == selected){
                document.getElementById(id).selectedIndex = item.index;
                return true;
            }
        }
        return false;
    }

    function add_aksesoris(data){
        var column_id = $("#aksesoris tr:last").attr('id');

        if (column_id != null) {
            var j = parseInt(column_id.replace('aksesoris_column', '')) + 1;
        } else {
            var j = 0;
        }
        
        var select='<tr id="aksesoris_column'+j+'"><td width="20px">'+(j+1)+'</td><td width="180px"><select id="aksesoris'+j+'" data-y="'+j+'"><option value="0"></option>';   
        for(var i in data){
            var item = data[i];
            select +='<option value="'+ item.aksesoris_kode +'">['+ item.aksesoris_kode +'] - '+ item.aksesoris_nama +'</option>';          
        }
        select += '</select></td><td width="5"><span id="remove_aksesoris'+j+'" style="cursor: pointer;" class="material-icons" style="font-size:20px">remove</span></td><td width="15"> Rp</td><td id="harga'+j+'" class="text-right remove">0</td></tr>';
        $("#aksesoris").append(select);
        $("#aksesoris"+j).change(function(){
            var y =$(this).data('y');
            var id = $(this).val();
            total_aksesoris -= string_format($("#harga"+y).html());
            for(var i in data){
                var item = data[i];
                if(item.aksesoris_kode == id){
                    $("#harga"+y).html(number_format(item.aksesoris_harga));
                    total_aksesoris += item.aksesoris_harga;
                    break;
                }
            }

            $("#total").val(number_format(total_aksesoris + string_format($('#cashback').val())));

            if (id==0){
                $("#harga"+y).html("0");
            }

            total_harga();
        });

        $('[id^="remove_aksesoris"]').click(function() {
            var id = $(this).closest("tr").attr('id');
            $("#"+id).remove();
            total_harga();
        });
    }

    function set_aksesoris(data, aksesoris){    
        $("#aksesoris").html('');
        for (var j = 0; j < aksesoris.length; j++){
            var select='<tr id="aksesoris_column'+j+'"><td width="20px">'+(j+1)+'</td><td width="180px"><select id="aksesoris'+j+'" data-y="'+j+'"><option value="0"></option>';   
            for(var i in data){
                var item = data[i];
                select +='<option value="'+ item.aksesoris_kode +'" '+ (item.aksesoris_kode == aksesoris[j].spka_kode ? 'selected' : '') +'>['+ item.aksesoris_kode +'] - '+ item.aksesoris_nama +'</option>';          
            }
            select += '</select></td><td width="5"><span id="remove_aksesoris'+j+'" style="cursor: pointer;" class="material-icons">remove</span></td><td width="15"> Rp</td><td id="harga'+j+'" class="text-right remove">0</td></tr>';
            $("#aksesoris").append(select);
            $("#aksesoris"+j).change(function(){
                var y =$(this).data('y');
                var id = $(this).val();
                total_aksesoris -= string_format($("#harga"+y).html());
                for(var i in data){
                    var item = data[i];
                    if(item.aksesoris_kode == id){
                        $("#harga"+y).html(number_format(item.aksesoris_harga));
                        total_aksesoris += item.aksesoris_harga;
                        break;
                    }
                }

                $("#total").val(number_format(total_aksesoris + string_format($('#cashback').val())));

                if (id==0){
                    $("#harga"+y).html("0");
                }

                total_harga();
            });
        }

        $('[id^="remove_aksesoris"]').click(function() {
            var id = $(this).closest("tr").attr('id');
            $("#"+id).remove();
            total_harga();
        });
    }

    $('#cashback').change(function(){
        $('#total').val(number_format(string_format($(this).val()) + total_aksesoris));
    });
    $('#total').change(function(){
        $('#cashback').val(number_format(string_format($(this).val()) - total_aksesoris));
    });


    $("#filter").click(function(e){
    e.preventDefault();
    loadData();
    });

    $("#reset").click(function(e){
        e.preventDefault();
        $("#filter_awal").val("");
        $("#filter_akhir").val("");
        status="";
        $(".tab li").removeClass("active");
        $(".tab li a[data-id='']").parent().addClass("active");
        loadData();
    });

    $("#export").click('click', function (event) {
        var args = [$('#dataDiskon'), 'DISKON_SPK_<?php echo date('dmY') ?>.xls'];   
        exportTableToExcel.apply(this, args);
    });

    function loadData(){
        var db_diskon = {
            loadData: function(filter) {
                var filter_awal = $("#filter_awal").val().trim();
                var filter_akhir = $("#filter_akhir").val().trim();
                if (filter_awal != ""){
                    filter['filter_awal'] = filter_awal;
                }
                if (filter_akhir != ""){
                    filter['filter_akhir'] = filter_akhir;
                }
                filter['spk_status'] = status;
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/diskon/request')}}",
                    data: filter
                });
            },
        };

        $("#dataDiskon").jsGrid({
            height: "98%",
            width: "100%",
     
            sorting: true,
            autoload: true,
            paging: true,
            noDataContent: "Tidak Ada Data",
            rowDoubleClick:function(data){
                detail(data.item);
            },
     
            deleteConfirm: "Anda yakin akan menghapus data ini?",
     
            controller: db_diskon,
     
            fields: [
                { name: "spk_tgl", title:"Tanggal", type: "text", width: 80, align:"center" },
                { name: "spk_id", title:"No SPK", type: "text", width: 100, align:"center" },
                { name: "spk_pel_nama", title:"Nama Pelanggan", type: "text", width: 100},
                { name: "spk_sales", title:"Sales", type: "text", width: 100},
                { name: "spk_cashback", title:"Cashback", type: "number", width: 100, align:"right" },
                { name: "spk_taksesoris", title:"Aksesoris", type: "number", width: 100, align:"right" },
                { name: "spk_komisi", title:"Komisi", type: "number", width: 100, align:"right"},
                { name: "spk_total", title:"Total", type: "number", width: 100, align:"right" },
                { name: "spk_ket", title:"Keterangan", type: "text", width: 140},
                { name: "spk_ref", title:"Nama Referral", type: "text", width: 120}
            ]
        });
    }
    loadData();
 
});
</script>

@endsection