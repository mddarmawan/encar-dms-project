<div class="row" style="margin:0">
	<input type="hidden" id="state" value="add"/>
	<input type="hidden" id="_id" value=""/>
	<div class="col s6 m6">
		<table class="info payment" style="border:0;margin-top:0">
	      	<tr>
			    <td width="140px">No Referensi</td>
			    <td width="10px">:</td>
			    <td class="bold"><input type="text" id="trk_ref" readonly required="" value="1"/></td>
			</tr>
	      	<tr>
			    <td width="140px">Vendor</td>
			    <td width="10px">:</td>
			    <td class="bold">
			    	<select id="trk_vendor">

			    	</select>
			    </td>
			</tr>
		</table>
	</div>
	<div class="col s6 m6">
		<table class="info payment" style="border:0;margin-top:0">
	      	<tr>
			    <td width="140px">Vendor Invoice</td>
			    <td width="10px">:</td>
			    <td class="bold"><input type="text"  id="trk_invoice"  required="" /></td>
			</tr>
	      	<tr>
			    <td width="140px">Tanggal Invoice</td>
			    <td width="10px">:</td>
			    <td class="bold"><input type="text" class="datepicker" id="trk_tgl" value="<?php echo date("d/m/Y") ?>" required="" /></td>
			</tr>
		</table>
	</div>
	<h6 style="padding:0 12px;font-weight:bold">Produk</h6>

	<div class="col s6 m6">
		<table class="info payment" style="border:0;margin-top:0">
	      	<tr>
			    <td width="100px">Type</td>
			    <td width="10px">:</td>
			    <td class="bold">
			    	<select id="type">

			    	</select>
			    </td>
			</tr>
	      	<tr>
			    <td width="100px">Variant</td>
			    <td width="10px">:</td>
			    <td class="bold">
			    	<select id="trk_variantid">

			    	</select>
			    </td>
			</tr>
	      	<tr>
			    <td width="100px">Warna</td>
			    <td width="10px">:</td>
			    <td class="bold">
			    	<select id="trk_warna">

			    	</select>
			    </td>
			</tr>
	      	<tr>
			    <td width="100px">Tahun</td>
			    <td width="10px">:</td>
			    <td class="bold">
			    	<input type="number" id="trk_tahun" min="2000" step="1" value="<?php echo date("Y") ?>"  required="" />
			    </td>
			</tr>

		</table>
	</div>
	<div class="col s6 m6">
		<table class="info payment" style="border:0;margin-top:0">
	      	<tr>
			    <td width="140px">No DH</td>
			    <td width="10px">:</td>
			    <td class="bold">
			    	<input type="number" id="trk_dh" readonly  required="" />
			    </td>
			</tr>
	      	<tr>
			    <td width="140px">RRN</td>
			    <td width="10px">:</td>
			    <td class="bold">
			    	<input type="text" id="trk_rrn" class="uppercase" required="" />
			    </td>
			</tr>
	      	<tr>
			    <td width="140px">No Rangka</td>
			    <td width="10px">:</td>
			    <td class="bold">
			    	<input type="text" id="trk_rangka" class="uppercase"  required="" />
			    </td>
			</tr>
	      	<tr>
			    <td width="140px">No Mesin</td>
			    <td width="10px">:</td>
			    <td class="bold">
			    	<input type="text" id="trk_mesin" class="uppercase" required="" />
			    </td>
			</tr>
	      	<tr>
			    <td width="140px">Harga (Rp)</td>
			    <td width="10px">:</td>
			    <td class="bold">
			    	<input type="number" min="0" step="1" id="trk_dpp" required="" />
			    </td>
			</tr>
		</table>
	</div>
</div>

<script>
	$(function() {
		function setVariant(id){
			$.ajax({
			    type: "GET",
			    url: "{{url('api/kendaraan/variant/')}}" + "/" + id
			}).done(function(data) {	
				$("#trk_variantid").html("");			
				jQuery.each(data, function(i, item) {
					$("#trk_variantid").append("<option value='"+ item.variant_id +"'>"+ item.variant_serial +" - "+ item.variant_nama +"</option>");
				});
			});
		}

		function getNew(){
			$.ajax({
			    type: "GET",
			    url: "{{url('api/pembelian/new/')}}"
			}).done(function(data) {				
				$("#trk_dh").val(data.dh);		
				$("#trk_ref").val(data.ref);
			});
		}

		function setVendor(){
			$.ajax({
			    type: "GET",
			    url: "{{url('api/vendor')}}"
			}).done(function(data) {
				$("#trk_vendor").html("");
				jQuery.each(data, function(i, item) {
					$("#trk_vendor").append("<option value='"+ item.vendor_id +"'>"+ item.vendor_nama +"</option>");
				});
			});
		}

		function setType(){
			$.ajax({
			    type: "GET",
			    url: "{{url('api/kendaraan/type')}}"
			}).done(function(data) {
				$("#type").html("");
				jQuery.each(data, function(i, item) {
					$("#type").append("<option value='"+ item.type_id +"'>"+ item.type_nama +"</option>");
				});
				setVariant(data[0].type_id);
			});
		}


		function setWarna(){
			$.ajax({
			    type: "GET",
			    url: "{{url('api/warna')}}"
			}).done(function(data) {
				$("#trk_warna").html("");
				jQuery.each(data, function(i, item) {
					$("#trk_warna").append("<option value='"+ item.warna_id +"'>"+ item.warna_nama +"</option>");
				});
			});
		}

		function clear(){
   			$("#trk_ref").val("");
   			$("#trk_invoice").val("");
   			$("#trk_tgl").val("<?php echo date('d/m/Y') ?>");
   			$("#trk_tahun").val("<?php echo date('Y') ?>");
   			$("#trk_dh").val("");
   			$("#trk_rrn").val("");
   			$("#trk_rangka").val("");
   			$("#trk_mesin").val("");
   			$("#trk_dpp").val("");

			getNew();
   			setVendor();
   			setType();
   			setWarna();
		}

		$("#type").change(function(){
			var id = $(this).val();
			setVariant(id);
		});

	$(".tambah").click(function(){
		$("#form_title").html("Pembelian Baru");
		$("#btn_save").html("Konfirmasi Pembelian");
		clear();
		$("#state").val("add");
	});


		
    $(".save").click(function(){

        var item = {
        	trk_ref:$("#trk_ref").val(),
        	trk_dh:$("#trk_dh").val(),
        	trk_vendor:$("#trk_vendor").val(),
        	trk_invoice:$("#trk_invoice").val(), 
        	trk_tgl:$("#trk_tgl").val(),
        	trk_dh:$("#trk_dh").val(),
        	trk_variantid:$("#trk_variantid").val(),
        	trk_warna:$("#trk_warna").val(),
        	trk_tahun:$("#trk_tahun").val(),
        	trk_rrn:$("#trk_rrn").val(),  
        	trk_rangka:$("#trk_rangka").val(),
        	trk_mesin:$("#trk_mesin").val(),
        	trk_tahun:$("#trk_tahun").val(),
        	trk_dpp:$("#trk_dpp").val(),
        	_token:'{{csrf_token()}}'
    	};

    	var _method = "POST";
        if ($("#state").val()==="add"){
            _method = "POST";
        }else{
        	_method = "PUT";
        	item['trk_id'] = $("#_id").val();
        }

        $.ajax({
            type: _method,
            url: "{{url('/api/pembelian')}}",
            data: item
        }).fail(function(response) {
            alert("ERR-42 Pembelian gagal disimpan!, silahkan hubungi administrator");
            console.log(response);
        }).done(function(response){
            if (response==1){
      			clear();
                loadData();
                $(".modal").modal("close");
            }else{
                alert("ERR-00 Pembelian gagal disimpan!, silahkan hubungi administrator");
            }
        }); 
    });

    clear();

});
</script>