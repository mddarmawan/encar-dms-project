@extends('layout')

@section('content')

   <div class="content-header">
		<h6 class="left">
			<small>Persediaan</small>
			Stock Kendaraan
		</h6>
	</div>

<div id="data_grid" class="wrapper">
        <div class="nav-wrapper">
            <div class="nav-right">
                <i style="margin-right: 5px" class="fa fa-filter" aria-hidden="true"></i>
                <span class="bold" style="font-size: 13px"> Periode :</span>
                <div class="input-group">
                    <input type="text" class="input-filter datepicker" id="filter_awal" placeholder="Tanggal Awal" required="" />
                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                </div>
                <span class="bold" style="font-size: 13px">s.d</span>
                <div class="input-group">
                    <input type="text" class="input-filter datepicker"  id="filter_akhir" placeholder="Tanggal Akhir"  required="" />
                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                </div>
                <a class="waves-effect waves-light btn-flat btn-filter" id="filter">Cari</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="reset">Reset</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
            </div>
        </div>
        <div id="data">

        </div>  
    </div>

<script>

	
$("#import").click(function() {
    $("#import_file").trigger("click");
});

$("input:file").change(function() {
    $("#import_form").submit();
    loadData();
});

$("#filter").click(function(e){
    e.preventDefault();
    loadData();
	});

	$("#reset").click(function(e){
	    e.preventDefault();
	    $("#filter_awal").val("");
	    $("#filter_akhir").val("");
	    status="";
	    $(".tab li").removeClass("active");
	    $(".tab li a[data-id='']").parent().addClass("active");
	    loadData();
	});


	$("#export").click('click', function (event) {
	    var args = [$('#data'), 'STOCK_KENDARAAN_<?php echo date('dmY') ?>.xls'];   
	    exportTableToExcel.apply(this, args);
	});

function loadData() {

    var db = {
        loadData: function(filter) {
                var filter_awal = $("#filter_awal").val().trim();
                var filter_akhir = $("#filter_akhir").val().trim();
                if (filter_awal != ""){
                    filter['filter_awal'] = filter_awal;
                }
                if (filter_akhir != ""){
                    filter['filter_akhir'] = filter_akhir;
                }
                filter['spk_status'] = status;
                return $.ajax({
                    type: "GET",
                    url: "{{url('/api/persediaan/stock')}}",
                    data: filter
                });
            },


        deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/persediaan/stock')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                });
            }
    };

    $("#data").jsGrid({
        height: "calc(100% - 40px)",
		width: "100%",
		sorting: true,
		filtering: true,
		autoload: true,
		paging: true,
		pageSize: 30,
		pageButtonCount: 5,
		noDataContent: "Tidak Ada Data",
 
		controller: db,
 
		fields: [
			{ name: "trk_dh", title:"No. DH", type: "text", width: 80, align:"" },
			{ name: "trk_masuk", title:"Tgl Masuk", type: "text", width: 80, align:"" },
			{ name: "trk_tahun", title:"Tahun Unit", type: "text", width: 100, align:"center"},
			{ name: "variant_nama", title:"Tipe", type: "text", width: 200},
			{ name: "trk_rangka", title:"No Rangka", type: "number", width: 170, align:"" },
			{ name: "trk_mesin", title:"No Mesin", type: "number", width: 170, align:"" },
			{ name: "variant_serial", title:"ID", type: "text", width: 150, align:""},
			{ name: "warna_nama", title:"Warna", type: "text", width: 140, align:"" },
			{ name: "trk_invoice", title:"No Invoice", type: "number", width: 100},
			{ name: "trk_tgl", title:"Tgl. Invoice", type: "number", width: 100, align:"center" },
			{ name: "ekspedisi_nama", title:"Ekspedisi", type: "text", width: 120, align:"" },
			{ name: "trk_status", title:"Status", type: "text", width: 120, align:"center" },
			{ name: "trk_lokasi", title:"Lokasi", type: "text", width: 160, align:"" },
			{ name: "spk_do", title:"DO", type: "text", width: 120, align:"center" }
		]
	});
   
};
loadData();
</script>

@endsection