<style>
	body{
		margin:0;
		padding:0;
		font-size:14px;
		/*font-family: "Open Sans","Helvetica Neue",Helvetica,Arial,sans-serif;*/
	}
	td, th{
		font-size:14px;
	}
	h5{
		margin: 5px 0 20px;
		font-weight:400;
	}
	.text-center{
		text-align:center;
	}
	.text-right{
		text-align:right;
	}
	table{
		width:100%;		
	}
	table, td, th, tr{
		border:0;
		border-spacing: 0;
		border-color:#fff;
		padding:0;
		border-collapse: collapse;
		vertical-align:top;
	}
	table th{
		border-top:1px solid #ddd;
		border-left:1px solid #ddd;
		border-right:1px solid #ddd;
		border-bottom:4px solid #ddd;
		padding:3px 7px;
		vertical-align:middle;
		background:#f5f5f5;
		border-spacing: 0;
		border-collapse: collapse;
	}
	
	table tfoot th{
		border-bottom:1px solid #ddd;
		border-top:4px solid #ddd;
	}
	
	.table td{
		padding:3px 7px;
		border-top:1px solid #ddd;
		border-left:1px solid #ddd;
		border-right:1px solid #ddd;
		border-bottom:1px solid #ddd;		
	}

	.bold{
		font-weight:bold;
	}

</style>

<page pageset="old">
	<body style="margin: 50px; font-family: arial;">
		<div class="text-center" style="margin-bottom: 20px;"><b><u><h3>SURAT PERNYATAAN</h3></u></b></div>
		<p style="margin-bottom: 10px;">Yang bertanda tangan dibawah ini :</p>
		<table style="margin-bottom: 20px;">
			<tr>
				<td style="width: 120px;"><b>Nama</b></td>
				<td style="width: 5px;"> : &nbsp;&nbsp;</td>
				<td><b> Daniel Saragat <!-- {{ $data->spk_sales_nama }} --> </b></td>
			</tr>
			<tr>
				<td><b>Jabatan</td>
				<td> : &nbsp;&nbsp;</td>
				<td><b> Branch Manager<!-- {{ $data->spk_sales_jabatan }} --> </b></td>
			</tr>
			<tr>
				<td><b>Alamat</td>
				<td> : &nbsp;&nbsp;</td>
				<td><b> Jl. Angkatan 45 Palembang</td>
			</tr>				
		</table>
		<br>
		<div style="margin-bottom: 10px;">Dengan ini menyatakan bahwa BPKB (Buku Pemilik Kendaraan Bermotor) dan Faktur serta dokumen lainnya untuk kendaraan bermotor sbb: </div>
		<table style="margin-bottom: 20px;">
			<tr>
				<td style="width: 120px;"><b>Merk / Type</b></td>
				<td style="width: 5px;"> : &nbsp;&nbsp;</td>
				<td><b> DAIHATSU / {{ $data->spk_variant }} </b></td>
			</tr>
			<tr>
				<td><b>Rangka</b></td>
				<td> : &nbsp;&nbsp;</td>
				<td><b> {{ $data->spk_no_rangka }} </b></td>
			</tr>
			<tr>
				<td><b>Mesin</b></td>
				<td> : &nbsp;&nbsp;</td>
				<td><b> {{ $data->spk_no_mesin }} </b></td>
			</tr>
			<tr>
				<td><b>Warna</b></td>
				<td> : &nbsp;&nbsp;</td>
				<td><b> {{ $data->spk_warna }} </b></td>
			</tr>
			<tr>
				<td><b>Tahun/CC</b></td>
				<td> : &nbsp;&nbsp;</td>
				<td><b> {{ $data->spk_tahun }} / 1500 </b></td>
			</tr>	
			<tr>
				<td><b>Nama BPKB</b></td>
				<td> : &nbsp;&nbsp;</td>
				<td><b> {{ $data->spk_stnk_nama }} </b></td>
			</tr>
			<tr>
				<td><b>Alamat</b></td>
				<td> : &nbsp;&nbsp;</td>
				<td><b> {{ $data->spk_stnk_alamat }} </b></td>
			</tr>					
		</table>
		<br><br>
		<p style="text-align: justify;">
			Surat-surat kendaraan tersebut (BPKB, FAKTUR, NOTICE PAJAK, NIK) masih dalam proses pengurusan di Kantor SAMSAT oleh kami sebagai penjual kendaraan bermotor dan apabila BPKB serta dokumen-dokumen yang dimaksud selesai akan segera kami serahkan langsung kepada {{ $data->spk_leasing_nama }}, selambat-lambatnya 3 ( <b>Tiga</b> ) bulan sejak surat pernyataan ini dibuat.
		<br>
		<br>
			Dan surat penyataan BPKB ini wajib dikembalikan kepada PT. ENCARTHA INDONESIA pada saat serah terima BPKB.
		<br>
		<br>
			Selanjutnya dengan ini kami nyatakan bahwa di samping BPKB dan dokumen-dokumen dimaksud tidak akan kami berikan kepada siapapun / pihak manapun dengan dalih dan alsan apapun, kecuali kepada {{ $data->spk_leasing_nama }}, Cab. {{ $data->spk_leasing_kota }}.
		<br>
		<br>
			Demikianlah surat penyataan ini dibuat untuk dapat dipergunakan sebagaimana mestinya.
		</p>
		<br>
		<table style="text-align: center; margin-left: 500px;">
			<tr>
				<td>
					Palembang, 16 Januari 2017
					<br>
					<br>
					<br>
					<br>
					<b><u>Daniel Saragat <!-- {{ $data->spk_sales_nama }} --></u></b>
					<br>
					<b>Branch Manager <!-- {{ $data->spk_sales_jabatan }} --></b>
				</td>
			</tr>
		</table>
		<br>

<!-- ########################################################################################################################### -->

		<br>
		Palembang, 16 Januari 2017
		<br>
		<br>
		<table style="width: 250px;">
			<tr>
				<td>
					<p style="margin-bottom: 2px;">Kepada Yth, </p>
					<p style="margin-bottom: 2px;">{{ $data->spk_leasing_nama }}</p>
					<p style="margin-bottom: 2px;">{{ $data->spk_leasing_alamat }}</p>
					<p style="margin-bottom: 2px;">{{ $data->spk_leasing_kota }}</p>
				</td>
			</tr>
		</table>
		<br>
		<div style="margin-bottom: 2px;">Dengan hormat, </div>
		<div style="margin-bottom: 2px;">Sehubungan dengan penjualan 1 (satu) unit kendaraan Daihatsu yang pendanaannya dilakukan oleh <b>{{ $data->spk_leasing_nama }}</b> dengan perincian sbb: </div>
		<br>
		<table>
			<tr>
				<td style="width: 200px;"><b>Nama</b></td>
				<td style="width: 2px;"> : &nbsp;&nbsp;</td>
				<td><b> {{ $data->spk_pel_nama }} </b></td>
			</tr>
			<tr>
				<td><b>Alamat</b></td>
				<td> : &nbsp;&nbsp;</td>
				<td><b> {{ $data->spk_pel_alamat }} </b></td>
			</tr>
			<tr>
				<td><b>Merk / Type</b></td>
				<td> : &nbsp;&nbsp;</td>
				<td><b> DAIHATSU / {{ $data->spk_variant }}</b></td>
			</tr>
			<tr>
				<td><b>Rangka</b></td>
				<td> : &nbsp;&nbsp;</td>
				<td><b> {{ $data->spk_no_rangka }} </b></td>
			</tr>
			<tr>
				<td><b>Mesin</b></td>
				<td> : &nbsp;&nbsp;</td>
				<td><b> {{ $data->spk_no_mesin }} </b></td>
			</tr>
			<tr>
				<td><b>Warna</td>
				<td> : &nbsp;&nbsp;</td>
				<td><b> {{ $data->spk_warna }} </td>
			</tr>
			<tr>
				<td><b>STNK a/n</b></td>
				<td> : &nbsp;&nbsp;</td>
				<td><b> {{ $data->spk_stnk_nama }} </b></td>
			</tr>
			<tr>
				<td><b>Tahun/CC</b></td>
				<td> : &nbsp;&nbsp;</td>
				<td><b> {{ $data->spk_tahun }} / 1500 </b></td>
			</tr>
		</table>
		<br>
		<table>
			<tr>
				<td style="width: 520px;">Harga On The Road</td>
				<td style="width: 2px;"> : &nbsp;&nbsp;</td>
				<td>Rp. &nbsp;&nbsp;&nbsp; {{ $data->spk_variant_on }}</td>
			</tr>
			<tr>
				<td>Uang Muka</td>
				<td style="width: 2px;"> : &nbsp;&nbsp;</td>
				<td>Rp. &nbsp;&nbsp;&nbsp; {{ $data->spk_dp }}</td>
			</tr>
			<tr>
				<td></td>
				<td style="width: 2px;"></td>
				<td> ----------------------- (-) </td>
			</tr>
			<tr>
				<td><b>Total Pelunasan ...........</b></td>
				<td style="width: 2px;"> : &nbsp;&nbsp;</td>
				<td>Rp. &nbsp;&nbsp;&nbsp; <b>{{ $data->spkp_pelunasan }}</b> </td>
			</tr>
		</table>
		<table>
			<tr>
				<td style="width: 200px;">Terbilang</a></td>
				<td style="width: 2px;"> : &nbsp;&nbsp;</td>
				<td><b> {{ $data->spkp_terbilang }}</b></td>
			</tr>
		</table>
		<p style="margin-bottom: 5px;">Maka kami harapkan sisa pembayaran tersebut dapat ditransfer ke rekening sbb:</p>
		<table>
			<tr>
				<td style="width: 200px;"> Bank </td>
				<td style="width: 2px;"> : &nbsp;&nbsp;</td>
				<td><b> Bank Permata Jl. Kol.Atmo No. 479 Palembang</b></td>
			</tr>
			<tr>
				<td> A/C </td>
				<td style="width: 2px;"> : &nbsp;&nbsp;</td>
				<td><b> 131.100.5835</b></td>
			</tr>
			<tr>
				<td> A/N </td>
				<td style="width: 2px;"> : &nbsp;&nbsp;</td>
				<td><b> PT. Encartha Indonesia</b></td>
			</tr>
		</table>
		<p style="margin-bottom: 2px;">Atas bantuan dan kerjasamanya kami ucapkan terimakasih.</p>
		<p style="margin-bottom: 2px;">Hormat kami,</p>
		<br><br>
		<b><u>Daniel Saragat</u></b><br>
		Branch Manager
	</body>
</page>