
<div id="detail" class="modal" style="width:960px;overflow-y:hidden">   
    <h6 class="modal-title blue-grey darken-1">
        <span id="spk_id">SPK</span>
        <i class="fa fa-calendar" style="margin:0 5px 0 25px"></i> <span id="spk_tgl"></span>
        <i class="fa fa-user"  style="margin:0 5px 0 25px"></i> <span id="spk_sales"></span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <ul class="tabs blue-grey darken-1 white-text">
        <li class="tab col s3"><a class="active" href="#pemesan" >Pemesan</a></li>
        <li class="tab col s3"><a href="#kendaraan">Kendaraan</a></li>
        <li class="tab col s3"><a href="#diskon">Diskon</a></li>
        <li class="tab col s3"><a href="#pembayaran">Pembayaran</a></li>
        <li class="tab col s3"><a href="#faktur">Faktur</a></li>
      </ul>
    <div class="modal-content" style="padding:0px;position: relative;">
      	<div id="pemesan">
      		<div class="row" style="margin:0">
      			<div class="col s12 m6">
                              <div style="display:none" id="uid"></div>
      				<table class="info">
      					<tr>
      						<td width="180px">Nama Pemesan</td>
      						<td width="10px">:</td>
      						<td class="bold" id="pel_nama"></td>
      					</tr>
      					<tr>
      						<td width="180px">Alamat Domisili/Usaha</td>
      						<td width="10px">:</td>
      						<td class="bold" id="pel_alamat"></td>
      					</tr>
      					<tr>
      						<td width="180px">Kode Pos</td>
      						<td width="10px">:</td>
      						<td class="bold" id="pel_pos"></td>
      					</tr>
      					<tr>
      						<td width="180px">Telepon</td>
      						<td width="10px">:</td>
      						<td class="bold" id="pel_telp"></td>
      					</tr>
      					<tr>
      						<td width="180px">Ponsel</td>
      						<td width="10px">:</td>
      						<td class="bold" id="pel_ponsel"></td>
      					</tr>
      					<tr>
      						<td width="180px">Email</td>
      						<td width="10px">:</td>
      						<td class="bold" id="pel_email"></td>
      					</tr>
      					<tr>
      						<td width="180px">Kode PPN</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_ppn"></td>
      					</tr>
      					<tr>
      						<td width="180px">NPWP</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_npwp"></td>
      					</tr>
      					<tr>
      						<td width="180px">Faktur Pajak</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_pajak"></td>
      					</tr>
      					<tr>
      						<td colspan="3">Nama dan Jabatan Contact Person Customer Corporate/Fleet:</td>
      					</tr>
      					<tr>
      						<td colspan="3" id="spk_fleet"></td>
      					</tr>
      				</table>
      			</div>      			
      			<div class="col s12 m6"  style="border-left:1px solid #ddd">
      				<table class="info">
      					<tr>
      						<td width="180px">Faktur STNK a/n</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_nama"></td>
      					</tr>
      					<tr>
      						<td width="180px">No. KTP</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_identitas"></td>
      					</tr>
      					<tr>
      						<td width="180px">Alamat KTP/KIMS</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_alamat"></td>
      					</tr>
      					<tr>
      						<td width="180px">Kode Pos</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_pos"></td>
      					</tr>
      					<tr>
      						<td width="180px">Alamat Tempat Tinggal</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_alamatd"></td>
      					</tr>
      					<tr>
      						<td width="180px">Kode Pos</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_posd"></td>
      					</tr>
      					<tr>
      						<td width="180px">Telepon</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_telp"></td>
      					</tr>
      					<tr>
      						<td width="180px">Ponsel</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_ponsel"></td>
      					</tr>
      					<tr>
      						<td width="180px">Email</td>
      						<td width="10px">:</td>
      						<td class="bold" id="spk_stnk_email"></td>
      					</tr>
      				</table>
      			</div>
      		</div>
      	</div>
      	<div id="kendaraan">
      		<div class="row" style="margin:0">
		      	<div class="col m6"  style="border-right:1px solid #ddd;">
		      		<table class="info">
		      			<tr>
		      				<td width="180px">Variant</td>
		      				<td width="10px">:</td>
		      				<td class="bold" id="variant_nama"></td>
		      			</tr>
		      			<tr>
		      				<td width="180px">ID</td>
		      				<td width="10px">:</td>
		      				<td class="bold" id="variant_id"></td>
		      			</tr>
		      			<tr>
		      				<td width="180px">Warna</td>
		      				<td width="10px">:</td>
		      				<td class="bold" id="spk_warna"></td>
		      			</tr>
		      			<tr>
		      				<td width="180px">Keterangan</td>
		      				<td width="10px">:</td>
		      				<td class="bold" id="variant_ket"></td>
		      			</tr>
		      		</table>
	      		</div>      		
		      	<div class="col m6">
      				<div class="no-data">Belum Ada Kendaraan yang Match</div>
		      		<table class="info">
		      			<tr>
		      				<td width="180px">No DH</td>
		      				<td width="10px">:</td>
		      				<td class="bold" id="trk_dh"></td>
		      			</tr>
		      			<tr>
		      				<td width="180px">Warna</td>
		      				<td width="10px">:</td>
		      				<td class="bold" id="trk_warna"></td>
		      			</tr>
		      			<tr>
		      				<td width="180px">No Rangka</td>
		      				<td width="10px">:</td>
		      				<td class="bold" id="trk_rangka"></td>
		      			</tr>
		      			<tr>
		      				<td width="180px">No Mesin</td>
		      				<td width="10px">:</td>
		      				<td class="bold" id="trk_mesin"></td>
		      			</tr>
		      		</table>
		      	</div>
	      	</div>
      	</div>
      	<div id="diskon" style="padding:0px">
      		<div class="no-data">Diskon Belum Ada / Belum Disetujui</div>
      		<div class="row" style="margin:0px; padding:0px;">
      			<div class="col m5"  style="border-right:1px solid #ddd">
      				<table class="info payment" style="border:0;margin-bottom:0">
	      					<tr class="jsgrid-row">
	      						<td class="jsgrid-cell" width="180px">Cashback</td>
	      						<td class="jsgrid-cell bold text-right" id="diskon_cashback"></td>
	      					</tr>
	      					<tr class="jsgrid-row">
	      						<td class="jsgrid-cell" colspan="2">Aksesoris</td>
	      					</tr>
	      			</table>
	      			<table id="aksesoris" class="info payment" style="border:0;margin:0">
	      			</table>
	      			<table class="info payment" style="border:0;margin-top:0">
	      					<tr class="jsgrid-row">
	      						<td class="jsgrid-cell" width="180px">Komisi</td>
	      						<td class="bold jsgrid-cell text-right" id="diskon_komisi"></td>
	      					</tr>
	      					<tr class="jsgrid-row">
	      						<td class="jsgrid-cell bold text-right" style="background: #f3f3f3">TOTAL</td>
	      						<td class="jsgrid-cell bold text-right" style="background: #f3f3f3" id="diskon_total"></td>
	      					</tr>
      				</table>
      			</div>
      			<div class="col m7">
      				<table class="info" style="margin-top:0">
      					<tr>
      						<td colspan="3">Keterangan</td>
      					</tr>
      					<tr>
      						<td class="bold" colspan="3" id="diskon_ket"></td>
      					</tr>
      				</table>
      			</div>
      		</div>
      	</div>
            <div id="pembayaran" style="padding:0px;">                  
                  <div class="row" style="margin:0;padding:0 9px;">
                        <div class="col m4">
                              <table class="info payment" style="border:0;">
                                    <tr>
                                          <td width="140px">Jenis Pembayaran</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_metode"></td>
                                    </tr>
                              </table>
                              <table class="info payment" style="border:0; background: #f2f2f2; width: 100%;">
                                    <tr>
                                          <td><p style="margin:5px;"><b>Rincian PO Leasing</b></p></td>
                                    </tr>
                              </table>
                              <table class="info payment" style="border:0">
                                    <tr class="leasing">
                                          <td width="140px">Leasing</td>
                                          <td width="10px">:</td>
                                          <td class="bold"><input type="text"  placeholder="0" class="text-right" id="spk_leasing" disabled="" /></td>
                                    </tr>
                                    <tr class="leasing">
                                          <td width="140px">Jenis Asuransi</td>
                                          <td width="10px">:</td>
                                          <td class="bold"><input type="text"  placeholder="0" class="text-right" id="spk_asuransi" disabled="" /></td>
                                    </tr>
                                    <tr class="leasing">
                                          <td width="140px">Jangka Waktu</td>
                                          <td width="10px">:</td>
                                          <td class="bold"><input type="text"  placeholder="0" class="text-right" id="spk_waktu" disabled="" /></td>
                                    </tr>
                                    <tr class="leasing">
                                          <td width="140px">Angsuran/bln</td>
                                          <td width="10px">:</td>
                                          <td class="bold"><input type="text"  placeholder="0" class="text-right" id="spk_angsuran" disabled="" /></td>
                                    </tr>
                                    <tr class="leasing">
                                          <td width="140px">Down Payment</td>
                                          <td width="10px">:</td>
                                          <td class="bold"><input type="text"  placeholder="0" class="text-right" id="spk_dp" disabled="" /></td>
                                    </tr>
                                    <tr class="leasing">
                                          <td width="140px">Dropping</td>
                                          <td width="10px">:</td>
                                          <td class="bold"><input type="text"  placeholder="0" class="text-right" id="spk_droping" disabled="" /></td>
                                    </tr>
                                    
                                    <tr class="leasing">
                                          <td width="140px" class="bold">Total </td>
                                          <td width="10px">:</td>
                                          <td class="bold"><input type="text"  placeholder="0" class="text-right" id="spk_tleasing" disabled="" /></td>
                                    </tr>
                              </table>
                    
                        </div>
                        <div class="col m8 p0"  style="border-left:1px solid #ddd"> 
                              <div class="row" style="margin:0">
                                    <div class="col m5 ">
                                          <table class="info payment" style="border:0; margin:0; margin-bottom:15px;">
                                                <tr><td style=" background: #f2f2f2;" colspan="3"><p style="margin:5px;"><b>Rincian Biaya:</b></p></td></tr>
                                          </table>
                                          <table class="info payment" style="border:0;margin:0">
                                                <tr>
                                                      <td width="100px">DPP</td>
                                                      <td width="10px">:</td>
                                                      <td class="bold right"><input type="text"  placeholder="0"  class="text-right" id="trk_dpp" readonly="" /></td>
                                                </tr>
                                                <tr>
                                                      <td width="100px">PPN</td>
                                                      <td width="10px">:</td>
                                                      <td class="bold right"><input type="text"  placeholder="0"  class="text-right" id="trk_ppn" readonly="" /></td>
                                                </tr>
                                                <tr>
                                                      <td width="100px" class="bold">Off The Road</td>
                                                      <td width="10px">:</td>
                                                      <td class="bold right"><input type="text"  placeholder="0" id="off"  class="number text-right"  /></td>
                                                </tr>
                                                <tr id="tr_bbn">
                                                      <td width="100px">BBN</td>
                                                      <td width="">:</td>
                                                      <td class="bold right"><input type="text"  placeholder="0" id="bbn"  class="number text-right" disabled="" /></td>
                                                </tr>
                                                <tr id="tr_on">
                                                      <td width="100px" class="bold">On The Road</td>
                                                      <td width="10px">:</td>
                                                      <td class="bold right"><input type="text" readonly="" placeholder="0" id="on"  class="number text-right"  /></td>
                                                </tr>
                                          </table>
                                    </div>
                                    <div class="col m7">
                                          <table class="info payment" style="border:0;margin:0; background: #f2f2f2;">
                                                <tr><td><p style="margin:5px;"><b>Rincian Pembayaran:</b></p></td></tr>
                                          </table>
                                          <div style="height: 150px;overflow:hidden;overflow-y:auto;border:1px solid #ddd; background:#f9f9f9;margin-bottom:10px">
                                                <table class="info payment" style="border:0;margin:0">
                                                      <thead>
                                                            <tr class="jsgrid-header-row">
                                                                  <th class="jsgrid-header-cell p5">Tanggal</th>
                                                                  <th class="jsgrid-header-cell p5 text-right">Jumlah (Rp)</th>
                                                            </tr>
                                                      </thead>
                                                      <tbody id="tabel_pembayaran">
                                                      </tbody>
                                                </table>
                                          </div>
                                          <table class="info payment bold" style="border:0;margin-top:0">
                                                <tr>
                                                      <td width="160px">Subtotal Biaya</td>
                                                      <td width="10px">:</td>
                                                      <td class="bold right"><input type="text" disabled="" placeholder="0" id="hpp" class="text-right" /></td>
                                                </tr>
                                                <tr>
                                                      <td width="160px">Potongan Harga</td>
                                                      <td width="10px">:</td>
                                                      <td class="bold right"><input type="text" disabled="" placeholder="0" id="cashback" class="text-right" /></td>
                                                </tr>
                                                <tr>
                                                      <td width="160px">Total Biaya</td>
                                                      <td width="10px">:</td>
                                                      <td class="bold right"><input type="text" disabled="" placeholder="0" id="total"  class="text-right" /></td>
                                                </tr>
                                                <tr>
                                                      <td width="160px">Total Bayar</td>
                                                      <td width="10px">:</td>
                                                      <td class="bold right"><input type="text" disabled="" placeholder="0" id="total_bayar"  class="text-right" /></td>
                                                </tr>
                                                <tr>
                                                      <td width="160px">Piutang (Kurang Bayar)</td>
                                                      <td width="10px">:</td>
                                                      <td class="bold right"><input type="text" disabled="" placeholder="0" id="piutang"  class="text-right" /></td>
                                                </tr>
                                        </table>
                                    </div>
                              </div> 
                        </div>
                  </div>
            </div>
      	<div id="faktur" style="padding:0px;">      		
      		<div class="row" style="margin:0;padding:0 9px;">
      			<div class="col m5">
      			 	<p style=" margin-left: 10px;">Status SPK</p>
      			 	<table style="margin:0; margin-left: 10px; width: 100%;">
      			 		<tr>
      			 			<td style="padding: 5px; width: 90%; border: 1px solid #e6e6e6;"><p style="margin: 0; margin-left: 8px;">Matching Unit</p></td>
      			 			<th style="padding: 5px; width: 10%; border: 1px solid #e6e6e6;">
      			 				<i class="material-icons green-text" ><div id="status_matching"></div></i>
      			 			</th>
      			 		</tr>
      			 		<tr>
      			 			<td style="padding: 5px; width: 90%; border: 1px solid #e6e6e6;"><p style="margin: 0; margin-left: 8px;">Diskon</p></td>
      			 			<th style="padding: 5px; width: 10%; border: 1px solid #e6e6e6;">
      			 				<i class="material-icons green-text" ><div id="status_diskon"></div></i>
      			 			</th>
      			 		</tr>
      			 		<tr id="status_po_row">
      			 			<td style="padding: 5px; width: 90%; border: 1px solid #e6e6e6;"><p style="margin: 0; margin-left: 8px;">PO Leasing</p></td>
      			 			<th style="padding: 5px; width: 10%; border: 1px solid #e6e6e6;">
      			 				<i class="material-icons green-text" ><div id="status_po"></div></i>
      			 			</th>
      			 		</tr>
      			 		<tr>
      			 			<td style="padding: 5px; width: 90%; border: 1px solid #e6e6e6;"><p style="margin: 0; margin-left: 8px;">Pelunasan</p></td>
      			 			<th style="padding: 5px; width: 10%; border: 1px solid #e6e6e6;">
      			 				<i class="material-icons green-text" ><div id="status_pelunasan"></div></i>
      			 			</th>
      			 		</tr>
      			 		<tr>
      			 			<td colspan="2" style="padding: 5px; width: 90%; border: 1px solid #e6e6e6;">
      			 				<p style="margin: 0; margin-left: 8px;">
      			 					Keterangan
      			 				</p>
      			 				<div style="margin-bottom: 10%;" id="keterangan"></div>
      			 			</td>
      			 		</tr>
      			 	</table>
      			</div>
      			<div class="col m1">
      			</div>
      			<div class="col m7">
      			 	<p style="margin-left: 30px;">Rincian Faktur Penjualan</p>
      			 	<table style="margin:0; margin-left: 30px; width: 92%;">
      			 		<tr>
      			 			<td style="padding: 5px; width: 30%; border: 1px solid #e6e6e6;"><p style="margin: 0; margin-left: 8px;">Nomor Faktur</p></td>
      			 			<th style="padding: 5px; width: 70%; border: 1px solid #e6e6e6;">
      			 				<div id="spk_id_faktur"></div>
      			 			</th>
      			 		</tr>
      			 		<tr>
      			 			<td style="padding: 5px; width: 30%; border: 1px solid #e6e6e6;"><p style="margin: 0; margin-left: 8px;">Tanggal Faktur</p></td>
      			 			<th style="padding: 5px; width: 70%; border: 1px solid #e6e6e6;">
      			 				<div id="spk_tgl_faktur"></div>
      			 			</th>
      			 		</tr>
      			 	</table>
      			</div>
      		</div>
      		<div style="padding:10px;background:#f5f5f5;margin-top:30px;">
      			<center>
      				<button class="waves-effect waves-light btn orange" id="konfirmasi" title="Cetak Faktur">
      					<i class="material-icons left" >done</i>
      					<span>Konfirmasi</span>
      				</button>
      			</center>
      		</div>
      	</div>
    </div>
</div>

<div id="konfirmasi_spk" class="modal"  style="width:450px;">  
    <h6 class="modal-title blue-grey darken-1">
        <span>Konfirmasi Spk</span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:15px; position: relative;">
        <div class="row" style="margin:0;margin-bottom:10px;">
          <span style="color: red">*</span> <span style="color: black">Berikan Catatan Saat Anda Melakukan Konfirmasi</span> 
                  <div class="col m12" style="font-size:12px;margin-bottom:15px">
                         <textarea id="spk_catt_permintaanDo" class="" style="height: 120px" placeholder="Berikan Catatan"></textarea>
                  </div>
                  <div class="col m6">
                        <a id="tanggapi" target="_blank" class="waves-effect waves-light orange btn-large" style="width:100%"><i class="material-icons left">error_outline</i> Tanggapi</a>
                  </div>
                  <div class="col m6">
                        <a id="setujui" target="_blank" class="waves-effect waves-light green btn-large" style="width:100%"><i class="material-icons left">done</i> Setujui</a>
                  </div>
        </div>
    </div>
</div>
