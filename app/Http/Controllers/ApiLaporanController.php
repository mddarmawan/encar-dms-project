<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ApiLaporanController extends Controller
{
    function f_kendaraan($id){
		$data = DB::table('tb_spk_faktur')
    		->leftjoin('tb_spk', 'tb_spk_faktur.spkf_spk', '=', 'tb_spk.spk_id')
    		->leftjoin('tb_spk_diskon', 'tb_spk_diskon.spkd_spk', '=', 'tb_spk.spk_id')
    		->leftjoin('tb_tr_kendaraan', 'tb_tr_kendaraan.trk_dh', '=', 'tb_spk.spk_dh')
    		->leftjoin('tb_spk_leasing', 'tb_spk_leasing.spkl_spk', '=', 'tb_spk.spk_id')
    		->leftjoin('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
    		->leftjoin('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
    		->leftjoin('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
    		->leftjoin('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
    		->leftjoin('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
    		->leftjoin('tb_referral', 'tb_spk_diskon.spkd_ref_nama', '=', 'tb_referral.referral_nama')
    		->leftjoin('tb_asuransi', 'tb_spk_leasing.spkl_asuransi', '=', 'tb_asuransi.asuransi_id')
    		->leftjoin('tb_asuransi_jenis', 'tb_spk_leasing.spkl_jenis_asuransi', '=', 'tb_asuransi_jenis.ajenis_id')
    		->leftjoin('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk.spk_leasing')
    		->leftjoin('tb_warna', 'warna_id', '=', 'tb_spk.spk_warna')
    		->where('spkf_id', $id)
			->get();
		$result = $data;

		$data = array();
		foreach($result as $r){
			$item = array();
			$item['spkf_id'] = $r->spkf_id;
			$item['spkf_tgl'] = date_format(date_create($r->spkf_tgl),"d/m/Y");
			$item['spk_id'] = $r->spk_id;
			$item['spk_referral'] = strtoupper($r->referral_nama);
			$item['spk_tgl'] = date_format(date_create($r->spk_tgl),"d/m/Y");
            $item['spk_pel_nama'] = strtoupper($r->spk_pel_nama);
            $item['spk_pel_alamat'] = strtoupper($r->spk_pel_alamat); 
            $item['spk_stnk_nama'] = strtoupper($r->spk_stnk_nama);
            $item['spk_stnk_alamat'] = strtoupper($r->spk_stnk_alamat);
            //$item['spk_dpp'] = ucwords($r->spk_dpp);
            //$item['spk_bbn'] = ucwords($r->spk_bbn);
			$item['spk_type'] = ucwords($r->type_nama);
			$item['spk_variant'] = ucwords($r->type_nama." ".$r->variant_nama);
			$item['spk_warna'] = ucwords ($r->warna_nama);
			$item['spk_no_rangka'] = strtoupper($r->trk_rangka);
			$item['spk_no_mesin'] = strtoupper($r->trk_mesin);
			$item['spk_serial'] = strtoupper($r->variant_serial);
			$item['spk_sales'] = ucwords($r->karyawan_nama);
			$item['spk_team'] = strtoupper($r->team_nama);
			
			$pembayaran = number_format(DB::table('tb_spk_pembayaran')->select(DB::raw("SUM(spkp_jumlah) as bayar"))
    		->where("spkp_spk",$r->spk_id)->first()->bayar,0,",",".");

    		$item['spk_pembayaran'] = $pembayaran;

    		$item['spk_riwayat_pembayaran'] = DB::table('tb_spk_pembayaran')->where('spkp_spk', $r->spk_id)->get();
			$item['spk_pel_kota'] = strtoupper($r->spk_pel_kota);
			$item['spk_metode'] = ($r->spk_pembayaran == 0 ? 'CASH' : 'CREDIT');
			if ($r->spk_pembayaran == 0){
				$item['spk_via'] = "CASH";		
			}else{
				$item['spk_via'] = $r->leasing_nick;			
			}
			$item['spk_status'] = $r->spk_status;
			$item['spk_ket'] = "-";
			$item['spkl_droping'] = number_format($r->spkl_droping,0,",",".");

			$cashback = number_format($r->spkd_cashback, 0, ",", ".");
			$item['spk_cashback'] = $cashback;
			$item['spk_bayar'] =number_format($r->spkd_cashback + (DB::table('tb_spk_pembayaran')->select(DB::raw("SUM(spkp_jumlah) as bayar"))
    		->where("spkp_spk",$r->spk_id)->first()->bayar),0,",",".");

			$item['spk_asuransi'] = ucwords($r->asuransi_nama);
			$item['spk_asuransi_jenis'] = ucwords($r->ajenis_nama);
			$item['spk_waktu'] = ucwords($r->spkl_waktu);
			$item['spk_leasing'] = ucwords($r->leasing_nama);
			$item['spk_dh'] = $r->trk_dh;
			$item['spk_variant_off'] = number_format(($r->variant_on - $r->variant_bbn), 0, ",", ".");
			$item['spk_variant_on'] = number_format($r->variant_on, 0, ",", ".");
			$item['spk_variant_bbn'] = number_format($r->variant_bbn, 0, ",", ".");

			$item['diskon'] = DB::table('tb_spk_diskon')
	    		->where('spkd_status',1)
	    		->where('spkd_spk', $r->spk_id)->get();

	    	$item['aksesoris'] = array();
	    	if ($item['diskon'] != NULL && $item['aksesoris'] != NULL){
	    		$item['aksesoris'] = DB::table('tb_spk_aksesoris')
	    		->where('spka_diskon', $item['diskon'][0]->spkd_id)->get();
	    	}

			array_push($data, $item);
		}

    	return json_encode($data);
    }

   
    public function t_tagihan_leasing($id){
		$data = DB::table('tb_spk_leasing')
			->leftjoin('tb_spk', 'tb_spk_leasing.spkl_spk', '=', 'tb_spk.spk_id')
			->leftjoin('tb_spk_pembayaran', 'tb_spk_pembayaran.spkp_spk', '=', 'tb_spk.spk_id')
			->leftjoin('tb_spk_diskon', 'tb_spk_diskon.spkd_spk', '=', 'tb_spk.spk_id')
			//->leftjoin('tb_spk_aksesoris', 'tb_spk_aksesoris.spka_diskon', '=', 'tb_spk_diskon.spkd_id')
			->leftjoin('tb_leasing', 'tb_spk_leasing.spkl_leasing', '=', 'tb_leasing.leasing_id')
			->leftjoin('tb_tr_kendaraan', 'tb_spk.spk_dh', '=', 'tb_tr_kendaraan.trk_dh')
			->leftjoin('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
			->leftjoin('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
    		->leftjoin('tb_warna', 'warna_id', '=', 'tb_spk.spk_warna')
			->where('spkl_spk', $id)
			->get();
		$result = $data;

		$data = array();
		foreach($result as $r){
			$item = array();
			$item['spkp_id'] = $r->spkp_id;
			$item['spk_id'] = $r->spk_id;
			$item['spkd_id'] = $r->spkd_id;
			$item['spk_dh'] = $r->spk_dh;
			$item['spk_bbn'] = $r->spk_bbn;
			$item['spk_dp'] = number_format($r->spk_dp, 0, ",", ".");
			$item['spk_warna'] = ucwords ($r->warna_nama);
			$item['spk_no_mesin'] = strtoupper($r->trk_mesin);
			$item['spk_no_rangka'] = strtoupper($r->trk_rangka);
			//$item['spka_diskon'] = strtoupper($r->spka_diskon);
			$item['spk_pel_nama'] = strtoupper($r->spk_pel_nama);
			$item['spk_pel_alamat'] = strtoupper($r->spk_pel_alamat);
			$item['spk_stnk_nama'] = strtoupper($r->spk_stnk_nama);
			$item['spk_stnk_alamat'] = strtoupper($r->spk_stnk_alamat);
			$item['spk_variant'] = strtoupper($r->type_nama." ".$r->variant_nama);
			$item['spk_tahun'] = $r->trk_tahun;
			$item['spkp_jumlah'] = number_format($r->spkp_jumlah, 0, ",", ".");
			$item['spkp_ket'] = ucwords($r->spkp_ket);
			//$item['spka_harga'] = ucwords($r->spka_harga);
			$item['spk_leasing_nama'] = strtoupper($r->leasing_nama);
			$item['spk_leasing_alamat'] = ucwords($r->leasing_alamat);
			$item['spk_variant_off'] = number_format(($r->variant_on - $r->variant_bbn), 0, ",", ".");
			$item['spk_variant_on'] = number_format($r->variant_on, 0, ",", ".");
			$item['spk_variant_bbn'] = number_format($r->variant_bbn, 0, ",", ".");
			$item['spkp_pelunasan'] = number_format(($r->variant_on - $r->spk_dp), 0, ",", ".");
			$item['spkp_terbilang'] = ucfirst($this->Terbilang($r->variant_on - $r->spkp_jumlah));
			array_push($data, $item);
		}

		return json_encode($data);
	}

	public function t_pembayaran_leasing($id){
		setlocale(LC_ALL,"ID");

		$data = DB::table('tb_leasing_bayar')
    		->leftjoin('tb_spk_leasing', 'tb_leasing_bayar.lbayar_spkl', '=', 'tb_spk_leasing.spkl_id')
    		->leftjoin('tb_spk', 'tb_spk_leasing.spkl_spk', '=', 'tb_spk.spk_id')
    		->leftjoin('tb_leasing', 'tb_spk_leasing.spkl_leasing', '=', 'tb_leasing.leasing_id')
    		->leftjoin('tb_tr_kendaraan', 'tb_spk.spk_dh', '=', 'tb_tr_kendaraan.trk_dh')
    		->leftjoin('tb_warna', 'tb_tr_kendaraan.trk_warna', '=', 'tb_warna.warna_id')
    		->leftjoin('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
    		->leftjoin('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
    		->where('spkl_spk', $id)
			->get();
		$result = $data;

		$data = array();
		foreach($result as $r){
			$date_unix = strtotime($r->spk_tgl);
			$date_local = strftime("%d %B %Y", $date_unix);

			$item = array();
			$item['lbayar_id'] = $r->spkl_id;
			$item['spk_id'] = $r->spk_id;
			$item['spk_tgl'] = $date_local;
			$item['spk_dh'] = $r->spk_dh;
			$item['spk_dpp'] = $r->spk_dpp;
			$item['spk_bbn'] = $r->spk_bbn;
			$item['spk_dp'] = $r->spk_dp;
			$item['spk_no_rangka'] = strtoupper($r->trk_rangka);
			$item['spk_no_mesin'] = strtoupper($r->trk_mesin);
			$item['spk_warna'] = strtoupper($r->warna_nama);
            $item['spk_pel_nama'] = strtoupper($r->spk_pel_nama);
			$item['spk_variant'] = strtoupper($r->type_nama." ".$r->variant_nama);
            $item['lbayar_nominal'] = number_format($r->lbayar_nominal, 0, ".", ",");
            $item['lbayar_ket'] = strtoupper($r->lbayar_ket);
            $item['lbayar_terbilang'] = ucfirst($this->Terbilang($r->lbayar_nominal));
			array_push($data, $item);
		}

    	return json_encode($data);
    }

    function t_terima_bpkb($id){
		$data = DB::table('tb_spk_ttbj')
    		->join('tb_spk', 'tb_spk_ttbj.spkt_spk', '=', 'tb_spk.spk_id')
    		->join('tb_tr_kendaraan', 'tb_spk.spk_dh', '=', 'tb_tr_kendaraan.trk_dh')
    		->join('tb_spk_leasing', 'tb_spk_leasing.spkl_spk', '=', 'tb_spk.spk_id')
    		->join('tb_leasing', 'tb_spk_leasing.spkl_leasing', '=', 'tb_leasing.leasing_id')
    		->join('tb_asuransi_jenis', 'tb_spk_leasing.spkl_jenis_asuransi', '=', 'tb_asuransi_jenis.ajenis_id')
    		->where('spkt_id', $id)
			->get();
		$result = $data;

		$data = array();
		foreach($result as $r){
			$item = array();
			$item['spkt_id'] = $r->spkt_id;
            $item['spk_terima_nama'] = ucwords($r->spkt_terima_nama);
            $item['spk_terima_alamat'] = ucwords($r->spkt_terima_alamat);
			$item['spk_id'] = $r->spk_id;
            $item['spk_stnk_nama'] = ucwords($r->spk_stnk_nama);
            $item['spk_stnk_alamat'] = ucwords($r->spk_stnk_alamat);
            $item['spk_no_bpkb'] = ucwords($r->spkt_nobpkb);
            $item['spk_no_mesin'] = ucwords($r->trk_mesin);
			$item['spk_rangka'] = ucwords($r->trk_rangka);
			$item['spk_nopol'] = ucwords($r->spkt_nopol);
			$item['spk_dh'] = ucwords ($r->trk_dh);
			$item['spk_leasing_nick'] = ucwords ($r->leasing_nick);
			$item['spk_asuransi_jenis'] = ucwords ($r->ajenis_nama);
			
			array_push($data, $item);
		}

    	return json($data);
    }


     function t_kwitansi_pembayaran($id){
     	setlocale(LC_ALL,"ID");
		$data = DB::table('tb_spk_pembayaran')
			->leftjoin('tb_spk', 'tb_spk_pembayaran.spkp_spk', '=', 'tb_spk.spk_id')
			->leftjoin('tb_akun', 'tb_spk_pembayaran.spkp_akun', '=', 'tb_akun.akun_id')
			->leftjoin('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
			->leftjoin('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
    		->leftjoin('tb_warna', 'warna_id', '=', 'tb_spk.spk_warna')
			->where('spkp_id', $id)
			->get();
		$result = $data;

		$data = array();
		foreach($result as $r){
			$date_unix = strtotime($r->created_at);
			$date_local = strftime("%d %B %Y", $date_unix);
			$item = array();
			$item['spkp_id'] = $r->spkp_id;
			$item['spk_id'] = $r->spk_id;
			$item['spkp_nominal'] = number_format($r->spkp_jumlah, 0, ",", ".");
			$item['spk_pel_nama'] = $r->spk_pel_nama;
			$item['spkp_ket'] = $r->spkp_ket;
			$item['spk_variant'] = strtoupper($r->type_nama." ".$r->variant_nama);
			$item['spk_variant'] = strtoupper($r->warna_nama);
			$item['spkp_akun'] = $r->akun_nama;
			$item['spkp_akun_id'] = $r->akun_id;
			$item['spkp_tgl'] =$date_local;
			$item['spkp_terbilang'] = ucfirst($this->Terbilang($r->spkp_jumlah));
			array_push($data, $item);
		}
		return json_encode($data);
	}

	function f_BAST($id){
     	setlocale(LC_ALL,"ID");
		$data = DB::table('tb_spk')
    		->join('tb_spk_faktur', 'tb_spk_faktur.spkf_spk', '=', 'tb_spk.spk_id')
    		->join('tb_spk_diskon', 'tb_spk_diskon.spkd_spk', '=', 'tb_spk.spk_id')
    		->join('tb_tr_kendaraan', 'tb_tr_kendaraan.trk_dh', '=', 'tb_spk.spk_dh')
    		->join('tb_spk_leasing', 'tb_spk_leasing.spkl_spk', '=', 'tb_spk.spk_id')
    		->join('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk_leasing.spkl_leasing')
    		->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
    		->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
    		->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
    		->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
    		->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
    		->leftjoin('tb_warna', 'warna_id', '=', 'tb_spk.spk_warna')
    		->where('spk_id', $id)
			->get();
		$result = $data;

		$data = array();
		foreach($result as $r){
			$date_unix = strtotime($r->spkf_tgl);
			$date_local = strftime("%d %B %Y", $date_unix);
			$item = array();
			$item['spkf_id'] = $r->spkf_id;
			$item['spkf_tgl'] =$date_local;

			$item['spk_id'] = $r->spk_id;
			//$item['spk_referral'] = strtoupper($r->referral_nama);
			$item['spk_tgl'] = date_format(date_create($r->spk_tgl),"d/m/Y");
            $item['spk_pel_nama'] = strtoupper($r->spk_pel_nama);
            $item['spk_dh'] = strtoupper($r->spk_dh);
            $item['spk_pel_alamat'] = strtoupper($r->spk_pel_alamat); 
            $item['spk_stnk_nama'] = strtoupper($r->spk_stnk_nama);
            $item['spk_stnk_alamat'] = strtoupper($r->spk_stnk_alamat);
			$item['spk_type'] = ucwords($r->type_nama);
			$item['spk_variant'] = ucwords($r->type_nama." ".$r->variant_nama);
			$item['spk_warna'] = ucwords ($r->warna_nama);
			$item['spk_no_rangka'] = strtoupper($r->trk_rangka);
			$item['spk_no_mesin'] = strtoupper($r->trk_mesin);
			$item['spk_serial'] = strtoupper($r->variant_serial);
			$item['spk_sales'] = ucwords($r->karyawan_nama);
			$item['spk_team'] = strtoupper($r->team_nama);
			$item['spk_pembayaran'] = number_format(DB::table('tb_spk_pembayaran')->select(DB::raw("SUM(spkp_jumlah) as bayar"))
    		->where("spkp_spk",$r->spk_id)->first()->bayar,0,",",".");
    		
			$item['spk_pel_kota'] = strtoupper($r->spk_pel_kota);
			$item['spk_metode'] = ($r->spk_pembayaran == 0 ? 'CASH' : 'CREDIT');

			if ($r->spk_pembayaran == 0){
				$item['spk_via'] = "CASH";		
			}else{
				$item['spk_via'] = $r->leasing_nick;			
			}

			$item['spk_status'] = $r->spk_status;
			$item['spk_ket'] = "-";
		
			
			array_push($data, $item);
		}

    	return json_encode($data);
    }


	public function Terbilang($angka) {
		// pastikan kita hanya berususan dengan tipe data numeric
		$angka = (float)$angka;
		 
		// array bilangan 
		// sepuluh dan sebelas merupakan special karena awalan 'se'
		$bilangan = array(
				'',
				'satu',
				'dua',
				'tiga',
				'empat',
				'lima',
				'enam',
				'tujuh',
				'delapan',
				'sembilan',
				'sepuluh',
				'sebelas'
		);
		 
		// pencocokan dimulai dari satuan angka terkecil
		if ($angka < 12) {
			// mapping angka ke index array $bilangan
			return $bilangan[$angka];
		} else if ($angka < 20) {
			// bilangan 'belasan'
			// misal 18 maka 18 - 10 = 8
			return $bilangan[$angka - 10] . ' belas';
		} else if ($angka < 100) {
			// bilangan 'puluhan'
			// misal 27 maka 27 / 10 = 2.7 (integer => 2) 'dua'
			// untuk mendapatkan sisa bagi gunakan modulus
			// 27 mod 10 = 7 'tujuh'
			$hasil_bagi = (int)($angka / 10);
			$hasil_mod = $angka % 10;
			return trim(sprintf('%s puluh %s', $bilangan[$hasil_bagi], $bilangan[$hasil_mod]));
		} else if ($angka < 200) {
			// bilangan 'seratusan' (itulah indonesia knp tidak satu ratus saja? :))
			// misal 151 maka 151 = 100 = 51 (hasil berupa 'puluhan')
			// daripada menulis ulang rutin kode puluhan maka gunakan
			// saja fungsi rekursif dengan memanggil fungsi $this->Terbilang(51)
			return sprintf('seratus %s', $this->Terbilang($angka - 100));
		} else if ($angka < 1000) {
			// bilangan 'ratusan'
			// misal 467 maka 467 / 100 = 4,67 (integer => 4) 'empat'
			// sisanya 467 mod 100 = 67 (berupa puluhan jadi gunakan rekursif $this->Terbilang(67))
			$hasil_bagi = (int)($angka / 100);
			$hasil_mod = $angka % 100;
			return trim(sprintf('%s ratus %s', $bilangan[$hasil_bagi], $this->Terbilang($hasil_mod)));
		} else if ($angka < 2000) {
			// bilangan 'seribuan'
			// misal 1250 maka 1250 - 1000 = 250 (ratusan)
			// gunakan rekursif $this->Terbilang(250)
			return trim(sprintf('seribu %s', $this->Terbilang($angka - 1000)));
		} else if ($angka < 1000000) {
			// bilangan 'ribuan' (sampai ratusan ribu
			$hasil_bagi = (int)($angka / 1000); // karena hasilnya bisa ratusan jadi langsung digunakan rekursif
			$hasil_mod = $angka % 1000;
			return sprintf('%s ribu %s', $this->Terbilang($hasil_bagi), $this->Terbilang($hasil_mod));
		} else if ($angka < 1000000000) {
			// bilangan 'jutaan' (sampai ratusan juta)
			// 'satu puluh' => SALAH
			// 'satu ratus' => SALAH
			// 'satu juta' => BENAR 
			// @#$%^ WT*
			 
			// hasil bagi bisa satuan, belasan, ratusan jadi langsung kita gunakan rekursif
			$hasil_bagi = (int)($angka / 1000000);
			$hasil_mod = $angka % 1000000;
			return trim(sprintf('%s juta %s', $this->Terbilang($hasil_bagi), $this->Terbilang($hasil_mod)));
		} else if ($angka < 1000000000000) {
			// bilangan 'milyaran'
			$hasil_bagi = (int)($angka / 1000000000);
			// karena batas maksimum integer untuk 32bit sistem adalah 2147483647
			// maka kita gunakan fmod agar dapat menghandle angka yang lebih besar
			$hasil_mod = fmod($angka, 1000000000);
			return trim(sprintf('%s milyar %s', $this->Terbilang($hasil_bagi), $this->Terbilang($hasil_mod)));
		} else if ($angka < 1000000000000000) {
			// bilangan 'triliun'
			$hasil_bagi = $angka / 1000000000000;
			$hasil_mod = fmod($angka, 1000000000000);
			return trim(sprintf('%s triliun %s', $this->Terbilang($hasil_bagi), $this->Terbilang($hasil_mod)));
		} else {
			return 'Wow...';
		}
	}


}
