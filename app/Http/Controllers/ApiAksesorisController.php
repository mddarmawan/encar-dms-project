<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ApiAksesorisController extends Controller
{
    function all() {
        return json_encode(DB::table("tb_aksesoris")->get());
    }
    
    function index(){
        $data = DB::table('tb_aksesoris')
                ->where("aksesoris_hapus", '0')
                ->get();

            $result = array();
            foreach($data as $r){
            $item = array();
            $item['aksesoris_id'] = $r->aksesoris_id;
            $item['aksesoris_kode'] = $r->aksesoris_kode;
            $item['aksesoris_nama'] = $r->aksesoris_nama;
            $item['aksesoris_kendaraan'] = $r->aksesoris_kendaraan;
            $item['aksesoris_harga'] = number_format($r->aksesoris_harga,0,',','.');
            $item['aksesoris_vendor'] = $r->aksesoris_vendor;
            $item['aksesoris_status'] = $r->aksesoris_status;
            $item['aksesoris_tgl'] = date_format(date_create($r->created_at),"d/m/Y");
            if((!request("aksesoris_kode") || strrpos(strtolower($item['aksesoris_kode']), strtolower(request("aksesoris_kode"))) > -1) &&
                (!request("aksesoris_nama") || strrpos(strtolower($item['aksesoris_nama']), strtolower(request("aksesoris_nama"))) > -1) &&
                (!request("aksesoris_kendaraan") || strrpos(strtolower($item['aksesoris_kendaraan']), strtolower(request("aksesoris_kendaraan"))) > -1) &&
                (!request("aksesoris_harga") || strrpos(strtolower($item['aksesoris_harga']), strtolower(request("aksesoris_harga"))) > -1) &&
                (!request("aksesoris_vendor") || strrpos(strtolower($item['aksesoris_vendor']), strtolower(request("aksesoris_vendor"))) > -1) &&
                (!request("aksesoris_tgl") || strrpos(strtolower($item['aksesoris_tgl']), strtolower(request("aksesoris_tgl"))) > -1))   {
                $tgl = strtotime(str_replace("/","-",$item['aksesoris_tgl']));
                if (request("filter_awal") && request("filter_akhir")){
                    $filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
                    $filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
                    if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
                        array_push($result, $item);                     
                    }
                }else if (request("filter_awal")){
                    $filter_awal = strtotime(request("filter_awal"));
                    if ($filter_awal<=$tgl){
                        array_push($result, $item);                     
                    }
                }else if (request("filter_akhir")){
                    $filter_akhir = strtotime(request("filter_akhir"));
                    if ($filter_akhir>=$tgl){
                        array_push($result, $item);                     
                    }
                }else{
                    array_push($result, $item);
                }
            }
        }

        return json_encode($result);
    }

    function store(){
        $this->validate(request(), [
            "aksesoris_kode"      => "required",
            "aksesoris_nama"      => "required",
            "aksesoris_vendor"      => "required",
            "aksesoris_harga"     => "required"        
            ]);

       $insert = array(
            "aksesoris_kode"     =>  request("aksesoris_kode"),
            "aksesoris_nama"     =>  request("aksesoris_nama"),
            "aksesoris_kendaraan"    =>  request("aksesoris_kendaraan"),
            "aksesoris_harga"    =>  request("aksesoris_harga"),
            "aksesoris_vendor"    =>  request("aksesoris_vendor")
        );

       $id = DB::table('tb_aksesoris')->insertGetId($insert, 'aksesoris_id');

        return json_encode(DB::table('tb_aksesoris')->where("aksesoris_id", $id)->first());
    }

    function update(){
        $this->validate(request(), [
            "aksesoris_kode"      => "required",
            "aksesoris_nama"      => "required",
            "aksesoris_vendor"      => "required",
            "aksesoris_harga"     => "required"
        ]);

        $update = DB::table('tb_aksesoris')
            ->where("aksesoris_id",request("aksesoris_id"))
            -> update([
                "aksesoris_kode"     =>  request("aksesoris_kode"),
                "aksesoris_nama"     =>  request("aksesoris_nama"),
                "aksesoris_kendaraan"     =>  request("aksesoris_kendaraan"),
                "aksesoris_harga"    =>  request("aksesoris_harga"),
                "aksesoris_vendor"    =>  request("aksesoris_vendor")
            ]);

       return json_encode(DB::table('tb_aksesoris')->where("aksesoris_id",request("aksesoris_id"))->first());
    }

    function destroy(){
        $update = DB::table('tb_aksesoris')
            ->where("aksesoris_id",request("aksesoris_id"))
            -> update([
                "aksesoris_hapus"     => '1'
            ]);

        return json_encode($update);
    } 
}
