<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Variant;
use App\Tipekend;
class ApiKendaraanController extends Controller
{
    function all_type() {
        return json_encode(DB::table("tb_type")->get());
    }

    function all_variant() {
        return json_encode(DB::table("tb_variant")->get());
    }

    function variant_read(){
        $variant = DB::table('tb_variant')
                ->join('tb_type', 'tb_variant.variant_type', '=', 'tb_type.type_id')
                ->where("variant_hapus", 0)
                ->get();

        $result = $variant->filter(function ($variant) {
            return 
                (!request("variant_serial") || strrpos(strtolower($variant->variant_serial), strtolower(request("variant_serial"))) > -1) && 
                (!request("variant_nama") || strrpos(strtolower($variant->variant_nama), strtolower(request("variant_nama"))) > -1)&& 
                (!request("variant_status") || strrpos(strtolower($variant->variant_status), strtolower(request("variant_status"))) > -1)&& 
                (!request("variant_type") || strrpos(strtolower($variant->variant_type), strtolower(request("variant_type"))) > -1);

                 });
        $data = array();
        foreach($result as $d){
            $item = array();
            $item['variant_id'] = $d->variant_id;
            $item['variant_serial'] = $d->variant_serial;
            $item['variant_nama'] = $d->variant_nama;
            $item['variant_type'] = $d->variant_type;
            $item['variant_ket'] = $d->variant_ket;
            $item['variant_status'] = $d->variant_status;
            $item['type_poin'] = $d->type_poin;
            array_push($data, $item);
        }
        return json_encode($data);
    }

    function variant_type($id){ 
       $variant = DB::table('tb_variant')
                ->join('tb_type', 'tb_variant.variant_type', '=', 'tb_type.type_id')
                ->where("variant_type",$id)
                ->get();
        $data = array();
        foreach($variant as $d){
            $item = array();
            $item['variant_id'] = $d->variant_id;
            $item['variant_serial'] = $d->variant_serial;
            $item['variant_nama'] = $d->variant_nama;
            $item['variant_type'] = $d->type_nama;
            $item['variant_ket'] = $d->variant_ket;
            $item['variant_status'] = $d->variant_status;
            array_push($data, $item);
        }
        return json_encode($data);
    }

    
   function variant_store(){
        $this->validate(request(), [
            "variant_serial"      => "required",
            "variant_type"      => "required",
            "variant_nama"     => "required",
            "variant_ket"     => "required",
            "variant_status"     => "required",
        ]);

        
        $insert = array(
            "variant_serial"     =>  request("variant_serial"),
            "variant_type"     =>  request("variant_type"),
            "variant_nama"   =>  request("variant_nama"),
            "variant_ket"     =>  request("variant_ket"),
            "variant_status"     =>  request("variant_status"),
            "created_at"    => date("Y-m-d H:i:s"),
            "updated_at"    => date("Y-m-d H:i:s")
            );

        $id = DB::table('tb_variant')->insertGetId($insert, 'variant_id');
        return json_encode(DB::table('tb_variant')->where("variant_id", $id)->first());
    }   

    function variant_update(){
        $this->validate(request(), [
            "variant_serial"      => "required",
            "variant_type"      => "required",
            "variant_nama"     => "required",
            "variant_ket"     => "required",
            "variant_status"     => "required",
        ]);

        DB::table('tb_variant')-> where("variant_id",request("variant_id"))->update([
            "variant_serial"     =>  request("variant_serial"),
            "variant_type"     =>  request("variant_type"),
            "variant_nama"   =>  request("variant_nama"),
            "variant_ket"     =>  request("variant_ket"),
            "variant_status"     =>  request("variant_status"),
            "updated_at"    => date("Y-m-d H:i:s")
        ]);

        return json_encode(DB::table('tb_variant')-> where("variant_id",request("variant_id"))->first());
    }

    function variant_destroy(){
        $update = DB::table('tb_variant')
            ->where("variant_id",request("variant_id"))
            -> update([
                "variant_hapus"     => '1',
                "updated_at"    => date("Y-m-d H:i:s")
            ]);

        return json_encode($update);
    }    

    //type kendaraan 

    function type_read($id = NULL){
        if ($id === NULL) {
            $data = DB::table('tb_type')
                    ->orderBy("type_nama","ASC")
                    ->where("type_hapus", 0)
                    ->get();
            $result = $data->filter(function ($data) {
                return 
                    (!request("type_nama") || strrpos(strtolower($data->type_nama), strtolower(request("type_nama"))) > -1) &&
                    (!request("type_poin") || strrpos(strtolower($data->type_point), strtolower(request("type_poin"))) > -1) &&
                    (!request("type_status") || strrpos(strtolower($data->type_status), strtolower(request("type_status")))) ;
                });

            return json_encode($result);
        } else {
            $id = str_replace("%20", " ", $id);
            $data = DB::table('tb_type') 
                    -> where("type_nama", $id)
                    ->get();
            $result = $data->filter(function ($data) {
                return 
                    (!request("type_nama") || strrpos(strtolower($data->type_nama), strtolower(request("type_nama"))) > -1) &&
                     (!request("type_poin") || strrpos(strtolower($data->type_point), strtolower(request("type_poin"))) > -1)&&
                     (!request("type_status") || strrpos(strtolower($data->type_status), strtolower(request("type_status"))) > -1) ;
            });

            return json_encode($result);
        }
    }
    
   function type_store(){
        $this->validate(request(), [
            "type_nama"      => "required",
            "type_poin"     => "required",
        ]);

        
        $insert = array(
            "type_nama"     => request("type_nama"),
            "type_poin"     => request("type_poin"),
            "created_at"    => date("Y-m-d H:i:s"),
            "updated_at"    => date("Y-m-d H:i:s")
            );

        $id = DB::table('tb_type')->insertGetId($insert,'type_id');
        return json_encode(DB::table('tb_type')->where("type_id",$id)->first());
    }   



    function type_update(){
        $this->validate(request(), [
            "type_nama"      => "required",
            "type_poin"     => "required",
        ]);

         DB::table('tb_type')-> where("type_id",request("type_id"))->update([
            "type_nama"     =>  request("type_nama"),
            "type_poin"   =>  request("type_poin"),
            "type_status"   =>  request("type_status"),
            "updated_at"    => date("Y-m-d H:i:s")
        ]);

        return json_encode(DB::table('tb_type')->where("type_id",request("type_id"))->first());
    }



    function type_destroy(){
        $update = DB::table('tb_type')
            ->where("type_id",request("type_id"))
            -> update([
                "type_hapus"    => '1',
                "updated_at"    => date("Y-m-d H:i:s")
            ]);

        return json_encode($update);
    }   



    function bbn_master(){
        $data = DB::table("tb_variant")->join("tb_type","type_id","=","variant_type")->orderBy("type_nama","ASC")->orderBy("variant_nama","ASC")->get();
        $result = $data->filter(function ($data) {
            return 
                (!request("variant_serial") || strrpos(strtolower($data->variant_serial), strtolower(request("variant_serial"))) > -1) &&
                (!request("variant_type") || strrpos(strtolower($data->variant_type), strtolower(request("variant_type"))) > -1) &&
                (!request("variant_nama") || strrpos(strtolower($data->variant_nama), strtolower(request("variant_nama"))) > -1) &&
                 (!request("variant_ket") || strrpos(strtolower($data->variant_ket), strtolower(request("variant_ket"))) > -1) &&
                 (!request("variant_status") || strrpos(strtolower($data->variant_status), strtolower(request("variant_status"))) > -1);
        });

        $data = array();
        foreach($result as $r){
            $item = array();
            $item['variant_id'] = $r->variant_id;
            $item['variant_serial'] = $r->variant_serial;
            $item['variant_nama'] = $r->variant_nama;
            $item['variant_type'] = $r->variant_type;
            $item['variant_off'] = number_format($r->variant_off,0,",",".");
            $item['variant_on'] = number_format($r->variant_on,0,",",".");
            $item['variant_bbn'] = number_format($r->variant_bbn,0,",",".");
            array_push($data, $item);
        }

        return json_encode($data);
    }


        function bbn_update(){
            $this->validate(request(), [
                "variant_bbn"      => "required",
                "variant_on"      => "required",
            ]);

            DB::table("tb_variant")-> where("variant_id",request("variant_id"))->update([
                "variant_bbn"     =>  request("variant_bbn"),
                "variant_on"     =>  request("variant_on"),
            ]);

            
            $r = DB::table("tb_variant")-> where("variant_id",request("variant_id"))->first();
                $item = array();
                $item['variant_id'] = $r->variant_id;
                $item['variant_serial'] = $r->variant_serial;
                $item['variant_nama'] = $r->variant_nama;
                $item['variant_type'] = $r->variant_type;
                $item['variant_off'] = number_format($r->variant_off,0,",",".");
                $item['variant_on'] = number_format($r->variant_on,0,",",".");
                $item['variant_bbn'] = number_format($r->variant_bbn,0,",",".");

            return $item ;
        }

}
