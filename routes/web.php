<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'auth'], function(){
    Route::get('/', function () { 
        $user = \Auth::user(); 
        return view('home'); 
    });
});

//-------------------------------//
// PENJUALAN
//-------------------------------//

Route::group(['middleware' => 'auth'], function(){
    Route::get('/pelanggan', function () { 
        $user = \Auth::user(); 
        return view('modules.pelanggan.data');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/spk/permintaan_spk', function () { 
        $user = \Auth::user(); 
        return view('modules.spk.permintaan_spk');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/spk/permintaan_do', function () { 
        $user = \Auth::user(); 
        return view('modules.spk.permintaan_do');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/spk', function () { 
        $user = \Auth::user(); 
        return view('modules.spk.data');
    });
});


Route::group(['middleware' => 'auth'], function(){
    Route::get('/spk/cancel', function () { 
        $user = \Auth::user(); 
        return view('modules.spk.cancel');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/aksesoris', function () { 
        $user = \Auth::user(); 
        return view('modules.aksesoris.data');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/reqdiskon', function () { 
        $user = \Auth::user(); 
        return view('modules.diskon.req');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/diskon', function () { 
        $user = \Auth::user(); 
        return view('modules.diskon.data');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/samsat', function () { 
        $user = \Auth::user(); 
        return view('modules.samsat.data');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/penjualan', function () { 
        $user = \Auth::user(); 
        return view('modules.penjualan.data');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/match', function () { 
        $user = \Auth::user(); 
        return view('modules.match.data');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('perpanjangan/match', function () { 
        $user = \Auth::user(); 
        return view('modules.match.data_perpanjangan');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('manual/match', function () { 
        $user = \Auth::user(); 
        return view('modules.match.data_manual');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/samsat/riwayat', function () { 
        $user = \Auth::user(); 
        return view('modules.samsat.riwayat');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/kendaraan/warna', function () { 
        $user = \Auth::user(); 
        return view('modules.kendaraan.data_warna');
    });
});

//-------------------------------//
// PEMBELIAN
//-------------------------------//

Route::group(['middleware' => 'auth'], function(){
    Route::get('/pembelian', function () { 
        $user = \Auth::user(); 
        return view('modules.pembelian.data');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/leasing', function () { 
        $user = \Auth::user(); 
        return view('modules.leashing.data');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/vendor', function () { 
        $user = \Auth::user(); 
        return view('modules.vendor.data');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/ekspedisi', function () { 
        $user = \Auth::user(); 
        return view('modules.ekspedisi.data');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/reqkendaraan', function () { 
        $user = \Auth::user(); 
        return view('modules.reqKendaraan.data');
    });
});

//-------------------------------//
// PERSEDIAAN
//-------------------------------//

Route::group(['middleware' => 'auth'], function(){
    Route::get('/kendaraan', function () { 
        $user = \Auth::user(); 
        return view('modules.kendaraan.data');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/bbn', function () { 
        $user = \Auth::user(); 
        return view('modules.bbn.data');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/gudang', function () { 
        $user = \Auth::user(); 
        return view('modules.gudang.data');
    });
});

//-------------------------------//
//pengaturan spk
//-------------------------------//

Route::group(['middleware' => 'auth'], function(){
    Route::get('/spk/no_spk', function () { 
        $user = \Auth::user(); 
        return view('modules.spk.data_nospk');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/spk/no_spk', function () { 
        $user = \Auth::user(); 
        return view('modules.spk.data_nospk');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/data/leasing', function () { 
        $user = \Auth::user(); 
        return view('modules.pengaturan.data_leasing');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/data/cmo', function () { 
        $user = \Auth::user(); 
        return view('modules.pengaturan.data_cmo');
    });
});

Route::group(['middleware' => 'auth'], function(){
    Route::get('/data/asuransi', function () { 
        $user = \Auth::user(); 
        return view('modules.pengaturan.data_asuransi');
    });
});

//-------------------------------//
//KASIR
//-------------------------------//

Route::group(['middleware' => 'auth'], function(){
    Route::get('/kasir', function () { 
        $user = \Auth::user(); 
        return view('modules.kasir.data');
    });
});

//-------------------------------//
//STOCKMOVES
//-------------------------------//

Route::group(['middleware' => 'auth'], function(){
    Route::get('/stockmoves', function () { 
        $user = \Auth::user(); 
        return view('modules.stockmove.data');
    });
});

//-------------------------------//
//STOCK
//-------------------------------//

Route::group(['middleware' => 'auth'], function(){
    Route::get('/stock', function () { 
        $user = \Auth::user(); 
        return view('modules.stock.data');
    });
});

//----------------------------------------------------------//
// DATA API
//----------------------------------------------------------//

//-------------------------------//
//Leasing
//-------------------------------//
Route::get('api/leasing/all', ['middleware' => 'cors','uses'=>'ApiLeasingController@all']);
Route::get('api/leasing', ['middleware' => 'cors','uses'=>'ApiLeasingController@index']);
Route::post('api/leasing', ['middleware' => 'cors','uses'=>'ApiLeasingController@store']);
Route::put('api/leasing', ['middleware' => 'cors','uses'=>'ApiLeasingController@update']);
Route::delete('api/leasing', ['middleware' => 'cors','uses'=>'ApiLeasingController@destroy']);

//-------------------------------//
// Aksesoris
//-------------------------------//
Route::get('api/aksesoris/all', ['middleware' => 'cors','uses'=>'ApiAksesorisController@all']);
Route::get('api/aksesoris', ['middleware' => 'cors','uses'=>'ApiAksesorisController@index']);
Route::post('api/aksesoris', ['middleware' => 'cors','uses'=>'ApiAksesorisController@store']);
Route::put('api/aksesoris', ['middleware' => 'cors','uses'=>'ApiAksesorisController@update']);
Route::delete('api/aksesoris', ['middleware' => 'cors','uses'=>'ApiAksesorisController@destroy']);

//-------------------------------//
// ekspedisi
//-------------------------------//
Route::get('api/ekspedisi', ['middleware' => 'cors','uses'=>'ApiEkspedisiController@index']);
Route::post('api/ekspedisi', ['middleware' => 'cors','uses'=>'ApiEkspedisiController@store']);
Route::put('api/ekspedisi', ['middleware' => 'cors','uses'=>'ApiEkspedisiController@update']);
Route::delete('api/ekspedisi', ['middleware' => 'cors','uses'=>'ApiEkspedisiController@destroy']);

//-------------------------------//
// CMO
//-------------------------------//
Route::get('api/cmo', ['middleware' => 'cors','uses'=>'ApiCMOController@index']);
Route::post('api/cmo', ['middleware' => 'cors','uses'=>'ApiCMOController@store']);
Route::put('api/cmo', ['middleware' => 'cors','uses'=>'ApiCMOController@update']);
Route::delete('api/cmo', ['middleware' => 'cors','uses'=>'ApiCMOController@destroy']);

//-------------------------------//
// Asuransi
//-------------------------------//
Route::get('api/asuransi', ['middleware' => 'cors','uses'=>'ApiAsuransiController@asuransi_read']);
Route::post('api/asuransi', ['middleware' => 'cors','uses'=>'ApiAsuransiController@add_asuransi']);
Route::put('api/asuransi', ['middleware' => 'cors','uses'=>'ApiAsuransiController@update_asuransi']);
Route::delete('api/asuransi', ['middleware' => 'cors','uses'=>'ApiAsuransiController@destroy_asuransi']);

//-------------------------------//
// BIRO
//-------------------------------//
Route::get('api/biro', ['middleware' => 'cors','uses'=>'ApiBiroController@index']);
Route::post('api/biro', ['middleware' => 'cors','uses'=>'ApiBiroController@store']);
Route::put('api/biro', ['middleware' => 'cors','uses'=>'ApiBiroController@update']);
Route::delete('api/biro', ['middleware' => 'cors','uses'=>'ApiBiroController@destroy']);

//-------------------------------//
//Gudang
//-------------------------------//
Route::get('api/gudang', ['middleware' => 'cors','uses'=>'ApiGudangController@index']);
Route::post('api/gudang', ['middleware' => 'cors','uses'=>'ApiGudangController@store']);
Route::put('api/gudang', ['middleware' => 'cors','uses'=>'ApiGudangController@update']);
Route::delete('api/gudang', ['middleware' => 'cors','uses'=>'ApiGudangController@destroy']);

//-------------------------------//
//Master Kendaraan
//-------------------------------//
Route::get('api/kendaraan/type/all', ['middleware' => 'cors','uses'=>'ApiKendaraanController@all_type']);
Route::get('api/kendaraan/variant/all', ['middleware' => 'cors','uses'=>'ApiKendaraanController@all_variant']);

Route::get('api/kendaraan/type', ['middleware' => 'cors','uses'=>'ApiKendaraanController@type_read']);
Route::post('api/kendaraan/type', ['middleware' => 'cors','uses'=>'ApiKendaraanController@type_store']);
Route::put('api/kendaraan/type', ['middleware' => 'cors','uses'=>'ApiKendaraanController@type_update']);
Route::delete('api/kendaraan/type', ['middleware' => 'cors','uses'=>'ApiKendaraanController@type_destroy']);
Route::get('api/kendaraan/variant', ['middleware' => 'cors','uses'=>'ApiKendaraanController@variant_read']);
Route::get('api/kendaraan/variant/{id}', ['middleware' => 'cors','uses'=>'ApiKendaraanController@variant_type']);
Route::post('api/kendaraan/variant', ['middleware' => 'cors','uses'=>'ApiKendaraanController@variant_store']);
Route::put('api/kendaraan/variant', ['middleware' => 'cors','uses'=>'ApiKendaraanController@variant_update']);
Route::delete('api/kendaraan/variant', ['middleware' => 'cors','uses'=>'ApiKendaraanController@variant_destroy']);
Route::get('api/kendaraan/type/{id}', ['middleware' => 'cors','uses'=>'ApiKendaraanController@type_read']);

//-------------------------------//
//Master BBN
//-------------------------------//
Route::get('api/kendaraan/bbn', ['middleware' => 'cors','uses'=>'ApiKendaraanController@bbn_master']);
Route::put('api/kendaraan/bbn', ['middleware' => 'cors','uses'=>'ApiKendaraanController@bbn_update']);

//-------------------------------//
//Warna
//-------------------------------//
Route::get('api/warna/all', ['middleware' => 'cors','uses'=>'ApiWarnaController@all']);
Route::get('api/warna', ['middleware' => 'cors','uses'=>'ApiWarnaController@index']);
Route::get('api/warna/variant', ['middleware' => 'cors','uses'=>'ApiWarnaController@warna_variant']);
Route::post('api/warna', ['middleware' => 'cors','uses'=>'ApiWarnaController@store']);
Route::put('api/warna', ['middleware' => 'cors','uses'=>'ApiWarnaController@update']);
Route::delete('api/warna', ['middleware' => 'cors','uses'=>'ApiWarnaController@destroy']);

//-------------------------------//
//VendorAks
//-------------------------------//
Route::get('api/vendorAks', ['middleware' => 'cors','uses'=>'ApiVendorAksController@index']);
Route::post('api/vendorAks', ['middleware' => 'cors','uses'=>'ApiVendorAksController@store']);
Route::put('api/vendorAks', ['middleware' => 'cors','uses'=>'ApiVendorAksController@update']);
Route::delete('api/vendorAks', ['middleware' => 'cors','uses'=>'ApiVendorAksController@destroy']);

//-------------------------------//
//Pelanggan
//-------------------------------//
Route::get('api/pelanggan', ['middleware' => 'cors','uses'=>'ApiPelangganController@index']);
Route::delete('api/pelanggan', ['middleware' => 'cors','uses'=>'ApiPelangganController@destroy']);
Route::get('api/pelanggan/{id}', ['middleware' => 'cors','uses'=>'ApiPelangganController@detail']);

//-------------------------------//
//SPK
//-------------------------------//
Route::post('api/spk/cetak_faktur', ['middleware' => 'cors','uses'=>'ApiSPKController@cetak_faktur']);
Route::post('api/spk/variant_update', ['middleware' => 'cors','uses'=>'ApiSPKController@variant_update']);
Route::post('api/spk/leasing_save', ['middleware' => 'cors','uses'=>'ApiSPKController@leasing_save']);
Route::post('api/spk/acc_pengajuan_do', ['middleware' => 'cors','uses'=>'ApiSPKController@acc_pengajuan_do']);
Route::post('api/spk/tolak_pengajuan_do', ['middleware' => 'cors','uses'=>'ApiSPKController@tolak_pengajuan_do']);
Route::post('api/spk/pengajuan_do', ['middleware' => 'cors','uses'=>'ApiSPKController@pengajuan_do']);
Route::post('api/spk/terbitkan', ['middleware' => 'cors','uses'=>'ApiSPKController@terbitkan']);
Route::post('api/spk/acc_permintaan_spk', ['middleware' => 'cors','uses'=>'ApiSPKController@acc_permintaan_spk']);
Route::post('api/spk/tanggapi_permintaan_spk', ['middleware' => 'cors','uses'=>'ApiSPKController@tanggapi_permintaan_spk']);
Route::post('api/spk/cancel/acc', ['middleware' => 'cors','uses'=>'ApiSPKController@cancel_save']);
Route::post('api/spk/cancel/tanggapi', ['middleware' => 'cors','uses'=>'ApiSPKController@tanggapi_save']);
Route::post('api/spk/cancel', ['middleware' => 'cors','uses'=>'ApiSPKController@cancel_save']);
Route::get('api/spk', ['middleware' => 'cors','uses'=>'ApiSPKController@index']);
Route::get('api/spk/permintaan_spk', ['middleware' => 'cors','uses'=>'ApiSPKController@permintaan_spk']);
Route::get('api/spk/permintaan_do', ['middleware' => 'cors','uses'=>'ApiSPKController@permintaan_do']);
Route::get('api/spk/cancel', ['middleware' => 'cors','uses'=>'ApiSPKController@cancel']);
Route::get('api/spk/get/{id}', ['middleware' => 'cors','uses'=>'ApiSPKController@getSPK']);
Route::get('api/spk/get', ['middleware' => 'cors','uses'=>'ApiSPKController@getSPK']);
Route::get('api/spk/{id}', ['middleware' => 'cors','uses'=>'ApiSPKController@detail']);


//-------------------------------//
//Matching
//-------------------------------//
Route::post('api/matching/update', ['middleware' => 'cors','uses'=>'ApiMatchController@updateManualMatch']);
Route::get('api/matching', ['middleware' => 'cors','uses'=>'ApiMatchController@index']);
Route::get('api/manual_spk/matching', ['middleware' => 'cors','uses'=>'ApiMatchController@spk_manual']);
Route::get('api/manual_kendaraan/matching', ['middleware' => 'cors','uses'=>'ApiMatchController@kendaraan_manual']);
Route::get('api/matching/addtempo', ['middleware' => 'cors','uses'=>'ApiMatchController@add_tempo']);
Route::PUT('api/matching', ['middleware' => 'cors','uses'=>'ApiMatchController@update']);

//-------------------------------//
//Tagihan LEasing
//-------------------------------//
Route::get('api/leasing/tagihan', ['middleware' => 'cors','uses'=>'ApiTagihanLeasingController@index']);
Route::get('api/tagihan/leasing/asuransi', ['middleware' => 'cors','uses'=>'ApiTagihanLeasingController@get_asuransi']);
Route::post('api/tagihan/leasing/cetak_save', ['middleware' => 'cors','uses'=>'ApiTagihanLeasingController@cetak_save']);
Route::PUT('api/tagihan/leasing/tagihan_save', ['middleware' => 'cors','uses'=>'ApiTagihanLeasingController@tagihan_save']);
Route::PUT('api/tagihan/leasing/lunas_save', ['middleware' => 'cors','uses'=>'ApiTagihanLeasingController@lunas_save']);
Route::PUT('api/tagihan/leasing/refund_save', ['middleware' => 'cors','uses'=>'ApiTagihanLeasingController@refund_save']);
Route::PUT('api/tagihan/leasing/asuransi_save', ['middleware' => 'cors','uses'=>'ApiTagihanLeasingController@asuransi_save']);



//-------------------------------//
//PENJUALAN
//-------------------------------//
Route::get('api/penjualan', ['middleware' => 'cors','uses'=>'ApiPenjualanController@penjualan']);
Route::get('api/spk/penjualan/{id}', ['middleware' => 'cors','uses'=>'ApiPenjualanController@detail_penjualan']);

//-------------------------------//
//KARYAWAN
//-------------------------------//
Route::get('api/karyawan/sales', ['middleware' => 'cors','uses'=>'ApiKaryawanController@sales_view']);

//-------------------------------//
//NO_SPK
//-------------------------------//
Route::get('api/pengaturan/nospk', ['middleware' => 'cors','uses'=>'ApiNospkController@index']);
Route::get('api/pengaturan/lastno', ['middleware' => 'cors','uses'=>'ApiNospkController@lastNo']);
Route::post('api/pengaturan/nospk', ['middleware' => 'cors','uses'=>'ApiNospkController@store']);
Route::put('api/pengaturan/nospk', ['middleware' => 'cors','uses'=>'ApiNospkController@update']);
Route::delete('api/pengaturan/nospk', ['middleware' => 'cors','uses'=>'ApiNospkController@destroy']);

//-------------------------------//
//Referral
//-------------------------------//
Route::get('api/referral', ['middleware' => 'cors','uses'=>'ApiReferralController@index']);
Route::post('api/referral', ['middleware' => 'cors','uses'=>'ApiReferralController@store']);
Route::put('api/referral', ['middleware' => 'cors','uses'=>'ApiReferralController@update']);
Route::delete('api/referral', ['middleware' => 'cors','uses'=>'ApiReferralController@destroy']);

//-------------------------------//
//Diskon
//-------------------------------//
Route::get('api/diskon', ['middleware' => 'cors','uses'=>'ApiDiskonController@index']);
Route::get('api/diskon/aksesoris/{type}', ['middleware' => 'cors','uses'=>'ApiDiskonController@aksesoris']);
Route::get('api/diskon/request', ['middleware' => 'cors','uses'=>'ApiDiskonController@req']);
Route::get('api/diskon/request/{id}', ['middleware' => 'cors','uses'=>'ApiDiskonController@req']);
Route::get('api/diskon/referral', ['middleware' => 'cors','uses'=>'ApiDiskonController@referral']);
Route::get('api/diskon/riwayat', ['middleware' => 'cors','uses'=>'ApiDiskonController@riwayat_referral']);
Route::get('api/diskon/status', ['middleware' => 'cors','uses'=>'ApiDiskonController@status']);
Route::post('api/diskon/app', ['middleware' => 'cors','uses'=>'ApiDiskonController@app']);
Route::post('api/diskon/konfirmasi', ['middleware' => 'cors','uses'=>'ApiDiskonController@konfirmasi']);

//-------------------------------//
//Pembelian
//-------------------------------//
Route::get('api/pembelian/all', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@all']);
Route::get('api/pembelian/new', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@baru']);
Route::get('api/pembelian', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@index']);
Route::post('api/pembelian/audit', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@audit']);
Route::post('api/pembelian/import', 'ApiTrKendaraanController@import');
Route::post('api/pembelian', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@store']);
Route::put('api/pembelian', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@update']);
Route::delete('api/pembelian', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@destroy']);


//-------------------------------//
//Req Kendaraaan
//-------------------------------//
Route::get('api/po/request_kendaraan', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@request_kendaraan']);

//-------------------------------//
//Vendor
//-------------------------------//
Route::get('api/vendor', ['middleware' => 'cors','uses'=>'ApiVendorController@index']);
Route::post('api/vendor', ['middleware' => 'cors','uses'=>'ApiVendorController@store']);
Route::put('api/vendor', ['middleware' => 'cors','uses'=>'ApiVendorController@update']);
Route::delete('api/vendor', ['middleware' => 'cors','uses'=>'ApiVendorController@destroy']);

//-------------------------------//
//Cetak Laporan
//-------------------------------//
Route::get('api/laporan/kendaraan/{id}', ['middleware' => 'cors','uses'=>'ApiLaporanController@f_kendaraan']);
Route::get('api/laporan/bast/{id}', ['middleware' => 'cors','uses'=>'ApiLaporanController@f_BAST']);
Route::get('api/laporan/tagihan/{id}', ['middleware' => 'cors','uses'=>'ApiLaporanController@t_tagihan_leasing']);
Route::get('api/laporan/pembayaran/{id}', ['middleware' => 'cors','uses'=>'ApiLaporanController@t_pembayaran_leasing']);
Route::get('api/laporan/bpkb/{id}', ['middleware' => 'cors','uses'=>'ApiLaporanController@t_terima_bpkb']);

Route::get('cetak/f_kendaraan/{id}', 'CetakController@f_kendaraan');
Route::get('cetak/leasing/tagihan/{id}', 'CetakController@t_tagihan_leasing');
Route::get('cetak/leasing/dp/{id}', 'CetakController@t_kwitansi_dp');
Route::get('cetak/leasing/lunas/{id}', 'CetakController@t_kwitansi_lunas');
Route::get('cetak/samsat/t_terima/{id}', 'CetakController@t_terima_bpkb');
Route::get('cetak/kwitansi/pembayaran/{id}', 'CetakController@t_pembayaran');
Route::get('cetak/kwitansi/{id}', 'CetakController@t_kwitansi_pembayaran');
Route::get('cetak/f_BAST/{id}', 'CetakController@f_BAST');

//-------------------------------//
//TTBJ
//-------------------------------//
Route::get('api/ttbj', ['middleware' => 'cors','uses'=>'ApiTTBJController@index']);
Route::get('api/ttbj/riwayat', ['middleware' => 'cors','uses'=>'ApiTTBJController@riwayat']);
Route::put('api/ttbj', ['middleware' => 'cors','uses'=>'ApiTTBJController@store']);
Route::put('api/ttbj/terima', ['middleware' => 'cors','uses'=>'ApiTTBJController@terima_save']);

//-------------------------------//
//Persediaan Stok
//-------------------------------//
Route::post('api/persediaan/stockmove', ['middleware' => 'cors','uses'=>'ApiStockMoveController@store']);
Route::get('api/persediaan/stock', ['middleware' => 'cors','uses'=>'ApiTrKendaraanController@stock']);
Route::get('api/persediaan/stockmove', ['middleware' => 'cors','uses'=>'ApiStockMoveController@index']);
Route::get('api/persediaan/getstock', ['middleware' => 'cors','uses'=>'ApiStockMoveController@getstock']);
Route::get('api/persediaan/getgudang', ['middleware' => 'cors','uses'=>'ApiStockMoveController@getgudang']);

//-------------------------------//
//MATCHING
//-------------------------------//
Route::get('api/matching', ['middleware' => 'cors','uses'=>'ApiMatchController@index']);
Route::get('api/matching/addtempo', ['middleware' => 'cors','uses'=>'ApiMatchController@add_tempo']);
Route::PUT('api/matching', ['middleware' => 'cors','uses'=>'ApiMatchController@update']);

//-------------------------------//
//KASIR
//-------------------------------//
Route::post('api/kasir', ['middleware' => 'cors','uses'=>'ApiKasirController@store']);
Route::get('api/kasir/piutang', ['middleware' => 'cors','uses'=>'ApiKasirController@getPiutang']);
Route::get('api/kasir/daftarkonfirmasi', ['middleware' => 'cors','uses'=>'ApiKasirController@daftarkonfirmasi']);
Route::get('api/kasir', ['middleware' => 'cors','uses'=>'ApiKasirController@index']);
Route::get('api/kasir/pembayaran/{id}', ['middleware' => 'cors','uses'=>'ApiKasirController@get_pembayaran']);
Route::get('api/kasir/kode', ['middleware' => 'cors','uses'=>'ApiKasirController@generate_kode']);
Route::get('api/kasir/spk/{id}', ['middleware' => 'cors','uses'=>'ApiKasirController@getSPK']);
Route::get('api/kasir/spk', ['middleware' => 'cors','uses'=>'ApiKasirController@getSPK']);

//-------------------------------//
//COA
//-------------------------------//
Route::get('api/coa/kasbank', ['middleware' => 'cors','uses'=>'ApiCOAController@akun_kasbank']);
Route::get('api/coa/bank', ['middleware' => 'cors','uses'=>'ApiCOAController@akun_bank']);

//-------------------------------//
//PENJUALAN
//-------------------------------//
Route::get('api/penjualan', ['middleware' => 'cors','uses'=>'ApiPenjualanController@index']);\
Route::get('api/penjualan/{id}', ['middleware' => 'cors','uses'=>'ApiPenjualanController@detail']);

//-------------------------------//
//FIREBASE
//-------------------------------//
Route::post('api/firebase/spk', ['middleware' => 'cors','uses'=>'ApiFirebaseController@storeSPK']);
Route::post('api/firebase/diskon', ['middleware' => 'cors','uses'=>'ApiFirebaseController@storeDiskon']);
Route::get('api/firebase/sales/uid/{spk}', ['middleware' => 'cors','uses'=>'ApiFirebaseController@getSalesUid']);
Route::get('api/firebase/sales/uid', ['middleware' => 'cors','uses'=>'ApiFirebaseController@getSalesUid']);
Route::get('api/firebase/sales/{uid}', ['middleware' => 'cors','uses'=>'ApiFirebaseController@getSales']);
Route::get('api/firebase/sales/', ['middleware' => 'cors','uses'=>'ApiFirebaseController@getSales']);

//-------------------------------//
//NOTIF & ACTIVITY
//-------------------------------//
Route::get('api/na', ['middleware' => 'cors','uses'=>'ApiNotifActivity@index']);
Route::get('api/na/{kategori}/{status}/{spk}', ['middleware' => 'cors','uses'=>'ApiNotifActivity@get']);

Route::get('storage/{filename}', function ($filename)
{
    $path = storage_path('public/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('api/{table}/', ['middleware' => 'cors','uses'=>'ApiFirebaseController@getTableData']);