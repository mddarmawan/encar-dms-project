<div id="dataSpk">

</div>

<script>
$(function() {
	var db_spk = {
        loadData: function(filter) {
			return $.ajax({
                type: "GET",
                url: "{{url('api/spk')}}",
                data: filter
            });
        }
    };

    db_spk.status = [
        {
            "status_id": "",
            "status_nama": "",           
        },
        {
            "status_id": "9",
            "status_nama": "INVALID",           
        },
        {
            "status_id": "1",
            "status_nama": "VALID",           
        },
        {
            "status_id": "2",
            "status_nama": "MATCHED",           
        },
        {
            "status_id": "3",
            "status_nama": "DO",           
        },
    ];

    function getPembayaran(id) {
        $.ajax({
            type: "GET",
            url: "{{url('api/penjualan')}}/"+id
        }).done(function(json) {
            var bayar = json.bayar;
            $("#total_bayar").val("");
            $("#keterangan").val("");
            $("#tabel_pembayaran").html('');
            if (bayar != null) {
                var total_bayar = 0;
                for(var i in bayar){
                    var item = bayar[i];
                    $("#tabel_pembayaran").append('<tr class="jsgrid-row"><td class="jsgrid-cell">'+date_format(item.spkp_tgl)+'</td><td class="jsgrid-cell text-right">'+number_format(item.spkp_jumlah)+'</td></tr>');
                    total_bayar += item.spkp_jumlah;
                    $("#Total").val(total_bayar);
                }
            }
        });
    }

    function set_data(item){
        //$("#id").val(item.spkt_id);
        $('#spk_id').html(item.spk_id);
        $('#spk_tgl').val(item.spk_tgl);
        $('#spk_faktur').val(item.spk_faktur);
        $('#spk_pel_nama').html(item.spk_pel_nama);
        $('#spk_pel_alamat').html(item.spk_pel_alamat);
        $('#spk_variant').html(item.spk_variant);
        $('#spk_warna').html(item.spk_warna);
        $('#spk_sales').html(item.spk_sales);
        $('#spk_team').html(item.spk_team);

            if (item.spk_pembayaran==0){
                $('#spk_pembayaran').html("CASH");
                
            }else{
                $('#spk_pembayaran').html("CREDIT");
                            
            }
        $('#msg').html("");
        $("#keterangan").val("");
        getPembayaran(item.spk_id);
    }

    $("#dataSpk").jsGrid({
        height: "380px",
        width: "100%",
        filtering: true,
        sorting: true,
        autoload: true,
        deleteConfirm: "Anda yakin akan menghapus data ini?",
        controller: db_spk,
        rowDoubleClick:function(data){
            var item = data.item;
            set_data(item);
        },
        fields: [
            { name: "spk_tgl", title:"Tanggal", type: "text", width: 80, align:"center" },
            { name: "spk_id", title:"No SPK", type: "text", width: 80, align:"center" },
            { name: "spk_pel_nama", title:"Nama Pelanggan", type: "text", width: 170},
           // { name: "spk_type", title:"Type", type: "text", width: 70, align:"center" },
            { name: "spk_variant", title:"Varian", type: "text", width: 150 },
            { name: "spk_warna", title:"Warna", type: "text", width: 70, align:"center"},
            { name: "spk_sales", title:"Sales", type: "text", width: 100},
            { name: "spk_team", title:"Team", type: "text", width: 100, align:"center" },
            { name: "spk_pembayaran", title:"Pembayaran", type: "text", width: 100, align:"right" },
            { name: "spk_via", title:"Via", type: "text", width: 70, align:"center" },
            { name: "spk_kota", title:"Kota", type: "text", width: 70, align:"center" },
            { name: "spk_status", title:"Status", type: "select", width: 70, items: db_spk.status, valueField: "status_id", textField: "status_nama", align:"center" },
            { name: "spk_ket", title:"Keterangan", type: "text", width: 180 }
        ]
    });
 
});
</script>