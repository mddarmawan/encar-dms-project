@extends('layout')

@section('content')
<div class="content-header">
	<h6 class="left">
	  <small>Kasir</small>
		   Kasir Showroom
	 </h6>
	 <ul class="header-tools right">
			<li><a href="#tambah" class="chip tambah"><i class="fa fa-plus"></i>Buat Kwitansi Baru</a></li>
	</ul>
</div>


<div class="wrapper">
		<div class="nav-wrapper">
			<div class="nav-left">
				<ul class="tab">
					<li class="active"><a data-id="riwayat" >Riwayat Pembayaran</a></li>
					<li><a data-id="spk">SPK Belum Lunas</a></li>
					<li><a data-id="daftarkonfirmasi">Daftar Konfirmasi Pembayaran</a></li>
				</ul>
			</div>
			<div class="nav-right">
				<i style="margin-right: 5px" class="fa fa-filter" aria-hidden="true"></i>
				<span class="bold" style="font-size: 13px"> Periode :</span>
				<div class="input-group">
					<input type="text" class="input-filter datepicker" id="filter_awal" placeholder="Tanggal Awal" required="" />
					<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
				</div>
				<span class="bold" style="font-size: 13px">s.d</span>
				<div class="input-group">
					<input type="text" class="input-filter datepicker"  id="filter_akhir" placeholder="Tanggal Akhir"  required="" />
					<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
				</div>
				<a class="waves-effect waves-light btn-flat btn-filter" id="filter">Cari</a>
				<a class="waves-effect waves-light btn-flat btn-filter" id="reset">Reset</a>
				<a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
			</div>
		</div>
	<div id="data">

	</div>  
</div>

<div id="tambah" class="modal modal-fixed-footer" style="width:1300px; height: auto;">
		<h6 class="modal-title blue-grey darken-1">
			<span id="form_title">Pembayaran</span>
			<span class="modal-close right material-icons" style="margin-top:-3px">close</span>
		</h6>

		<div class="modal-content" style="padding:10px 0 0;position: relative;">
			@include("modules.kasir.tambah")
		</div>
</div>
<div id="bukti" class="modal" style="height: auto;">
	<h6 class="modal-title blue-grey darken-1">
		<span id="form_title">Bukti Transfer: <b id="bukti_spk"></b></span>
		<span class="modal-close right material-icons" style="margin-top:-3px">close</span>
	</h6>
	<div class="modal-content" style="padding:0;position: relative;">
		<img class="img-responsive img-block" src="" id="bukti_foto"/>
	</div>
</div>
<script>

	function bukti(sender, e){
		e.preventDefault();
		$("#bukti_spk").html($(sender).data("id"));
		$("#bukti_foto").attr("src",$(sender).attr("href"));
		$("#bukti").modal("open");
	}



	$(function() {


	function setSPK(){
		$.ajax({
			type:'GET',
			dataType: 'json',
			url: '{{url("/api/kasir/spk")}}'
		}).done(function(data){
			$('#spk_spk').autocomplete({
				source:data,
				select: function( event, ui ) {
					_model = ui.item.id;

					setForm(ui.item);
					
					return false;
				}
			}).bind('focus', function() {
				$(this).keydown();
			}).autocomplete('instance' )._renderItem = function( ul, item ) {
				return $('<li>').append("<div class='stock-item'><h5>" + item.value + "<span class='pull-right'>"+ item.pemesan.spk_tgl +"</span></h5><h5><b>"+item.pemesan.type_nama+" "+item.pemesan.variant_nama+"</b> / "+item.pemesan.warna_nama+"</h5><h5><i>"+item.pemesan.spk_pel_nama+" <span class='pull-right'>"+item.pemesan.karyawan_nama+"</span></i></h5></div>").appendTo(ul);
			};	
		});
	}

	function setForm(data){
		var total_diskon=0;
					var subtotal = data.pemesan.spk_harga - data.pemesan.diskon;
					$('#total_invoice').html("RP " + number_format(subtotal)+",-");					
					getPembayaran(data.pemesan.spk_id, subtotal);						
					$('#spk_spk').val(data.value);
					$("#uid").html(data.pemesan.sales_salesUid);						
					$('#spk_id').val(data.pemesan.spk_id);
					$('#pel_nama').html(data.pemesan.spk_pel_nama);
					$('#pel_alamat').html(data.pemesan.spk_pel_alamat);
					$('#pel_pos').html(data.pemesan.spk_pel_pos);
					$('#pel_telp').html(data.pemesan.spk_pel_telp);
					$('#pel_ponsel').html(data.pemesan.spk_pel_ponsel);
					$('#pel_email').html(data.pemesan.spk_pel_email);
					// $('#spk_ppn').html(data.pemesan.);
					$('#spk_npwp').html(data.pemesan.spk_npwp);
					$('#spk_kendaraan').html(data.pemesan.type_nama + data.pemesan.variant_nama);
					$('#spk_kendaraan_serial').html(data.pemesan.variant_serial);
					$('#spk_warna').html(data.pemesan.warna_nama);
					$('#spk_ket_harga').html(data.pemesan.spk_ket_harga);
					$('#spk_pembayaran').html(data.pemesan.spk_pembayaran);
					$('#spk_dp').html("Rp. " + number_format (data.pemesan.spk_dp));
					$('#spk_sales').html(data.pemesan.karyawan_nama);
					$('#spk_leasing').html(data.pemesan.leasing_nama);
					$('#spk_pajak').html(data.pemesan.spk_pajak);
					$('#spk_fleet').html(data.pemesan.spk_fleet);
					$('#spk_stnk_nama').html(data.pemesan.spk_stnk_nama);
					$('#spk_stnk_identitas').html(data.pemesan.spk_stnk_identitas);
					$('#spk_stnk_alamat').html(data.pemesan.spk_stnk_alamat);
					$('#spk_stnk_pos').html(data.pemesan.spk_stnk_pos);
					$('#spk_stnk_alamatd').html(data.pemesan.spk_stnk_alamatd);
					$('#spk_stnk_posd').html(data.pemesan.spk_stnk_posd);
					$('#spk_stnk_telp').html(data.pemesan.spk_stnk_telp);
					$('#spk_stnk_ponsel').html(data.pemesan.spk_stnk_ponsel);
					$('#spk_stnk_email').html(data.pemesan.spk_stnk_email);
					$('#tanggal_bayar').html('{{ date("Y-m-d") }}');

					if($('#spk_pembayaran').html() == "CASH"){
						$(".pembayaran").hide();
					}else{
						$(".pembayaran").show();
					}
	}


	$(window).keydown(function(event) {
		if(event.altKey && event.keyCode == 78) {
		    event.preventDefault(); 
			clearForm();
			$("#tambah").modal("open");
		}
	});

	$("#ket").keypress(function(e) {
		if(e.which == 13) {
			$("#save").trigger("click");
			return false;
		}
	});


	var currentData = "riwayat";
	$(".tambah").click(function(e){
		e.preventDefault();
		clearForm();
		$("#tambah").modal("open");
	});


	$("#filter").click(function(e){
		e.preventDefault();
		if (currentData=="spk"){
			loadSPK();
		}else if (currentData=="daftarkonfirmasi"){
			loadKonfirmasi();
		}else{
			loadData();
		}
	});

	$("#reset").click(function(e){
		e.preventDefault();
		$("#filter_awal").val("");
		$("#filter_akhir").val("");
		if (currentData=="spk"){
			loadSPK();
		}else if (currentData=="daftarkonfirmasi"){
			loadKonfirmasi();
		}else{
			loadData();
		}
	});


	$("#export").click('click', function (event) {
	    var args = [$('#data'), 'Pembayaran_Showroom_<?php echo date('dmY') ?>.xls'];   
	    exportTableToExcel.apply(this, args);
	});


	$(".tab li a").click(function(e){
		e.preventDefault();

		$("#filter_awal").val("");
		$("#filter_akhir").val("");

		currentData = $(this).data("id");
		
		if (currentData=="spk"){
			loadSPK();
		}else if (currentData=="daftarkonfirmasi"){
			loadKonfirmasi();
		}else{
			loadData();
		}

		$(".tab li").removeClass("active");
		$(this).parent().addClass("active");
	});

	function loadSPK() {
		$(".loading").addClass("show");
		$("#data").html("");
		var db = {
					loadData: function(filter) {
						var filter_awal = $("#filter_awal").val().trim();
						var filter_akhir = $("#filter_akhir").val().trim();
						if (filter_awal != ""){
							filter['filter_awal'] = filter_awal;
						}
						if (filter_akhir != ""){
							filter['filter_akhir'] = filter_akhir;
						}
						return $.ajax({
							type: "GET",
							url: "{{url('api/kasir/piutang')}}",
							data: filter
						}).fail(function(){
							$(".loading").removeClass("show");
						}).done(function(){
							$(".loading").removeClass("show");
						});;
					}
				};

				$("#data").jsGrid({
					height: "calc(100% - 40px)",
					width: "100%",
			 
					filtering: true,
					sorting: true,
					autoload: true,
					noDataContent: "Data tidak ditemukan",			 
					controller: db,
			 
					fields: [
						{ name: "spk_tgl", title:"Tgl. SPK", type: "text", width: 100, align:"center" },
						{ name: "spk_id", title:"No. spk", type: "text", width: 100, align: "center" },
						{ name: "spk_pel_nama", title:"Nama Pemesan", type: "text", width: 150, align: "left" },
						{ name: "variant_nama", title:"Variant", type: "number", width: 200, align:"left"},
						{ name: "warna_nama", title:"Warna", type: "number", width: 100, align: "center" },
						{ name: "karyawan_nama", title:"Sales", type: "text", width: 120, align:"left" },
						{ name: "team_nama", title:"Team", type: "text", width: 100, align:"center" },
						{ name: "spk_harga", title:"Total Invoice", type: "number", width: 120, align:"right", css :"bold f13"},
						{ name: "spk_bayar", title:"Total Bayar", type: "number", width: 120, align:"right", css :"bold f13"},
						{ name: "spk_kurang", title:"Kurang Bayar", type: "number", width: 120, align:"right", css :"bold f13"}
					  
					]
		});
	}



	function loadKonfirmasi() {
			$(".loading").addClass("show");
			$("#data").html("");
			$.ajax({
		        type: "GET",
		        url: "{{url('api/coa/bank')}}"
		    }).fail(function(){
				$(".loading").removeClass("show");
			}).done(function(akun) {
		        akun.unshift({ bank_rek: "0", akun_nama: "" });

				var db = {
					loadData: function(filter) {
						var filter_awal = $("#filter_awal").val().trim();
						var filter_akhir = $("#filter_akhir").val().trim();
						if (filter_awal != ""){
							filter['filter_awal'] = filter_awal;
						}
						if (filter_akhir != ""){
							filter['filter_akhir'] = filter_akhir;
						}
						return $.ajax({
							type: "GET",
							url: "{{url('api/kasir/daftarkonfirmasi')}}",
							data: filter
						}).fail(function(){
							$(".loading").removeClass("show");
						}).done(function(){
							$(".loading").removeClass("show");
						});
					}
				};

				$("#data").jsGrid({
					height: "calc(100% - 40px)",
					width: "100%",
			 
					filtering: true,
					sorting: true,
					autoload: true,
					noDataContent: "Data tidak ditemukan",			 
					controller: db,
			 
					fields: [
						{ name: "konfirmasi_tgl", title:"Tgl. Konfirmasi", type: "text", width: 100, align:"center" },
						{ name: "konfirmasi_spk", title:"No. SPK", type: "text", width: 80, align: "center" },
						{ name: "konfirmasi_tujuan", title:"Tujuan", type: "select", width: 100, align:"left",items: akun, valueField: "bank_rek", textField: "akun_nama" },
						{ name: "konfirmasi_bank", title:"Bank", type: "number", width: 100, align:"left"},
						{ name: "konfirmasi_rek", title:"Rekening", type: "number", width: 100, align: "center" },
						{ name: "konfirmasi_an", title:"Atas Nama", type: "text", width: 120, align:"left" },
						{ name: "konfirmasi_jumlah", title:"Jumlah", type: "number", width: 100, align:"right" },
						{ name: "konfirmasi_ket", title:"Keterangan", type: "text", width: 100, align:"left" },
						{ name: "konfirmasi_bukti", title:"Bukti Transfer", width: 50, align:"center", filtering:false},
					  	{ type: "control",  width:50, align:"center", deleteButton:false, editButton:false, itemTemplate: function(value, item) {
			                    var $result = jsGrid.fields.control.prototype.itemTemplate.apply(this, arguments);
			                    
			                    var $customButton = $("<a href='javascript:;' class='green-text' title='Konfirmasi'><span class='material-icons'  style='font-size:20px'>done</span></a>")
			                        .click(function(e) {
										$(".loading").addClass("show");
			                        	clearForm();
										$("#akun").val(item.bank_akun);
										$("#akun").attr("disabled","true");	
			                        	$("#spk_spk").attr("disabled","true");
			                        	$("#jumlah").attr("disabled","true");
			                        	$("#tanggal_bayar").attr("disabled","true");
			                        	$("#konfirmasi_id").val(item.konfirmasi_id);
			                        	$.ajax({
											type:'GET',
											dataType: 'json',
											url: '{{url("/api/kasir/spk")}}/'+item.konfirmasi_spk
										}).fail(function(data){
											$(".loading").removeClass("show");
										}).done(function(data){
											$(".loading").removeClass("show");
											if(data){
												setForm(data);

			                        			$("#jumlah").val(item.konfirmasi_jumlah);
			                        			$("#tanggal_bayar").val(date_only(item.konfirmasi_tgl));
			                        			$("#ket").val(item.konfirmasi_ket);
												$("#tambah").modal("open");
											}
										});
			                            e.stopPropagation();
			                        });
			                    
			                    return $result.add($customButton);
			                } 
			            }
					]
				});


			});
		}


		function loadData() {
			$(".loading").addClass("show");
			$("#data").html("");
			 $.ajax({
		        type: "GET",
		        url: "{{url('api/coa/kasbank')}}"
		    }).fail(function(){
				$(".loading").removeClass("show");
			}).done(function(akun) {
		    	$("#akun").html("");
		    	for(i in akun){
		    		var item = akun[i];
		    		$("#akun").append("<option value='"+item.akun_id+"'>"+item.akun_nama+"</option>");		    		
		    	}
		        akun.unshift({ akun_id: "0", akun_nama: "" });

				var db_kasir = {
					loadData: function(filter) {
						var filter_awal = $("#filter_awal").val().trim();
						var filter_akhir = $("#filter_akhir").val().trim();
						if (filter_awal != ""){
							filter['filter_awal'] = filter_awal;
						}
						if (filter_akhir != ""){
							filter['filter_akhir'] = filter_akhir;
						}
						return $.ajax({
							type: "GET",
							url: "{{url('api/kasir')}}",
							data: filter
						}).fail(function(){
							$(".loading").removeClass("show");
						}).done(function(){
							$(".loading").removeClass("show");
						});;
					}
				};

				db_kasir.via = [
					{"via_id":"","via_nama":""},
					{"via_id":1,"via_nama":"TUNAI"},
					{"via_id":2,"via_nama":"TRANSFER"}
				];

				$("#data").jsGrid({
					height: "calc(100% - 40px)",
					width: "100%",
			 
					filtering: true,
					sorting: true,
					autoload: true,
					noDataContent: "Data tidak ditemukan",			 
					controller: db_kasir,

			 
					fields: [
						{ name: "spkp_tgl", title:"Tgl. Bayar", type: "text", width: 100, align:"center" },
						{ name: "spkp_id", title:"No. Kwitansi", type: "text", width: 100, align: "center" },
						{ name: "spkp_spk", title:"No. spk", type: "text", width: 100, align: "center" },
						{ name: "spk_pel_nama", title:"Nama Pemesan", type: "text", width: 150, align: "left" },
						{ name: "variant_nama", title:"Variant", type: "number", width: 200, align:"left"},
						{ name: "warna_nama", title:"Warna", type: "number", width: 100, align: "center" },
						{ name: "karyawan_nama", title:"Sales", type: "text", width: 120, align:"left" },
						{ name: "team_nama", title:"Team", type: "text", width: 100, align:"center" },
						{ name: "spkp_via", title:"Via", type: "select", width: 100, align:"center",items: db_kasir.via, valueField: "via_id", textField: "via_nama" },
						{ name: "spkp_akun", title:"Akun", type: "select", width: 130, align:"left",items: akun, valueField: "akun_id", textField: "akun_nama" },
						{ name: "spkp_jumlah", title:"Jumlah", type: "number", width: 100, align:"right", css :"bold f13"},
						{ name: "spkp_ket", title:"Keterangan", type: "text", width: 100 },
					  
					]
				});
			});
		}

		function clearForm(){
			setSPK();
			$("#tabel_pembayaran").html("");
			$("#akun").removeAttr("disabled");
			$("#jumlah").removeAttr("disabled");
			$("#tanggal_bayar").removeAttr("disabled");
			$("#spk_spk").removeAttr("disabled");	

			$("#konfirmasi_id").val("");
			$("#spk_spk").val("");
			$("#total_invoice").html("RP 0,-");
			$("#total_bayar").html("RP 0,-");
			$("#total_kurang").html("RP 0,-");
			$("#no_faktur").val("");
			$("#tanggal_bayar").val("{{date('d/m/Y')}}");
			$("#jumlah").val("");
			$("#akun")[0].selectedIndex = 0;
			$("#ket").val("");

			$("#pel_nama").html("");	
			$("#pel_alamat").html("");	
			$("#pel_pos").html("");	
			$("#pel_telp").html("");	
			$("#pel_ponsel").html("");	
			$("#pel_email").html("");	
			$("#spk_ppn").html("");	
			$("#spk_npwp").html("");	
			$("#spk_pajak").html("");	
			$("#spk_fleet").html("");	
			$("#spk_kendaraan").html("");	
			$("#spk_kendaraan_serial").html("");
			$("#spk_warna").html("");
			$("#spk_ket_harga").html("");
			$("#spk_leasing").html("");
			$("#spk_dp").html("");
			$("#spk_sales").html("");	
			$("#spk_sales").html("");

			$.ajax({				
				dataType: 'text',
				type: "GET",
				url: "{{url('api/kasir/kode')}}"
			}).done(function(kode) {
				$("#no_faktur").val(kode);
				$("#spk_spk").focus();
			});

		}

		function getPembayaran(id, subtotal) {
			$("#tabel_pembayaran").html("");
			$.ajax({
				type: "GET",
				url: "{{url('api/kasir/pembayaran')}}/"+id
			}).done(function(bayar) {
				var total_bayar = 0;
				if (bayar != null) {
					for(var i in bayar){
						var item = bayar[i];
						$("#tabel_pembayaran").append('<tr class="jsgrid-row"><td class="jsgrid-cell">'+item.spkp_tgl+'</td><td class="jsgrid-cell">'+item.spkp_ket+'</td><td class="jsgrid-cell text-right">'+number_format(item.spkp_jumlah)+'</td></tr>');
						total_bayar += item.spkp_jumlah;
					}
				}
				$("#total_bayar").html("RP "+number_format(total_bayar)+",-");
				if (subtotal>0 && subtotal > total_bayar){
					var total_kurang = subtotal - total_bayar;
					$("#total_kurang").html("RP "+number_format(total_kurang)+",-");
					if($("#konfirmasi_id").val()==""){
						$("#jumlah").val(number_format(total_kurang));
					}
				}
			});
		}

		$("#save").click(function(){
			var data = {
				_token:'{{csrf_token()}}',
				"konfirmasi_id":$("#konfirmasi_id").val(),
				"spkp_id":$("#no_faktur").val(),
				"spkp_spk":$("#spk_id").val(),
				"spkp_tgl":$("#tanggal_bayar").val(),
				"spkp_jumlah":$("#jumlah").val(),
				"spkp_akun":$("#akun").val(),
				"spkp_ket":$("#ket").val()
			};
			$.ajax({
				type: "POST",
				dataType:"json",
				url: "{{url('/api/kasir')}}",
				data: data
			}).fail(function(response) {
				console.log(response);
				alert("ERR-42 Data Gagal Disimpan !");
			}).done(function(data){
				if (data.result!=1){
					alert(data.msg);
				}else{
					window.open("{{url('/cetak/kwitansi/pembayaran')}}/" + $("#no_faktur").val(), "_blank");
					//window.location = "{{url('/cetak/kwitansi/pembayaran')}}/" + $("#no_faktur").val();

					if (data.statusPembayaran == 1) {		
						var spk = $("#spk_id").val();
						var uid = $("#uid").html();
						var response = data;

						var notif = {
							"notif_judul":"VALID",
							"notif_ket":"SPK " + spk + " telah dinaikan ke status VALID",
							"notif_ref":spk,
							"notif_kategori":"spk",
							"notif_tgl":response.tgl,
						};

						var activity = {
								"activity_judul":spk + " - VALID",
								"activity_ket":"SPK ini telah melakukan pembayaran di atas Rp. 3,500,000.00 .",
								"activity_kategori":"spk",
								"activity_author":"ADH",
								"activity_tgl":response.tgl
						};

						var db = firebase.database().ref('spk_update/' + uid + '/' + spk);
						db.set({"spk_status":1});

						var db = firebase.database().ref('activity/' + uid );
						db.push(activity);

						var db = firebase.database().ref('notif/' + uid);
						db.push(notif);
					}

					var spk_id = $("#spk_id").val();
					$.ajax({
						type: "GET",
						url: "{{url('/api/firebase/sales/uid')}}/" + spk_id
					}).done(function(response){
						var salesUid_sales = response.salesUid_sales;
						var db = firebase.database().ref('sales/' + salesUid_sales + '/pembayaran/' + spk_id+"/"+$("#no_faktur").val());
						db.update({
							"spkp_id":$("#no_faktur").val(),
							"spkp_spk":$("#spk_id").val(),
							"spkp_tgl":$("#tanggal_bayar").val(),
							"spkp_jumlah":string_format($("#jumlah").val()),
							"spkp_ket":$("#ket").val().toUpperCase()
						});				
					});
					alert('Pembayaran Berhasil Disimpan !');
					$("#filter_awal").val("");
					$("#filter_akhir").val("");
					currentData="riwayat";
					$(".tab li").removeClass("active");
					$(".tab li a[data-id='riwayat']").parent().addClass("active");
					loadData();
					$("#tambah").modal("close");
				}
			});

		});

		loadData();
	});
</script>

@endsection