@extends('layout')

@section('content')

	<div class="content-header">
		<h6>
			<small>Matching</small>
			Manual Matching
		</h6>
        @include("modules.match.nav")
	</div>
<div id="cb_matching" class="modal modal-fixed-footer" style="width:400px; height: auto;">
        <form method="POST" action="{{url('/leashing')}}">
            <h6 class="modal-title blue-grey darken-1">
                <span>Manual Matching</span>
                <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
            </h6>
            <div class="modal-content" style="padding:10px;position: relative;">
             @include("modules.match.cb_matching")  
            </div>
              <div style="padding:10px;text-align:right;background:#f5f5f5">
                <a id="manualMatch_save" class="waves-effect waves-light btn save"><i class="material-icons left">save</i> <span id="btn_save"> Save</span></a>
            </div>
         </form>
    </div>

<div id="data_grid" class="wrapper">
	<div class="row" style="margin:0;height:100%;position:relative;">
        <div class="col s12 m6" style="padding:0;height:100%;position:relative; border-right: #ddd 1px solid">
        <div class="nav-wrapper">
            
            <div class="nav-right">
                <a class="waves-effect waves-light btn-flat btn-filter" id="reset_type">Reset</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="export_type">Export</a>
            </div>
        </div>
        	@include("modules.match.data_spk")  
        </div>
	    <div class="col s12 m6" style="padding:0;height:100%;position:relative;">
	    <div class="nav-wrapper">
            
            <div class="nav-right">
                <a class="waves-effect waves-light btn-flat btn-filter" id="reset_variant">Reset</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="export_variant">Export</a>
            </div>
        </div>
	        @include("modules.match.data_kendaraan")  
	    </div>
    </div>
</div>

<script>
$("#manualMatch_save").click(function() {
    manualMatch_save();
});

function manualMatch_save() {
    var item = {
        "spk_id" : $("#spk_id").html(),
        "trk_dh" : $("#trk_dh").val()
    }

    $.ajax({
        type: "POST", 
        url: "{{url('api/matching/update')}}",
        data: item
    }).done(function(response) {
        if (response.result) {
            loadDataSPK();
            loadDataStock();
            $("#cb_matching").modal("close");
        }
    }).fail(function(response) {
        console.log(response);
        alert(response.msg);
    });
}

function setNoDH() {
    $('#trk_dh option').remove();

    $.ajax({
        type: "GET",
        url: "{{url('api/manual_kendaraan/matching')}}"
    }).done(function(response) { 
        $.each(response, function (i, item) {
            $('#trk_dh').append($('<option>', { 
                value: item.trk_dh,
                text : item.trk_dh 
            }));
        });
    }); 
}
loadDataSPK();
loadDataStock();
</script>

@endsection