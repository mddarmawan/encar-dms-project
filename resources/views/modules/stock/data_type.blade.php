<div id="dataType">

</div>

<script>
$(function() {
        var db = {
            loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/kendaraan/type')}}",
                    data: filter
                });
            },

            insertItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "POST",
                    url: "{{url('/api/kendaraan/type')}}",
                    data: item
                }).done(function(){
                    variant();                    
                }).fail(function(response) {
                    console.log(response);
                });    
            },

            updateItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/kendaraan/type')}}",
                    data: item
                }).done(function(){
                    variant();                    
                }).fail(function(response) {
                    console.log(response);
                });
            },

            deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/kendaraan/type')}}",
                    data: item
                }).done(function(){
                    variant();                    
                }).fail(function(response) {
                    console.log(response);
                });
            }
        };

        $("#dataType").jsGrid({
            height: "380px",
            width: "100%",
     
            filtering: true,
            editing: true,
            inserting: true,
            sorting: true,
            autoload: true,
     
            deleteConfirm: "Anda yakin akan menghapus data ini?",
     
            controller: db,
     
            fields: [
                { name: "type_nama", title:"Type", type: "text", width: 120, validate: "required" },
                { name: "type_poin", title:"Point", type: "number", width: 80, validate: "required" },
                { type: "control", width:70 }
            ]
        });
});
</script>