<div id="detail_cancel" class="modal" style="width:1180px; overflow-x: hidden;">  
    <h6 class="modal-title blue-grey darken-1">
        Detail SPK
        <span id="spk_id">SPK</span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:0;position: relative;">
        <div class="row" style="margin:0">
                        <div class="col s12 m6">
                              <table class="info">
                                    <tr>
                                          <td width="180px">Nama Pemesan</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_nama"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Alamat Domisili/Usaha</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_alamat"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode Pos</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_pos"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Telepon</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_telp"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Ponsel</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_ponsel"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Email</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="pel_email"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode PPN</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_ppn"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">NPWP</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_npwp"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Faktur Pajak</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_pajak"></td>
                                    </tr>
                                    <tr>
                                          <td colspan="3">Nama dan Jabatan Contact Person Customer Corporate/Fleet:</td>
                                    </tr>
                                    <tr>
                                          <td colspan="3" id="spk_fleet"></td>
                                    </tr>
                                    <tr>
                                          <td colspan="3" id="spk_pel_fotoid"></td>
                                    </tr>
                              </table>
                        </div>                        
                        <div class="col s12 m6" >
                              <table class="info">
                                    <tr>
                                          <td width="180px">Faktur STNK a/n</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_nama"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">No. KTP</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_identitas"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Alamat KTP/KIMS</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_alamat"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode Pos</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_pos"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Alamat Tempat Tinggal</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_alamatd"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Kode Pos</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_posd"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Telepon</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_telp"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Ponsel</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_stnk_ponsel"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Email</td>
                                          <td width="10px">:</td>
                                          <td class="bold" style="text-center" id="spk_stnk_email"></td>
                                    </tr>
                                    <tr>
                                          <td colspan="3" style="text-center" id="spk_stnk_fotoid"></td>
                                    </tr>
                              </table>
                        </div>
                      </div>
                    <div class="row" style="border-top:1px solid #eee">
                      <div class="col s12 m6"  style="border-bottom:1px solid #eee;font-size:12px; padding:5px 20px;">
                        KETERANGAN
                      </div>
                      <div class="col s12 m6"  style="border-bottom:1px solid #eee;font-size:12px; padding:5px 20px;">
                        ALASAN & CATATAN SALES
                      </div>
                        <div class="col s12 m6">
                            <table class="info" style="margin-top:0">
                                    <tr>
                                          <td width="180px">Variant</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="variant_nama"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Warna</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_warna"></td>
                                    </tr>
                                    <tr>
                                          <td width="180px">Keterangan Harga</td>
                                          <td width="10px">:</td>
                                          <td class="bold" id="spk_kategori"></td>
                                    </tr>
                              </table>
                        </div>

                        <div class="col s12 m6 info">
                            <p class="bold" id="spk_ket_cancel" style="padding-left:10px"></p>
                            <i id="spk_catt_cancel" style="padding-left:10px"></i>
                        </div>

                  </div>  
      </div>
                  <div class="modal-footer text-right">
                      <a class="waves-effect waves-light btn green konfirmasi" id="konfirmasi" title="Konfirmasi SPK">
                          <i class="material-icons left" >done</i>
                          <span>Konfirmasi</span>
                      </a>
                  </div>  
</div>

<div id="konfirmasi_batal" class="modal"  style="width:450px;">  
    <h6 class="modal-title blue-grey darken-1">
        <span>Konfirmasi Cancel</span>
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:15px; position: relative;">
        <div class="row" style="margin:0;margin-bottom:10px;">
                  <div class="col m6">
                        <a id="tanggapi" target="_blank" class="waves-effect waves-light orange btn-large" style="width:100%"><i class="material-icons left">error_outline</i> Tanggapi</a>
                  </div>
                  <div class="col m6">
                        <a id="setujui" target="_blank" class="waves-effect waves-light green btn-large" style="width:100%"><i class="material-icons left">done</i> Setujui</a>
                  </div>
        </div>
    </div>
</div>
