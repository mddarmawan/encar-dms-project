@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Pengaturan</small>
            Data Asuransi
        </h6>
    </div>

<div id="data_grid" class="wrapper">
        <div class="nav-wrapper">
            
            <div class="nav-right">
                <form action="" method="get" style="display: inline-block;">
                    <button type="submit" class="waves-effect waves-light btn-flat btn-filter" style="margin:10px;"><i class="fa fa-refresh" aria-hidden="true" style="font-size: 15px;"></i> &nbsp;Refresh</button>
                </form>
                <a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
            </div>

            

        </div>

        <div id="dataAsuransi">

        </div>  

</div>

<script>

     $("#filter").click(function(e){
        e.preventDefault();
        loadData();
    });


    $("#reset").click(function(e){
        e.preventDefault();
        $("#filter_awal").val("");
        $("#filter_akhir").val("");
        status="";
        $(".tab li").removeClass("active");
        $(".tab li a[data-id='']").parent().addClass("active");
        loadData();
    });


    $("#export").click('click', function (event) {
       var args = [$('#dataAsuransi'), 'DATA_ASURANSI_<?php echo date('dmY') ?>.xls'];    
        exportTableToExcel.apply(this, args);
    });
$(function() {

    var db_asuransi = {
        loadData: function(filter) {
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/asuransi')}}",
                    data: filter
                });
            },

            insertItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "POST",
                    url: "{{url('/api/asuransi')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                });    
            },

            updateItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/asuransi')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                });
            },

            deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/asuransi')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                });
            }
        };

    $("#dataAsuransi").jsGrid({
        height: "calc(100% - 40px)",
        width: "100%",
 
        filtering: true,
        editing: true,
        inserting: true,
        sorting: true,
        autoload: true,
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db_asuransi,
 
         fields: [
                { name: "asuransi_nama", title:"Nama", type: "text", width: 100, validate: "required" , align:"" },

                { name: "asuransi_telp", title:"No. Telp", type: "text", width: 130, validate: "required" , align:""},
                { name: "asuransi_alamat", title:"Alamat", type: "text", width: 170, validate: "" , align:""},
                { name: "asuransi_email", title:"email", type: "text", width: 170, validate: "" , align:""},
                { name: "asuransi_keterangan", title:"Keterangan", type: "text", width: 120, align:"" },
                { type: "control", width:70 }
            ]
    });
 
});
</script>

@endsection