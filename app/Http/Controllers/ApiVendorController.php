<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Vendor;

class ApiVendorController extends Controller
{
    function index(){
    	$vendor = DB::table('tb_vendor')-> get();
		$result = $vendor->filter(function ($vendor) {
		    return 
		    	(!request("vendor_nama") || strrpos(strtolower($vendor->vendor_nama), strtolower(request("vendor_nama"))) > -1) &&
				 (!request("vendor_alamat") || strrpos(strtolower($vendor->vendor_alamat), strtolower(request("vendor_alamat"))) > -1) &&
				 (!request("vendor_kota") || strrpos(strtolower($vendor->vendor_kota), strtolower(request("vendor_kota"))) > -1) &&
				 (!request("vendor_kodepos") || strrpos(strtolower($vendor->vendor_kodepos), strtolower(request("vendor_kodepos"))) > -1) &&
				 (!request("vendor_notelp") || strrpos(strtolower($vendor->vendor_notelp), strtolower(request("vendor_notelp"))) > -1) &&
				 (!request("vendor_email") || strrpos(strtolower($vendor->vendor_email), strtolower(request("vendor_email"))) > -1);
		});

    	return json_encode($result);
    }

    function store(){
    	$this->validate(request(), [
            "vendor_nama"      => "required",
            "vendor_kota"     => "required",
            "vendor_notelp"     => "required",
            "vendor_email"     => "required",
        ]);

        
        $insert = array(
            "vendor_nama"     =>  request("vendor_nama"),
            "vendor_alamat"     =>  request("vendor_alamat"),
            "vendor_kota"     =>  request("vendor_kota"),
            "vendor_kodepos"   =>  request("vendor_kodepos"),
            "vendor_notelp"     =>  request("vendor_notelp"),
            "vendor_email"     =>  request("vendor_email"),
        );

        $id = DB::table('tb_vendor')-> insertGetId($insert,'vendor_id');
        return json_encode(DB::table('tb_vendor')->where("vendor_id",$id)->first()) ;
    }

    function update(){
    	$this->validate(request(), [
            "vendor_nama"      => "required",
            "vendor_kota"     => "required",
            "vendor_notelp"     => "required",
            "vendor_email"     => "required",
        ]);

	    DB::table('tb_vendor')-> where("vendor_id",request("vendor_id"))->update([
            "vendor_nama"     =>  request("vendor_nama"),
            "vendor_alamat"     =>  request("vendor_alamat"),
            "vendor_kota"     =>  request("vendor_kota"),
            "vendor_kodepos"   =>  request("vendor_kodepos"),
            "vendor_notelp"     =>  request("vendor_notelp"),
            "vendor_email"     =>  request("vendor_email"),
	    ]);

	    return json_encode(DB::table('tb_vendor')-> where("vendor_id",request("vendor_id"))->first()) ;
    }

    function destroy(){
		return DB::table('tb_vendor')-> where('vendor_id', request("vendor_id"))->delete();
    }    
}
