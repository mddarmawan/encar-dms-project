@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Pembelian</small>
            Permintaan Pembelian Kendaraan (PO Request)
        </h6>
    </div>

<div id="data_grid" class="wrapper">
        <div class="nav-wrapper">
            <div class="nav-right">
                <i style="margin-right: 5px" class="fa fa-filter" aria-hidden="true"></i>
                <span class="bold" style="font-size: 13px"> Periode :</span>
                <div class="input-group">
                    <input type="text" class="input-filter datepicker" id="filter_awal" placeholder="Tanggal Awal" required="" />
                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                </div>
                <span class="bold" style="font-size: 13px">s.d</span>
                <div class="input-group">
                    <input type="text" class="input-filter datepicker"  id="filter_akhir" placeholder="Tanggal Akhir"  required="" />
                    <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                </div>
                <a class="waves-effect waves-light btn-flat btn-filter" id="filter">Cari</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="reset">Reset</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
            </div>
        </div>
        <div id="data">

        </div>  
    </div>

<script>
$("#filter").click(function(e){
    e.preventDefault();
    loadData();
    });

    $("#reset").click(function(e){
        e.preventDefault();
        $("#filter_awal").val("");
        $("#filter_akhir").val("");
        status="";
        $(".tab li").removeClass("active");
        $(".tab li a[data-id='']").parent().addClass("active");
        loadData();
    });


    $("#export").click('click', function (event) {
        var args = [$('#data'), 'REQUEST_KENDARAAN_<?php echo date('dmY') ?>.xls'];   
        exportTableToExcel.apply(this, args);
    }); 

function loadData() {

    var db_req = {
        loadData: function(filter) {
                var filter_awal = $("#filter_awal").val().trim();
                var filter_akhir = $("#filter_akhir").val().trim();
                if (filter_awal != ""){
                    filter['filter_awal'] = filter_awal;
                }
                if (filter_akhir != ""){
                    filter['filter_akhir'] = filter_akhir;
                }
                filter['spk_status'] = status;
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/po/request_kendaraan')}}",
                    data: filter
                });
            },

    };

  $("#data").jsGrid({
        height: "100%",
        width: "100%",
        sorting: true,
        filtering: true,
        autoload: true,
        paging: true,
        pageSize: 30,
        pageButtonCount: 5,
        noDataContent: "Tidak Ada Data",
 
 
        controller: db_req,
 
        fields: [

            { name: "spk_tgl", title:"Tanggal", type: "text", width: 100, align:"" },
            { name: "spk_id", title:"No. SPK", type: "text", width: 100, align:"" },
            { name: "spk_pel_nama", title:"Pelanggan", type: "text", width: 100},
            { name: "karyawan_nama", title:"Sales", type: "text", width: 100 },
            { name: "team_nama", title:"Sales", type: "text", width: 100, align:"center" },
            { name: "type_nama", title:"Type", type: "text", width: 100, align:""},
            { name: "variant_nama", title:"Variant", type: "text", width: 150},
            { name: "warna_nama", title:"Warna", type: "text", width: 100, align:""},
            { name: "variant_serial", title:"ID", type: "text", width: 100, align:"" }            
        ]
    });
   
};
loadData();
</script>

@endsection