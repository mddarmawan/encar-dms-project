<div id="dataLeashing">

</div>

<script src="https://www.gstatic.com/firebasejs/4.2.0/firebase.js"></script>

    <script>
      // Initialize Firebase
      var config = {
        apiKey: "AIzaSyAbGQT6dUxS5arqAz5DgG1RskqUMX-XOPk",
        authDomain: "dealer-ms.firebaseapp.com",
        databaseURL: "https://dealer-ms.firebaseio.com",
        projectId: "dealer-ms",
        storageBucket: "dealer-ms.appspot.com",
        messagingSenderId: "950788435795"
      };
      firebase.initializeApp(config);
      firebase.auth().signInWithEmailAndPassword("encar.dms.app@gmail.com", ".encar87").catch(function(error) {
          console.log(error.message);
        });
    </script>


<script>
$(function() {


	var db_leashing = {
        loadData: function(filter) {
			return $.ajax({
                        type: "GET",
                        url: "{{url('api/leasing')}}",
                        data: filter
                    });
        },

       insertItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "POST",
                    url: "{{url('/api/leasing')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    var db = firebase.database().ref("data/tb_leasing/" + item.leasing_id + "/");
                    db.set({
                        leasing_id: item.leasing_id,
                        leasing_nama: item.leasing_nama,
                        leasing_status: item.leasing_status
                    });
                });    
            },

       updateItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/leasing')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(item){
                    var db = firebase.database().ref("data/tb_leasing/" + item.leasing_id + "/");
                    db.set({
                        leasing_id: item.leasing_id,
                        leasing_nama: item.leasing_nama,
                        leasing_status: item.leasing_status
                    });
                });
            },

       deleteItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "DELETE",
                    url: "{{url('/api/leasing')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                }).done(function(response){
                    firebase.database().ref("data/tb_leasing/" + item.leasing_id + "/").remove();                   
                });
            }
        };

    $("#dataLeashing").jsGrid({
        height: "auto",
        width: "100%",
        
        filtering: true,
        editing: true,
        inserting: true,
        sorting: true,
        autoload: true,
 
        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db_leashing,
 
        fields: [
            { name: "leasing_nama", title:"Nama Leashing", type: "text", width: 150, validate: "required" },
            { name: "leasing_nick", title:"Singkatan", type: "text", width: 120, validate: "required" },
            { name: "leasing_alamat", title:"Alamat", type: "text", width: 200 },
            { name: "leasing_kota", title:"Kota", type: "text", width: 100, validate: "required" },
            { name: "leasing_telp", title:"Telepon", type: "text", width: 100, validate: "required" },
            { type: "control", width:70 }
        ]
    });
 
});
</script>