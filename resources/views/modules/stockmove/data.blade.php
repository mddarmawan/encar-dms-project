@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Persediaan</small>
            Perpindahan Stock Kendaraan
        </h6>
		<ul class="header-tools right">
            <li><a href="javascript:;" class="chip add"><i class="fa fa-plus"></i> Pindahkan Stock</a></li>
		</ul>
    </div>

<div class="wrapper">
    <div id="data">

    </div>  
</div>
<div id="add" class="modal">    
    <h6 class="modal-title blue-grey darken-1">
        Entry Perpindahan Stock Kendaraan
        <span class="modal-close right material-icons" style="margin-top:-3px">close</span>
    </h6>
    <div class="modal-content" style="padding:10px 14px 0;position: relative;">
        <div class="row" style="margin:0">
            <div class="col s6" style="padding-left:0">
				<input type="hidden" id="stock_id" />
                <table class="info payment" style="margin:0;border-bottom:#777">
                    <tr>
                        <td>
							<strong>Kendaraan</strong>
							<div class="input-field" style="margin:0">
								<i class="fa-sufix fa fa-search blue"></i>
								<input type="text" id="stock" placeholder="Masukkan No DH Kendaraan" value="" >
							</div>
						</td>
                    </tr>
                </table>
                <table class="info payment" style="margin-bottom:0">
					<tr>
                        <td width="200">NO DH</td>
						<td><input type="text" id="dh" readonly /></td>
                    </tr>
					<tr>
                        <td width="200">TIPE</td>
						<td><input type="text" id="tipe" readonly /></td>
                    </tr>
					<tr>
                        <td width="200">WARNA</td>
						<td><input type="text" id="warna" readonly /></td>
                    </tr>
					<tr>
                        <td width="200">TAHUN</td>
						<td><input type="text" id="tahun" readonly /></td>
                    </tr>
					<tr>
                        <td width="200">NO MESIN</td>
						<td><input type="text" id="mesin" readonly /></td>
                    </tr>
					<tr>
                        <td width="200">NO RANGKA</td>
						<td><input type="text" id="rangka" readonly /></td>
                    </tr>
					<tr>
                        <td width="200">LOKASI</td>
						<td><input type="text" id="lokasi" readonly /></td>
                    </tr>
                </table>
                <table class="info payment" style="margin:10px 0 0;border-bottom:#777">
                    <tr>
                        <td>
							<strong>Ekspedisi</strong><p style="margin:0">* Lewatkan jika tidak menggunakan ekspedisi</p>
						</td>
                    </tr>
                </table>
            </div>
            <div class="col s6" style="padding-right:0">
                <table class="info payment" style="margin:0;border-bottom:#777">
                    <tr>
                        <td>
							<strong>Tujuan</strong>
							<div class="input-field" style="margin:0">
								<i class="fa-sufix fa fa-caret-down blue"></i>
								<select id="gudang"></select>
							</div>
						</td>
                    </tr>
                </table>
                <table class="info payment" style="margin-bottom:0">
					<tr>
                        <td width="150" style="vertical-align:top">ALAMAT</td>
						<td><textarea type="text" id="alamat" style="border:inset 2px;height:5rem;resize:none;background:#f5f5f5;outline:none;" readonly ></textarea></td>
                    </tr>
					<tr>
                        <td width="150">KOTA</td>
						<td><input type="text" id="kota" readonly /></td>
                    </tr>
					<tr>
                        <td width="150">TELEPON</td>
						<td><input type="text" id="telepon" readonly /></td>
                    </tr>
					<tr>
                        <td width="150">EMAIL</td>
						<td><input type="text" id="email" readonly /></td>
                    </tr>
                </table>
            </div>
			
        </div>
    </div>
	<div class="row" style="padding:0 10px 10px;text-align:right;margin:0;background:#fff">
		<div class=" col s6">
			<div class="input-field" style="margin:0;padding-left:0;">
				<i class="fa-sufix fa fa-caret-down blue"></i>
				<select id="ekspedisi"></select>
			</div>
		</div>
		<div class=" col s6">
			<a class="waves-effect waves-light btn save"><i class="material-icons left">save</i>Simpan</a>
		</div>
	</div>
</div>
<script>
$(function() {
	function loadData(){
		var db = {
		   loadData: function(filter) {
				return $.ajax({
					type: "GET",
					url: "{{url('api/persediaan/stockmove')}}",
					data: filter
				});
			},
		};
		
		
		db.status = [
			{"status_id":"", "status_nama":""},
			{"status_id":"WAITING", "status_nama":"WAITING"},
			{"status_id":"INTRANSIT", "status_nama":"INTRANSIT"},
			{"status_id":"GI", "status_nama":"GI"}
		];

		$("#data").jsGrid({
			height: "100%",
			width: "100%",
			sorting: true,
			filtering: true,
			autoload: true,
			paging: true,
			pageSize: 30,
			pageButtonCount: 5,
			noDataContent: "Tidak Ada Data",
			
	 
			controller: db,
	 
			fields: [
				{ name: "trk_dh", title:"No. DH", type: "text", width: 80, align:"center" },
				{ name: "trk_tgl", title:"Tgl Stock", type: "text", width: 80, align:"center" },
				{ name: "trk_tahun", title:"Tahun Unit", type: "text", width: 100, align:"center"},
				{ name: "variant_nama", title:"Tipe", type: "text", width: 150, align:"center"},
				{ name: "variant_serial", title:"ID", type: "text", width: 150, align:"center"},
				{ name: "warna_nama", title:"Warna", type: "text", width: 100, align:"center" },
				{ name: "trk_rangka", title:"No Rangka", type: "text", width: 170, align:"center" },
				{ name: "trk_mesin", title:"No Mesin", type: "text", width: 170, align:"center" },
				{ name: "sm_dari", title:"Dari", type: "text", width: 150, align:"center" },
				{ name: "sm_ke", title:"Ke", type: "number", width: 150, align:"center" },
				{ name: "ekspedisi_nama", title:"Ekspedisi", type: "text", width: 120, align:"center" },
				{ name: "sm_tglout", title:"Tgl Keluar", type: "text", width: 80, align:"center" },
				{ name: "sm_tglin", title:"Tgl Sampai", type: "text", width: 80, align:"center" },
				{ name: "sm_status", title:"Status", type: "select", items: db.status, valueField: "status_id", textField: "status_nama", width: 120, align:"center" },
			]
		});
	 
	}
	loadData();
	
	$(".add").click(function(){
		document.getElementById("gudang").selectedIndex = 0;
		document.getElementById("ekspedisi").selectedIndex = 0;
		$('#stock').val("");
		$('#stock_id').val("");
		$('#dh').val("");
		$('#tipe').val("");
		$('#mesin').val("");
		$('#rangka').val("");
		$('#warna').val("");
		$('#tahun').val("");
		$('#lokasi').val("");
		$("#alamat").val("");
		$("#kota").val("");
		$("#telepon").val("");
		$("#email").val("");
		
		$("#add").modal("open");
	});
	
	$(".save").click(function(){
		var item = {
            sm_trk:$("#stock_id").val(),
            sm_gudang:$("#gudang").val(),
            sm_ekspedisi:$("#ekspedisi").val(),
            _token:'{{csrf_token()}}'
        }

        if ($("#stock_id").val() != "" && $("#gudang").val()!=""){
            $.ajax({
                type: "POST",
                url: "{{url('/api/persediaan/stockmove')}}",
                data: item
            }).fail(function(response) {
                alert("ERR-42 Perpindahan Stock gagal disimpan!, silahkan hubungi administrator");
                console.log(response);
            }).done(function(response){
                if (response==1){
                    loadData();
                    $("#add").modal("close");
                }else{
                    alert("ERR-00 Perpindahan Stock gagal disimpan!, silahkan hubungi administrator");
                }
            }); 
        }else{
            alert("Kendaraan atau Tujuan belum dipilih !");
        }
	});
	
	$.ajax({
		type:'GET',
		dataType: 'json',
		url: '{{url("/api/persediaan/getgudang")}}'
	}).done(function(data){
		$("#gudang").html("<option value=''>----</option>");
		for(var i in data){
			var item = data[i];
			$("#gudang").append("<option value='"+ item.gudang_id +"'>"+item.gudang_nama+"</option>");
		}
		
		$("#gudang").change(function(){
			var index = $(this).prop('selectedIndex') -1;
			if (index > -1){
				var item = data[index];
				$("#alamat").val(item.gudang_alamat);
				$("#kota").val(item.gudang_kota);
				$("#telepon").val(item.gudang_telp);
				$("#email").val(item.gudang_email);
			}else{
				$("#alamat").val("");
				$("#kota").val("");
				$("#telepon").val("");
				$("#email").val("");
			}			
		});
	});
	
	
	$.ajax({
		type:'GET',
		dataType: 'json',
		url: '{{url("/api/ekspedisi")}}'
	}).done(function(data){
		$("#ekspedisi").html("<option value=''>----</option>");
		for(var i in data){
			var item = data[i];
			$("#ekspedisi").append("<option value='"+ item.ekspedisi_id +"'>"+item.ekspedisi_nama+"</option>");
		}
	});
	
	$.ajax({
		type:'GET',
		dataType: 'json',
		url: '{{url("/api/persediaan/getstock")}}'
	}).done(function(data){
		$('#stock').autocomplete({
			source:data,
			select: function( event, ui ) {
				_model = ui.item.id;							
				$('#stock').val(ui.item.value);
				$('#stock_id').val(ui.item.id);
				$('#dh').val(ui.item.value);
				$('#tipe').val(ui.item.type_nama+" "+ui.item.variant_nama);
				$('#mesin').val(ui.item.trk_mesin);
				$('#rangka').val(ui.item.trk_rangka);
				$('#warna').val(ui.item.warna_nama);
				$('#tahun').val(ui.item.trk_tahun);
				$('#lokasi').val(ui.item.trk_lokasi);
				
				return false;
			}
		}).bind('focus', function() {
			$(this).keydown();
		}).autocomplete('instance' )._renderItem = function( ul, item ) {
			return $('<li>').append("<div class='stock-item'><h5>" + item.value + "<span class='pull-right'>"+ item.trk_tahun +"</span></h5><h5><b>"+item.type_nama+" "+item.variant_nama+"</b> / "+item.warna_nama+"</h5><h5><i>"+item.trk_mesin+" <span class='pull-right'>"+item.trk_rangka+"</span></i></h5></div>").appendTo(ul);
		};		
	});
});
</script>

@endsection