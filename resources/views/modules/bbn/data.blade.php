@extends('layout')

@section('content')

   <div class="content-header">
        <h6 class="left">
            <small>Penjualan</small>
            BBN
        </h6>
    </div>

<<div id="data_grid" class="wrapper">
        <div class="nav-wrapper">
            
            <div class="nav-right">
                <a class="waves-effect waves-light btn-flat btn-filter" id="reset">Reset</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
            </div>

            

        </div>

        <div id="dataBbn">

        </div>  

</div>


<script>

 $("#filter").click(function(e){
        e.preventDefault();
        loadData();
    });


    $("#reset").click(function(e){
        e.preventDefault();
        $("#filter_awal").val("");
        $("#filter_akhir").val("");
        status="";
        $(".tab li").removeClass("active");
        $(".tab li a[data-id='']").parent().addClass("active");
        loadData();
    });


    $("#export").click('click', function (event) {
        var args = [$('#dataBbn'), 'DATA_MASTER_BBN_<?php echo date('dmY') ?>.xls'];   
        exportTableToExcel.apply(this, args);
    });

function vendor() {
    $.ajax({
        type: "GET",
        url: "{{url('api/kendaraan/type')}}"
    }).done(function(type) {
        type.unshift({ type_id: "0", type_nama: "" });
        var db = {
            loadData: function(filter) {
                
                return $.ajax({
                    type: "GET",
                    url: "{{url('api/kendaraan/bbn')}}",
                    data: filter
                });
            },

            

            updateItem: function(item) {
                item['_token'] = '{{csrf_token()}}';
                return $.ajax({
                    type: "PUT",
                    url: "{{url('/api/kendaraan/bbn')}}",
                    data: item
                }).fail(function(response) {
                    console.log(response);
                });
            },

        };

    $("#dataBbn").jsGrid({
        height: "calc(100% - 40px)",
        width: "100%",
        filtering: true,
        editing: true,
        inserting: false,
        sorting: true,
        autoload: true,

        // int to string
        onItemEditing: function(args){
            var bbn = args.item.variant_bbn; 
            var on = args.item.variant_on; 
            args.item.variant_bbn= string_format(bbn);
            args.item.variant_on= string_format(on);
           },

        deleteConfirm: "Anda yakin akan menghapus data ini?",
 
        controller: db,
 
        fields: [
                { name: "variant_type", title:"Type", editing: false, type: "select", items: type, valueField: "type_id", textField: "type_nama", width: 100, align:"left"},
                { name: "variant_nama", title:"Variant", editing: false, type: "text", width: 120, align:"" },
                { name: "variant_serial", title:"Id Kendaraan", editing: false, type: "text", width: 200,  align:"" },
                { name:"variant_off", title:"Off The Road", editing: false, type: "text", width: 100, align:"right"},
                { name: "variant_on", title:"On The Road", type: "text", width: 100,  align:"right"},
                { name: "variant_bbn", title:"BBN Kendaraan", type: "text", width: 100,  align:"right"},
                { type: "control", insertButton:false, deleteButton:false, width:70 }
            ]
        });
    });
 
};
vendor();
</script>

@endsection