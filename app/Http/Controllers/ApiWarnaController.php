<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class ApiWarnaController extends Controller
{
    function all() {
        return json_encode(DB::table("tb_warna")->get());
    }

    function index(){
        $data = DB::table('tb_warna') 
                ->leftjoin('tb_type', 'tb_warna.warna_type', '=', 'tb_type.type_id')
                ->where("warna_hapus", 0)
                ->where("warna_id",">",0)
                ->get();

        $result = $data->filter(function ($data) {
            return 
                (!request("warna_nama") || strrpos(strtolower($data->warna_nama), strtolower(request("warna_nama"))) > -1) && 
                (!request("warna_type") || strrpos(strtolower($data->warna_type), strtolower(request("warna_type"))) > -1);
                 });
        $data = array();
        foreach($result as $r){
            $item = array();
            $item['warna_nama'] = $r->warna_nama;
            $item['warna_type'] = $r->warna_type;
            array_push($data, $item);
        }
        return json_encode($data);
    }

    //  function index(){
    //    $data = DB::table('tb_warna') 
    //             ->leftjoin('tb_type', 'tb_warna.warna_type', '=', 'tb_type.type_id')
    //             ->where("warna_hapus", 0)
    //             ->where("warna_id",">",0)
    //             ->get();

    //         $result = array();
    //         foreach($data as $r){
    //         $item = array();
    //         $item['type_id'] = $r->type_id;
    //         $item['warna_nama'] = $r->warna_nama;
    //         $item['warna_type'] = $r->warna_type;
    //         if((!request("warna_nama") || strrpos(strtolower($item['warna_nama']), strtolower(request("warna_nama"))) > -1) &&
    //             (!request("warna_type") || strrpos(strtolower($item['warna_type']), strtolower(request("warna_type")))))

    //          array_push($result, $item);

    //     }

    //     return json_encode($result);
    // }

    function warna_variant(){
      $data =DB::table('tb_warna')->get();
        $result = $data->filter(function ($data) {
            return 
                (!request("warna_nama") || strrpos(strtolower($data->warna_nama), strtolower(request("warna_nama"))) > -1);
        });

        return json_encode($result);
    }

    function store(){
    	$this->validate(request(), [
            "warna_nama"      => "required",
            "warna_type"      => "required",
        ]);

	    $insert= array (
	        "warna_nama"        =>  request("warna_nama"),
            "warna_type"        =>  request("warna_type"),
            "created_at"        => date("Y-m-d H:i:s"),
            "updated_at"        => date("Y-m-d H:i:s")
	    );

        $id = DB::table('tb_warna')-> insertGetId($insert, 'warna_id');
        return json_encode(DB::table('tb_warna')-> where("warna_id",$id)->first());
    }

    function update(){
    	$this->validate(request(), [
            "warna_nama"      => "required"
        ]);

	    DB::table('tb_warna')-> where("warna_id",request("warna_id"))->update([
	        "warna_nama"       => request("warna_nama"),
            "updated_at"       => date("Y-m-d H:i:s")
        ]);

	    return json_encode(DB::table('tb_warna')-> where("warna_id",request("warna_id"))->first());
    }

    function destroy(){
        $update = DB::table('tb_warna')
            ->where("warna_id",request("warna_id"))
            -> update([
                "warna_hapus"    => '1'
            ]);

        return json_encode($update);
    }    
}
