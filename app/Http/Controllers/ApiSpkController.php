<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use DateTime;

class ApiSpkController extends Controller
{

//-------------------------------//
// Tampilan Data
//-------------------------------//

	 function index(){
		$data = DB::table('tb_spk')
			->leftjoin('tb_spk_leasing', 'tb_spk_leasing.spkl_spk', '=', 'tb_spk.spk_id')
			->leftjoin('tb_spk_diskon', 'tb_spk_diskon.spkd_spk', '=', 'tb_spk.spk_id')
			->leftjoin('tb_spk_ttbj', 'tb_spk_ttbj.spkt_spk', '=', 'tb_spk.spk_id')
			->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
			->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
			->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
			->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
			->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
			->leftjoin('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk.spk_leasing')
			->leftjoin("tb_warna", 'warna_id', '=', 'tb_spk.spk_warna')
			->whereNull("spk_do")
			->where("spk_status","<",4)
			->orwhere("spk_status","=",9)
			->orwhere("spkl_doc","=",0)
			->orwhere("spkl_doc","=",1)
			->get();

			$result = array();
			foreach($data as $r){
				if ($r->spk_do != NULL && $r->spkt_id != NULL) {
					continue;
				}

			$item = array();
			$item['spkt_id'] = $r->spkt_id;
			$item['spk_id'] = $r->spk_id;
			$item['spk_tgl'] = date_format(date_create($r->spk_tgl),"d/m/Y");
			$item['spk_pel_nama'] = ucwords($r->spk_pel_nama);
			$item['spk_pel_alamat'] = ucwords($r->spk_stnk_alamat);
			$item['spk_pel_kota'] = ucwords($r->spk_pel_kota);
			$item['spk_type'] = $r->type_nama;
			$item['spk_kendaraan_harga'] = $r->spk_ket_harga;
			$item['spk_variant'] = $r->type_nama." ".$r->variant_nama;
			$item['spk_warna'] = $r->warna_nama;
			$item['spk_sales'] = $r->karyawan_nama;
			$item['spk_team'] = $r->team_nama;
			$item['spk_bbn'] = number_format($r->spk_bbn ,0,',','.');
			if ($r->spkl_doc == 1){
				$item['spk_doc'] = $r->spkl_doc;	
			}else{
				$item['spk_doc'] =0;	 
			}

			if ($r->spkd_status == 1){
				$item['spk_diskon'] = $r->spkd_status;	
			}else{
				$item['spk_diskon'] =0;	 
			}
			//&& $r->trk_dh !=NULL
			if ($r->spk_dh !=NULL){
				$item['spk_matching'] = $r->spk_dh;	
			}else{
				$item['spk_matching'] =0;	 
			}
			$item['spk_pembayaran'] = number_format(DB::table('tb_spk_pembayaran')->select(DB::raw("SUM(spkp_jumlah) as bayar"))
			->where("spkp_spk",$r->spk_id)->first()->bayar,0,",","."); 
			$item['spk_pel_kota'] = $r->spk_pel_kota;
			$item['spk_metode'] =$r->spk_pembayaran+1;
			if ($r->spk_pembayaran == 0){
				$item['spk_via'] = "CASH";	  
			}else{
				$item['spk_via'] = $r->leasing_nick;			
			}
			if ($r->spk_ket_harga == 1){
				$item['spk_harga'] = number_format( $r->variant_on);	 
			}else{
				$item['spk_harga'] = number_format($r->variant_on - $r->variant_bbn) ;			
			}

			 if ($r->spk_ket_harga == 1){
				$item['spk_subtotal'] = number_format( $r->variant_on -$r->spkd_cashback) ;	 
			}else{
				$item['spk_subtotal'] = number_format($r->variant_on - $r->variant_bbn -  $r->spkd_cashback) ;			
			} 

			if ($r->spk_ket_harga == 1){
				$item['spk_piutang'] = number_format (($r->variant_on   - $r->variant_bbn -$r->spkd_cashback - (DB::table('tb_spk_pembayaran')-> select(DB::raw("SUM(spkp_jumlah) as bayar"))->where("spkp_spk",$r->spk_id)->first()->bayar)-$r->spkl_droping),0,",",".") ;  
			}else{
				$item['spk_piutang'] = number_format(($r->variant_on  - $r->spkd_cashback - ( DB::table('tb_spk_pembayaran')->select(DB::raw("SUM(spkp_jumlah) as bayar")) ->where("spkp_spk",$r->spk_id)->first()->bayar)-$r->spkl_droping),0,",",".");		 
			}
			$item['spk_status'] = $r->spk_status;
			$item['spk_catt_permintaan'] = $r->spk_catt_permintaan;
			if((!request("spk_id") || strrpos(strtolower($item['spk_id']), strtolower(request("spk_id"))) > -1) &&
				(!request("spk_tgl") || strrpos(strtolower($item['spk_tgl']), strtolower(request("spk_tgl"))) > -1) &&
				(!request("spk_pel_nama") || strrpos(strtolower($item['spk_pel_nama']), strtolower(request("spk_pel_nama"))) > -1) &&
				(!request("spk_dp") || strrpos(strtolower($item['spk_dp']), strtolower(request("spk_dp"))) > -1) &&
				(!request("spk_sales") || strrpos(strtolower($item['spk_sales']), strtolower(request("spk_sales"))) > -1) &&
				(!request("spk_team") || strrpos(strtolower($item['spk_team']), strtolower(request("spk_team"))) > -1) &&
				(!request("spk_variant") || strrpos(strtolower($item['spk_variant']), strtolower(request("spk_variant"))) > -1) &&
				(!request("spk_pembayaran") || strrpos(strtolower($item['spk_pembayaran']), strtolower(request("spk_pembayaran"))) > -1) &&
				(!request("spk_piutang") || strrpos(strtolower($item['spk_piutang']), strtolower(request("spk_piutang"))) > -1) &&
				(!request("spk_pel_kota") || strrpos(strtolower($item['spk_pel_kota']), strtolower(request("spk_pel_kota"))) > -1) &&
				 (!request("spk_warna") || strrpos(strtolower($item['spk_warna']), strtolower(request("spk_warna"))) > -1) &&
				 (!request("spk_kendaraan_harga") || strtolower($item['spk_kendaraan_harga'])== strtolower(request("spk_kendaraan_harga"))) &&
				 (!request("spk_metode") || strtolower($item['spk_metode'])== strtolower(request("spk_metode")))&&
				 (!request("spk_status") || strtolower($item['spk_status'])== strtolower(request("spk_status"))))	{

				$tgl = strtotime(str_replace("/","-",$item['spk_tgl']));
				if (request("filter_awal") && request("filter_akhir")){
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
					if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_awal")){
					$filter_awal = strtotime(request("filter_awal"));
					if ($filter_awal<=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_akhir")){
					$filter_akhir = strtotime(request("filter_akhir"));
					if ($filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else{
					array_push($result, $item);
				}
			}
		}


		return json_encode($result);
	}

	public function getSPK($id = null) {
		if (is_null($id)) {
			$result = DB::table('tb_spk')->get();

			$data = array();
			foreach ($result as $r) {
				$id = $r->spk_id;
				$item = array();
				$item['id'] = $r->spk_id;
                $item['value'] = $r->spk_id . ' / ' . $r->spk_pel_nama;
				$item['pemesan'] = DB::table('tb_spk')
					->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
					->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
					->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
					->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
					->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
					->leftjoin('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk.spk_leasing')
					->leftjoin("tb_warna", 'warna_id', '=', 'tb_spk.spk_warna')
					->leftjoin('vw_bayar', 'bayar_spk', '=', 'spk_id')
		            ->leftjoin('vw_diskon', 'diskon_spk', '=', 'spk_id')
		            ->leftjoin('tb_spk_leasing', 'spkl_spk', '=', 'spk_id')
					->where("spk_id",$id)
					->first();

				$item['kendaraan'] = DB::table('tb_spk')
					->join("tb_tr_kendaraan","tb_tr_kendaraan.trk_dh","=","tb_spk.spk_dh")
					->join('tb_warna', 'tb_warna.warna_id', '=', 'tb_tr_kendaraan.trk_warna')
					->where("spk_status","<",10)
					->where("spk_id",$id)->first();

				$item['bayar'] = DB::table('tb_spk_pembayaran')
					->where("spkp_spk",$id)->get();


				$item['faktur'] = DB::table('tb_spk_faktur')
					->where("spkf_spk",$id)->first();

				$faktur = DB::table('tb_spk_faktur')->orderBy("spkf_id","DESC")->first();

				$item['no_faktur'] = "F-".date("Y")."0001";
				if($faktur){
					$tmp = $faktur->spkf_id;
					$tahun = substr($tmp,2,4);
					$num = substr($tmp,6,strlen($tmp));
					if(date("Y") == $tahun){
						$num++;
						if(strlen($num)==1){
							$num = "000".$num;
						}elseif(strlen($num)==2){
							$num = "00".$num;
						}elseif(strlen($num)==3){
							$num = "0".$num;
						}
					}
					$item['no_faktur']="F-".date("Y").$num;
				};

				$item['diskon'] = DB::table('tb_spk_diskon')
					->where("spkd_status",1)
					->where("spkd_spk",$id)->first();

				$item['aksesoris'] = array();
				if ($item['diskon']){
				$item['aksesoris'] = DB::table('tb_spk_aksesoris')
					->where("spka_diskon",$item['diskon']->spkd_id)->get();
				}

				$item['d_leasing'] = DB::table('tb_leasing')
					->where("leasing_status",1)->get();

				$item['d_asuransi'] = DB::table('tb_asuransi_jenis')->get();

				$item['leasing'] = DB::table('tb_spk_leasing')
					->leftjoin('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk_leasing.spkl_leasing')
					->where("spkl_spk",$id)->first();

				array_push($data, $item);
			}
		} else {
			$data = DB::table('tb_spk')
				->where("spk_id", $id)
				->first();

			$source = $data->spk_pel_lahir;
			$date = DateTime::createFromFormat('Y-m-d', $source);
			$data->spk_pel_lahir = $date->format('d/m/Y');
		}

		return json_encode($data);
	}

	public function detail_penjualan($id){
		$data['bayar'] = DB::table('tb_spk_pembayaran')
			->where("spkp_spk",$id)->get();

        for ($i = 0; $i < count($data['bayar']); $i++) {
            unset($data['bayar'][$i]->created_at);
            unset($data['bayar'][$i]->updated_at);
        }

		$data['diskon'] = DB::table('tb_spk_diskon')
			->where("spkd_status",1)
			->where("spkd_spk",$id)->first();

		$data['aksesoris'] = array();
		if ($data['diskon']){
		$data['aksesoris'] = DB::table('tb_spk_aksesoris')
			->where("spka_diskon",$data['diskon']->spkd_id)->get();
		}

		return json_encode($data);
	}

	function detail($id){
		$data['pemesan'] = DB::table('tb_spk')
			->leftjoin('tb_spk_ttbj', 'tb_spk_ttbj.spkt_spk', '=', 'tb_spk.spk_id')
			->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
			->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
			->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
			->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
			->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
			->leftjoin('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk.spk_leasing')
			->leftjoin("tb_warna", 'warna_id', '=', 'tb_spk.spk_warna')
			->where("spk_id",$id)
			->first();

		$data['kendaraan'] = DB::table('tb_spk')
			->join("tb_tr_kendaraan","tb_tr_kendaraan.trk_dh","=","tb_spk.spk_dh")
			->join('tb_warna', 'tb_warna.warna_id', '=', 'tb_tr_kendaraan.trk_warna')
			->where("spk_status","<",10)
			->where("spk_id",$id)->first();

		$data['bayar'] = DB::table('tb_spk_pembayaran')
			->where("spkp_spk",$id)->get();


		$data['faktur'] = DB::table('tb_spk_faktur')
			->where("spkf_spk",$id)->first();

		$faktur = DB::table('tb_spk_faktur')->orderBy("spkf_id","DESC")->first();

		$data['no_faktur'] = "F-".date("Y")."0001";
		if($faktur){
			$tmp = $faktur->spkf_id;
			$tahun = substr($tmp,2,4);
			$num = substr($tmp,6,strlen($tmp));
			if(date("Y") == $tahun){
				$num++;
				if(strlen($num)==1){
					$num = "000".$num;
				}elseif(strlen($num)==2){
					$num = "00".$num;
				}elseif(strlen($num)==3){
					$num = "0".$num;
				}
			}
			$data['no_faktur']="F-".date("Y").$num;
		};

		$data['diskon'] = DB::table('tb_spk_diskon')
			->where("spkd_status",1)
			->where("spkd_spk",$id)->first();

		$data['aksesoris'] = array();
		if ($data['diskon']){
		$data['aksesoris'] = DB::table('tb_spk_aksesoris')
			->where("spka_diskon",$data['diskon']->spkd_id)->get();
		}

		$data['d_leasing'] = DB::table('tb_leasing')
			->where("leasing_status",1)->get();

		$data['d_asuransi'] = DB::table('tb_asuransi_jenis')->get();

		$data['leasing'] = DB::table('tb_spk_leasing')
			->leftjoin('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk_leasing.spkl_leasing')
			->where("spkl_spk",$id)->first();

		return json_encode($data);
	}

	function permintaan_spk(){
		$data = DB::table('tb_spk')
			->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
			->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
			->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
			->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
			->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
			->leftjoin('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk.spk_leasing')
			->leftjoin("tb_warna", 'warna_id', '=', 'tb_spk.spk_warna')
			->where("spk_status","90")
			->orwhere("spk_status","95")
			->get();
			

		$result = array();
		foreach($data as $r){
			$item = array();
			$item['spk_id'] = $r->spk_id;
			$item['spk_tgl'] = date_format(date_create($r->spk_tgl),"d/m/Y");
			$item['spk_pel_nama'] = $r->spk_pel_nama;

			$item['spk_pel_kategori'] = $r->spk_pel_kategori;
			$item['spk_pel_identitas'] = $r->spk_pel_identitas;
			$item['spk_pel_alamat'] = $r->spk_pel_alamat;
			$item['spk_pel_pos'] = $r->spk_pel_pos;
			$item['spk_pel_telp'] = $r->spk_pel_telp;
			$item['spk_pel_ponsel'] = $r->spk_pel_ponsel;
			$item['spk_pel_email'] = $r->spk_pel_email;
			$item['spk_pel_kota'] = $r->spk_pel_kota;
			$item['spk_stnk_nama'] = $r->spk_stnk_nama;
			$item['spk_stnk_identitas'] = $r->spk_stnk_identitas;
			$item['spk_stnk_alamat'] = $r->spk_stnk_alamat;
			$item['spk_stnk_pos'] = $r->spk_stnk_pos;
			$item['spk_stnk_alamatd'] = $r->spk_stnk_alamatd;
			$item['spk_stnk_posd'] = $r->spk_stnk_posd;
			$item['spk_stnk_telp'] = $r->spk_stnk_telp;
			$item['spk_stnk_ponsel'] = $r->spk_stnk_ponsel;
			$item['spk_stnk_email'] = $r->spk_stnk_email;
			$item['spk_pembayaran_raw'] = $r->spk_pembayaran;
			$item['spk_ket_permintaan'] = $r->spk_ket;
			$item['spk_pel_fotoid'] = $r->spk_pel_fotoid;
			$item['spk_stnk_fotoid'] = $r->spk_stnk_fotoid;
			$item['spk_sales_id'] = $r->karyawan_id;
			$item['spk_fleet'] = $r->spk_fleet;
			$item['spk_dp'] =number_format($r->spk_dp,0,',','.');
			$item['spk_kendaraan_harga'] = $r->spk_ket_harga+1;
			$item['sales_uid'] = $r->sales_salesUid;
			$item['spk_type'] = $r->type_nama;
			$item['spk_variant'] = $r->type_nama." ".$r->variant_nama;
			$item['spk_warna'] = $r->warna_nama;
			$item['spk_sales'] = $r->karyawan_nama;
			$item['spk_team'] = $r->team_nama;
			$item['spk_pembayaran'] = number_format(DB::table('tb_spk_pembayaran')->select(DB::raw("SUM(spkp_jumlah) as bayar"))
			->where("spkp_spk",$r->spk_id)->first()->bayar,0,",",".");
			$item['spk_metode'] =$r->spk_pembayaran+1;
			$item['leasing_nama'] = $r->leasing_nama;
			$item['spk_status'] = $r->spk_status;
			$item['spk_ket'] = "-";

			if((!request("spk_id") || strrpos(strtolower($item['spk_id']), strtolower(request("spk_id"))) > -1) &&
				(!request("spk_tgl") || strrpos(strtolower($item['spk_tgl']), strtolower(request("spk_tgl"))) > -1) &&
				(!request("spk_pel_nama") || strrpos(strtolower($item['spk_pel_nama']), strtolower(request("spk_pel_nama"))) > -1) &&
				(!request("spk_dp") || strrpos(strtolower($item['spk_dp']), strtolower(request("spk_dp"))) > -1) &&
				(!request("spk_sales") || strrpos(strtolower($item['spk_sales']), strtolower(request("spk_sales"))) > -1) &&
				(!request("spk_team") || strrpos(strtolower($item['spk_team']), strtolower(request("spk_team"))) > -1) &&
				(!request("spk_variant") || strrpos(strtolower($item['spk_variant']), strtolower(request("spk_variant"))) > -1) &&
				 (!request("spk_warna") || strrpos(strtolower($item['spk_warna']), strtolower(request("spk_warna"))) > -1) &&
				 (!request("spk_kendaraan_harga") || strtolower($item['spk_kendaraan_harga'])== strtolower(request("spk_kendaraan_harga"))) &&
				 (!request("spk_metode") || strtolower($item['spk_metode'])== strtolower(request("spk_metode")))&&
				 (!request("spk_status") || strtolower($item['spk_status'])== strtolower(request("spk_status"))))	{

				$tgl = strtotime(str_replace("/","-",$item['spk_tgl']));
				if (request("filter_awal") && request("filter_akhir")){
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
					if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_awal")){
					$filter_awal = strtotime(request("filter_awal"));
					if ($filter_awal<=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_akhir")){
					$filter_akhir = strtotime(request("filter_akhir"));
					if ($filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else{
					array_push($result, $item);
				}
			}
		}

		return json_encode($result);
	}

	function cancel(){
		$data = DB::table('tb_spk')
			->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
			->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
			->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
			->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
			->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
			->leftjoin('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk.spk_leasing')
			->leftjoin("tb_warna", 'warna_id', '=', 'tb_spk.spk_warna')
			->whereNull("spk_do")
			->where("spk_status","=",10)
			->orwhere("spk_status","=",11)
			->get();

			$result = array();
			foreach($data as $r){
			$item = array();
			$item['spk_id'] = $r->spk_id;
			$item['spk_tgl'] = date_format(date_create($r->spk_tgl),"d/m/Y");
			$item['spk_pel_nama'] = $r->spk_pel_nama;

			$item['spk_pel_kategori'] = $r->spk_pel_kategori;
			$item['spk_pel_identitas'] = $r->spk_pel_identitas;
			$item['spk_pel_alamat'] = $r->spk_pel_alamat;
			$item['spk_pel_pos'] = $r->spk_pel_pos;
			$item['spk_pel_telp'] = $r->spk_pel_telp;
			$item['spk_pel_ponsel'] = $r->spk_pel_ponsel;
			$item['spk_pel_email'] = $r->spk_pel_email;
			$item['spk_pel_kota'] = $r->spk_pel_kota;
			$item['spk_stnk_nama'] = $r->spk_stnk_nama;
			$item['spk_stnk_identitas'] = $r->spk_stnk_identitas;
			$item['spk_stnk_alamat'] = $r->spk_stnk_alamat;
			$item['spk_stnk_pos'] = $r->spk_stnk_pos;
			$item['spk_stnk_alamatd'] = $r->spk_stnk_alamatd;
			$item['spk_stnk_posd'] = $r->spk_stnk_posd;
			$item['spk_stnk_telp'] = $r->spk_stnk_telp;
			$item['spk_stnk_ponsel'] = $r->spk_stnk_ponsel;
			$item['spk_stnk_email'] = $r->spk_stnk_email;
			$item['spk_pembayaran_raw'] = $r->spk_pembayaran;
			$item['spk_sales_id'] = $r->karyawan_id;
			$item['spk_fleet'] = $r->spk_fleet;
			$item['spk_ket_cancel'] = $r->spk_ket_cancel;
			$item['spk_type'] = $r->type_nama;
			$item['spk_variant'] = $r->type_nama." ".$r->variant_nama;
			$item['spk_warna'] = $r->warna_nama;
			$item['spk_sales'] = $r->karyawan_nama;

			$item['spk_pel_fotoid'] = $r->spk_pel_fotoid;
			$item['spk_stnk_fotoid'] = $r->spk_stnk_fotoid;
			$item['spk_kendaraan_harga'] = $r->spk_ket_harga+1;
			$item['spk_team'] = $r->team_nama;
			$item['spk_pembayaran'] = number_format(DB::table('tb_spk_pembayaran')->select(DB::raw("SUM(spkp_jumlah) as bayar"))
			->where("spkp_spk",$r->spk_id)->first()->bayar,0,",",".");
			// $item['spk_kota'] = $r->pel_kota;
			$item['spk_metode'] =$r->spk_pembayaran+1;
			if ($r->spk_pembayaran == 0){
				$item['spk_via'] = "CASH";	  
			}else{
				$item['spk_via'] = $r->leasing_nick;			
			}
			$item['spk_status'] = $r->spk_status;
			$item['spk_ket'] = "-";
			if((!request("spk_id") || strrpos(strtolower($item['spk_id']), strtolower(request("spk_id"))) > -1) &&
				(!request("spk_tgl") || strrpos(strtolower($item['spk_tgl']), strtolower(request("spk_tgl"))) > -1) &&
				(!request("spk_pel_nama") || strrpos(strtolower($item['spk_pel_nama']), strtolower(request("spk_pel_nama"))) > -1) &&
				(!request("spk_dp") || strrpos(strtolower($item['spk_dp']), strtolower(request("spk_dp"))) > -1) &&
				(!request("spk_sales") || strrpos(strtolower($item['spk_sales']), strtolower(request("spk_sales"))) > -1) &&
				(!request("spk_team") || strrpos(strtolower($item['spk_team']), strtolower(request("spk_team"))) > -1) &&
				(!request("spk_variant") || strrpos(strtolower($item['spk_variant']), strtolower(request("spk_variant"))) > -1) &&
				 (!request("spk_warna") || strrpos(strtolower($item['spk_warna']), strtolower(request("spk_warna"))) > -1) &&
				 (!request("spk_kendaraan_harga") || strtolower($item['spk_kendaraan_harga'])== strtolower(request("spk_kendaraan_harga"))) &&
				 (!request("spk_metode") || strtolower($item['spk_metode'])== strtolower(request("spk_metode")))&&
				 (!request("spk_status") || strtolower($item['spk_status'])== strtolower(request("spk_status"))))	{

				$tgl = strtotime(str_replace("/","-",$item['spk_tgl']));
				if (request("filter_awal") && request("filter_akhir")){
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
					if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_awal")){
					$filter_awal = strtotime(request("filter_awal"));
					if ($filter_awal<=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_akhir")){
					$filter_akhir = strtotime(request("filter_akhir"));
					if ($filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else{
					array_push($result, $item);
				}
			}
		}


		return json_encode($result);
	}



	  function permintaan_do(){
		$data = DB::table('tb_spk')
			->join('tb_spk_faktur', 'tb_spk_faktur.spkf_spk', '=', 'tb_spk.spk_id')
			->leftjoin('tb_spk_leasing', 'tb_spk_leasing.spkl_spk', '=', 'tb_spk.spk_id')
			->leftjoin('tb_spk_diskon', 'tb_spk_diskon.spkd_spk', '=', 'tb_spk.spk_id')
			->join('tb_sales', 'tb_spk.spk_sales', '=', 'tb_sales.sales_id')
			->join('tb_karyawan', 'tb_sales.sales_karyawan', '=', 'tb_karyawan.karyawan_id')
			->join('tb_team', 'tb_sales.sales_team', '=', 'tb_team.team_id')
			->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id')
			->join('tb_type', 'tb_type.type_id', '=', 'tb_variant.variant_type')
			->leftjoin('tb_leasing', 'tb_leasing.leasing_id', '=', 'tb_spk.spk_leasing')
			->leftjoin("tb_warna", 'warna_id', '=', 'tb_spk.spk_warna')
			->whereNull("spk_do")
			->where("spk_status","<",3)
			->where("spkf_cetak","=",0)
			->get();
			$result = array();
			foreach($data as $r){
			$item = array();
			$item['sales_uid'] = $r->sales_salesUid;
			$item['spk_id'] = $r->spk_id;
			$item['spk_tgl'] = date_format(date_create($r->spk_tgl),"d/m/Y");
			$item['spk_pel_nama'] = $r->spk_pel_nama;
			$item['spk_type'] = $r->type_nama;
			$item['spk_variant'] = $r->type_nama." ".$r->variant_nama;
			$item['spk_warna'] = $r->warna_nama;
			$item['spk_sales'] = $r->karyawan_nama;
			$item['spk_dp'] = $r->spkl_dp;
			$item['spk_droping'] = $r->spkl_droping;
			$item['spk_potongan'] = number_format( $r->spkd_cashback);
			$item['spk_team'] = $r->team_nama;
			$item['spk_pembayaran'] = number_format(DB::table('tb_spk_pembayaran')->select(DB::raw("SUM(spkp_jumlah) as bayar"))
			->where("spkp_spk",$r->spk_id)->first()->bayar,0,",",".");
			$item['spk_pel_kota'] = $r->spk_pel_kota;
			$item['spk_metode'] =0;
			if ($r->spk_pembayaran == 0){
				$item['spk_via'] = "CASH";	  
			}else{
				$item['spk_via'] = $r->leasing_nick;			
			}
			$item['spk_status'] = $r->spk_status;
			$item['spk_kendaraan_harga'] = $r->spk_ket_harga;
			$item['spk_pel_kota'] = $r->spk_pel_kota;

			if ($r->spk_ket_harga == 1){
				$item['spk_harga'] = number_format( $r->variant_on);	 
			}else{
				$item['spk_harga'] = number_format($r->variant_on - $r->variant_bbn) ;			
			}

			 if ($r->spk_ket_harga == 1){
				$item['spk_subtotal'] = number_format( $r->variant_on -$r->spkd_cashback) ;	 
			}else{
				$item['spk_subtotal'] = number_format($r->variant_on - $r->variant_bbn -  $r->spkd_cashback) ;			
			} 

			if ($r->spk_ket_harga == 1){
				$item['spk_piutang'] = number_format ( $r->spkl_droping - ($r->variant_on  - $r->variant_bbn -$r->spkd_cashback - (DB::table('tb_spk_pembayaran')-> select(DB::raw("SUM(spkp_jumlah) as bayar"))->where("spkp_spk",$r->spk_id)->first()->bayar)),0,",",".") ;  
			}else{
				$item['spk_piutang'] = number_format($r->spkl_droping - ($r->variant_on  - $r->spkd_cashback - ( DB::table('tb_spk_pembayaran')->select(DB::raw("SUM(spkp_jumlah) as bayar")) ->where("spkp_spk",$r->spk_id)->first()->bayar)),0,",",".");		 
			}

			$item['spk_dp_po'] = number_format($r->spkl_dp);
			$item['spk_droping_po'] = number_format($r->spkl_droping) ;

			$item['spk_ket'] = "-";
			if((!request("spk_id") || strrpos(strtolower($item['spk_id']), strtolower(request("spk_id"))) > -1) &&
				(!request("spk_tgl") || strrpos(strtolower($item['spk_tgl']), strtolower(request("spk_tgl"))) > -1) &&
				(!request("spk_pel_nama") || strrpos(strtolower($item['spk_pel_nama']), strtolower(request("spk_pel_nama"))) > -1) &&
				(!request("spk_dp") || strrpos(strtolower($item['spk_dp']), strtolower(request("spk_dp"))) > -1) &&
				(!request("spk_sales") || strrpos(strtolower($item['spk_sales']), strtolower(request("spk_sales"))) > -1) &&
				(!request("spk_team") || strrpos(strtolower($item['spk_team']), strtolower(request("spk_team"))) > -1) &&
				(!request("spk_variant") || strrpos(strtolower($item['spk_variant']), strtolower(request("spk_variant"))) > -1) &&
				 (!request("spk_warna") || strrpos(strtolower($item['spk_warna']), strtolower(request("spk_warna"))) > -1) &&
				 (!request("spk_kendaraan_harga") || strtolower($item['spk_kendaraan_harga'])== strtolower(request("spk_kendaraan_harga"))) &&
				 (!request("spk_metode") || strtolower($item['spk_metode'])== strtolower(request("spk_metode")))&&
				 (!request("spk_status") || strtolower($item['spk_status'])== strtolower(request("spk_status"))))	{

				$tgl = strtotime(str_replace("/","-",$item['spk_tgl']));
				if (request("filter_awal") && request("filter_akhir")){
					$filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
					$filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
					if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_awal")){
					$filter_awal = strtotime(request("filter_awal"));
					if ($filter_awal<=$tgl){
						array_push($result, $item);						
					}
				}else if (request("filter_akhir")){
					$filter_akhir = strtotime(request("filter_akhir"));
					if ($filter_akhir>=$tgl){
						array_push($result, $item);						
					}
				}else{
					array_push($result, $item);
				}
			}
		}


		return json_encode($result);
	}

//-------------------------------//
// Proses Konfirmasi SPK
//-------------------------------//

	function acc_permintaan_spk() {
		$validator = Validator::make(request()->all(), [
			"spk_id"	=> "required"
		]);

		$proses['result'] = false;
		$proses['msg'] = "";

		if ($validator->fails()) {
			$proses['msg'] = $validator->messages();
		} else {
			$tgl =  date("Y-m-d H:i:s");
			$data['old'] = DB::table('tb_spk')->where("spk_id", request("spk_id"))->first();

			$proses['result'] = DB::table('tb_spk')->where("spk_id", request("spk_id"))->update([
				"spk_status"			=> "9",
				"spk_automatching"		 =>  request("spk_automatching"),
				"updated_at"			=> $tgl,
			]);

			$data['new'] = DB::table('tb_spk')->where("spk_id", request("spk_id"))->first();
			

			$proses['result'] = true;
			$proses['tgl'] = $tgl;
		}

		return json_encode($proses);
	}

	function tanggapi_permintaan_spk() {
		$validator = Validator::make(request()->all(), [
			"spk_id"	=> "required"
		]);

		$proses['result'] = false;
		$proses['msg'] = "";

		if ($validator->fails()) {
			$proses['msg'] = $validator->messages();
		} else {
			$tgl =  date("Y-m-d H:i:s");
			$data['old'] = DB::table('tb_spk')->where("spk_id", request("spk_id"))->first();

			$proses['result'] = DB::table('tb_spk')->where("spk_id", request("spk_id"))->update([
				"spk_status"			=> "95",
				"updated_at"			=> $tgl,
			]);

			$data['new'] = DB::table('tb_spk')->where("spk_id", request("spk_id"))->first();

			

			$proses['result'] = true;
			$proses['tgl'] = $tgl;
		}

		return json_encode($proses);
	}

//-------------------------------//
// Proses Konfirmasi DO
//-------------------------------//

	function pengajuan_do(){
		$validator = Validator::make(request()->all(), [
			"spk_id"		=> "required",
			"spkf_id"	   => "required",
		]);

		$proses['result'] =false;
		$proses['msg'] = "";
		if ($validator->fails())
		{
			$proses['msg'] =  $validator->messages();

		}else{
			if(DB::table('tb_spk_faktur')-> where("spkf_id",request("spkf_id"))->first()){
				$proses['result'] = DB::table('tb_spk_faktur')-> where("spkf_id",request("spkf_id"))->update([
					"spkf_tgl"	  =>  date("Y-m-d"),
					"spkf_cetak"	=> 0,
					"updated_at"	=> date("Y-m-d H:i:s"),
				]);

				$proses['result'] = 1;
			}else{
				$proses['result'] = DB::table('tb_spk_faktur')-> insert([
					"spkf_id"	   =>  request("spkf_id"),
					"spkf_spk"	  =>  request("spk_id"),
					"spkf_tgl"	  =>  date("Y-m-d"),
					"spkf_cetak"	=>  0,
					"created_at"	=> date("Y-m-d H:i:s"),
					"updated_at"	=> date("Y-m-d H:i:s"),
				]);

				$proses['result'] = 1;
			}

			$proses['result'] = DB::table('tb_spk')-> where("spk_id",request("spk_id"))->update([
				"spk_ket_permintaanDo"   =>  "ok",
				"updated_at"	=> date("Y-m-d H:i:s"),
			]);

			$proses['result'] = 1;
		}
		
		return json_encode($proses);
	}


	function acc_pengajuan_do(){
		$validator = Validator::make(request()->all(), [
			"spk_id"		=> "required",
			"spkf_spk"	   => "required",
		]);

		$proses['result'] = false;
		$proses['msg'] = "";

		if ($validator->fails()) {
			$proses['msg'] =  $validator->messages();
		} else {
			$proses['tgl'] = date("Y-m-d H:i:s");
			$proses['result'] = DB::table('tb_spk_faktur')-> where("spkf_spk", request("spkf_spk"))->update([
				"spkf_cetak"	=> 1,
			]);
			
			// $data['old'] = DB::table('tb_spk')->where("spk_id", request("spk_id"))->first();

			$proses['result'] = DB::table('tb_spk')->where("spk_id",request("spk_id"))->update([
				"spk_do"		=> date("Y-m-d"),
				"spk_status"	=> 3,
				"spk_catt_permintaanDo"   =>  request("spk_catt_permintaanDo"),
				"updated_at"	=> date("Y-m-d H:i:s"),
			]);

			// $data['new'] = DB::table('tb_spk')->where("spk_id", request("spk_id"))->first();

			// foreach ($data['new'] as $key => $value) {
			// 	if ($data['old']->$key != $data['new']->$key) {
			// 		$proses['items'][$key] = $value;
			// 	}
			// }

			$proses['result'] = 1;
		}
		
		return json_encode($proses);
	}


	function tolak_pengajuan_do() {
		$validator = Validator::make(request()->all(), [
			"spk_id"	=> "required",
			"spkf_spk"   => "required",
		]);

		$proses['result'] = false;
		$proses['msg'] = "";

		if ($validator->fails()) {
			$proses['msg'] = $validator->messages();
		} else {
			// $data['old'] = DB::table('tb_spk')->where("spk_id", request("spk_id"))->first();

			$proses['tgl'] = date("Y-m-d H:i:s");
			$proses['result'] = DB::table('tb_spk')-> where("spk_id",request("spk_id"))->update([
				"spk_cat_cancel"   =>  request("spk_cat_cancel"),
				"updated_at"	=> date("Y-m-d H:i:s"),
			]);

			// $data['new'] = DB::table('tb_spk')->where("spk_id", request("spk_id"))->first();

			// foreach ($data['new'] as $key => $value) {
			// 	if ($data['old']->$key != $data['new']->$key) {
			// 		$proses['items'][$key] = $value;
			// 	}
			// }

			$proses['result'] = DB::table('tb_spk_faktur')-> where("spkf_spk", request("spkf_spk"))->delete();
		}

		return json_encode($proses);
	}

	function leasing_save(){
		$validator = Validator::make(request()->all(), [
			"spkl_spk"	  => "required",
			"spkl_dp"	   => "required",
			"spkl_leasing"  => "required",
			"spkl_asuransi" => "required",
			"spkl_droping"  => "required",
		]);

		$proses['result'] =false;
		$proses['msg'] = "";
		if ($validator->fails())
		{
			$proses['msg'] =  $validator->messages();

		}else{
			if( DB::table('tb_spk_leasing')-> where("spkl_spk",request("spkl_spk"))->first()){
				$proses['result'] = DB::table('tb_spk_leasing')
					->where("spkl_spk",request("spkl_spk"))
					->update([
						"spkl_leasing"		  =>  request("spkl_leasing"),
						"spkl_jenis_asuransi"   =>  request("spkl_asuransi"),
						"spkl_dp"			   =>  request("spkl_dp"),
						"spkl_droping"		  =>  request("spkl_droping"),
						"spkl_waktu"			=>  request("spkl_waktu"),
						"spkl_angsuran"		 =>  request("spkl_angsuran"),
						"spkl_doc"			  =>  request("spkl_doc"),
					]);

					$proses['result'] = 1;
			}else{
				$proses['result'] = DB::table('tb_spk_leasing')->insert([
					"spkl_spk"			  =>  request("spkl_spk"),
					"spkl_leasing"		  =>  request("spkl_leasing"),
					"spkl_jenis_asuransi"   =>  request("spkl_asuransi"),
					"spkl_dp"			   =>  request("spkl_dp"),
					"spkl_droping"		  =>  request("spkl_droping"),
					"spkl_waktu"			=>  request("spkl_waktu"),
					"spkl_angsuran"		 =>  request("spkl_angsuran"),
					"spkl_doc"			  =>  request("spkl_doc"),
				]);

				$proses['result'] = 1;
			}
		}
		
		return json_encode($proses);
	}

//-------------------------------//
// Proses Update Variant Kendaraan
//-------------------------------//

	function variant_update(){
		$validator = Validator::make(request()->all(), [
			"variant_id"		=> "required",
			"variant_off"	   => "required"
		]);

		$proses['result'] = false;
		$proses['msg'] = "";
		if ($validator->fails())
		{
			$proses['msg'] =  $validator->messages();

		} else {
			$query = DB::table("tb_variant")
				->where("variant_serial", request("variant_id"))
				->update([
					"variant_off"   => request("variant_off"),
					"variant_bbn"	=> request("variant_bbn")
			]);

			$kendaraan = DB::table("tb_variant")
				->where("variant_serial", request("variant_id"))
				->first();

			$query = DB::table("tb_spk")
				->where("spk_kendaraan", $kendaraan->variant_id)
				->update([
					"spk_bbn"		=> request("variant_bbn")
			]);
			
			$proses['result'] = 1;
		}
		
		return json_encode($proses);
	}

//-------------------------------//
// Proses CETAK SPK
//-------------------------------//
	 function cetak_faktur(){
		$validator = Validator::make(request()->all(), [
			"spkf_id"	   => "required",
			"spkf_spk"	  => "required"
		]);

		$proses['result'] =false;
		$proses['msg'] = "";
		if ($validator->fails())
		{
			$proses['msg'] =  $validator->messages();

		}else{
			if(DB::table('tb_spk_faktur')-> where("spkf_id",request("spkf_id"))->first()){
				$proses['result'] = DB::table('tb_spk_faktur')->where("spkf_id",request("spkf_id"))->update([
					"spkf_tgl"		=>  date("Y-m-d"),
					"spkf_cetak"	=> NULL,
					"updated_at"	=> date('Y-m-d H:i:s'),
				]);

				$proses['result'] = 1;
			}else{
				$proses['result'] = DB::table('tb_spk_faktur')->insert([
					"spkf_id"	 =>  request("spkf_id"),
					"spkf_spk"	=>  request("spkf_spk"),
					"spkf_tgl"	=>  date("Y-m-d"),
					"created_at"	=> date('Y-m-d H:i:s'),
					"updated_at"	=> date('Y-m-d H:i:s'),
				]);

				$proses['result'] = 1;
			}
		}
		
		return json_encode($proses);
	}

	function terbitkan(){
		$validator = Validator::make(request()->all(), [
			"spk_id"		=> "required",
			"spkf_id"	   => "required",
		]);

		$proses['result'] = false;
		$proses['msg'] = "";

		if ($validator->fails()) {
			$proses['msg'] =  $validator->messages();

		} else {
			$spkf_cetak = DB::table('tb_spk_faktur')-> where("spkf_spk",request("spk_id"))->first();
			if (count($spkf_cetak) > 0) {
				$proses['result'] = DB::table('tb_spk_faktur')-> where("spkf_spk",request("spk_id"))->update([
					"spkf_tgl"	=>  date('Y-m-d'),
					"spkf_cetak"	=> 2,
					"updated_at"	=> date('Y-m-d H:i:s'),
				]);

				$proses['result'] = 1;  
			} else {
				$proses['result'] = DB::table('tb_spk_faktur')->insert([
					"spkf_id"	 =>  request("spkf_id"),
					"spkf_spk"	=>  request("spk_id"),
					"spkf_tgl"	=>  date("Y-m-d"),
					"spkf_cetak"	=> 2,
					"created_at"	=> date('Y-m-d H:i:s'),
					"updated_at"	=> date('Y-m-d H:i:s'),
				]); 

				$proses['result'] = 1;	   
			}
			
			// SPK TTBJ
			$check = DB::table('tb_spk_ttbj')->where('spkt_spk', request("spk_id"))->first();
			if (count($check) > 0) {
				$proses['result'] = DB::table('tb_spk_ttbj')
					->where('spkt_spk', request("spk_id"))
					->update([
						"spkt_spk"		=> request("spk_id"),
						"created_at"	=> date("Y-m-d H:i:s"),
						"updated_at"	=> date("Y-m-d H:i:s")
					]);
			} else {
				$proses['result'] = DB::table('tb_spk_ttbj')->insert([
					"spkt_spk"		=> request("spk_id"),
					"created_at"	=> date("Y-m-d H:i:s"),
					"updated_at"	=> date("Y-m-d H:i:s")
				]);
			}

			$proses['result'] = DB::table('tb_spk')-> where("spk_id",request("spk_id"))->update([
				"spk_do"		=> date("Y-m-d"),
				"spk_status"	=> 3,
				"updated_at"	=> date('Y-m-d H:i:s')
			]);

			$proses['result'] = 1;
		}
		
		return json_encode($proses);
	}

	
	 function cancel_save() {
		$validator = Validator::make(request()->all(), [
			"spk_id"	=> "required"
		]);

		$proses['result'] = false;
		$proses['msg'] = "";

		if ($validator->fails()) {
			$proses['msg'] = $validator->messages();
		} else {
			$proses['result'] = DB::table('tb_spk')-> where("spk_id", request("spk_id"))->update([
				"spk_status"	=> "10"
			]);
		}

		return json_encode($proses);
	}

	function tanggapi_save() {
		$validator = Validator::make(request()->all(), [
			"spk_id"	=> "required"
		]);

		$proses['result'] = false;
		$proses['msg'] = "";

		if ($validator->fails()) {
			$proses['msg'] = $validator->messages();
		} else {
			$proses['result'] = DB::table('tb_spk')-> where("spk_id", request("spk_id"))->update([
				"spk_status"	=> "11"
			]);
		}

		return json_encode($proses);
	}
}
