<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ApiTTBJController extends Controller
{
    function index(){
    	$data = DB::table('tb_spk')
    		->leftjoin('tb_spk_ttbj',function($join){
    			$join->on('tb_spk_ttbj.spkt_spk', '=', 'tb_spk.spk_id');
    		})
    		->leftjoin('tb_biro', 'tb_spk_ttbj.spkt_biro', '=', 'tb_biro.biro_id')
    		->join('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id') 		
            ->join('tb_type', 'tb_variant.variant_type', '=', 'tb_type.type_id')  
    		->join("tb_sales","tb_sales.sales_id","=","tb_spk.spk_sales")
    		->join("tb_karyawan","tb_karyawan.karyawan_id","=","tb_sales.sales_karyawan")
    		->where("spk_do","!=",null)
    		->where("spkt_status",null)
    		->orWhere("spkt_status",0)
    		->get();

    	$result = array();
    	foreach($data as $d){
    		$item = array();
    		$item['spkt_id'] = $d->spkt_id;
    		$item['spk_id'] = $d->spk_id;
    		$item['spk_tgl'] = date_format(date_create($d->spk_tgl),"d/m/Y");
    		$item['spk_pel_nama'] = $d->spk_pel_nama;
            $item['spk_stnk_nama'] = $d->spk_stnk_nama;
    		$item['variant_nama'] =$d->type_nama." ".$d->variant_nama;
    		$item['spkt_stck'] = $d->spkt_stck;
    		$item['biro_id'] = $d->biro_id;
    		$item['biro_nama'] = $d->biro_nama;
    		$item['spkt_tglfaktur'] =  date_format(date_create($d->spkt_tglfaktur),"d/m/Y");
    		if (is_null($d->spkt_tglfaktur)){
    			$item['spkt_tglfaktur']="";
    		}
    		$item['spkt_ujitype'] = date_format(date_create($d->spkt_ujitype),"d/m/Y");

    		if (is_null($d->spkt_ujitype)){
    			$item['spkt_ujitype']="";
    		}
    		$item['spkt_nopol'] = ucwords($d->spkt_nopol);
    		$item['spkt_plat'] = $d->spkt_plat;
            $item['spkt_status'] = $d->spkt_status;
    		$item['spkt_notis'] = $d->spkt_notis;
    		$item['spkt_stnk'] = $d->spkt_stnk;
    		$item['spkt_waktu_stnk'] = "0";
    		$item['waktu_stnk'] = $d->spkt_waktu_stnk;
    		$item['spkt_status_stnk'] = "-";
    		if (!is_null( $d->spkt_tglfaktur)){
    			$item['spkt_waktu_stnk'] = floor((time() - strtotime($d->spkt_tglfaktur))/ (60*60*24));

    			if (!is_null($d->spkt_waktu_stnk)){
    				$item['spkt_waktu_stnk'] = $d->spkt_waktu_stnk;
    			}

    			if ($item['spkt_waktu_stnk'] <=31){
    				$item['spkt_status_stnk'] = "IN PROSES";					
				}else{
					$item['spkt_status_stnk'] = "OVD";
				}

				if ($d->spkt_stnk==1){
    				$item['spkt_status_stnk'] = "OKE";
    			}
    		}
    		$item['spkt_nobpkb'] = $d->spkt_nobpkb;
    		$item['spkt_waktu_bpkb'] = "0";
    		$item['waktu_bpkb'] = $d->spkt_waktu_bpkb;
    		$item['spkt_status_bpkb'] = "-";
    		if (!is_null( $d->spkt_tglfaktur) && $d->spkt_plat==1 && $d->spkt_notis==1 && $d->spkt_stnk==1){
    			$item['spkt_waktu_bpkb'] = floor((time() - strtotime($d->spkt_tglfaktur))/ (60*60*24));

    			if (!is_null($d->spkt_waktu_bpkb)){
    				$item['spkt_waktu_bpkb'] = $d->spkt_waktu_bpkb;
    			}
    			
    			if ($item['spkt_waktu_bpkb'] <=31){
    				$item['spkt_status_bpkb'] = "IN PROSES";					
				}else{
					$item['spkt_status_bpkb'] = "OVD";
				}
				
				if (!is_null($d->spkt_nobpkb)){
    				$item['spkt_status_bpkb'] = "OKE";
    			}
    		          }if((!request("spk_id") || strrpos(strtolower($item['spk_id']), strtolower(request("spk_id"))) > -1) &&
                (!request("spk_tgl") || strrpos(strtolower($item['spk_tgl']), strtolower(request("spk_tgl"))) > -1) &&
                (!request("spk_pel_nama") || strrpos(strtolower($item['spk_pel_nama']), strtolower(request("spk_pel_nama"))) > -1) &&
                (!request("spk_stnk_nama") || strrpos(strtolower($item['spk_stnk_nama']), strtolower(request("spk_stnk_nama"))) > -1) &&
                (!request("spkt_stck") || strrpos(strtolower($item['spkt_stck']), strtolower(request("spkt_stck"))) > -1) &&
                (!request("biro_nama") || strrpos(strtolower($item['biro_nama']), strtolower(request("biro_nama"))) > -1) &&
                (!request("variant_nama") || strrpos(strtolower($item['variant_nama']), strtolower(request("variant_nama"))) > -1) &&
                (!request("variant_nama") || strrpos(strtolower($item['variant_nama']), strtolower(request("variant_nama"))) > -1) &&
                 (!request("spkt_tglfaktur") || strrpos(strtolower($item['spkt_tglfaktur']), strtolower(request("spkt_tglfaktur"))) > -1) &&
                 (!request("spkt_waktu_stnk") || strrpos(strtolower($item['spkt_waktu_stnk']), strtolower(request("spkt_waktu_stnk"))) > -1) &&
                 (!request("spkt_status_stnk") || strrpos(strtolower($item['spkt_status_stnk']), strtolower(request("spkt_status_stnk"))) > -1) &&
                 (!request("spkt_status_bpkb") || strrpos(strtolower($item['spkt_status_bpkb']), strtolower(request("spkt_status_bpkb"))) > -1) &&
                 (!request("spkt_waktu_bpkb") || strrpos(strtolower($item['spkt_waktu_bpkb']), strtolower(request("spkt_waktu_bpkb"))) > -1) &&
                 (!request("spkt_nobpkb") || strrpos(strtolower($item['spkt_nobpkb']), strtolower(request("spkt_nobpkb"))) > -1) &&
                 (!request("spkt_nopol") || strtolower($item['spkt_nopol'])== strtolower(request("spkt_nopol"))) &&
                 (!request("spkt_stnk") || strtolower($item['spkt_stnk'])== strtolower(request("spkt_stnk")))&&
                 (!request("spkt_status") || strtolower($item['spk_status'])== strtolower(request("spk_status"))))   {

                $tgl = strtotime(str_replace("/","-",$item['spk_tgl']));
                if (request("filter_awal") && request("filter_akhir")){
                    $filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
                    $filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
                    if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
                        array_push($result, $item);                     
                    }
                }else if (request("filter_awal")){
                    $filter_awal = strtotime(request("filter_awal"));
                    if ($filter_awal<=$tgl){
                        array_push($result, $item);                     
                    }
                }else if (request("filter_akhir")){
                    $filter_akhir = strtotime(request("filter_akhir"));
                    if ($filter_akhir>=$tgl){
                        array_push($result, $item);                     
                    }
                }else{
                    array_push($result, $item);
                }
            }
        }

    	return json_encode($result);
    }


    function riwayat(){
    	$data = DB::table('tb_spk')
    		->join('tb_spk_ttbj',function($join){
    			$join->on('tb_spk_ttbj.spkt_spk', '=', 'tb_spk.spk_id')
    			->where("spkt_status",1);
    		})
            ->leftjoin('tb_tr_kendaraan', 'tb_tr_kendaraan.trk_dh', '=', 'tb_spk.spk_dh')
    		->join('tb_biro', 'tb_spk_ttbj.spkt_biro', '=', 'tb_biro.biro_id')
    		->leftjoin('tb_variant', 'tb_spk.spk_kendaraan', '=', 'tb_variant.variant_id') 		
    		->join("tb_sales","tb_sales.sales_id","=","tb_spk.spk_sales")
    		->join("tb_karyawan","tb_karyawan.karyawan_id","=","tb_sales.sales_karyawan")
            ->leftjoin('tb_spk_leasing', 'tb_spk_leasing.spkl_spk', '=', 'tb_spk.spk_id')
            ->leftjoin('tb_asuransi_jenis', 'tb_spk_leasing.spkl_jenis_asuransi', '=', 'tb_asuransi_jenis.ajenis_id')
            ->leftjoin('tb_asuransi', 'tb_spk_leasing.spkl_asuransi', '=', 'tb_asuransi.asuransi_id')
            ->leftjoin('tb_leasing', 'tb_spk_leasing.spkl_leasing', '=', 'tb_leasing.leasing_id')
    		->where("spk_do","!=",null)
    		->get();

    	$result = array();
    	foreach($data as $d){
    		$item = array();
    		$item['spkt_id'] = $d->spkt_id;
    		$item['spk_id'] = $d->spk_id;
    		$item['spk_tgl'] = date_format(date_create($d->spk_tgl),"d/m/Y");
    		$item['spk_pel_nama'] = $d->spk_pel_nama;
            $item['spk_stnk_nama'] = $d->spk_stnk_nama;
    		$item['variant_nama'] = $d->variant_nama;
    		$item['spkt_stck'] = $d->spkt_stck;
    		$item['biro_nama'] = $d->biro_nama;
    		$item['spkt_tglfaktur'] =  date_format(date_create($d->spkt_tglfaktur),"d/m/Y");
    		if (is_null($d->spkt_tglfaktur)){
    			$item['spkt_tglfaktur']="";
    		}
    		$item['spkt_ujitype'] = date_format(date_create($d->spkt_ujitype),"d/m/Y");

    		if (is_null($d->spkt_ujitype)){
    			$item['spkt_ujitype']="";
    		}
    		$item['spkt_nopol'] = $d->spkt_nopol;
    		$item['spkt_plat'] = $d->spkt_plat;
    		$item['spkt_notis'] = $d->spkt_notis;
    		$item['spkt_stnk'] = $d->spkt_stnk;
    		$item['spkt_waktu_stnk'] = "";
    		$item['spkt_terima_nama'] = $d->spkt_terima_nama;
            $item['spkt_terima_alamat'] = $d->spkt_terima_alamat;
            $item['spkt_terima_tgl'] = $this->get_date_format($d->spkt_terima_tgl);
    		if (!is_null( $d->spkt_tglfaktur)){
    			$item['spkt_waktu_stnk'] = floor((time() - strtotime($d->spkt_tglfaktur))/ (60*60*24));

    			if (!is_null($d->spkt_waktu_stnk)){
    				$item['spkt_waktu_stnk'] = $d->spkt_waktu_stnk;
    			}

    			if ($item['spkt_waktu_stnk'] <=31){
    				$item['spkt_status_stnk'] = "IN PROSES";					
				}else{
					$item['spkt_status_stnk'] = "OVD";
				}

				if ($d->spkt_stnk==1){
    				$item['spkt_status_stnk'] = "OKE";
    			}
    		}
    		$item['spkt_nobpkb'] = $d->spkt_nobpkb;
    		$item['spkt_waktu_bpkb'] = "";
    		$item['spkt_status_bpkb'] = "";
            $item['spkt_mesin'] = $d->trk_mesin;
            $item['spkt_rangka'] = $d->trk_rangka;
            $item['spkt_dh'] = $d->trk_dh;
            $item['spkt_leasing'] = "";
            if (!is_null($d->leasing_nama)){
                $item['spkt_leasing'] .= $d->leasing_nama;
            }
            if (!is_null($d->asuransi_nama)){
                if (is_null($d->leasing_nama)){
                    $item['spkt_leasing'] .= $d->asuransi_nama;
                }else{
                    $item['spkt_leasing'] .= " / ".$d->asuransi_nama;
                }
            }
            if (!is_null($d->ajenis_nama)){
                if (is_null($d->leasing_nama) and is_null($d->asuransi_nama)){
                    $item['spkt_leasing'] .= $d->ajenis_nama;
                }else{
                    $item['spkt_leasing'] .= " / ".$d->ajenis_nama;
                }
            }

    		if (!is_null( $d->spkt_tglfaktur) && $d->spkt_plat==1 && $d->spkt_notis==1 && $d->spkt_stnk==1){
    			$item['spkt_waktu_bpkb'] = floor((time() - strtotime($d->spkt_tglfaktur))/ (60*60*24));

    			if (!is_null($d->spkt_waktu_bpkb)){
    				$item['spkt_waktu_bpkb'] = $d->spkt_waktu_bpkb;
    			}
    			
    			if ($item['spkt_waktu_bpkb'] <=31){
    				$item['spkt_status_bpkb'] = "IN PROSES";					
				}else{
					$item['spkt_status_bpkb'] = "OVD";
				}
				
				if (!is_null($d->spkt_nobpkb)){
    				$item['spkt_status_bpkb'] = "OKE";
    			}}if((!request("spk_id") || strrpos(strtolower($item['spk_id']), strtolower(request("spk_id"))) > -1) &&
                (!request("spk_tgl") || strrpos(strtolower($item['spk_tgl']), strtolower(request("spk_tgl"))) > -1) &&
                (!request("spk_pel_nama") || strrpos(strtolower($item['spk_pel_nama']), strtolower(request("spk_pel_nama"))) > -1) &&
                (!request("spk_stnk_nama") || strrpos(strtolower($item['spk_stnk_nama']), strtolower(request("spk_stnk_nama"))) > -1) &&
                (!request("spkt_stck") || strrpos(strtolower($item['spkt_stck']), strtolower(request("spkt_stck"))) > -1) &&
                (!request("biro_nama") || strrpos(strtolower($item['biro_nama']), strtolower(request("biro_nama"))) > -1) &&
                (!request("variant_nama") || strrpos(strtolower($item['variant_nama']), strtolower(request("variant_nama"))) > -1) &&
                (!request("variant_nama") || strrpos(strtolower($item['variant_nama']), strtolower(request("variant_nama"))) > -1) &&
                 (!request("spkt_tglfaktur") || strrpos(strtolower($item['spkt_tglfaktur']), strtolower(request("spkt_tglfaktur"))) > -1) &&
                 (!request("spkt_waktu_stnk") || strrpos(strtolower($item['spkt_waktu_stnk']), strtolower(request("spkt_waktu_stnk"))) > -1) &&
                 (!request("spkt_status_stnk") || strrpos(strtolower($item['spkt_status_stnk']), strtolower(request("spkt_status_stnk"))) > -1) &&
                 (!request("spkt_status_bpkb") || strrpos(strtolower($item['spkt_status_bpkb']), strtolower(request("spkt_status_bpkb"))) > -1) &&
                 (!request("spkt_waktu_bpkb") || strrpos(strtolower($item['spkt_waktu_bpkb']), strtolower(request("spkt_waktu_bpkb"))) > -1) &&
                 (!request("spkt_nobpkb") || strrpos(strtolower($item['spkt_nobpkb']), strtolower(request("spkt_nobpkb"))) > -1) &&
                 (!request("spkt_nopol") || strtolower($item['spkt_nopol'])== strtolower(request("spkt_nopol"))) &&
                 (!request("spkt_stnk") || strtolower($item['spkt_stnk'])== strtolower(request("spkt_stnk")))&&
                 (!request("spkt_status") || strtolower($item['spk_status'])== strtolower(request("spk_status"))))   {

                $tgl = strtotime(str_replace("/","-",$item['spk_tgl']));
                if (request("filter_awal") && request("filter_akhir")){
                    $filter_awal = strtotime(str_replace("/","-",request("filter_awal")));
                    $filter_akhir = strtotime(str_replace("/","-",request("filter_akhir")));
                    if ($filter_awal<=$tgl && $filter_akhir>=$tgl){
                        array_push($result, $item);                     
                    }
                }else if (request("filter_awal")){
                    $filter_awal = strtotime(request("filter_awal"));
                    if ($filter_awal<=$tgl){
                        array_push($result, $item);                     
                    }
                }else if (request("filter_akhir")){
                    $filter_akhir = strtotime(request("filter_akhir"));
                    if ($filter_akhir>=$tgl){
                        array_push($result, $item);                     
                    }
                }else{
                    array_push($result, $item);
                }
            }
    	}
    	return json_encode($result);
    }

    function store(){
    	$this->validate(request(), [
            "spkt_spk"      	=> "required"
        ]);
        $proses = false;
        $tglfaktur = null;
        if (!is_null(request("spkt_tglfaktur"))){
        	$tglfaktur = date_format(date_create(str_replace("/","-",request("spkt_tglfaktur"))),"Y-m-d");
        }
        $ujitype = null;
        if (!is_null(request("spkt_ujitype"))){
        	$ujitype = date_format(date_create(str_replace("/","-",request("spkt_ujitype"))),"Y-m-d");
        }
        $waktustnk = request("spkt_waktu_stnk");
        if (!is_null(request("spkt_tglfaktur")) && !is_null(request("spkt_nopol")) && request("spkt_plat")==1 & request("spkt_notis")==1 && request("spkt_stnk")==1 && is_null(request("spkt_waktu_stnk"))){
        	$waktustnk = floor((time() - strtotime($tglfaktur))/ (60*60*24));
        }

        $waktubpkb = request("spkt_waktu_bpkb");
        if (!is_null(request("spkt_tglfaktur")) && !is_null(request("spkt_nobpkb"))){
        	$waktubpkb = floor((time() - strtotime($tglfaktur))/ (60*60*24));
        }

        $status=0;
        if (!is_null(request("spkt_tglfaktur")) && !is_null(request("spkt_nopol")) && request("spkt_plat")==1 & request("spkt_notis")==1 && request("spkt_stnk")==1 && !is_null(request("spkt_nobpkb"))){
        	$status=1;
        }

        if (is_null(request("spkt_id"))){
        	//INSERT
        	$proses = DB::table('tb_spk_ttbj')-> insert([
	        "spkt_spk"     	=>  request("spkt_spk"),
	        "spkt_stck"     		=>  request("spkt_stck"),
	        "spkt_biro"     		=>  request("spkt_biro"),
	        "spkt_tglfaktur"     	=>  $tglfaktur,
	        "spkt_ujitype"     		=>  $ujitype,
	        "spkt_nopol"     		=>  request("spkt_nopol"),
	        "spkt_plat"     		=>  request("spkt_plat"),
	        "spkt_notis"     		=>  request("spkt_notis"),
	        "spkt_stnk"     		=>  request("spkt_stnk"),
	        "spkt_nobpkb"     		=>  request("spkt_nobpkb"),
	        "spkt_waktu_stnk"     	=>  $waktustnk,
	        "spkt_waktu_bpkb"     	=>  $waktubpkb,
	        "spkt_status"     		=>  $status,
            ]);
        }else{
        	//UPDATE
        	$proses = DB::table('tb_spk_ttbj')->where("spkt_id",request("spkt_id"))->update([
	        "spkt_spk"     	=>  request("spkt_spk"),
	        "spkt_stck"     		=>  request("spkt_stck"),
	        "spkt_biro"     		=>  request("spkt_biro"),
	        "spkt_tglfaktur"     	=>  $tglfaktur,
	        "spkt_ujitype"     		=>  $ujitype,
	        "spkt_nopol"     		=>  request("spkt_nopol"),
	        "spkt_plat"     		=>  request("spkt_plat"),
	        "spkt_notis"     		=>  request("spkt_notis"),
	        "spkt_stnk"     		=>  request("spkt_stnk"),
	        "spkt_nobpkb"     		=>  request("spkt_nobpkb"),
	        "spkt_waktu_stnk"     	=>  $waktustnk,
	        "spkt_waktu_bpkb"     	=>  $waktubpkb,
	        "spkt_status"     		=>  $status,
            ]);
        }

	    if ($proses){
	    	return 1;
	    }
	    return 0;
    }


    function terima_save(){
        $this->validate(request(), [
            "spkt_id"      => "required",
            "spkt_terima_nama"      => "required",
            "spkt_terima_alamat"      => "required"
        ]);

        $proses = DB::table('tb_spk_ttbj')-> where("spkt_id",request("spkt_id"))->update([
            "spkt_terima_nama"  =>  strtoupper(request("spkt_terima_nama")),
            "spkt_terima_alamat"  =>  strtoupper(request("spkt_terima_alamat")),
            "spkt_terima_tgl"  =>  date("Y-m-d")
        ]);

       
            return 1;
      
    }

    private function get_date_format($date){
        if (!is_null($date)){
            return date_format(date_create($date),"d/m/Y");
        }
        return "";
    }
}
