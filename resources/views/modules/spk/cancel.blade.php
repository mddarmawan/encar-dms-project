@extends('layout')

@section('content')

	<div class="content-header">
		<h6>
			<small>Penjualan</small>
			Batal SPK
		</h6>
		@include("modules.spk.nav")
	</div>		
<div id="data_grid" class="wrapper">
		<div class="nav-wrapper">
			<div class="nav-left">
				<ul class="tab">
					<li class="active"><a data-id="" >Semua</a></li>
					<li><a data-id="11">Req</a></li>
					<li><a data-id="10">Batal</a></li>
				</ul>
			</div>
			<div class="nav-right">
				<i style="margin-right: 5px" class="fa fa-filter" aria-hidden="true"></i>
				<span class="bold" style="font-size: 13px"> Periode :</span>
				<div class="input-group">
					<input type="text" class="input-filter datepicker" id="filter_awal" placeholder="Tanggal Awal" required="" />
					<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
				</div>
				<span class="bold" style="font-size: 13px">s.d</span>
				<div class="input-group">
					<input type="text" class="input-filter datepicker"  id="filter_akhir" placeholder="Tanggal Akhir"  required="" />
					<span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
				</div>
				<a class="waves-effect waves-light btn-flat btn-filter" id="filter">Cari</a>
				<a class="waves-effect waves-light btn-flat btn-filter" id="reset">Reset</a>
				<a class="waves-effect waves-light btn-flat btn-filter" id="export">Export</a>
			</div>
		</div>
		<div id="dataSPK">

		</div>	
</div>


@include("modules.spk.detail_cancel")

<script>

	function detail(id){
		$("#spk_id").html(id);

		$.ajax({
			type: "GET",
			url: "http://localhost:8000/api/spk/"+id
		}).done(function(json) {
		 	var pemesan = json.pemesan;
		 	var kendaraan = json.kendaraan;
		 	var leasing = json.leasing;
		 	
			 if(pemesan.spk_status==10){
			 	$("#konfirmasi").hide();
			 } else {
			 	$("#konfirmasi").show();
			 }

		 	if (pemesan.spk_ppn===1){
		 		$("#spk_ppn").html("YA");
		 	}
		 	if (pemesan.spk_pajak===1){
		 		$("#spk_pajak").html("DIMINTA");
		 	}else{
		 		$("#spk_pajak").html("TIDAK DIMINTA");
		 	}
		 	$("#spk_tgl").html(date_format(pemesan.spk_tgl));
		 	$("#spk_sales").html(pemesan.karyawan_nama + " / " +pemesan.team_nama );

		 	$("#pel_nama").html(pemesan.spk_pel_nama);
		 	$("#pel_identitas").html(pemesan.spk_pel_identitas);
		 	$("#pel_alamat").html(pemesan.spk_pel_alamat);
		 	$("#pel_pos").html(pemesan.spk_pel_pos);
		 	$("#pel_telp").html(pemesan.spk_pel_telp);
		 	$("#pel_ponsel").html(pemesan.spk_pel_ponsel);
		 	$("#pel_email").html(pemesan.spk_pel_email);
		 	$("#pel_kategori").html(pemesan.spk_pel_kategori);
		 	$("#spk_npwp").html(pemesan.spk_npwp);
		 	$("#spk_fleet").html(pemesan.spk_fleet);
		 	$("#spk_ket_permintaan").html(pemesan.spk_ket);
		 	$("#spk_ket_cancel").html(pemesan.spk_ket_cancel);
		 	$("#spk_catt_cancel").html(pemesan.spk_catt_cancel);
		 	$("#spk_stnk_nama").html(pemesan.spk_stnk_nama);
		 	$("#spk_stnk_alamat").html(pemesan.spk_stnk_alamat);
		 	$("#spk_stnk_pos").html(pemesan.spk_stnk_pos);
		 	$("#spk_stnk_alamatd").html(pemesan.spk_stnk_alamatd);
		 	$("#spk_stnk_posd").html(pemesan.spk_stnk_posd);
		 	$("#spk_stnk_telp").html(pemesan.spk_stnk_telp);	
		 	$("#spk_stnk_ponsel").html(pemesan.spk_stnk_ponsel);
		 	$("#spk_stnk_email").html(pemesan.spk_stnk_email);
		 	$("#spk_stnk_identitas").html(pemesan.spk_stnk_identitas);
		 	$("#variant_nama").html(pemesan.type_nama+ " " +pemesan.variant_nama);
		 	$("#spk_warna").html(pemesan.warna_nama);
		 	$("#variant_id").html(pemesan.variant_serial);
		 	$("#spk_pel_fotoid").html("<img height='150px' src='http://localhost:8000/storage/data/identitas/"+pemesan.spk_pel_fotoid+"'/>");
		 	$("#spk_stnk_fotoid").html("<img height='150px' src='http://localhost:8000/storage/data/identitas/"+pemesan.spk_stnk_fotoid+"'/>");

		 	$("#spkt_stck").val(pemesan.spk_catt_permintaan);

		 	if (kendaraan!=null){
				$("#kendaraan .no-data").removeClass("show");
			 	$("#trk_dh").html(kendaraan.trk_dh);
			 	$("#trk_mesin").html(kendaraan.trk_mesin);
			 	$("#trk_rangka").html(kendaraan.trk_rangka);
			 	$("#trk_warna").html(kendaraan.warna_nama);		 		
		 	}else{
				$("#kendaraan .no-data").addClass("show");
			 	$("#trk_dh").html('');
			 	$("#trk_mesin").html('');
			 	$("#trk_rangka").html('');
			 	$("#trk_warna").html('');
		 	}

		 	if(pemesan.spk_kategori==1){	
		 		$("#spk_kategori").html("ON THE ROAD");
		 	}else{
		 		$("#spk_kategori").html("OFF THE ROAD");
		 	}
			
            $("#detail_cancel").modal("open");

		});
	};
	

	$("#cancel").click(function(){
		$("#cancel_request").modal("open");
	});

	$("#konfirmasi").click(function(){
		$("#konfirmasi_batal").modal("open");
	});

	$("#setujui").click(function(){
		var data = {
			"spk_id":$("#spk_id").html(),
			_token: "{{csrf_token()}}"
		};
		
		$.ajax({
			type: "POST",
			dataType:"json",
			url: "http://localhost:8000/api/spk/cancel/acc",
			data: data
		}).fail(function(response) {
			alert("ERR-42 Data Gagal Dibatalkan !");
		}).done(function(data){
			if (!data.result){
				alert(data.msg);
			}else{
				var spk_id = $("#spk_id").html();
				$.ajax({
					type: "GET",
					url: "http://localhost:8000/api/firebase/sales/uid/" + spk_id
				}).done(function(response){
					var salesUid_sales = response.salesUid_sales;

					$.ajax({
						type: "GET",
						url: "http://localhost:8000/api/spk/get/" + spk_id
					}).done(function(response){
						var data = response;
						var db = firebase.database().ref('sales/' + salesUid_sales + '/spk/' + spk_id);
						db.set(data);

						$('#msg').html('Data Disetujui !');
						$("#konfirmasi_batal").modal("close");
						$("#detail_cancel").modal("close");
						$("#konfirmasi").hide();
						loadData();
					});						
				});
			}
		});
	});

	$("#tanggapi").click(function(){
		var data = {
			"spk_id":$("#spk_id").html(),
			"spk_catt_cancel" : $("#spk_catt_cancel").val(),
			_token: "{{csrf_token()}}"
		};

		$.ajax({
			type: "POST",
			dataType:"json",
			url: "http://localhost:8000/api/spk/cancel/tanggapi",
			data: data
		}).fail(function(response) {
			alert("ERR-42 Data Gagal Dibatalkan !");
		}).done(function(data){
			if (!data.result){
				alert(data.msg);
			}else{
				var spk_id = $("#spk_id").html();
				$.ajax({
					type: "GET",
					url: "http://localhost:8000/api/firebase/sales/uid/" + spk_id
				}).done(function(response){
					var salesUid_sales = response.salesUid_sales;

					$.ajax({
						type: "GET",
						url: "http://localhost:8000/api/spk/get/" + spk_id
					}).done(function(response){
						var data = response;
						var db = firebase.database().ref('sales/' + salesUid_sales + '/spk/' + spk_id);
						db.set(data);

						$('#msg').html('Data Dibatalkan !');
						$("#konfirmasi_batal").modal("close");
						$("#detail_cancel").modal("close");
						$("#konfirmasi").hide();
						loadData();
					});						
				});
			}
		});
	});



	$("#save").click(function(){
		var data = {
			"spk_id":$("#spk_id").html(),
			"spk_dpp":toInt($("#off").val()),
			"spk_bbn":toInt($("#bbn").val()),
			_token: "{{csrf_token()}}"
		};

		$.ajax({
			type: "POST",
			dataType:"json",
			url: "http://localhost:8000/api/spk/update",
			data: data
		}).fail(function(response) {
			alert("ERR-42 Data Gagal Disimpan !");
		}).done(function(data){
			if (!data.result){
				alert(data.msg);
			}else{
				if ($("#spk_pembayaran").val('CREDIT')){
					var data = {
						"spkl_spk":$("#spk_id").html(),
						"spkl_leasing":$("#spk_leasing").val(),
						"spkl_asuransi":$("#spk_asuransi").val(),
						"spkl_angsuran":toInt($("#spk_angsuran").val()),
						"spkl_waktu":$("#spk_waktu").val().toUpperCase(),
						"spkl_dp":toInt($("#spk_dp").val()),
						"spkl_droping":toInt($("#spk_droping").val()),
						_token: "{{csrf_token()}}"
					};
					$.ajax({
						type: "POST",
						dataType:"json",
						url: "http://localhost:8000/api/spk/leasing",
						data: data
					}).fail(function(response) {
						alert("ERR-42 Data Gagal Disimpan !");
					}).done(function(data){
						if (!data.result){
							alert(data.msg);
						}else{
							$('#msg').html('Tersimpan !');
						}
					});
				}else{
					$('#msg').html('Tersimpan !');
				}
			}
		});
	});

$("#filter").click(function(e){
	e.preventDefault();
	loadData();
});

$("#reset").click(function(e){
	e.preventDefault();
	$("#filter_awal").val("");
	$("#filter_akhir").val("");
	status="";
	$(".tab li").removeClass("active");
	$(".tab li a[data-id='']").parent().addClass("active");
	loadData();
});


$("#export").click('click', function (event) {
    var args = [$('#dataSPK'), 'CANCEL_SPK_<?php echo date('dmY') ?>.xls'];   
    exportTableToExcel.apply(this, args);
});

var status="";
function loadData() {
	loadNotif();
	var db_spk = {
		loadData: function(filter) {
			var filter_awal = $("#filter_awal").val().trim();
			var filter_akhir = $("#filter_akhir").val().trim();
			if (filter_awal != ""){
				filter['filter_awal'] = filter_awal;
			}
			if (filter_akhir != ""){
				filter['filter_akhir'] = filter_akhir;
			}
			filter['spk_status'] = status;
			return $.ajax({
				type: "GET",
				url: "/api/spk/cancel",
				data: filter
			});
		}
	};

	db_spk.harga = [
			{
				"harga_id": "",
				"harga_nama": "",		   
			},
			{
				"harga_id": 1,
				"harga_nama": "ON-TR",		   
			},
			{
				"harga_id": 2,
				"harga_nama": "OFF-TR",		   
			},
			
		];
	
	db_spk.via = [
			{
				"via_id": "",
				"via_nama": "",		   
			},
			{
				"via_id": 1,
				"via_nama": "CASH",		   
			},
			{
				"via_id": 2,
				"via_nama": "CREDIT",		   
			},
			
		];


	db_spk.status = [
			{
				"status_id": "",
				"status_nama": "",	   
			},
			{
				"status_id": "10",
				"status_nama": "<span class='box-span red'>BATAL</span>",		   
			},
			{
				"status_id": "11",
				"status_nama": "<span class='box-span orange'>REQ.BATAL</span>",		   
			},
			
		];
	

	$("#dataSPK").jsGrid({
		height: "calc(100% - 40px)",
		width: "100%",
 
		sorting: true,
		filtering: true,
		autoload: true,
		paging: true,
		pageSize: 30,
		pageButtonCount: 5,
		noDataContent: "Tidak Ada Data",
 		rowClick:function(data){
            var item = data.item;
            detail(item.spk_id);
        },
		controller: db_spk,
 
		fields: [

			{ name: "spk_status", title:"Status", type: "select", width: 100, items: db_spk.status, valueField: "status_id", textField: "status_nama", align:"center", filtering:false  },
			 { name: "spk_tgl", title:"Tanggal", type: "text", width: 80, align:"center" },
			{ name: "spk_id", title:"No SPK", type: "text", width: 80, align:"center" },
			{ name: "spk_pel_nama", title:"Nama Pelanggan", type: "text", width: 170},
			{ name: "spk_variant", title:"Varian", type: "text", width: 150 },
			{ name: "spk_warna", title:"Warna", type: "text", width: 120, align:"center"},

			{ name: "spk_sales", title:"Sales", type: "text", width: 120},

			{ name: "spk_team", title:"Team", type: "text", width: 80, align:"center" },
			{ name: "spk_kendaraan_harga", title:"Jenis Harga", type: "select", width: 100, items: db_spk.harga, valueField: "harga_id", textField: "harga_nama", align:"center" },

			{ name: "spk_pembayaran", title:"Pembayaran", type: "text", width: 100, align:"right" },
			{ name: "spk_pel_kota", title:"Kota", type: "text", width: 140, align:"center" },
			{ name: "spk_metode", title:"CARA BAYAR", type: "select", width: 90,items: db_spk.via, valueField: "via_id", textField: "via_nama", align:"center" }
			
		]
	});
}
loadData();
$(".tab li a").click(function(e){
	e.preventDefault();

	status = $(this).data("id");
	
	loadData();
	$(".tab li").removeClass("active");
	$(this).parent().addClass("active");
});
</script>

@endsection