<style>
	body{
		margin:0;
		padding:0;
		font-size:12px;
		color:#737373;
		font-family: Courier;
	}
	td, th{
		font-size:12px;
	}
	h5{
		margin: 5px 0 20px;
		font-weight:400;
	}
	.text-center{
		text-align:center;
	}
	.text-right{
		text-align:right;
	}
	table{
		width:100%;		
	}
	table, td, th, tr{
		border:0;
		border-spacing: 0;
		border-color:#fff;
		padding:0;
		border-collapse: collapse;
		vertical-align:top;
	}
	table th{
		border-top:1px solid #ddd;
		border-left:1px solid #ddd;
		border-right:1px solid #ddd;
		border-bottom:4px solid #ddd;
		padding:3px 7px;
		vertical-align:middle;
		background:#f5f5f5;
		border-spacing: 0;
		border-collapse: collapse;
	}
	
	table tfoot th{
		border-bottom:1px solid #ddd;
		border-top:4px solid #ddd;
	}
	
	.table td{
		padding:3px 7px;
		border-top:1px solid #ddd;
		border-left:1px solid #ddd;
		border-right:1px solid #ddd;
		border-bottom:1px solid #ddd;		
	}

	.bold{
		font-weight:bold;
	}
</style>
<page>
	<body>
		<table style="width: 100%">
			<tr>
				<td style="width: 70%;">
					<p style="margin-top: 20px;">
						PT. Encartha Indonesia <br>
						JL.Angkatan 45 Palembang 30137 <br>
						Telp. 0711-374000 fax. 0711-374500 <br>
						NPWP : 01.596.627.6.308.000 Tgl. Pengukuhan : 07-04-2008 <br>
						BKB : 01.596.627.6.308.000 <br><br>
						No. SPK : {{ $data->spk_id }} <br>
						No. Indent : {{ $data->spk_type }} <br>
						Referensi : {{ $data->spk_referral }}
					</p>
				</td>
				<td>
					<h4 style="margin-left: 20px;">FAKTUR KENDARAAN</h4>
					Nomor : {{ $data->spkf_id }} <br>
					Tanggal : {{ $data->spkf_tgl }} <br><br>
					Pemesan : <br>
					{{ $data->spk_pel_nama }} <br>
					{{ $data->spk_pel_alamat }} <br>
					{{ $data->spk_pel_kota }}
				</td>
			</tr>
		</table>
		<div style="border-top: 1px solid #000; margin: 10px 0;"></div>
		<div style="margin-bottom: 10px;">
			<div style="width: 70%; float: left;">
				<table width="100%">
					<tr>
						<td width="10%">Keterangan Kendaraan</td>
						<td width="2%"> : </td>
						<td width="30%">1 (satu) Unit {{ $data->spk_type }} {{ $data->spk_variant }}</td>
					</tr>
					<tr>
						<td width="15%">Nomor Rangka</td>
						<td width="2%"> : </td>
						<td width="30%">{{ $data->spk_no_rangka }}</td>
					</tr>
					<tr>
						<td width="10%">Nomor Mesin</td>
						<td width="2%"> : </td>
						<td width="30%">{{ $data->spk_no_mesin }}</td>
					</tr>
					<tr>
						<td width="15%">Serial</td>
						<td width="2%"> : </td>
						<td width="30%">{{ $data->spk_serial }}</td>
					</tr>
					<tr>
						<td width="15%">Warna</td>
						<td width="2%"> : </td>
						<td width="30%">{{ $data->spk_warna }}</td>
					</tr>
					<tr>
						<td width="15%">No. DH</td>
						<td width="2%"> : </td>
						<td width="30%">{{ $data->spk_dh }}</td>
					</tr>
				</table>
			</div>
			<div style="width: 30%; float: right; font-size: 12px;">
				<p style="margin:0; margin-bottom: 2px;">STNK atas Nama : </p>
				<p style="margin:0; margin-bottom: 1px;">{{ $data->spk_stnk_nama }}</p>
				<p style="margin:0; margin-bottom: 1px;">{{ $data->spk_stnk_alamat }}</p>
				<p style="margin:0;">{{ $data->spk_pel_kota }}</p>
			</div>
		</div>
		<table width="100%" style="margin-top: 20px;">
			<tr>
				<td width="50%"><b>KETERANGAN</b></td>
				<td width="50%" class="text-right"><b>HARGA &nbsp; &nbsp;</b></td>
			</tr>
			<tr>
				<td width="50%">Harga Off The Road</td>
				<td width="50%" class="text-right">{{ $data->spk_variant_off }}</td>
			</tr>
			<tr>
				<td width="50%">BBN</td>
				<td width="50%" class="text-right">{{ $data->spk_variant_bbn }}</td>
			</tr>
			<tr>
				<td width="50%">On The Road</td>
				<td width="50%" class="text-right">{{ $data->spk_variant_on }}</td>
			</tr>
		</table>
		<div style="border-top: 1px solid #000; margin: 10px 0;"></div>
		<table width="100%">
		@foreach ($data->spk_riwayat_pembayaran as $r)
			<tr>
				<td width="50%" style="margin-bottom: 4px;">{{ $r->spkp_tgl }}</td>
				<td width="50%" class="text-right"><p style="float: right; margin-bottom: 4px;">{{ (number_format($r->spkp_jumlah, 0, ",", ".")) }}</p></td>
			</tr>
		@endforeach
			<tr>
				<td width="50%"></td>
				<td width="50%" class="text-right"><p style="float: right;">___________________</p></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" class="text-right"><p style="float: right;">&nbsp;</p></td>
			</tr>
			<tr>
				<td width="50%" style="margin-bottom: 4px;">Pembayaran Konsumen</td>
				<td width="50%" class="text-right"><p style="float: right; margin-bottom: 4px;">{{ $data->spk_pembayaran }}</p></td>
			</tr>
			<tr>
				<td width="50%">Cash Back Dealer</td>
				<td width="50%" class="text-right">
					<p style="float: right;">
						{{ $data->spk_cashback }}
					</p>
				</td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" class="text-right"><p style="float: right;">___________________</p></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" class="text-right"><p style="float: right;">&nbsp;</p></td>
			</tr>
				<?php
					$bayar = $data->spk_riwayat_pembayaran[0];
				?>
			<tr>
				<td width="50%">BAYAR I</td>
				<td width="50%" class="text-right"><p style="float: right;"> {{ $data->spk_bayar }} </p></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" class="text-right"><p style="float: right;">___________________</p></td>
			</tr>
			<tr>
				<td width="50%"></td>
				<td width="50%" class="text-right"><p style="float: right;">&nbsp;</p></td>
			</tr>
			<tr>
				<td width="50%">Dropping</td>
				<td width="50%" class="text-right"><p style="float: right;">{{ $data->spkl_droping }} </p></td>
			</tr>
		</table>
		<table style="margin-top: 150px;">
			<tr>
				<td width="9%">Jenis Pembayaran</td>
				<td width="2%"> : </td>
				<td width="70%">{{ $data->spk_metode }}</td>
			</tr>
			<tr>
				<td>Leasing</td>
				<td> : </td>
				<td>{{ $data->spk_via }} {{ ($data->spk_asuransi_jenis != NULL ? ' / ' . $data->spk_asuransi_jenis : '') }}</td>
			</tr>
			<tr>
				<td>Jangka Waktu</td>
				<td> : </td>
				<td>{{ $data->spk_waktu }}</td>
			</tr>
		</table>
		<div style="border-top: 1px solid #000; margin: 10px 0;"></div>
		<table width="100%" style="margin-top: 20px;">
			<tr>
				<td width="50%"><b>Dibuat oleh</b></td>
				<td width="50%" class="text-right"><b>Pemimpin</b></td>
			</tr>
		</table>
		<table width="100%" style="margin-top: 50px;">
			<tr>
				<td width="50%"><b>__________________</b></b></td>
				<td width="50%" class="text-right"><b>__________________</b></td>
			</tr>
		</table>
</page>