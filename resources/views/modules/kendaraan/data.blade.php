@extends('layout')

@section('content')

	<div class="content-header">
		<h6>
			<small>Persediaan</small>
			Kendaraan
		</h6>
	</div>



<div id="data_grid" class="wrapper">
	<div class="row" style="margin:0;height:100%;position:relative;">
        <div class="col s12 m3" style="padding:0;height:100%;position:relative; border-right: #ddd 1px solid">
        <div class="nav-wrapper">
            
            <div class="nav-right">
                <a class="waves-effect waves-light btn-flat btn-filter" id="reset_type">Reset</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="export_type">Export</a>
            </div>
        </div>
        	@include("modules.kendaraan.data_type")  
        </div>
	    <div class="col s12 m9" style="padding:0;height:100%;position:relative;">
	    <div class="nav-wrapper">
            
            <div class="nav-right">
                <a class="waves-effect waves-light btn-flat btn-filter" id="reset_variant">Reset</a>
                <a class="waves-effect waves-light btn-flat btn-filter" id="export_variant">Export</a>
            </div>
        </div>
	        @include("modules.kendaraan.data_variant")  
	    </div>
    </div>
</div>


@endsection