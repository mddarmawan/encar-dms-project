<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<style type="text/css">
			body{
				font-family: arial;
				padding: 0px;
			}
			td{
				font-size: 13px
			}
			p{
				font-size: 12px
			}
			.col-1{
				width: 34%;
				display: inline-block;
			}
			.col-2{
				width: 30%;
				display: inline-block;
			}
			.col-3{
				width: 33%;
				display: inline-block;
			}

		</style>
	</head>
	<body>
		<div style="">
			<p>
				<h2 style="margin: 0;">BUKTI SERAH TERIMA KENDARAAN</h2>
			</p>
			<div style="display: inline-block; float: left; width: 55%;">
				<tt>
				<table class="table" style="font-size: 12px; margin-top: 10px;">
					<tr>
						<td class="td"><a style="margin: 30px 0;">PT. Encartha Indonesia</a></td>
					</tr>
					<tr>
						<td class="td"><a style="margin: 30px 0;">JL.Angkatan 45 Palembang 30137</a></td>
						<td> </td>
					</tr>
					<tr>
						<td class="td"><a style="margin: 30px 0;">Telp. 0711-374000 fax. 0711-374500</a></td>
						<th></th>
						<td></td>
					</tr>
				</table>
				<p style="font-size: 12px; margin-top: -2px; margin-left: 3px ">NPWP : 01.596.627.6.308.000 Tgl. Pengukuhan : 07-04-2008</p>
				</tt>
			</div>

			<div style="display: inline-block; float: right; width: 45%;">
				<tt>
				<table class="table" style="font-size: 12px; margin-left: 55px; margin-top: 10px;">
					<tr>
						<td class="td"><a style="margin: 30px 0;">Nomor</a></td>
						<th> : </th>
						<td class="td_isi"> BP{{$data->spk_id}} </td>
					</tr>
					<tr>
						<td class="td"><a style="margin: 30px 0;">Tanggal</a></td>
						<th> : </th>
						<td> {{ $data->spkf_tgl }} </td>
					</tr>
					<tr>
						<td class="td"><a style="margin: 30px 0;">&nbsp;</a></td>
						<th></th>
						<td></td>
					</tr>
					<tr>
						<td class="td"><a style="margin: 30px 0;">&nbsp;</a></td>
						<th></th>
						<td></td>
					</tr>
				</table>
				</tt>
			</div>
			<div style="display: inline-block; float: left; width: 48.5%%;">
				<tt>
					<table style="font-size: 12px;  float: left;">
					<tr>
						<td><b>Pesanan :</b></td>
					</tr>
					<tr>
						<td><p style="font-size: 12px">{{$data->spk_pel_nama}}</p></td>
					</tr>
					<tr>
						<td><p style="font-size: 12px">{{$data->spk_pel_alamat}}</p></td>
					</tr>
				</table>
				</tt>
			</div>

			<div style="display: inline-block; float: right; width: 48.5%%;">
				<tt>
				<table style="font-size: 12px;  float: left;">
					<tr>
						<td><b>STNK atas Nama :</b></td>
					</tr>
					<tr>
						<td><p style="font-size: 12px">{{$data->spk_stnk_nama}}</p></td>
					</tr>
					<tr>
						<td><p style="font-size: 12px">{{$data->spk_stnk_alamat}}</p></td>
					</tr>
				</table>
				</tt>
			</div>
		</div>




			<div style="border-top: 1px solid #000; margin-bottom: 10px;"></div>
				<div style="margin-bottom:;">
					<div style="width: 50%; float: left;">
						<table width="100%">
							<tr>
								<td width="15%"><p style="font-size: 12px">No. SPK</p></td>
								<td width="2%"><p style="font-size: 12px">:</p></td>
								<td width="30%"><p style="font-size: 12px">{{$data->spk_id}}</p></td>
							</tr>
							<tr>
								<td width="15%"><p style="font-size: 12px">Tipe</p></td>
								<td width="2%"><p style="font-size: 12px">:</p></td>
								<td width="30%"><p style="font-size: 12px">{{$data->spk_serial}}</p></td>
							</tr>
							<tr>
								<td width="15%"><p style="font-size: 12px">Jenis Pembayaran</p></td>
								<td width="2%"><p style="font-size: 12px">:</p></td>
								<td width="30%"><p style="font-size: 12px">{{$data->spk_metode}}</p></td>
							</tr>
							<tr>
								<td width="15%"><p style="font-size: 12px">No. DH</p></td>
								<td width="2%"><p style="font-size: 12px">:</p></td>
								<td width="30%"><p style="font-size: 12px">{{$data->spk_dh}}</p></td>
							</tr>
							
						</table>
					</div>
					<div style="width: 50%; float: right;">
							<table width="100%" style="margin-top: -2px; margin-left: 55px">
								<tr>
									<td width="15%"><p style="font-size: 12px; margin-top:-0.5px">1 (satu) Unit DAIHATSU {{$data->spk_variant}}</p></td>
								</tr>
							</table>
							<table width="100%" style="margin-top: -2px; margin-left: 55px">
								<tr>
									<td width="15%"><p style="font-size: 12px">No. Rangka</p></td>
									<td width="2%"><p style="font-size: 12px">:</p></td>
									<td width="30%"><p style="font-size: 12px">{{$data->spk_no_rangka}}</p></td>
								</tr>
								<tr>
									<td width="15%"><p style="font-size: 12px">No. Mesin</p></td>
									<td width="2%"><p style="font-size: 12px">:</p></td>
									<td width="30%"><p style="font-size: 12px">{{$data->spk_no_mesin}}</p></td>
								</tr>
								<tr>
									<td width="15%"><p style="font-size: 12px">Warna</p></td>
									<td width="2%"><p style="font-size: 12px">:</p></td>
									<td width="30%"><p style="font-size: 12px">{{$data->spk_warna}}</p></td>
								</tr>
								<tr>
									<td width="15%"><p style="font-size: 12px">Referensi</p></td>
									<td width="2%"><p style="font-size: 12px">:</p></td>
									<td width="30%"><p style="font-size: 12px">1612106-BONI FRANSISCA</p></td>
								</tr>
							</table>
					</div>
				</div>
				<div style="border-top: 1px solid #000; margin-top: 7px "></div>
				<div style="margin-top:4px;">
					<table>
						<tr>
							<td width="250px"><p style="margin: 0;">1 (satu) buah Ban SerepKend. Velg</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="75px">Standar</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Racing</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>
				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">1 (satu) buah Buku Pedoman Pemilik</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>

				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">1 (satu) buah Dongkrak + Gagang</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>

				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">1 (satu) buah Lighter + Asbak Rokok</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>
				
				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">1 (satu) set Tools Standar + Kunci Roda</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>
					
				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">1 (satu) set Kaca Spion Kiri, Kanan dan Dalam</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>

				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">1 (satu) set Karpet Depan dan Belakang</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>
				
				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">1 (satu) set Karpet Dalam</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>
				
				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">1 (satu) set Karpet Bagasi</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>
				
				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">1 (satu) set Karpet Penahan Lumpur Depan dan Belakang</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>

				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">1 (satu) set Pelindung Sinar Matahari Kiri dan Kanan</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>

				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">Emblem</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>				
				
				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">Gardan Depan</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>	
			
				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">Wiper Kaca Depan</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>	

				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">Lampu Jauh, Dekat, Sinyal, Hasard, Rem dan Mundur</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>	
				
				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">Buku Service No..............................................</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>
				
				<div style="margin:0;">
					<table>
						<tr>
							<td width="503px"><p style="margin: 0;">Plat Profit No........................./STCK</p></td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="">Ada</td>
							<td><img style="width: 35px;" src="./assets/images/box.png"></td>
							<td width="100px">Tidak Ada</td>
						</tr>
					</table>
				</div>
				
			<div style="border-top: 1px solid #000; margin: 3px 0;"></div>
				<div style="width: 100%; ">
					<div  >
						<table width="100%" style="margin-top:3px">
							<tr>
								<td width="40%"><p >Telah diperiksa dan diterima<br>Dengan baik oleh:</p>
								<br>
								<br>
								<br>
								<br>	
								<p >__________________________<br>Tanda tangan & Nama Jelas</p>
								</td>

								<td width="40%"><p >Mengetahui,</p>
								<br>
								<br>
								<br>
								<br>	
								<p >__________________________</p>
								</td>

								<td><p >Yang Menyerahkan,</p>
								<br>
								<br>
								<br>
								<br>	
								<p >__________________________</p>
								</td>
							</tr>
						</table>
					</div>	
				</div>

			<div style="border-bottom: 3px dashed #000; margin: 10px 0;"></div>

			<div style="display: inline-block; float: left; width: 50%;">
				<tt>
				<table style="font-size: 12px;  float: left;">
					<tr>
						<td><b>Pesanan :</b></td>
					</tr>
					<tr>
						<td><p style="font-size: 12px">{{$data->spk_pel_nama}}</p></td>
					</tr>
					<tr>
						<td><p style="font-size: 12px">{{$data->spk_pel_alamat}}</p></td>
					</tr>
					<br>
					<tr>
						<td><p style="font-size: 12px">Ket.</p></td>
					</tr>
				</table>
				</tt>
			</div>

			<div style="display: inline-block; float: right; width: 50%; ">
				<tt>
				<table width="100%" style="margin-top: -2px; margin-left: 55px">
								<tr>
									<td width="15%"><p style="font-size: 12px; margin-top:-0.5px">1 (satu) Unit DAIHATSU {{$data->spk_variant}}</p></td>
								</tr>
							</table>
							<table width="100%" style="margin-top: -2px; margin-left: 55px">
								<tr>
									<td width="15%"><p style="font-size: 12px">No. Rangka</p></td>
									<td width="2%"><p style="font-size: 12px">:</p></td>
									<td width="30%"><p style="font-size: 12px">{{$data->spk_no_rangka}}</p></td>
								</tr>
								<tr>
									<td width="15%"><p style="font-size: 12px">No. Mesin</p></td>
									<td width="2%"><p style="font-size: 12px">:</p></td>
									<td width="30%"><p style="font-size: 12px">{{$data->spk_no_mesin}}</p></td>
								</tr>
								<tr>
									<td width="15%"><p style="font-size: 12px">Warna</p></td>
									<td width="2%"><p style="font-size: 12px">:</p></td>
									<td width="30%"><p style="font-size: 12px">{{$data->spk_warna}}</p></td>
								</tr>
								<tr>
									<td width="15%"><p style="font-size: 12px">Referensi</p></td>
									<td width="2%"><p style="font-size: 12px">:</p></td>
									<td width="30%"><p style="font-size: 12px">1612106-BONI FRANSISCA</p></td>
								</tr>
								<tr>
									<td width="15%"><p style="font-size: 12px">No. DH</p></td>
									<td width="2%"><p style="font-size: 12px">:</p></td>
									<td width="30%"><p style="font-size: 12px">{{$data->spk_dh}}</p></td>
								</tr>
							</table>
				</tt>
			</div>

			
	</body>
</html>